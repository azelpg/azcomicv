/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * WorkData - ページ処理
 *****************************************/

#include <mlk_gui.h>
#include <mlk_pixbuf.h>
#include <mlk_guicol.h>

#include "def_macro.h"
#include "def_work.h"
#include "def_config.h"

#include "mainwin_func.h"
#include "image.h"
#include "work_main.h"
#include "work_sub.h"



//=========================
// 設定変更時
//=========================


/** 画像表示サイズタイプ変更 */

void workOpt_changeViewImageType(int type)
{
	APPCONF->imgsize_type = type;

	MainWindow_updateViewSize_resetscale();
}

/** ビュー固定サイズの変更 */

void workOpt_changeViewFixSize(void)
{
	int type = APPCONF->imgsize_type;

	if((APPCONF->fimgview & IMGVIEW_F_VIEWSIZE_FIT_FIX)
		&& (type != IMGSIZE_TYPE_ORIGINAL && type != IMGSIZE_TYPE_FIX_SCALE))
		MainWindow_updateViewSize_resetscale();
}

/** フラグ変更: 見開き先頭は常に単ページ */

void workOpt_toggle_topIsSingle(void)
{
	WorkData *p = APPWORK;

	APPCONF->fimgview ^= IMGVIEW_F_DOUBLE_TOP_IS_SINGLE;

	//現在、先頭画像が表示されている場合は更新

	if(p->item_cur
		&& (APPCONF->fimgview & IMGVIEW_F_DOUBLE_PAGE)
		&& workSub_isFirstImageItem(p->item_cur))
		MainWindow_updateFull();
}


//=========================
// 表示倍率
//=========================


/* 固定倍率の倍率調整 */

static double _adjust_scale(double d)
{
	if(d < VIEW_SCALE_MIN)
		d = VIEW_SCALE_MIN;
	else if(d > VIEW_SCALE_MAX)
		d = VIEW_SCALE_MAX;

	//拡大しない

	if(d > 1.0
		&& (APPCONF->fimgview & IMGVIEW_F_NO_SCALE_UP))
		d = 1;

	return d;
}

/* 固定倍率 (dfixscale) のセット
 *
 * step: [-1] 値指定 [0] 一段階拡大 [正] 一段階縮小
 * return: 倍率が変更されたか */

static mlkbool _set_scale_fix(WorkData *p,double d,int step)
{
	int n,step_under,step_upper,flag;

	d = _adjust_scale(d);

	//段階調整

	if(step != -1)
	{
		n = (int)(d * 100 + 0.5);
		step_upper = APPCONF->fixscale_step_upper;
		step_under = APPCONF->fixscale_step_under;

		//拡大時は値を調整

		if(step == 0)
			n += ((n >= 100)? step_upper: step_under) - 1;

		//

		flag = 0;

		if(n >= 100 && step_upper)
		{
			//100% 以上
			n = (n - 100) / step_upper * step_upper + 100;
			flag = 1;
		}
		else if(n <= 100 && step_under)
		{
			//100% 以下
			n = n / step_under * step_under;
			flag = 1;
		}
		
		if(flag)
			d = _adjust_scale((double)n / 100);
	}

	//セット

	if(d == p->view.dfixscale)
		return FALSE;
	else
	{
		p->view.dfixscale = d;
		return TRUE;
	}
}

/* 相対倍率 (drelscale) のセット */

static mlkbool _set_scale_rel(WorkData *p,double d)
{
	int n;

	if(d < VIEW_SCALE_MIN)
		d = VIEW_SCALE_MIN;
	else if(d > VIEW_SCALE_MAX)
		d = VIEW_SCALE_MAX;

	//拡大なし時
	// :相対倍率を適用した結果が、拡大になる場合は、
	// :画像の表示倍率が 1.0 になるように、相対倍率を調整

	if((APPCONF->fimgview & IMGVIEW_F_NO_SCALE_UP)
		&& d * p->view.dscale_rel1[0] > 1.0  //1枚目
		&& (!p->view.item_double || d * p->view.dscale_rel1[1] > 1.0)) //2枚目
	{
		//見開きの場合は、倍率の小さい方を基準にする
	
		n = (p->view.item_double
			&& p->view.dscale_rel1[1] < p->view.dscale_rel1[0]);

		d = 1.0 / p->view.dscale_rel1[n];
	}

	//変更

	if(d == p->view.drelscale)
		return FALSE;
	else
	{
		p->view.drelscale = d;
		return TRUE;
	}
}

/** 一段階拡大/縮小 */

void workSet_scaleUpDown(mlkbool down)
{
	WorkData *p = APPWORK;
	int itype,n;
	double d;

	itype = APPCONF->imgsize_type;

	//画像が表示されていない or 原寸サイズの場合は何もしない

	if(p->view.type == VIEWTYPE_NONE
		|| itype == IMGSIZE_TYPE_ORIGINAL)
		return;

	//一段階変更

	n = (down)? APPCONF->scalestep_down: APPCONF->scalestep_up;
	d = (itype == IMGSIZE_TYPE_FIX_SCALE)? p->view.dfixscale: p->view.drelscale;
	d = d * ((double)n / 1000);

	//セット

	if(itype == IMGSIZE_TYPE_FIX_SCALE)
		n = _set_scale_fix(p, d, down);
	else
		n = _set_scale_rel(p, d);

	//更新

	if(n)
		MainWindow_updateViewSize_keepscale();
}

/** 表示倍率をセット
 *
 * 固定倍率時は、固定倍率値。それ以外は、相対倍率値。 */

void workSet_scale(double d)
{
	WorkData *p = APPWORK;
	int ret;

	//セット
	
	if(APPCONF->imgsize_type == IMGSIZE_TYPE_FIX_SCALE)
		ret = _set_scale_fix(p, d, -1);
	else
		ret = _set_scale_rel(p, d);

	//更新

	if(ret)
		MainWindow_updateViewSize_keepscale();
}


//=========================
//
//=========================


/** スクロールバーの最大値取得 */

void workPage_getScrollMax(mSize *size)
{
	WorkData *p = APPWORK;

	if(p->view.type == VIEWTYPE_NONE)
		size->w = size->h = 0;
	else
	{
		size->w = p->view.size_dstfull.w;
		size->h = p->view.size_dstfull.h;
	}
}


//============================
// ルーペ用計算
//============================


/* キャンバス座標から、ソース画像座標に変換 */

static void _getloupepos_conv(WorkData *p,mPoint *ptdst,int x,int y,int imgno)
{
	x = (int)((double)p->view.size_src[imgno].w / p->view.size_dst[imgno].w * x);
	y = (int)((double)p->view.size_src[imgno].h / p->view.size_dst[imgno].h * y);

	if(x < 0)
		x = 0;
	else if(x >= p->view.size_src[imgno].w)
		x = p->view.size_src[imgno].w - 1;

	if(y < 0)
		y = 0;
	else if(y >= p->view.size_src[imgno].h)
		y = p->view.size_src[imgno].h - 1;

	ptdst->x = x;
	ptdst->y = y;
}

/* カーソル位置が画像範囲内か判定し、ソース画像座標取得 */

static mlkbool _getloupepos_judge(WorkData *p,mPoint *ptdst,int x,int y,mPoint *pttop,int imgno)
{
	int xx,yy;

	xx = pttop->x + p->view.ptdrawmov[imgno].x;
	yy = pttop->y + p->view.ptdrawmov[imgno].y;

	if(xx <= x && x < xx + p->view.size_dst[imgno].w
		&& yy <= y && y < yy + p->view.size_dst[imgno].h)
	{
		_getloupepos_conv(p, ptdst, x - xx, y - yy, imgno);
		return TRUE;
	}
	else
		return FALSE;
}

/* カーソル位置が、イメージ範囲外の部分の場合 */

static int _getloupepos_out(WorkData *p,mPoint *ptdst,int x,int y,mPoint *pttop)
{
	int x1,y1,x2,imgno = 0;
	mSize szdst1,szdst2;

	x1 = pttop->x + p->view.ptdrawmov[0].x;
	y1 = pttop->y + p->view.ptdrawmov[0].y;

	szdst1 = p->view.size_dst[0];
	szdst2 = p->view.size_dst[1];

	//調整
	// : ソース座標値は座標変換時に調整されるので、
	// : x,y はある程度範囲外の値になっても良い。

	if(!p->view.item_double)
	{
		//単体

		x -= x1;
		y -= y1;
		
		if(x < 0) x = 0;
		else if(x > szdst1.w) x = szdst1.w;

		if(y < 0) y = 0;
		else if(y > szdst1.h) y = szdst1.h;
	}
	else
	{
		//---- 見開き

		x2 = pttop->x + p->view.ptdrawmov[1].x;

		//X (画像番号を決める)

		if(p->view.ptdrawmov[0].x < p->view.ptdrawmov[1].x)
		{
			//1 -> 2

			if(x < x1)
				//左側の左端
				x = 0;
			else if(x < x1 + szdst1.w)
				//左側の内
				x = x - x1;
			else if(x < x2)
				//見開き余白 (左側の右端)
				x = szdst1.w;
			else if(x < x2 + szdst2.w)
				//右側の内
				imgno = 1, x = x - x2;
			else
				//右側の右端
				imgno = 1, x = szdst2.w;
		}
		else
		{
			//2 <- 1

			if(x < x2)
				//左側の左端
				imgno = 1, x = 0;
			else if(x < x2 + szdst2.w)
				//左側の内
				imgno = 1, x = x - x2;
			else if(x < x1)
				//見開き余白 (左側の右端)
				imgno = 1, x = szdst2.w;
			else if(x < x1 + szdst1.w)
				//右側の内
				x = x - x1;
			else
				//右側の右端
				x = szdst1.w;
		}

		//Y

		y = y - (pttop->y + p->view.ptdrawmov[imgno].y);

		if(y < 0)
			y = 0;
		else if(y > p->view.size_dst[imgno].h)
			y = p->view.size_dst[imgno].h;
	}

	//変換

	_getloupepos_conv(p, ptdst, x, y, imgno);

	return imgno;
}

/** [ルーペ] キャンバスウィジェット座標からソース画像座標取得
 *
 * return: 画像番号 (0 or 1) */

int workCalc_getLoupePos(mPoint *ptdst,int x,int y)
{
	WorkData *p = APPWORK;
	mSize sizewg;
	mPoint pttop;
	int imgno;

	if(p->view.type == VIEWTYPE_NONE)
	{
		ptdst->x = ptdst->y = 0;
		return 0;
	}

	sizewg = p->size_canvas;

	//画像全体の左上位置

	pttop.x = (p->view.size_dstfull.w < sizewg.w)? (sizewg.w - p->view.size_dstfull.w) / 2: -(p->view.ptscr.x);
	pttop.y = (p->view.size_dstfull.h < sizewg.h)? (sizewg.h - p->view.size_dstfull.h) / 2: -(p->view.ptscr.y);

	//判定と座標変換

	if(_getloupepos_judge(p, ptdst, x, y, &pttop, 0))
		imgno = 0;
	else if(p->view.item_double && _getloupepos_judge(p, ptdst, x, y, &pttop, 1))
		imgno = 1;
	else
		//範囲外
		imgno = _getloupepos_out(p, ptdst, x, y, &pttop);

	return imgno;
}

/** [ルーペ] ソース座標(中央位置)から、描画時の左上位置取得
 *
 * ptsrc: 画像の中央位置 */

void workCalc_getLoupeTopPos(mDoublePoint *ptdst,mPoint *ptsrc,int imgno,int areaw,int areah)
{
	double x,y,dscale;
	mSize szsrc;

	if(!APPWORK->view.imgsrc[imgno]) return;

	szsrc = APPWORK->view.size_src[imgno];

	//

	dscale = 100.0 / APPCONF->loupe_scale;

	x = ptsrc->x + 0.5 - areaw * 0.5 * dscale;
	y = ptsrc->y + 0.5 - areah * 0.5 * dscale;

	//調整

	if((int)(x + areaw * dscale) >= szsrc.w)
		x = szsrc.w - areaw * dscale;
	
	if((int)(y + areah * dscale) >= szsrc.h)
		y = szsrc.h - areah * dscale;

	if(x < 0) x = 0;
	if(y < 0) y = 0;

	ptdst->x = x;
	ptdst->y = y;
}


//=========================
// 描画
//=========================


static const unsigned char g_img_stop[]={
0xaa,0xae,0x2a,0x02,0x2e,0x20,0x02,0x2e,0x20,0x02,0x2e,0x20,0x02,0x2e,0x20,0x02,
0x2e,0x20,0x02,0x2e,0x20,0x02,0x2e,0x20,0x02,0x2e,0x20,0x02,0x2e,0x20,0x02,0x2e,
0x20,0x02,0x2e,0x20,0x02,0x2e,0x20,0x02,0x2e,0x20,0xaa,0xae,0x2a };

static const unsigned char g_img_arrow_left[]={
0xff,0xbf,0x02,0xff,0x2f,0x02,0xff,0x0b,0x02,0xff,0x02,0x02,0xbf,0x00,0x02,0x2f,
0x00,0x02,0x0b,0x00,0x02,0x02,0x00,0x02,0x0b,0x00,0x02,0x2f,0x00,0x02,0xbf,0x00,
0x02,0xff,0x02,0x02,0xff,0x0b,0x02,0xff,0x2f,0x02,0xff,0xbf,0x02 };

static const unsigned char g_img_arrow_right[]={
0xfa,0xff,0x03,0xe2,0xff,0x03,0x82,0xff,0x03,0x02,0xfe,0x03,0x02,0xf8,0x03,0x02,
0xe0,0x03,0x02,0x80,0x03,0x02,0x00,0x02,0x02,0x80,0x03,0x02,0xe0,0x03,0x02,0xf8,
0x03,0x02,0xfe,0x03,0x82,0xff,0x03,0xe2,0xff,0x03,0xfa,0xff,0x03 };

#define _MARK_MARGIN_IMG  10
#define _MARK_MARGIN_EDGE 3


/* 背景描画 */

static void _drawpage_bkgnd(WorkData *p,mPixbuf *pixbuf,int topx,int topy,int wgw,int wgh)
{
	mSize full,size;
	mPixCol col;
	int n,n2;

	col = mRGBtoPix(APPCONF->col_bkgnd);

	full = p->view.size_dstfull;

	//上と下の余白

	if(full.h < wgh)
	{
		n = topy + full.h;
	
		mPixbufFillBox(pixbuf, 0, 0, wgw, topy, col);
		mPixbufFillBox(pixbuf, 0, n, wgw, wgh - n, col);
	}

	//左右の余白 (上下の余白分は除く)

	if(full.w < wgw)
	{
		n = topx + full.w;

		mPixbufFillBox(pixbuf, 0, topy, topx, full.h, col);
		mPixbufFillBox(pixbuf, n, topy, wgw - n, full.h, col);
	}

	//見開き

	if(p->view.item_double)
	{
		//左右間余白
		
		if(APPCONF->doublepage_margin)
		{
			n = (p->view.ptdrawmov[1].x == 0);
		
			mPixbufFillBox(pixbuf, topx + p->view.size_dst[n].w, topy,
				APPCONF->doublepage_margin, full.h, col);
		}

		//高さが違う場合、低い方の上下余白

		if(p->view.size_dst[0].h != p->view.size_dst[1].h)
		{
			n = (p->view.size_dst[1].h < p->view.size_dst[0].h);
			n2 = p->view.ptdrawmov[n].y;
		
			size = p->view.size_dst[n];

			mPixbufFillBox(pixbuf, topx + p->view.ptdrawmov[n].x, topy,
				size.w, n2, col);

			mPixbufFillBox(pixbuf,
				topx + p->view.ptdrawmov[n].x, topy + n2 + size.h,
				size.w, full.h - size.h - n2, col);
		}
	}
}

/* ページ情報 (マーク) の描画 */

static void _drawpage_pageinfo(WorkData *p,mPixbuf *pixbuf,int wgw,int wgh)
{
	int n,x[2],y,pos_prev,pos_next,fmark;
	mPixCol col[4];

	pos_prev = (APPCONF->fimgview & IMGVIEW_F_RIGHT_BINDING)? 1: 0;
	pos_next = !pos_prev;

	//先頭/終端マークを表示するか
	// :fmark = [0bit] 先頭 [1bit] 終端

	fmark = (p->item_cur == p->item_top);

	fmark |= (p->item_cur == p->item_last || p->view.item_double == p->item_last) << 1;

	//Y位置

	n = PAGEMARK_GET_POS(APPCONF->pagemark_opt);

	if(n == 0)
		y = 2;
	else if(n == 1)
		y = (wgh - 15) >> 1;
	else
		y = wgh - 15 - 2;

	//X位置
	// 画像の左右に余白がある場合は、画像の側に表示する。

	x[0] = x[1] = 0;

	if(fmark & 1) x[pos_prev] += 11;
	if(fmark & 2) x[pos_next] += 11;

	x[pos_next] += (x[pos_next])? 9 + 2: 9;

	n = (wgw - p->view.size_dstfull.w) >> 1;

	x[0] = n - x[0] - _MARK_MARGIN_IMG;

	if(x[0] < _MARK_MARGIN_EDGE)
		x[0] = _MARK_MARGIN_EDGE;

	x[1] += n + p->view.size_dstfull.w + _MARK_MARGIN_IMG;

	if(x[1] > wgw - _MARK_MARGIN_EDGE)
		x[1] = wgw - _MARK_MARGIN_EDGE;

	//------ マーク描画

	col[1] = 0;
	col[2] = MGUICOL_PIX(WHITE);
	col[3] = MPIXBUF_TPCOL;

	//先頭
	
	if(fmark & 1)
	{
		col[0] = mRGBtoPix(0x00cc00);

		n = x[pos_prev];
		if(pos_prev) n -= 11;
		
		mPixbufDraw2bitPattern(pixbuf, n, y, g_img_stop, 11, 15, col);
	}

	//終端

	if(fmark & 2)
	{
		col[0] = mRGBtoPix(0xff0000);

		n = x[pos_next];
		if(pos_next) n -= 11;
		
		mPixbufDraw2bitPattern(pixbuf, n, y, g_img_stop, 11, 15, col);

		if(pos_next) n -= 2;
		else n += 13;
		x[pos_next] = n;
	}

	//進行方向

	col[0] = mRGBtoPix(0x60ff);

	if(pos_next)
		mPixbufDraw2bitPattern(pixbuf, x[pos_next] - 9, y, g_img_arrow_right, 9, 15, col);
	else
		mPixbufDraw2bitPattern(pixbuf, x[pos_next], y, g_img_arrow_left, 9, 15, col);
}

/** ページの描画 */

void workPage_draw(mPixbuf *pixbuf,int wgw,int wgh)
{
	WorkData *p = APPWORK;
	ImageBuf *img1,*img2;
	ImageBufDrawPageInfo info;
	int x,y;

	//画像
	// : imgdst は NULL でソース画像。1 で画像作成エラー。

	img1 = p->view.imgdst[0];
	img2 = p->view.imgdst[1];

	if(!img1) img1 = p->view.imgsrc[0];
	if(!img2) img2 = p->view.imgsrc[1];

	//キャンバス情報

	info.areaw = wgw;
	info.areah = wgh;
	info.movx = (p->view.size_dstfull.w < wgw)? (wgw - p->view.size_dstfull.w) / 2: -(p->view.ptscr.x);
	info.movy = (p->view.size_dstfull.h < wgh)? (wgh - p->view.size_dstfull.h) / 2: -(p->view.ptscr.y);
	info.splitno = -1;
	info.sizesrc = p->view.size_src[0];
	info.sizedst = p->view.size_dst[0];
	info.is_nearest = (APPCONF->scaleup_method == SCALEUP_METHOD_NEAREST);
	info.tblbuf = (APPCONF->color_level_sel == 0)? NULL: p->level_tbl;

	//背景

	_drawpage_bkgnd(p, pixbuf, info.movx, info.movy, wgw, wgh);

	//

	if(!p->view.item_double)
	{
		//---- 単体

		//表示用イメージがある場合は、そのまま表示するため分割指定なし。
		//ソース画像から描画する場合は、分割指定する。

		if(!p->view.imgdst[0])
			info.splitno = p->view.splitno;

		ImageBuf_drawPage(img1, pixbuf, &info);
	}
	else
	{
		//---- 見開き

		x = info.movx;
		y = info.movy;

		//1枚目

		info.movx = x + p->view.ptdrawmov[0].x;
		info.movy = y + p->view.ptdrawmov[0].y;

		ImageBuf_drawPage(img1, pixbuf, &info);

		//2枚目

		info.movx = x + p->view.ptdrawmov[1].x;
		info.movy = y + p->view.ptdrawmov[1].y;
		info.sizesrc = p->view.size_src[1];
		info.sizedst = p->view.size_dst[1];

		ImageBuf_drawPage(img2, pixbuf, &info);
	}

	//ページ情報

	if(APPCONF->pagemark_opt & PAGEMARK_F_SHOW)
		_drawpage_pageinfo(p, pixbuf, wgw, wgh);
}

