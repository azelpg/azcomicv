/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * main
 **********************************/

#include <stdio.h>
#include <string.h>

#include <mlk_gui.h>
#include <mlk_font.h>
#include <mlk_panel.h>
#include <mlk_str.h>
#include <mlk_string.h>
#include <mlk_list.h>
#include <mlk_imagelist.h>
#include <mlk_icontheme.h>

#include "def_macro.h"
#include "def_appdata.h"
#include "def_config.h"
#include "def_work.h"
#define ICON_DEF_NAME
#include "def_icon.h"

#include "mainwindow.h"
#include "panel.h"
#include "weight_table.h"
#include "bookmark.h"
#include "work_main.h"

#include "deftrans.h"


//-----------------------
//グローバル変数定義

static AppData g_app_data_body;

AppData *g_app_data = &g_app_data_body;
ConfigData *g_app_config = NULL;
WorkData *g_app_work = NULL;

//-----------------------

/* configdata.c */

int ConfigData_new(void);
void ConfigData_free(void);
void ConfigLoadFile(mToplevelSaveState *mainwin_state,mPanelState *panel_state);
void ConfigSaveFile(void);

/* 各パネル作成 */
void PanelFileBrowser_new(mPanelState *state);
void PanelFileList_new(mPanelState *state);
void PanelLoupe_new(mPanelState *state);

//-----------------------

#define _HELP_TEXT "[usage] exe <FILE>\n\n" \
 "--help-mlk : show mlk options"

//-----------------------


//===========================
// sub
//===========================


/** アイコン読み込み */

void createImageList_icon(void)
{
	mIconTheme theme;

	mImageListFree(APPDATA->imglist_iconbar);
	APPDATA->imglist_iconbar = NULL;

	//

	theme = mIconTheme_loadTheme(mGuiGetIconTheme());
	if(!theme) return;

	APPDATA->imglist_iconbar = mImageListCreate_icontheme(theme,
		g_icon_names, APPCONF->iconsize);

	mIconTheme_free(theme);
}

/** 初期化 */

static int _init_app(void)
{
	mToplevelSaveState mainwin_state;
	mPanelState panel_state[PANEL_NUM];

	//設定ファイル用ディレクトリ作成

	mGuiCreateConfigDir(NULL);

	//AppData

	mMemset0(&g_app_data_body, sizeof(AppData));

	//データ確保

	if(ConfigData_new()
		|| WorkData_new())
		return 1;

	//設定ファイル読み込み

	ConfigLoadFile(&mainwin_state, panel_state);

	BkmList_loadConfig();

	//データ初期化

	workData_setScaleDownTable();
	workData_setLevelTable();

	//フォント作成

	if(mStrIsnotEmpty(&APPCONF->strFont_panel))
		APPDATA->font_panel = mFontCreate_text(mGuiGetFontSystem(), APPCONF->strFont_panel.buf);

	//リソース読み込み

	APPDATA->imglist_fileicon = mImageListLoadPNG("!/fileicon.png", 16);

	createImageList_icon();

	//メインウィンドウ作成

	MainWindow_new();

	//パネル作成

	PanelFileBrowser_new(panel_state + PANEL_FILEBROWSER);
	PanelFileList_new(panel_state + PANEL_FILELIST);
	PanelLoupe_new(panel_state + PANEL_LOUPE);

	//ウィンドウ表示

	MainWindow_show(APPDATA->mainwin, &mainwin_state);

	Panel_all_show();

	return 0;
}

/** 引数のファイルを開く */

static void _open_arg_file(const char *fname)
{
	mStr str = MSTR_INIT;

	mStrSetText_locale(&str, fname, -1);
	
	MainWindow_open(str.buf, MAINWIN_OPEN_NORMAL);

	mStrFree(&str);
}


//===========================
// メイン
//===========================


/** 終了処理 */

static void _finish(void)
{
	//設定ファイル保存

	ConfigSaveFile();

	BkmList_saveConfig();

	//解放

	Panel_all_destroy();

	WorkData_free();
	ConfigData_free();

	//AppData

	mListDeleteAll(&APPDATA->list_bkm);

	mFontFree(APPDATA->font_panel);

	mImageListFree(APPDATA->imglist_fileicon);
	mImageListFree(APPDATA->imglist_iconbar);
}

/** 初期化メイン */

static int _init_main(int argc,char **argv)
{
	int top,i;

	if(mGuiInit(argc, argv, &top)) return 1;

	//"--help"

	for(i = top; i < argc; i++)
	{
		if(strcmp(argv[i], "--help") == 0)
		{
			puts(_HELP_TEXT);
			mGuiEnd();
			return 1;
		}
	}

	//

	mGuiSetWMClass("azcomicv", "azcomicv");

	//パスセット

	mGuiSetPath_data_exe("../share/azcomicv");
	mGuiSetPath_config_home(".config/azcomicv");

	//翻訳データ

	mGuiLoadTranslation(g_deftransdat, NULL, "tr");

	//バックエンド初期化

	if(mGuiInitBackend()) return 1;

	//アプリ初期化

	if(_init_app())
	{
		mError("failed initialize\n");
		return 1;
	}

	//ファイル開く

	if(top < argc)
		_open_arg_file(argv[top]);

	return 0;
}

/** メイン */

int main(int argc,char **argv)
{
	//初期化

	if(_init_main(argc, argv))
		return 1;

	//実行

	mGuiRun();

	//終了

	_finish();

	mGuiEnd();

	return 0;
}


//============================
// 共通関数
//============================


/** 共通ソート関数 (ディレクトリ内ソート) */

int sortfunc_in_directory(const char *name1,uint64_t modify1,const char *name2,uint64_t modify2)
{
	uint32_t opt;
	int ret;

	opt = APPCONF->filebrowser_opt;

	if(FILEBROWSER_OPT_GET_SORTTYPE(opt) == FILEBROWSER_OPT_SORTTYPE_MODIFY
		&& modify1 != modify2)
		//更新日時順 (同じなら名前順)
		ret = (modify1 < modify2)? -1: 1;
	else
		//名前順
		ret = mStringCompare_number(name1, name2);

	//逆順

	if(opt & FILEBROWSER_OPT_F_SORT_REVERSE)
		ret = -ret;

	return ret;
}
