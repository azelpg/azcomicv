/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * WorkData 関数
 *****************************************/

#include <mlk.h>
#include <mlk_str.h>
#include <mlk_list.h>
#include <mlk_tree.h>
#include <mlk_file.h>

#include "def_work.h"
#include "def_config.h"

#include "fileoperator.h"
#include "image.h"
#include "weight_table.h"

#include "work_main.h"
#include "work_sub.h"



/* ファイルツリー、アイテム破棄 */

static void _filetree_destory(mTree *p,mTreeItem *item)
{
	mFree(((FileListItem *)item)->name);
}

/* イメージキャッシュリスト、アイテム破棄 */

static void _imgcache_destroy(mList *p,mListItem *item)
{
	ImageBuf_free(((ImageCache *)item)->img);
}

/* 対応画像の拡張子文字列をセット */

static void _set_imgexts(WorkData *p)
{
	mStrSetText(&p->strImgExts, "png:jpg:jpeg:bmp:gif:");

#if defined(HAVE_FORMAT_TIFF)
	mStrAppendText(&p->strImgExts, "tif:tiff:");
#endif

#if defined(HAVE_FORMAT_PSD)
	mStrAppendText(&p->strImgExts, "psd:");
#endif

#if defined(HAVE_FORMAT_WEBP)
	mStrAppendText(&p->strImgExts, "webp:");
#endif

#if defined(HAVE_FORMAT_AVIF)
	mStrAppendText(&p->strImgExts, "avif:");
#endif

}


//=============================
// main
//=============================


/** 解放 */

void WorkData_free(void)
{
	WorkData *p = APPWORK;

	if(p)
	{
		workFile_close();
	
		mStrFree(&p->strOpenFile);
		mStrFree(&p->strImgExts);

		mFree(p->weighttbl_down);
		mFree(p->weighttbl_up);
		mFree(p->level_tbl);

		mFree(p);
	}
}

/** WorkData 作成 */

int WorkData_new(void)
{
	WorkData *p;

	p = (WorkData *)mMalloc0(sizeof(WorkData));
	if(!p) return FALSE;

	APPWORK = p;

	//確保

	p->level_tbl = (uint8_t *)mMalloc(256);
	p->weighttbl_up = (double *)mMalloc(sizeof(double) * WEIGHTTBL_LENVAL2);
	p->weighttbl_down = (double *)mMalloc(sizeof(double) * WEIGHTTBL_LENVAL3);

	if(!p->level_tbl || !p->weighttbl_up || !p->weighttbl_down)
		goto ERR;

	//初期化

	p->view.dfixscale = 1.0;

	p->filetree.item_destroy = _filetree_destory;
	p->list_imgcache.item_destroy = _imgcache_destroy;

	_set_imgexts(p);

	WeightTable_setUp_bicubic(p->weighttbl_up);

	return 0;

ERR:
	WorkData_free();
	return 1;
}

/** 縮小時の重みテーブルをセット */

void workData_setScaleDownTable(void)
{
	double *buf = APPWORK->weighttbl_down;

	switch(APPCONF->scaledown_method)
	{
		case SCALEDOWN_METHOD_MITCHELL:
			WeightTable_setDown_mitchell(buf);
			break;
		case SCALEDOWN_METHOD_LAGRANGE:
			WeightTable_setDown_lagrange(buf);
			break;
		case SCALEDOWN_METHOD_LANCZOS2:
			WeightTable_setDown_lanczos2(buf);
			break;
		case SCALEDOWN_METHOD_LANCZOS3:
			WeightTable_setDown_lanczos3(buf);
			break;
		case SCALEDOWN_METHOD_SPLINE16:
			WeightTable_setDown_spline16(buf);
			break;
		default:
			WeightTable_setDown_spline36(buf);
			break;
	}
}

/** レベル補正の色テーブル作成 */

void workData_setLevelTable(void)
{
	uint8_t *pd = APPWORK->level_tbl;
	int i,n,min,mid,max;
	double d,d1,d2;

	if(APPCONF->color_level_sel == 0)
		return;

	n = APPCONF->color_level[APPCONF->color_level_sel - 1];

	min = (n >> 16) & 255;
	mid = (n >> 8) & 255;
	max = n & 255;

	d1 = 128.0 / (mid - min);
	d2 = 127.0 / (max - mid);

	for(i = 0; i < 256; i++)
	{
		n = i;
	
		if(n < min)
			n = min;
		else if(n > max)
			n = max;

		if(n <= mid)
			d = (n - min) * d1;
		else
			d = (n - mid) * d2 + 128;

		*(pd++) = (int)(d + 0.5);
	}
}


//============================
// ファイル
//============================


/** 開いているファイルを閉じる */

void workFile_close(void)
{
	WorkData *p = APPWORK;
	int i;

	if(p->fileop)
	{
		(p->fileop->destroy)(p->fileop);
		p->fileop = NULL;
	}

	mTreeDeleteAll(&p->filetree);

	mStrEmpty(&p->strOpenFile);

	mListDeleteAll(&p->list_imgcache);

	for(i = 0; i < 2; i++)
	{
		ImageBuf_freeptr(p->view.imgdst + i);

		p->view.imgsrc[i] = NULL;
	}

	//

	p->opentype = OPENTYPE_EMPTY;
	p->imgfile_num = 0;
	p->item_cur = NULL;
	p->filelist_parent = NULL;

	p->view.type = VIEWTYPE_NONE;
	p->view.item_double = NULL;
}

/** 開く処理
 *
 * path: ディレクトリ/ファイルのパス
 * strfname: path からディレクトリを除いた名前
 * strdir: ディレクトリパス
 *  (path がディレクトリなら、path と同じ。ファイルなら、ディレクトリ部分)
 * directory: path がディレクトリかどうか
 * pagefilename: 初期ページのファイルパス (空で先頭から)
 * return: エラーコード */

mlkerr workFile_open(const char *path,mStr *strfname,mStr *strdir,
	mlkbool directory,const char *pagefilename)
{
	WorkData *p = APPWORK;
	int type,ret,fdirfile = FALSE;
	const char *pcname;
	FileListItem *pi;

	//開くタイプ

	if(directory)
		type = OPENTYPE_DIRECTORY;
	else if(workFile_checkArchive(path))
		type = OPENTYPE_ARCHIVE;
	else
	{
		//ディレクトリ内の指定ファイル

		type = OPENTYPE_DIRECTORY;
		fdirfile = TRUE;
	}

	p->opentype = type;

	//ファイルパスをセット

	if(type == OPENTYPE_DIRECTORY)
		mStrCopy(&p->strOpenFile, strdir);
	else
		mStrSetText(&p->strOpenFile, path);

	//ファイルリスト作成

	ret = workFileList_create();

	if(ret)
	{
		p->opentype = OPENTYPE_EMPTY;
		p->imgfile_num = 0;
		return ret;
	}

	//----- 最初のファイル

	if(fdirfile)
		//path で指定された画像
		pcname = strfname->buf;
	else if(pagefilename)
		//指定されたパス
		pcname = pagefilename;
	else
		//指定なし
		pcname = NULL;

	//ファイル指定がある場合は検索、見つからなければ先頭

	pi = NULL;

	if(pcname)
		pi = workSub_getItem_fromName(p, pcname);

	if(!pi)
		pi = p->item_top;

	p->item_cur = pi;

	return MLKERR_OK;
}

/** ファイルがアーカイブかどうか */

mlkbool workFile_checkArchive(const char *filename)
{
	uint8_t buf[4];
	uint32_t sig;

	if(mReadFileHead(filename, buf, 4))
		return FALSE;

	sig = MLK_MAKE32_4(buf[3], buf[2], buf[1], buf[0]);

	return (sig == 0x04034b50);
}
