/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * WorkData - ページ関連のセット処理
 *****************************************/

#include <mlk.h>

#include "def_work.h"
#include "def_config.h"

#include "image.h"
#include "work_sub.h"


/* ビュー設定から表示タイプ取得 */

static int _get_viewtype_auto(uint32_t fimgv,int horizontal)
{
	if(fimgv & IMGVIEW_F_DOUBLE_PAGE)
	{
		//見開き

		if((fimgv & IMGVIEW_F_DOUBLE_HORZ_SINGLE) && horizontal)
			return VIEWTYPE_SINGLE;

		if(fimgv & IMGVIEW_F_RIGHT_BINDING)
			return VIEWTYPE_DOUBLE_RL;
		else
			return VIEWTYPE_DOUBLE_LR;
	}
	else
	{
		//単ページ

		if((fimgv & IMGVIEW_F_SINGLE_HORZ_HALF) && horizontal)
			return VIEWTYPE_SINGLE_SPLIT1;

		return VIEWTYPE_SINGLE;
	}
}

/* 表示タイプに関する情報をセット
 *
 * (type, item_double) */

static void _set_viewtype(WorkData *p,int viewtype)
{
	FileListItem *pi = p->item_cur;
	int type;
	uint32_t fimgv;

	fimgv = APPCONF->fimgview;

	workSub_setImageInfo(p, pi);

	//表示タイプ

	if(viewtype >= 0)
		//指定値
		type = viewtype;
	else
	{
		//--- ビュー設定から
		
		type = _get_viewtype_auto(fimgv, FILELISTITEM_IS_HORIZONTAL(pi));

		//見開き時、先頭は常に単ページ

		if(viewtype != VIEWTYPE_AUTO_NO_FIRST_PAGE
			&& (fimgv & IMGVIEW_F_DOUBLE_TOP_IS_SINGLE)
			&& workSub_isFirstImageItem(pi)
			&& VIEWTYPE_IS_DOUBLE(type))
			type = VIEWTYPE_SINGLE;
	}

	//見開き時、次の画像アイテム
	
	p->view.item_double = NULL;

	if(VIEWTYPE_IS_DOUBLE(type))
	{
		pi = p->view.item_double = workSub_getItem_nextImage(p, pi, FALSE);

		if(!pi)
			//次がない場合は単ページ
			type = VIEWTYPE_SINGLE;
		else
		{
			workSub_setImageInfo(p, pi);

			//横長単体時
			// :2枚目が横長の場合は、次で単体表示する必要があるので、
			// :現在の画像は単ページとする。

			if((fimgv & IMGVIEW_F_DOUBLE_HORZ_SINGLE)
				&& FILELISTITEM_IS_HORIZONTAL(pi))
			{
				type = VIEWTYPE_SINGLE;
				p->view.item_double = NULL;
			}
		}
	}

	//タイプセット

	p->view.type = type;
}

/* ソース画像をセット */

static void _set_srcimage(WorkData *p)
{
	p->view.imgsrc[0] = workSub_loadImageCache(p, p->item_cur);

	//2枚目

	if(p->view.item_double)
		p->view.imgsrc[1] = workSub_loadImageCache(p, p->view.item_double);
	else
		p->view.imgsrc[1] = NULL;
}

/** ページの表示サイズ決定
 *
 * 表示画像の変更 (ページ位置の移動やページ画像の変化) がない場合に使う。
 *
 * reset_relscale: 相対倍率をリセットする */

void workPage_setViewSize(mlkbool reset_relscale)
{
	WorkData *p = APPWORK;

	if(p->view.type == VIEWTYPE_NONE) return;
	
	//相対倍率リセット

	if(reset_relscale)
		p->view.drelscale = 1;

	//
	
	ImageBuf_freeptr(p->view.imgdst);
	ImageBuf_freeptr(p->view.imgdst + 1);

	//横長分割時の表示番号
	// : 1枚目<->2枚目の移動時は、
	// : この関数のみ実行されるので、ここでセットする。

	if(p->view.type == VIEWTYPE_SINGLE_SPLIT1
		|| p->view.type == VIEWTYPE_SINGLE_SPLIT2)
	{
		p->view.splitno = (p->view.type == VIEWTYPE_SINGLE_SPLIT2);

		//右綴じ時は逆
		if(APPCONF->fimgview & IMGVIEW_F_RIGHT_BINDING)
			p->view.splitno ^= 1;
	}
	else
		p->view.splitno = -1;

	//画像表示サイズ決定

	workSub_setViewSize(p);

	//表示用イメージ作成

	workSub_createViewImage(p, 0);

	if(p->view.item_double)
		workSub_createViewImage(p, 1);
}

/** 画像とページ情報セット
 *
 * item_cur はあらかじめイメージの位置に設定しておく。
 *
 * viewtype: 表示タイプの指定
 * return: 更新したか */

mlkbool workPage_setFull(int viewtype)
{
	WorkData *p = APPWORK;

	if(!p->item_cur) return FALSE;

	//表示タイプ
	
	_set_viewtype(p, viewtype);

	//---- 表示イメージ変更

	ImageBuf_freeptr(p->view.imgdst);
	ImageBuf_freeptr(p->view.imgdst + 1);

	//ソース画像セット

	_set_srcimage(p);
	
	//表示サイズ決定

	workPage_setViewSize(TRUE);

	return TRUE;
}
