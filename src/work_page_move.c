/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * WorkData - ページ移動
 *****************************************/

#include <mlk.h>

#include "def_work.h"
#include "def_config.h"

#include "panel_func.h"
#include "work_main.h"
#include "work_sub.h"


//---------------

enum
{
	_RET_UNMOVED,		//移動なし (そのまま現状維持)
	_RET_UNMOVED_LAST,	//移動なし (終端の状態で次へ移動時)
	_RET_UNMOVED_TOP,	//移動なし (先頭の状態で前へ移動時)
	_RET_MOVED,			//移動した
	_RET_MOVED_SPLIT	//移動した (分割で片方への移動時)
};

//---------------


//===============================
// 位置移動
//===============================

/*
 - p->item_cur に先頭位置をセットすること。
 
 - 表示タイプを指定する場合は *pviewtype に値をセットする。
   デフォルトで自動。
 
 - 現在位置から移動する時、
   次に表示するページがない場合は _RET_UNMOVED を返して、
   位置はそのままで現状維持すること。

>> 見開きタイプで単体表示になるのは、以下の3通り。
 
 - 先頭 (先頭は常に単体の設定時)
 - 終端 (残りが一枚)
 - 横長単体 (現在が横長、または次が横長)
*/


/** 先頭ページ */

static int _top_page(WorkData *p)
{
	p->item_cur = workSub_getItem_nextImage(p, NULL, FALSE);

	return _RET_MOVED;
}

/** 終端ページ */

static int _end_page(WorkData *p,int *pviewtype)
{
	FileListItem *pi;

	//終端位置

	pi = workSub_getItem_prevImage(p, NULL, TRUE);
	if(!pi) return _RET_UNMOVED;

	//位置調整
	
	p->item_cur = workSub_getItem_prevAdjust(p, pi, pviewtype);

	return _RET_MOVED;
}

/** 次のページ */

static int _next_page(WorkData *p,int *pviewtype)
{
	FileListItem *pi;

	if(!p->item_cur)
		return _RET_UNMOVED_LAST;
	else if(p->view.type == VIEWTYPE_SINGLE_SPLIT1)
	{
		//分割で1枚目の場合は2枚目へ

		p->view.type = VIEWTYPE_SINGLE_SPLIT2;
		return _RET_MOVED_SPLIT;
	}
	else
	{
		pi = (p->view.item_double)? p->view.item_double: p->item_cur;

		//一つ次へ
	
		pi = workSub_getItem_nextImage(p, pi, TRUE);
		if(!pi)
			return _RET_UNMOVED_LAST;

		p->item_cur = pi;

		return _RET_MOVED;
	}
}

/** 前のページ */

static int _prev_page(WorkData *p,int *pviewtype)
{
	FileListItem *pi;

	if(!p->item_cur)
		return _RET_UNMOVED_TOP;
	else if(p->view.type == VIEWTYPE_SINGLE_SPLIT2)
	{
		//分割で2枚目の場合は1枚目へ

		p->view.type = VIEWTYPE_SINGLE_SPLIT1;
		return _RET_MOVED_SPLIT;
	}
	else
	{
		//一つ前へ

		pi = workSub_getItem_prevImage(p, p->item_cur, TRUE);
		if(!pi) return _RET_UNMOVED_TOP;

		//位置調整

		p->item_cur = workSub_getItem_prevAdjust(p, pi, pviewtype);

		return _RET_MOVED;
	}
}

/** 次の画像 */

static int _next_image(WorkData *p,int *pviewtype)
{
	FileListItem *pi,*pinext;

	if(!(APPCONF->fimgview & IMGVIEW_F_DOUBLE_PAGE))
		//単ページ
		return _next_page(p, pviewtype);
	else
	{
		//------- 見開き

		if(p->view.item_double)
			//現状が見開きの場合、2枚目の画像を先頭にする
			p->item_cur = p->view.item_double;
		else
		{
			//----- 現状が単体
		
			pi = p->item_cur;

			//次の画像

			pinext = workSub_getItem_nextImage(p, pi, TRUE);
			if(!pinext) return _RET_UNMOVED_LAST;

			//pi,pinext で見開きが可能なら、現在位置で見開き。
			// (先頭ページの単体表示の時、次の画像で見開きにするため)
			//見開き不可なら、次の画像へ。

			if(!workSub_isEnableDoublePage(p, pi, pinext))
				p->item_cur = pinext;

			//"先頭は常に単体" は無効にする

			*pviewtype = VIEWTYPE_AUTO_NO_FIRST_PAGE;
		}

		return _RET_MOVED;
	}
}

/** 前の画像 */

static int _prev_image(WorkData *p,int *pviewtype)
{
	FileListItem *pi,*piprev;

	if(!(APPCONF->fimgview & IMGVIEW_F_DOUBLE_PAGE))
		//単ページ
		return _prev_page(p, pviewtype);
	else
	{
		//------- 見開き
	
		//一つ前へ
			
		pi = workSub_getItem_prevImage(p, p->item_cur, FALSE);

		if(!pi)
		{
			//前がない(先頭画像)の場合

			if(p->view.item_double)
				//現状が見開きなら、1枚目のみを単体表示
				*pviewtype = VIEWTYPE_SINGLE;
			else
				return _RET_UNMOVED_TOP;
		}
		else
		{
			//---- 一つ前の画像へ
			
			p->item_cur = pi;
		
			//現状が単体画像の場合、
			//もう一つ前と見開き表示が可能なら、さらに一つ前へ。

			if(!p->view.item_double)
			{
				piprev = workSub_getItem_prevImage(p, pi, FALSE);

				if(piprev
					&& workSub_isEnableDoublePage(p, piprev, pi))
					p->item_cur = piprev;
			}

			//"先頭は常に単体" は無効にする

			*pviewtype = VIEWTYPE_AUTO_NO_FIRST_PAGE;
		}

		return _RET_MOVED;
	}
}

/** 画像のインデックス番号から */

static int _goto_indexno(WorkData *p,int no)
{
	FileListItem *pi;

	pi = workSub_getItem_fromSortedIndex(p, no);
	if(!pi) return _RET_UNMOVED;

	p->item_cur = pi;

	return _RET_MOVED;
}


//===============================
// main
//===============================


/** 指定位置へ移動
 *
 * 端の位置で移動した時に、次・前のアーカイブ/ディレクトリへの移動に対応する場合は、
 * 戻り値を処理する。
 *
 * no: 画像のインデックス位置、またはコマンド値
 * return: MOVEPAGE_RET_* */

int workPage_movePage(int no)
{
	WorkData *p = APPWORK;
	int ret,viewtype;

	if(!p->item_cur)
		return MOVEPAGE_RET_UNMOVED;

	//----- 指定位置へ移動 (item_cur に先頭位置をセット)

	ret = _RET_UNMOVED;
	viewtype = VIEWTYPE_AUTO;

	if(no >= 0)
	{
		//画像のインデックス番号

		ret = _goto_indexno(p, no);
	}
	else
	{
		switch(no)
		{
			//次のページ
			case MOVEPAGE_NEXT_PAGE:
				ret = _next_page(p, &viewtype);
				break;
			//前のページ
			case MOVEPAGE_PREV_PAGE:
				ret = _prev_page(p, &viewtype);
				break;
			//次の画像
			case MOVEPAGE_NEXT_IMAGE:
				ret = _next_image(p, &viewtype);
				break;
			//前の画像
			case MOVEPAGE_PREV_IMAGE:
				ret = _prev_image(p, &viewtype);
				break;
			//先頭
			case MOVEPAGE_TOP:
				ret = _top_page(p);
				break;
			//終端
			case MOVEPAGE_END:
				ret = _end_page(p, &viewtype);
				break;
		}
	}

	//----- 処理

	switch(ret)
	{
		//移動なし
		case _RET_UNMOVED:
			return MOVEPAGE_RET_UNMOVED;
		//移動なし (終端で次へ移動)
		case _RET_UNMOVED_LAST:
			return MOVEPAGE_RET_NEXT_TOP;
		//移動なし (先頭で前へ移動)
		case _RET_UNMOVED_TOP:
			return MOVEPAGE_RET_PREV_LAST;
		//分割で片方へ移動
		case _RET_MOVED_SPLIT:
			workPage_setViewSize(TRUE);
			break;
		//移動
		default:
			workPage_setFull(viewtype);

			//リスト同期

			if(APPCONF->foption & OPTION_F_SYNC_FILELIST)
				PanelFileList_sync_moved();
			break;
	}

	return MOVEPAGE_RET_OK;
}
