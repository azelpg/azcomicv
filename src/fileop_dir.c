/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * FileOperator : ディレクトリ操作
 *****************************************/

#include <mlk.h>
#include <mlk_str.h>
#include <mlk_dir.h>
#include <mlk_file.h>
#include <mlk_filestat.h>

#include "def_work.h"
#include "def_config.h"

#include "fileoperator.h"
#include "image.h"


//----------------

typedef struct
{
	FileOperator b;

	mStr strPath;
}_fileop;

//----------------



/* フルパス取得 */

static void _get_fullpath(_fileop *p,mStr *dst,const char *name)
{
	mStrCopy(dst, &p->strPath);
	mStrPathJoin(dst, name);
}



/* ファイル列挙 */

static mlkerr _enum_entry(FileOperator *ptr,FuncFileOpEnum func,void *param)
{
	_fileop *p = (_fileop *)ptr;
	mDir *dir;
	const char *pc_exts;
	mFileStat st;
	FileOpInfo info;
	mStr str = MSTR_INIT;

	pc_exts = APPWORK->strImgExts.buf;

	info.filepos = 0;

	//

	dir = mDirOpen(p->strPath.buf);
	if(!dir) return MLKERR_OPEN;

	while(mDirNext(dir))
	{
		mDirGetFilename_str(dir, &str, FALSE);
		mDirGetStat(dir, &st);

		if(!(st.flags & MFILESTAT_F_DIRECTORY)
			&& mStrPathCompareExts(&str, pc_exts))
		{
			info.name = str.buf;
			info.time_modify = st.time_modify;

			(func)(&info, param);
		}
	}

	mDirClose(dir);

	mStrFree(&str);

	return MLKERR_OK;
}

/* ファイルの抽出 */

static mlkbool _extract(FileOperator *p,FileOpHandle *fh,const char *filename)
{
	mStr str = MSTR_INIT;
	mlkbool ret;

	_get_fullpath((_fileop *)p, &str, fh->name);

	ret = (mCopyFile(str.buf, filename) == 0);

	mStrFree(&str);

	return ret;
}

/* 画像の読み込み */

static ImageBuf *_loadimage(FileOperator *ptr,FileOpHandle *fh)
{
	mStr str = MSTR_INIT;
	ImageBuf *img;

	_get_fullpath((_fileop *)ptr, &str, fh->name);

	img = ImageBuf_loadFile(str.buf, NULL, 0, APPCONF->col_blendbkgnd);

	mStrFree(&str);

	return img;
}

/* 解放 */

static void _destroy(FileOperator *ptr)
{
	mStrFree(&((_fileop *)ptr)->strPath);

	mFree(ptr);
}

/** ディレクトリ開く */

mlkerr FileOperator_openDir(FileOperator **pp,const char *path)
{
	_fileop *p;

	*pp = NULL;

	p = (_fileop *)mMalloc0(sizeof(_fileop));
	if(!p) return MLKERR_ALLOC;

	p->b.destroy = _destroy;
	p->b.enum_entry = _enum_entry;
	p->b.extract = _extract;
	p->b.loadimage = _loadimage;

	mStrSetText(&p->strPath, path);

	*pp = (FileOperator *)p;

	return MLKERR_OK;
}

