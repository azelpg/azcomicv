/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * FileOperator : ZIP ファイル操作
 *****************************************/

#include <stdio.h>

#include <mlk.h>
#include <mlk_str.h>
#include <mlk_stdio.h>
#include <mlk_file.h>
#include <mlk_zlib.h>
#include <mlk_charset.h>
#include <mlk_string.h>
#include <mlk_util.h>

#include "def_work.h"
#include "def_config.h"

#include "fileoperator.h"
#include "image.h"


//----------------

typedef struct
{
	FileOperator b;

	FILE *fp;
	mZlib *zlib;
	mIconv iconv[3];
	int iconv_num;
}_fileop;

//----------------

typedef struct
{
	mStr strname;
	uint32_t size_uncomp,
		size_comp,
		time,
		filepos;
}FILEHEADER;

//----------------


//======================
// sub
//======================


/* ファイルヘッダの位置へ */

static mlkerr _goto_fileheader(FILE *fp)
{
	int i;
	uint8_t d[4],sigb4[4] = {0x50,0x4b,0x05,0x06};
	uint32_t sig,size;

	rewind(fp);

	//ファイル先頭の識別子

	if(mFILEreadLE32(fp, &sig) || sig != 0x04034b50)
		return MLKERR_UNSUPPORTED;

	//----- 中央ディレクトリ終了の位置へ

	//終端 - 22byte

	if(fseek(fp, -22, SEEK_END))
		return MLKERR_DAMAGED;

	//識別子検索 (コメント最大長さまで)

	sig = *((uint32_t *)sigb4);

	for(i = 0; i < 0xffff; i++)
	{
		if(fread(d, 1, 4, fp) != 4)
			return MLKERR_DAMAGED;

		if(*((uint32_t *)d) == sig)
			break;

		if(fseek(fp, -5, SEEK_CUR))
			return MLKERR_DAMAGED;
	}

	if(i > 0xffff) return MLKERR_DAMAGED;

	//---- 中央ディレクトリ開始位置へ

	if(fseek(fp, 12, SEEK_CUR)
		|| mFILEreadLE32(fp, &size)
		|| fseek(fp, size, SEEK_SET))
		return MLKERR_DAMAGED;

	return MLKERR_OK;
}

/* ファイル名を文字コード変換してセット */

static void _convert_charcode(_fileop *p,mStr *str,const char *text,int len)
{
	int i,dlen;
	char *buf;

	for(i = 0; i < p->iconv_num; i++)
	{
		buf = mIconvConvert(p->iconv[i], text, len, &dlen, 0);
		if(!buf) continue;
		
		mStrSetText_len(str, buf, dlen);
		mFree(buf);
		return;
	}

	//デフォルトで、ロケール文字列

	mStrSetText_locale(str, text, len);
}

/* ファイルヘッダ読み込み
 *
 * return: -1 で終了 */

static mlkerr _read_fileheader(_fileop *p,FILE *fp,FILEHEADER *dst)
{
	uint8_t d[42];
	uint32_t sig,compsize,uncompsize;
	uint16_t flags,comp,ftime,fdate,namelen,exlen,comlen;
	char *name;

	dst->filepos = ftell(p->fp);

	//識別子
	//ファイルヘッダでない場合は、終了
	
	if(mFILEreadLE32(fp, &sig) || sig != 0x02014b50)
		return -1;

	//ファイル名部分まで読み込み

	if(fread(d, 1, 42, fp) != 42)
		return MLKERR_DAMAGED;

	mGetBuf_format(d, "4Shhhh4Siihhh",
		&flags, &comp, &ftime, &fdate, &compsize, &uncompsize,
		&namelen, &exlen, &comlen);

	//暗号化、圧縮メソッド

	if((flags & 1)
		|| (comp != 0 && comp != 8)
		|| compsize == 0xffffffff)	//0xffffffff で ZIP64
		return MLKERR_UNSUPPORTED;

	//

	dst->size_comp = compsize;
	dst->size_uncomp = uncompsize;
	dst->time = ((uint32_t)fdate << 16) | ftime;

	//名前 (パス区切りは '/' にする)

	if(namelen == 0)
		mStrEmpty(&dst->strname);
	else
	{
		//読み込み
		
		name = (char *)mMalloc(namelen + 1);
		if(!name) return MLKERR_ALLOC;

		if(fread(name, 1, namelen, fp) != namelen)
		{
			mFree(name);
			return MLKERR_DAMAGED;
		}
		
		name[namelen] = 0;

		//UTF-8 変換
		
		if(flags & (1<<11))
			//UTF-8
			mStrSetText(&dst->strname, name);
		else
			//変換
			_convert_charcode(p, &dst->strname, name, namelen);

		mFree(name);

		//パス区切り置き換え

		mStrReplaceChar(&dst->strname, '\\', '/');
	}

	//拡張、コメントをスキップ

	if(fseek(fp, exlen + comlen, SEEK_CUR))
		return MLKERR_DAMAGED;

	return MLKERR_OK;
}

/* 中央ディレクトリのファイルヘッダ位置からファイル展開 */

static mlkbool _decompress(_fileop *p,uint32_t filepos,mBufSize *dst)
{
	FILE *fp;
	uint8_t d[26];
	uint16_t method,namelen,exlen;
	uint32_t sig,size_comp,size_uncomp,pos;
	uint8_t *buf;

	fp = p->fp;

	//----- 中央ディレクトリ・ファイルヘッダ
	//汎用フラグの bit 3 が ON の場合、
	//ローカルファイルヘッダにはファイルサイズが記録されていないので、
	//中央ディレクトリのヘッダから取得する。

	fseek(fp, filepos + 20, SEEK_SET);

	//圧縮後のサイズ

	mFILEreadLE32(fp, &size_comp);

	//非圧縮サイズ

	mFILEreadLE32(fp, &size_uncomp);

	//ローカルファイルヘッダの位置

	fseek(fp, 14, SEEK_CUR);

	mFILEreadLE32(fp, &pos);

	fseek(fp, pos, SEEK_SET);
	

	//------ ローカルファイルヘッダ

	//識別子

	if(mFILEreadLE32(fp, &sig) || sig != 0x04034b50)
		return FALSE;

	//ファイル名まで

	if(fread(d, 1, 26, fp) != 26)
		return FALSE;

	mGetBuf_format(d, "4Sh16Shh",
		&method, &namelen, &exlen);

	//名前、拡張

	fseek(fp, namelen + exlen, SEEK_CUR);

	//------ 展開

	buf = (uint8_t *)mMalloc(size_uncomp);
	if(!buf) return FALSE;

	if(method == 0)
	{
		//無圧縮

		if(fread(buf, 1, size_uncomp, fp) != size_uncomp)
			goto ERR;
	}
	else
	{
		//Deflate

		mZlibDecReset(p->zlib);

		if(mZlibDecReadOnce(p->zlib, buf, size_uncomp, size_comp))
			goto ERR;
	}

	//

	dst->buf = buf;
	dst->size = size_uncomp;

	return TRUE;

ERR:
	mFree(buf);
	return FALSE;
}


//======================
// main
//======================


/* アイテムを列挙 */

static mlkerr _enum_entry(FileOperator *ptr,FuncFileOpEnum func,void *param)
{
	_fileop *p = (_fileop *)ptr;
	FileOpInfo info;
	FILEHEADER fh;
	mlkerr ret;

	ret = _goto_fileheader(p->fp);
	if(ret) return ret;

	mMemset0(&fh, sizeof(FILEHEADER));

	while(1)
	{
		//-1 で終了

		ret = _read_fileheader(p, p->fp, &fh);
		if(ret) break;

		//[空ファイル名] [サイズが0 (ディレクトリ含む)] [画像以外] は除く
		
		if(mStrIsnotEmpty(&fh.strname)
			&& fh.size_uncomp
			&& mStrPathCompareExts(&fh.strname, APPWORK->strImgExts.buf))
		{
			info.name = fh.strname.buf;
			info.time_modify = fh.time;
			info.filepos = fh.filepos;

			(func)(&info, param);
		}
	}

	mStrFree(&fh.strname);

	return (ret == -1)? MLKERR_OK: ret;
}

/* ファイルの抽出 */

static mlkbool _extract(FileOperator *p,FileOpHandle *fh,const char *filename)
{
	mBufSize buf;
	mlkerr ret;

	if(!_decompress((_fileop *)p, fh->filepos, &buf))
		return FALSE;

	ret = mWriteFile_fromBuf(filename, buf.buf, buf.size);

	mFree(buf.buf);

	return (ret == MLKERR_OK);
}

/* 画像の読み込み */

static ImageBuf *_loadimage(FileOperator *ptr,FileOpHandle *fh)
{
	mBufSize buf;
	ImageBuf *img;

	if(!_decompress((_fileop *)ptr, fh->filepos, &buf))
		return NULL;

	img = ImageBuf_loadFile(NULL, buf.buf, buf.size, APPCONF->col_blendbkgnd);

	mFree(buf.buf);

	return img;
}

/* 解放 */

static void _destroy(FileOperator *ptr)
{
	_fileop *p = (_fileop *)ptr;
	int i;

	mZlibFree(p->zlib);

	if(p->fp)
		fclose(p->fp);

	for(i = 0; i < p->iconv_num; i++)
		mIconvClose(p->iconv[i]);

	mFree(p);
}

/* iconv 作成 */

static void _create_iconv(_fileop *p)
{
	const char *pc,*pcend;
	mStr str = MSTR_INIT;
	int num = 0;

	pc = APPCONF->strArchiveCharcode.buf;

	while(mStringGetNextSplit(&pc, &pcend, ',') && num < 3)
	{
		mStrSetText_len(&str, pc, pcend - pc);
	
		if(mIconvOpen(p->iconv + num, str.buf, "UTF-8"))
			num++;
	
		pc = pcend;
	}

	p->iconv_num = num;

	mStrFree(&str);
}

/** 開く */

mlkerr FileOperator_openZip(FileOperator **pp,const char *filename)
{
	_fileop *p;
	FILE *fp;

	*pp = NULL;

	//ファイル開く

	fp = mFILEopen(filename, "rb");
	if(!fp) return MLKERR_OPEN;

	//確保

	p = (_fileop *)mMalloc0(sizeof(_fileop));
	if(!p)
	{
		fclose(fp);
		return MLKERR_ALLOC;
	}

	p->fp = fp;

	p->b.destroy = _destroy;
	p->b.enum_entry = _enum_entry;
	p->b.extract = _extract;
	p->b.loadimage = _loadimage;

	//zlib

	p->zlib = mZlibDecNew(8 * 1024, -15);
	if(!p->zlib)
	{
		_destroy((FileOperator *)p);
		return MLKERR_ALLOC;
	}

	mZlibSetIO_stdio(p->zlib, fp);

	//iconv

	_create_iconv(p);

	//

	*pp = (FileOperator *)p;

	return MLKERR_OK;
}

