/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * WorkData
 *
 * ページの表示サイズ決定
 *****************************************/

#include <mlk.h>

#include "def_macro.h"
#include "def_work.h"
#include "def_config.h"

#include "work_main.h"


//-------------------

typedef struct
{
	mSize szsrc1,		//1枚目のソースサイズ
		szsrc2,			//2枚目のソースサイズ
		szdst1,			//1枚目の表示サイズ
		szdst2,			//2枚目の表示サイズ
		szfit,			//合わせるサイズ
		szsrcfull;		//全体のソースサイズ (見開き時は2枚合わせたサイズ)
	int double_margin;	//見開きの左右間余白
	uint8_t fdoublepage,	//見開きか
		fnoscaleup;			//拡大しない
}_infodat;

//-------------------


//======================
// sub
//======================


/* 1枚目のソース幅を取得
 *
 * 分割の場合は、分割後のサイズ */

static int _get_1st_srcwidth(WorkData *p)
{
	int w = p->item_cur->width;

	switch(p->view.type)
	{
		case VIEWTYPE_SINGLE_SPLIT1:
			return w >> 1;
		case VIEWTYPE_SINGLE_SPLIT2:
			return w - (w >> 1);
		default:
			return w;
	}
}

/* 全体のサイズ取得 */

static void _get_fullsize(mSize *dst,_infodat *info,mSize *size1,mSize *size2)
{
	//幅
	
	dst->w = size1->w + size2->w;
	if(info->fdoublepage) dst->w += info->double_margin;

	//高さ

	dst->h = (size1->h > size2->h)? size1->h: size2->h;
}

/* 画像サイズ値の調整 */

static int _adjust_size(int n)
{
	if(n < 1) n = 1;
	else if(n > MAX_IMAGESIZE) n = MAX_IMAGESIZE;

	return n;
}

/* sh を高さ (dh) に合わせた時の幅取得 */

static int _get_fit_width(int sw,int sh,int dh)
{
	return _adjust_size((int)((double)dh / sh * sw + 0.5));
}

/* sw を幅 (dw) に合わせた時の高さ取得 */

static int _get_fit_height(int sw,int sh,int dw)
{
	return _adjust_size((int)((double)dw / sw * sh + 0.5));
}

/* src に倍率を適用したサイズ取得 */

static void _scale_size(mSize *dst,mSize *src,double d)
{
	dst->w = _adjust_size((int)(src->w * d + 0.5));
	dst->h = _adjust_size((int)(src->h * d + 0.5));
}

/* 現在の表示サイズから、表示倍率を取得
 *
 * dst: 各画像用、2個の配列 */

static void _get_curscale(double *dst,_infodat *p)
{
	dst[0] = (double)p->szdst1.w / p->szsrc1.w;

	if(p->fdoublepage)
		dst[1] = (double)p->szdst2.w / p->szsrc2.w;
	else
		dst[1] = 0;
}


//===================================
// 指定幅/高さに合わせたサイズ取得
//===================================


/* 指定幅に合わせたサイズ取得
 *
 * dst: NULL で原寸かどうかのみ判定
 * return: TRUE で原寸 */

static mlkbool _getsize_fit_width(mSize *dst,_infodat *p,mSize *src,int fitw)
{
	if(fitw < 1) fitw = 1;

	if(src->w <= fitw && p->fnoscaleup)
	{
		//全体が収まる場合 (拡大する場合は除く)

		if(dst) *dst = *src;
		return TRUE;
	}
	else
	{
		if(dst)
		{
			dst->h = _get_fit_height(src->w, src->h, fitw);
			dst->w = fitw;
		}
		return FALSE;
	}
}

/* 指定高さに合わせたサイズ取得
 *
 * return: TRUE で原寸 */

static mlkbool _getsize_fit_height(mSize *dst,_infodat *p,mSize *src,int fith)
{
	if(src->h <= fith && p->fnoscaleup)
	{
		//全体が収まる場合 (拡大する場合は除く)

		if(dst) *dst = *src;
		return TRUE;
	}
	else
	{
		if(dst)
		{
			dst->w = _get_fit_width(src->w, src->h, fith);
			dst->h = fith;
		}
		return FALSE;
	}
}

/* [見開き] 高さが低い方を高い方に合わせた時の、全体サイズ取得
 *
 * dst: 2個の配列
 * return: 低い方の番号 */

static int _getsize_double_src_fit_height(mSize *full,mSize *dst,_infodat *p)
{
	int no = 0;

	dst[0] = p->szsrc1;
	dst[1] = p->szsrc2;

	//高さが異なる時

	if(p->szsrc1.h != p->szsrc2.h)
	{
		no = (p->szsrc2.h < p->szsrc1.h);

		//高さの低い方を、高い方の高さに合わせて拡大
		// : _getsize_fit_height() だと、拡大なし時の判定がされてしまうので、
		// : 直接計算する。

		dst[no].w = _get_fit_width(dst[no].w, dst[no].h, dst[!no].h);
		dst[no].h = dst[!no].h;
	}

	_get_fullsize(full, p, dst, dst + 1);

	return no;
}


//=========================
// 表示サイズセット
//=========================


/* 表示サイズを原寸にセット */

static void _set_dstsize_original(_infodat *p)
{
	p->szdst1 = p->szsrc1;
	p->szdst2 = p->szsrc2;
}

/* 拡大なし時、拡大になっている場合は原寸にする */

static void _adjust_dstsize_noscaleup(_infodat *p)
{
	if(p->fnoscaleup)
	{
		if(p->szdst1.w > p->szsrc1.w
			|| p->szdst1.h > p->szsrc1.h)
			p->szdst1 = p->szsrc1;

		if(p->szdst2.w > p->szsrc2.w
			|| p->szdst2.h > p->szsrc2.h)
			p->szdst2 = p->szsrc2;
	}
}

/* 指定サイズに合わせる時、相対倍率適用 */

static void _set_dstsize_fitsize_rel(WorkData *p,_infodat *info)
{
	double d = p->view.drelscale;

	//現在の表示倍率

	_get_curscale(p->view.dscale_rel1, info);

	//倍率適用

	_scale_size(&info->szdst1, &info->szdst1, d);

	if(info->fdoublepage)
		_scale_size(&info->szdst2, &info->szdst2, d);

	//拡大しない

	_adjust_dstsize_noscaleup(info);
}


//=========================
// 各タイプ別のセット
//=========================


/* ビュー幅に合わせる */

static void _set_fit_width(_infodat *p)
{
	mSize full,size,src[2],dst[2];
	int fitw,lowno,highno;

	fitw = p->szfit.w;

	if(!p->fdoublepage)
		//単ページ
		_getsize_fit_width(&p->szdst1, p, &p->szsrc1, fitw);
	else
	{
		//----- 見開き

		//結果を原寸にしておく

		dst[0] = p->szsrc1;
		dst[1] = p->szsrc2;

		//拡大なし＆全体が原寸で収まる場合は、そのまま原寸

		if(!(p->fnoscaleup && p->szsrcfull.w <= fitw))
		{
			//高さが低い方を高い方に合わせた全体サイズを、ソースとする

			lowno = _getsize_double_src_fit_height(&full, src, p);

			highno = !lowno;

			//全体サイズをビュー幅に合わせる

			fitw -= p->double_margin;
			full.w -= p->double_margin;

			_getsize_fit_width(&size, p, &full, fitw);

			//[!] 以下、高い方が原寸になる場合もあり

			if(p->fnoscaleup && size.h >= src[lowno].h)
			{
				//拡大なし時、高さの低い方が拡大になる場合は、
				//低い方は原寸、高い方は残りの幅に合わせる。

				_getsize_fit_width(dst + highno, p, src + highno, fitw - dst[lowno].w);
			}
			else
			{
				//高い方は、全体の幅に合わせた高さに合わせる。
				//低い方は、残りの幅＋合わせた高さ。

				_getsize_fit_height(dst + highno, p, src + highno, size.h);
				
				dst[lowno].w = _adjust_size(fitw - dst[highno].w);
				dst[lowno].h = size.h;
			}
		}

		//セット

		p->szdst1 = dst[0];
		p->szdst2 = dst[1];
	}
}

/* ビュー高さに合わせる */

static void _set_fit_height(_infodat *p)
{
	int fith = p->szfit.h;

	//画像ごとに高さを合わせる

	_getsize_fit_height(&p->szdst1, p, &p->szsrc1, fith);

	if(p->fdoublepage)
		_getsize_fit_height(&p->szdst2, p, &p->szsrc2, fith);
}

/* ビュー全体に合わせる */

static void _set_fit_size(_infodat *p)
{
	mSize full;

	//まず高さに合わせる

	_set_fit_height(p);

	//全体の幅がビュー幅より大きい場合は、幅に合わせる

	_get_fullsize(&full, p, &p->szdst1, &p->szdst2);

	if(full.w > p->szfit.w)
		_set_fit_width(p);
}

/* 固定倍率 */

static void _set_fix_scale(_infodat *p,double d)
{
	_scale_size(&p->szdst1, &p->szsrc1, d);

	if(p->fdoublepage)
		_scale_size(&p->szdst2, &p->szsrc2, d);

	_adjust_dstsize_noscaleup(p);
}


//==============================
// main
//==============================


/* 描画時の表示位置セット */

static void _set_drawpos(WorkData *p)
{
	mPoint *ptdst = p->view.ptdrawmov;
	int no;

	mMemset0(ptdst, sizeof(mPoint) * 2);

	//見開き時

	if(p->view.type == VIEWTYPE_DOUBLE_LR
		|| p->view.type == VIEWTYPE_DOUBLE_RL)
	{
		//X (2枚目の位置をセット)

		no = (p->view.type == VIEWTYPE_DOUBLE_LR);

		ptdst[no].x = p->view.size_dst[!no].w + APPCONF->doublepage_margin;

		//Y (高さが低い方を中央寄せ)

		no = (p->view.size_dst[1].h < p->view.size_dst[0].h);
		
		ptdst[no].y = (p->view.size_dstfull.h - p->view.size_dst[no].h) >> 1;
	}
}

/* データの準備 */

static void _ready_info(WorkData *p,_infodat *pd)
{
	pd->double_margin = APPCONF->doublepage_margin;
	pd->fdoublepage = (p->view.item_double != 0);
	pd->fnoscaleup = ((APPCONF->fimgview & IMGVIEW_F_NO_SCALE_UP) != 0);

	//ソースサイズ

	pd->szsrc1.w = _get_1st_srcwidth(p);
	pd->szsrc1.h = p->item_cur->height;

	if(pd->fdoublepage)
	{
		pd->szsrc2.w = p->view.item_double->width;
		pd->szsrc2.h = p->view.item_double->height;
	}

	//ソースの全体サイズ

	_get_fullsize(&pd->szsrcfull, pd, &pd->szsrc1, &pd->szsrc2);

	//合わせるサイズ

	if(APPCONF->fimgview & IMGVIEW_F_VIEWSIZE_FIT_FIX)
		pd->szfit = APPCONF->size_view;
	else
		pd->szfit = p->size_canvas;
}

/** 表示サイズ情報セット */

void workSub_setViewSize(WorkData *p)
{
	_infodat info;
	int imgtype;

	//準備

	mMemset0(&info, sizeof(_infodat));

	_ready_info(p, &info);

	//----- 表示サイズセット

	imgtype = APPCONF->imgsize_type;

	switch(imgtype)
	{
		//ビューに合わせる
		case IMGSIZE_TYPE_FIT_VIEW:
			_set_fit_size(&info);
			_set_dstsize_fitsize_rel(p, &info);
			break;

		//ビューの幅に合わせる
		case IMGSIZE_TYPE_FIT_VIEW_WIDTH:
			_set_fit_width(&info);
			_set_dstsize_fitsize_rel(p, &info);
			break;

		//ビューの高さに合わせる
		case IMGSIZE_TYPE_FIT_VIEW_HEIGHT:
			_set_fit_height(&info);
			_set_dstsize_fitsize_rel(p, &info);
			break;

		//原寸
		case IMGSIZE_TYPE_ORIGINAL:
			_set_dstsize_original(&info);
			break;

		//固定倍率
		default:
			_set_fix_scale(&info, p->view.dfixscale);
			break;
	}

	//---- WorkData にセット

	//サイズセット

	p->view.size_src[0] = info.szsrc1;
	p->view.size_src[1] = info.szsrc2;
	p->view.size_dst[0] = info.szdst1;
	p->view.size_dst[1] = info.szdst2;

	//全体の表示サイズ

	_get_fullsize(&p->view.size_dstfull, &info, &info.szdst1, &info.szdst2);

	//描画位置

	_set_drawpos(p);

	//表示倍率

	if(imgtype == IMGSIZE_TYPE_FIX_SCALE)
		//固定倍率時
		p->view.dscale[0] = p->view.dscale[1] = p->view.dfixscale;
	else
		_get_curscale(p->view.dscale, &info);
}
