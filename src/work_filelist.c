/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * WorkData - ファイルリスト処理
 *****************************************/

#include <string.h>

#include <mlk.h>
#include <mlk_str.h>
#include <mlk_tree.h>
#include <mlk_string.h>

#include "def_work.h"
#include "def_config.h"

#include "fileoperator.h"
#include "work_main.h"


//=======================
// sub
//=======================


/* パス名からアイテムの親を作成して、戻り値で返す。
 * ppfname にファイル名部分をセット。
 *
 * アーカイブの場合、"dir/name" というパスになっているので、
 * ディレクリパスから親アイテムを作成する。
 * [!] ディレクトリ自体は列挙されない。
 *
 * pindex: 格納順でのインデックス番号
 * ppfname: パスを除いたファイル名部分の先頭位置が入る */

static mTreeItem *_get_item_parent(WorkData *p,const char *name,int *pindex,const char **ppfname)
{
	mTree *tree;
	mTreeItem *parent = NULL,*item;
	FileListItem *fi;
	const char *pc,*pcend;
	int len;

	tree = &p->filetree;

	for(pc = name; *pc; pc = pcend + 1)
	{
		//pc = ディレクトリパス部分
		
		pcend = strchr(pc, '/');
		if(!pcend) break;

		len = pcend - pc;

		//現在の親のアイテムの中に、指定名が存在するか

		item = (parent)? parent->first: tree->top;

		for(; item; item = item->next)
		{
			if(mStringCompare_null_len(((FileListItem *)item)->name, pc, len) == 0)
				break;
		}

		//

		if(item)
			//存在する場合、親移動
			parent = item;
		else
		{
			//存在しない場合、ディレクトリ作成
			
			parent = mTreeAppendNew(tree, parent, sizeof(FileListItem));
			if(!parent) return NULL;

			fi = (FileListItem *)parent;

			fi->name = mStrndup(pc, len);
			fi->item_type = FILELISTITEM_TYPE_DIRECTORY;
			fi->store_index = (*pindex)++;
		}
	}

	*ppfname = pc;

	return parent;
}

/* アイテム列挙のコールバック
 *
 * param = (int *) 格納順でのインデックス番号の変数ポインタ */

static void _callback_enum(FileOpInfo *info,void *param)
{
	WorkData *p = APPWORK;
	FileListItem *pi;
	mTreeItem *parent;
	const char *fname;
	int *pindex = (int *)param;

	//親を作成など

	parent = _get_item_parent(p, info->name, pindex, &fname);

	//ファイルアイテム作成

	pi = (FileListItem *)mTreeAppendNew(&p->filetree,
			parent, sizeof(FileListItem));

	if(pi)
	{
		pi->name = mStrdup(fname);
		pi->item_type = FILELISTITEM_TYPE_FILE;
		pi->time_modify = info->time_modify;
		pi->store_index = (*pindex)++;
		pi->filepos = info->filepos;
	}
}

/* ソート関数
 *
 * このソート順が、画像の開く順になる。 */

static int _sortfunc(mTreeItem *item1,mTreeItem *item2,void *param)
{
	FileListItem *p1,*p2;
	int n = 0;

	p1 = (FileListItem *)item1;
	p2 = (FileListItem *)item2;

	//ディレクトリはファイルより先に

	if(p1->item_type != p2->item_type)
		return (p1->item_type > p2->item_type)? -1: 1;

	//----- タイプが同じ

	switch(APPCONF->filesort_type)
	{
		//ファイル名 (自然)
		case FILESORT_TYPE_NAME_NATURAL:
			n = mStringCompare_number(p1->name, p2->name);
			break;
		//格納順
		case FILESORT_TYPE_STORING:
			n = (p1->store_index < p2->store_index)? -1: 1;
			break;
		//更新日時
		case FILESORT_TYPE_MODIFY:
			if(p1->time_modify < p2->time_modify)
				n = -1;
			else if(p1->time_modify > p2->time_modify)
				n = 1;
			break;
	}

	//名前 (他の比較で同じ値だった場合は名前順)

	if(n == 0)
		n = strcmp(p1->name, p2->name);

	//逆順

	if(APPCONF->fimgview & IMGVIEW_F_SORT_REVERSE)
		n = -n;
	
	return n;
}


//=======================
//
//=======================



/** 先頭のアイテムポインタ取得 */

FileListItem *workFileList_getTopItem(void)
{
	return (FileListItem *)APPWORK->filetree.top;
}

/** アイテムのフルパス名取得
 *
 * pi: NULL の場合あり */

void workFileList_getItemName_fullpath(mStr *str,FileListItem *pi)
{
	int f = 0;

	mStrEmpty(str);

	for(; pi; pi = (FileListItem *)pi->i.parent)
	{
		if(f) mStrPrependText(str, "/");
		
		mStrPrependText(str, pi->name);

		f = 1;
	}
}

/** ファイルリスト作成 */

mlkerr workFileList_create(void)
{
	WorkData *p = APPWORK;
	int store_index = 0;
	mlkerr ret;

	//FileOperator 作成

	switch(p->opentype)
	{
		case OPENTYPE_DIRECTORY:
			ret = FileOperator_openDir(&p->fileop, p->strOpenFile.buf);
			break;
		case OPENTYPE_ARCHIVE:
			ret = FileOperator_openZip(&p->fileop, p->strOpenFile.buf);
			break;
		default:
			ret = MLKERR_OK;
			break;
	}

	if(!p->fileop) return ret;

	//アイテム列挙
	// store_index = 格納順でのインデックス番号

	ret = (p->fileop->enum_entry)(p->fileop, _callback_enum, &store_index);

	//ソート

	workFileList_sort();

	return ret;
}

/** ファイルリストのソート */

void workFileList_sort(void)
{
	WorkData *p = APPWORK;
	FileListItem *pi;
	mTreeItem *pti;
	int no = 0;

	//ソート

	mTreeSortAll(&p->filetree, _sortfunc, NULL);

	//ソート後のインデックス番号セット

	for(pi = workFileList_getTopItem(); pi; )
	{
		if(pi->item_type == FILELISTITEM_TYPE_FILE)
			pi->sorted_index = no++;
		else
			pi->sorted_index = -1;

		pi = (FileListItem *)mTreeItemGetNext((mTreeItem *)pi);
	}

	//画像ファイル数

	p->imgfile_num = no;

	//先頭のファイルアイテム取得

	for(pti = p->filetree.top;
		pti && ((FileListItem *)pti)->item_type != FILELISTITEM_TYPE_FILE;
		pti = mTreeItemGetNext(pti));

	p->item_top = (FileListItem *)pti;

	//終端のファイルアイテム取得

	for(pti = mTreeGetBottomLastItem(&p->filetree);
		pti && ((FileListItem *)pti)->item_type != FILELISTITEM_TYPE_FILE;
		pti = mTreeItemGetPrev(pti));

	p->item_last = (FileListItem *)pti;
}

/** ファイルを抽出 */

mlkbool workFileList_extractFile(const char *filename,FileListItem *pi)
{
	FileOpHandle fh;

	fh.name = pi->name;
	fh.filepos = pi->filepos;

	return (APPWORK->fileop->extract)(APPWORK->fileop, &fh, filename);
}

