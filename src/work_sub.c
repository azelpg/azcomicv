/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * WorkData - サブ関数
 *****************************************/

#include <mlk.h>
#include <mlk_list.h>
#include <mlk_tree.h>
#include <mlk_string.h>

#include "def_work.h"
#include "def_config.h"

#include "image.h"
#include "fileoperator.h"

#include "work_sub.h"



//==========================
// ファイルアイテム移動
//==========================


/** (ページ位置移動用) 次の画像アイテムへ
 *
 * pi: NULL で先頭アイテム
 * nextdir: TRUE で、次がない場合は、次のディレクトリへ移動 */

FileListItem *workSub_getItem_nextImage(WorkData *p,FileListItem *pi,mlkbool nextdir)
{
	if(!pi)
		pi = p->item_top;
	else if(!nextdir)
	{
		//同じ階層の次の画像
	
		pi = (FileListItem *)pi->i.next;

		for(; pi && pi->item_type != FILELISTITEM_TYPE_FILE; pi = (FileListItem *)pi->i.next);
	}
	else
	{
		//ディレクトリをまたいで次へ

		pi = (FileListItem *)mTreeItemGetNext((mTreeItem *)pi);

		for(; pi && pi->item_type != FILELISTITEM_TYPE_FILE;
			pi = (FileListItem *)mTreeItemGetNext((mTreeItem *)pi));
	}

	return pi;
}

/** (ページ位置移動用) 前の画像アイテムへ
 *
 * pi: NULL で終端アイテム */

FileListItem *workSub_getItem_prevImage(WorkData *p,FileListItem *pi,mlkbool prevdir)
{
	if(!pi)
		//終端
		pi = p->item_last;
	else if(!prevdir)
	{
		//同じ階層の前へ

		pi = (FileListItem *)pi->i.prev;

		for(; pi && pi->item_type != FILELISTITEM_TYPE_FILE; pi = (FileListItem *)pi->i.prev);
	}
	else
	{
		//全体の前へ

		pi = (FileListItem *)mTreeItemGetPrev((mTreeItem *)pi);

		for(; pi && pi->item_type != FILELISTITEM_TYPE_FILE;
			pi = (FileListItem *)mTreeItemGetPrev((mTreeItem *)pi));
	}

	return pi;
}

/** 前に戻る時、pi を表示の後側として、先頭アイテムを取得
 *
 * pviewtype: 表示タイプが指定される場合は値が入る。 */

FileListItem *workSub_getItem_prevAdjust(WorkData *p,FileListItem *pi,int *pviewtype)
{
	FileListItem *pi2;
	uint32_t fimgv;
	int fsingle;

	fimgv = APPCONF->fimgview;

	workSub_setImageInfo(p, pi);

	if(!(fimgv & IMGVIEW_F_DOUBLE_PAGE))
	{
		//----- 単体

		//横長分割時は、2枚目にする

		if((fimgv & IMGVIEW_F_SINGLE_HORZ_HALF)
			&& FILELISTITEM_IS_HORIZONTAL(pi))
			*pviewtype = VIEWTYPE_SINGLE_SPLIT2;
	}
	else
	{
		//----- 見開き

		fsingle = fimgv & IMGVIEW_F_DOUBLE_HORZ_SINGLE;
	
		if(fsingle && FILELISTITEM_IS_HORIZONTAL(pi))
		{
			//横長単体

			*pviewtype = VIEWTYPE_SINGLE;
		}
		else
		{
			//---- 見開き
			
			//一つ前へ
			
			pi2 = workSub_getItem_prevImage(p, pi, FALSE);
			if(!pi2) return pi;

			workSub_setImageInfo(p, pi2);

			//

			if(fsingle && FILELISTITEM_IS_HORIZONTAL(pi2))
			{
				//横長単体が有効で、[pi2:横長] -> [pi:縦長] の場合、
				//pi を単体表示

				*pviewtype = VIEWTYPE_SINGLE;
			}
			else
			{
				//pi2 と pi で見開き
				//[!] pi2 が先頭画像で、"先頭は常に単体" が有効の場合は、
				//    pi2 のみ単体表示されてしまうので、設定を無効にする。

				pi = pi2;
				*pviewtype = VIEWTYPE_AUTO_NO_FIRST_PAGE;
			}
		}
	}

	return pi;
}


//==========================
// アイテム判定
//==========================


/** アイテムが先頭の画像か
 *
 * アーカイブの場合、ディレクトリ内の先頭画像を先頭とする。 */

mlkbool workSub_isFirstImageItem(FileListItem *pi)
{
	//前に画像のアイテムがない場合、先頭

	pi = (FileListItem *)pi->i.prev;
	
	for(; pi; pi = (FileListItem *)pi->i.prev)
	{
		if(pi->item_type == FILELISTITEM_TYPE_FILE)
			return FALSE;
	}

	return TRUE;
}

/** 見開きタイプ時、pi と pinext の2枚で見開き表示が可能な状態か */

mlkbool workSub_isEnableDoublePage(WorkData *p,FileListItem *pi,FileListItem *pinext)
{
	//横長単体有効時は、両方が横長でなければ見開き可能

	if(APPCONF->fimgview & IMGVIEW_F_DOUBLE_HORZ_SINGLE)
	{
		workSub_setImageInfo(p, pi);
		workSub_setImageInfo(p, pinext);

		return (!FILELISTITEM_IS_HORIZONTAL(pi)
			&& !FILELISTITEM_IS_HORIZONTAL(pinext));
	}

	//通常見開き
	return TRUE;
}


//==========================
// アイテム検索
//==========================


/** 画像インデックス番号からアイテム検索 */

FileListItem *workSub_getItem_fromSortedIndex(WorkData *p,int no)
{
	FileListItem *pi;

	for(pi = (FileListItem *)p->filetree.top; pi; )
	{
		if(pi->sorted_index == no) break;

		pi = (FileListItem *)mTreeItemGetNext((mTreeItem *)pi);
	}

	return pi;
}

/** 画像パス名からアイテム検索
 *
 * name: ディレクトリがある場合は、"path/" で指定 */

FileListItem *workSub_getItem_fromName(WorkData *p,const char *name)
{
	FileListItem *pi;
	const char *pc,*pcend;
	int len;

	pc = name;
	pi = (FileListItem *)p->filetree.top;

	while(*pc)
	{
		pcend = mStringFindChar(pc, '/');
		len = pcend - pc;
		
		for(; pi; pi = (FileListItem *)pi->i.next)
		{
			if(mStringCompare_null_len(pi->name, pc, len) == 0)
				break;
		}

		//見つからなかった

		if(!pi) break;

		//見つかった

		if(pi->item_type == FILELISTITEM_TYPE_FILE)
			return pi;

		//ディレクトリの場合、最初の子へ移動して、続きを検索

		if(!(*pcend)) break;

		pc = pcend + 1;

		pi = (FileListItem *)pi->i.first;
	}

	return NULL;
}


//==========================
// 画像
//==========================


/** キャッシュから画像取得 */

ImageBuf *workSub_getImageCache(WorkData *p,FileListItem *pi)
{
	ImageCache *item;

	MLK_LIST_FOR(p->list_imgcache, item, ImageCache)
	{
		if(item->item == pi)
		{
			//最近参照したものは終端へ
			mListMove(&p->list_imgcache, MLISTITEM(item), NULL);
			
			return item->img;
		}
	}

	return NULL;
}

/** アイテムの画像情報をセット
 *
 * return: 画像がない場合、FALSE */

mlkbool workSub_setImageInfo(WorkData *p,FileListItem *pi)
{
	ImageBuf *img;

	img = workSub_loadImageCache(p, pi);
	if(!img)
	{
		pi->width = pi->height = 0;
		return FALSE;
	}

	//セット

	pi->width = img->w;
	pi->height = img->h;

	return TRUE;
}

/** キャッシュに画像読み込み */

ImageBuf *workSub_loadImageCache(WorkData *p,FileListItem *pi)
{
	ImageBuf *img;
	ImageCache *pic;
	FileOpHandle fh;

	//キャッシュにあるか

	img = workSub_getImageCache(p, pi);
	if(img) return img;

	//読み込み

	fh.name = pi->name;
	fh.filepos = pi->filepos;

	img = (p->fileop->loadimage)(p->fileop, &fh);

	if(!img)
		img = ImageBuf_createErrImage();

	if(!img) return NULL;

	//古いキャッシュを削除

	if(p->list_imgcache.num >= IMAGECACHE_MAXNUM)
		mListDelete(&p->list_imgcache, p->list_imgcache.top);

	//追加

	pic = (ImageCache *)mListAppendNew(&p->list_imgcache, sizeof(ImageCache));

	pic->img = img;
	pic->item = pi;

	return img;
}

/** 表示用の拡大/縮小画像の作成 */

void workSub_createViewImage(WorkData *p,int no)
{
	ImageBuf *img;

	//原寸大の場合は NULL

	if(p->view.size_src[no].w != p->view.size_dst[no].w
		|| p->view.size_src[no].h != p->view.size_dst[no].h)
	{
		//拡大縮小あり
		// 拡大でニアレストネイバーの場合は、描画時に処理

		if(p->view.size_dst[no].w < p->view.size_src[no].w
			|| APPCONF->scaleup_method != SCALEUP_METHOD_NEAREST)
		{
			//分割時は、分割後の範囲のイメージをソースとする

			img = ImageBuf_resize(p->view.imgsrc[no],
				p->view.size_dst[no].w, p->view.size_dst[no].h,
				(APPCONF->scaledown_method == SCALEDOWN_METHOD_LANCZOS3)? 3: 2,
				p->view.size_src[no].w,
				p->view.splitno,
				p->weighttbl_up, p->weighttbl_down);

			//作成失敗時は何も表示させないようにするため、
			//(ImageBuf *)1 をセットしておく。

			p->view.imgdst[no] = (img)? img: (ImageBuf *)1;
		}
	}
}

