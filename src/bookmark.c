/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/************************************
 * しおりデータ
 ************************************/
/*
 * text: "<path>\t<filename>"
 *
 * データが更新したときのみ、ファイルに保存。
 */

#include <stdio.h>
#include <string.h>

#include <mlk_gui.h>
#include <mlk_str.h>
#include <mlk_list.h>
#include <mlk_iniread.h>
#include <mlk_iniwrite.h>

#include "def_appdata.h"
#include "def_macro.h"
#include "bookmark.h"


/** 追加 */

mlkbool BkmList_append(mStr *strpath,mStr *strfile)
{
	BkmItem *pi;
	mStr str = MSTR_INIT;

	if(APPDATA->list_bkm.num >= 500) return FALSE;

	mStrSetFormat(&str, "%t\t%t", strpath, strfile);

	pi = (BkmItem *)mListAppendNew(&APPDATA->list_bkm, sizeof(BkmItem) + str.len);
	if(pi)
	{
		memcpy(pi->text, str.buf, str.len);

		APPDATA->fbkm_update = TRUE;
	}

	mStrFree(&str);

	return TRUE;
}

/** 削除 */

void BkmList_delete(BkmItem *pi)
{
	mListDelete(&APPDATA->list_bkm, MLISTITEM(pi));

	APPDATA->fbkm_update = TRUE;
}

/** 上下に移動 */

void BkmList_moveUpDown(BkmItem *pi,mlkbool down)
{
	mListMoveUpDown(&APPDATA->list_bkm, MLISTITEM(pi), !down);

	APPDATA->fbkm_update = TRUE;
}

/** ファイルに保存 */

void BkmList_saveConfig(void)
{
	FILE *fp;
	BkmItem *pi;
	int no;

	//変更のあるときだけ保存

	if(!APPDATA->fbkm_update) return;

	//

	fp = mIniWrite_openFile_join(mGuiGetPath_config_text(), APP_BOOKMARK_FILENAME);
	if(!fp) return;

	mIniWrite_putGroup(fp, "bookmark");

	for(pi = (BkmItem *)APPDATA->list_bkm.top, no = 0; pi; pi = (BkmItem *)pi->i.next, no++)
		mIniWrite_putText_keyno(fp, no, pi->text);

	fclose(fp);
}

/** ファイルから読み込み */

void BkmList_loadConfig(void)
{
	mIniRead *ini;
	const char *key,*param;
	mList *list;
	BkmItem *pi;
	int len;

	list = &APPDATA->list_bkm;

	mIniRead_loadFile_join(&ini, mGuiGetPath_config_text(), APP_BOOKMARK_FILENAME);
	if(!ini) return;

	mIniRead_setGroup(ini, "bookmark");

	while(mIniRead_getNextItem(ini, &key, &param))
	{
		if(!param || !param[0]) continue;

		len = strlen(param);
	
		pi = (BkmItem *)mListAppendNew(list, sizeof(BkmItem) + len);
		if(pi)
			memcpy(pi->text, param, len);
	}

	mIniRead_end(ini);
}

