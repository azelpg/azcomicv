/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * ConfigData 関数
 *****************************************/

#include <stdio.h>

#include <mlk_gui.h>
#include <mlk_widget_def.h>
#include <mlk_window.h>
#include <mlk_panel.h>
#include <mlk_str.h>
#include <mlk_iniread.h>
#include <mlk_iniwrite.h>

#include "def_macro.h"
#include "def_appdata.h"
#include "def_config.h"
#include "def_optbutton.h"


//-------------------

//パネル名
static const char *g_panel_name[] = { "dirlist", "filelist", "loupe" };

//-------------------


//=============================
// main
//=============================


/** 解放 */

void ConfigData_free(void)
{
	ConfigData *p = APPCONF;
	int i;

	if(p)
	{
		mStrFree(&p->strFont_panel);
		mStrFree(&p->strArchiveCharcode);
		mStrFree(&p->strFilebrowserDir);
		mStrFree(&p->strFilelistExtractDir);

		for(i = 0; i < 3; i++)
			mStrArrayFree(p->strRecent[i], CONFIG_RECENTFILE_NUM);

		mFree(p->button_buf);

		mFree(p);
	}
}

/** ConfigData 作成 */

int ConfigData_new(void)
{
	ConfigData *p;

	p = (ConfigData *)mMalloc0(sizeof(ConfigData));
	if(!p) return 1;

	APPCONF = p;
	
	return 0;
}


//*********** 設定ファイル


//=============================
// 設定ファイル読み込み
//=============================


/* デフォルト値セット */

static void _set_load_default(ConfigData *p)
{
	int i;

	//色補正

	for(i = 0; i < CONFIG_COLOR_LEVEL_SLOT_NUM; i++)
		p->color_level[i] = (128 << 8) | 255;
}

/* ボタン操作のデフォルト値セット */

static void _set_button_default(ConfigData *p)
{
	uint16_t *pd;

	pd = p->button_buf = (uint16_t *)mMalloc(7 * 2);
	if(!pd) return;

	p->button_num = 7;

	pd[0] = OPTBUTTON_MAKE(MLK_BTT_LEFT, OPTBUTTON_CMD_NEXT_PAGE);
	pd[1] = OPTBUTTON_MAKE(MLK_BTT_RIGHT, OPTBUTTON_CMD_MENU);
	pd[2] = OPTBUTTON_MAKE(MLK_BTT_MIDDLE, OPTBUTTON_CMD_SCROLL);
	pd[3] = OPTBUTTON_MAKE(MLK_BTT_SCR_UP, OPTBUTTON_CMD_SCROLL_UP);
	pd[4] = OPTBUTTON_MAKE(MLK_BTT_SCR_DOWN, OPTBUTTON_CMD_SCROLL_DOWN);
	pd[5] = OPTBUTTON_MAKE(MLK_BTT_SCR_UP, OPTBUTTON_CMD_SCALE_UP) | OPTBUTTON_F_CTRL;
	pd[6] = OPTBUTTON_MAKE(MLK_BTT_SCR_DOWN, OPTBUTTON_CMD_SCALE_DOWN) | OPTBUTTON_F_CTRL;
}

/* ウィンドウ状態読み込み */

static void _load_winstate(mIniRead *ini,const char *key,mToplevelSaveState *state)
{
	int32_t n[7];

	if(mIniRead_getNumbers(ini, key, n, 7, 4, FALSE) != 7)
		mMemset0(state, sizeof(mToplevelSaveState));
	else
	{
		state->x = n[0];
		state->y = n[1];
		state->w = n[2];
		state->h = n[3];
		state->norm_x = n[4];
		state->norm_y = n[5];
		state->flags = n[6];
	}
}

/* ウィジェット状態読み込み */

static void _load_widgets_state(mIniRead *ini,ConfigData *cf,
	mToplevelSaveState *mainwin_state,mPanelState *panel_state)
{
	int i;
	int32_t val[2];
	uint16_t inith[] = { 200,200,150 };
	mToplevelSaveState winst;
	mStr str = MSTR_INIT;

	mIniRead_setGroup(ini, "widgets");

	//メインウィンドウ

	_load_winstate(ini, "mainwin", mainwin_state);

	cf->panelct_width = mIniRead_getInt(ini, "panelct_w", 200);

	_load_winstate(ini, "bkmwin", &APPDATA->bkmwin_state);

	cf->bkm_headerw[0] = mIniRead_getInt(ini, "bkm_header1", 100);
	cf->bkm_headerw[1] = mIniRead_getInt(ini, "bkm_header2", 100);

	//各パネル

	for(i = 0; i < PANEL_NUM; i++)
	{
		//mToplevelSaveState

		mStrSetFormat(&str, "%s_win", g_panel_name[i]);
		
		_load_winstate(ini, str.buf, &winst);

		if(!winst.w || !winst.h)
			winst.w = winst.h = 200;

		panel_state[i].winstate = winst;

		//高さ、フラグ

		mStrSetFormat(&str, "%s_st", g_panel_name[i]);
	
		if(mIniRead_getNumbers(ini, str.buf, val, 2, 4, FALSE) == 2)
		{
			panel_state[i].height = val[0];
			panel_state[i].flags = val[1];
		}
		else
		{
			//デフォルト

			panel_state[i].height = inith[i];
			panel_state[i].flags = (i == PANEL_LOUPE)? 0: MPANEL_F_CREATED;
		}

		//全画面の状態で終了した時は非表示状態になっているので、
		//VISIBLE は常に ON。
		
		panel_state[i].flags |= MPANEL_F_VISIBLE;
	}

	mStrFree(&str);
}

/* ConfigData 読み込み */

static void _load_configdata(mIniRead *ini,ConfigData *cf)
{
	uint32_t size;

	//----- 環境

	mIniRead_setGroup(ini, "env");

	//int

	cf->loupe_scale = mIniRead_getInt(ini, "loupe_scale", 100);
	cf->filesort_type = mIniRead_getInt(ini, "filesort_type", FILESORT_TYPE_NAME_NATURAL);
	cf->imgsize_type = mIniRead_getInt(ini, "imgsize_type", IMGSIZE_TYPE_FIT_VIEW);
	cf->doublepage_margin = mIniRead_getInt(ini, "double_margin", 0);
	cf->scaleup_method = mIniRead_getInt(ini, "scaleup_method", SCALEUP_METHOD_NEAREST);
	cf->scaledown_method = mIniRead_getInt(ini, "scaledown_method", SCALEDOWN_METHOD_SPLINE16);
	cf->scalestep_up = mIniRead_getInt(ini, "scalestep_up", 1250);
	cf->scalestep_down = mIniRead_getInt(ini, "scalestep_down", 800);
	cf->fixscale_step_under = mIniRead_getInt(ini, "fixscale_step_under", 0);
	cf->fixscale_step_upper = mIniRead_getInt(ini, "fixscale_step_upper", 0);
	cf->color_level_sel = mIniRead_getInt(ini, "color_level_sel", 0);
	cf->iconsize = mIniRead_getInt(ini, "iconsize", 22);
	cf->cursor_erase_sec = mIniRead_getInt(ini, "cursor_erase_sec", 0);
	cf->pagemark_opt = mIniRead_getInt(ini, "pagemark_opt", 5);

	mIniRead_getSize(ini, "viewsize", &cf->size_view, 1600, 1600);
	mIniRead_getNumbers(ini, "color_level", cf->color_level, CONFIG_COLOR_LEVEL_SLOT_NUM, 4, TRUE);

	//hex

	cf->fui = mIniRead_getHex(ini, "fui",
		UI_FLAGS_MENUBAR | UI_FLAGS_TOOLBAR | UI_FLAGS_STATUSBAR | UI_FLAGS_PANEL);
		//[!] パネルのフラグは、パネルのステータスで

	cf->foption = mIniRead_getHex(ini, "foption",
		OPTION_F_LAST_TO_NEXT | OPTION_F_TOP_TO_PREV
		| OPTION_F_SYNC_FILEBROWSER | OPTION_F_SYNC_FILELIST
		| OPTION_F_KEY_REVERSE | OPTION_F_VSCROLL_MOVE_PAGE);

	cf->fimgview = mIniRead_getHex(ini, "fimgview",
		IMGVIEW_F_DOUBLE_HORZ_SINGLE | IMGVIEW_F_DOUBLE_TOP_IS_SINGLE | IMGVIEW_F_NO_SCALE_UP);

	cf->filebrowser_opt = mIniRead_getHex(ini, "filebrowser_opt", 0);

	cf->fpanel_restore = mIniRead_getHex(ini, "fpanel_restore", 0);
	cf->fuicustom = mIniRead_getHex(ini, "uicustom", UI_FLAGS_MENUBAR);
	cf->fuicustom_restore = mIniRead_getHex(ini, "uicustom_restore", 0);

	cf->col_bkgnd = mIniRead_getHex(ini, "bkgndcol", 0);
	cf->col_blendbkgnd = mIniRead_getHex(ini, "blendbkgndcol", 0xffffff);

	//string

	mIniRead_getTextStr(ini, "filebrowser_dir", &cf->strFilebrowserDir, NULL);
	mIniRead_getTextStr(ini, "filelist_extract_dir", &cf->strFilelistExtractDir, NULL);
	mIniRead_getTextStr(ini, "archive_charcode", &cf->strArchiveCharcode, NULL);
	mIniRead_getTextStr(ini, "font_panel", &cf->strFont_panel, NULL);

	//ボタン操作

	if(!mIniRead_groupIsHaveKey(ini, "button"))
		//デフォルト
		_set_button_default(cf);
	else
	{
		cf->button_buf = mIniRead_getNumbers_alloc(ini, "button", 2, TRUE, 0, &size);

		cf->button_num = size / 2;
	}

	//------ 履歴

	mIniRead_setGroup(ini, "recent_file");
	mIniRead_getTextStrArray(ini, 0, cf->strRecent[RECENTFILE_ARRAY_FILE], CONFIG_RECENTFILE_NUM);

	mIniRead_setGroup(ini, "recent_archive");
	mIniRead_getTextStrArray(ini, 0, cf->strRecent[RECENTFILE_ARRAY_ARCHIVE], CONFIG_RECENTFILE_NUM);

	mIniRead_setGroup(ini, "recent_dir");
	mIniRead_getTextStrArray(ini, 0, cf->strRecent[RECENTFILE_ARRAY_DIR], CONFIG_RECENTFILE_NUM);
}

/** 設定ファイル読み込み */

void ConfigLoadFile(mToplevelSaveState *mainwin_state,mPanelState *panel_state)
{
	mIniRead *ini;
	ConfigData *conf = APPCONF;

	mIniRead_loadFile_join(&ini, mGuiGetPath_config_text(), APP_CONFIG_FILENAME);
	if(!ini) return;

	_set_load_default(conf);

	//バージョン

	mIniRead_setGroup(ini, APP_NAME);

	if(mIniRead_getInt(ini, "ver", 0) != 2)
		mIniRead_setEmpty(ini);

	//ウィジェット状態

	_load_widgets_state(ini, conf, mainwin_state, panel_state);

	//ConfigData

	_load_configdata(ini, conf);

	//mlk

	mGuiReadIni_system(ini);

	mIniRead_end(ini);
}


//=============================
// 設定ファイル書き込み
//=============================


/* ウィンドウ状態書き込み */

static void _save_winstate(FILE *fp,const char *key,mToplevel *win,mToplevelSaveState *state)
{
	mToplevelSaveState st;
	int32_t n[7];

	if(state)
		st = *state;
	else
		mToplevelGetSaveState(win, &st);

	n[0] = st.x;
	n[1] = st.y;
	n[2] = st.w;
	n[3] = st.h;
	n[4] = st.norm_x;
	n[5] = st.norm_y;
	n[6] = st.flags;

	mIniWrite_putNumbers(fp, key, n, 7, 4, FALSE);
}

/* ウィジェット状態 */

static void _save_widgets_state(FILE *fp,ConfigData *cf)
{
	AppData *p = APPDATA;
	int i;
	int32_t val[2];
	mPanelState st;
	mStr str = MSTR_INIT;

	mIniWrite_putGroup(fp, "widgets");

	//メインウィンドウ

	_save_winstate(fp, "mainwin", MLK_TOPLEVEL(p->mainwin), NULL);

	mIniWrite_putInt(fp, "panelct_w", p->panelct->w);

	_save_winstate(fp, "bkmwin", NULL, &APPDATA->bkmwin_state);

	mIniWrite_putInt(fp, "bkm_header1", cf->bkm_headerw[0]);
	mIniWrite_putInt(fp, "bkm_header2", cf->bkm_headerw[1]);

	//各パネル

	for(i = 0; i < PANEL_NUM; i++)
	{
		if(!p->panel[i]) continue;

		mPanelGetState(p->panel[i], &st);

		//mToplevelSaveState

		mStrSetFormat(&str, "%s_win", g_panel_name[i]);

		_save_winstate(fp, str.buf, NULL, &st.winstate);

		//高さ、フラグ

		mStrSetFormat(&str, "%s_st", g_panel_name[i]);

		val[0] = st.height;
		val[1] = st.flags;

		mIniWrite_putNumbers(fp, str.buf, val, 2, 4, FALSE);
	}

	mStrFree(&str);
}

/* ConfigData */

static void _save_configdata(FILE *fp,ConfigData *cf)
{
	//----- env

	mIniWrite_putGroup(fp, "env");

	//int

	mIniWrite_putInt(fp, "loupe_scale", cf->loupe_scale);
	mIniWrite_putInt(fp, "filesort_type", cf->filesort_type);
	mIniWrite_putInt(fp, "imgsize_type", cf->imgsize_type);
	mIniWrite_putInt(fp, "double_margin", cf->doublepage_margin);
	mIniWrite_putInt(fp, "scaleup_method", cf->scaleup_method);
	mIniWrite_putInt(fp, "scaledown_method", cf->scaledown_method);
	mIniWrite_putInt(fp, "scalestep_up", cf->scalestep_up);
	mIniWrite_putInt(fp, "scalestep_down", cf->scalestep_down);
	mIniWrite_putInt(fp, "fixscale_step_under", cf->fixscale_step_under);
	mIniWrite_putInt(fp, "fixscale_step_upper", cf->fixscale_step_upper);
	mIniWrite_putInt(fp, "color_level_sel", cf->color_level_sel);
	mIniWrite_putInt(fp, "iconsize", cf->iconsize);
	mIniWrite_putInt(fp, "cursor_erase_sec", cf->cursor_erase_sec);
	mIniWrite_putInt(fp, "pagemark_opt", cf->pagemark_opt);

	mIniWrite_putSize(fp, "viewsize", &cf->size_view);
	mIniWrite_putNumbers(fp, "color_level", cf->color_level, CONFIG_COLOR_LEVEL_SLOT_NUM, 4, TRUE);

	//hex

	mIniWrite_putHex(fp, "foption", cf->foption);
	mIniWrite_putHex(fp, "fui", cf->fui);
	mIniWrite_putHex(fp, "fimgview", cf->fimgview);
	mIniWrite_putHex(fp, "filebrowser_opt", cf->filebrowser_opt);
	mIniWrite_putHex(fp, "fpanel_restore", cf->fpanel_restore);
	mIniWrite_putHex(fp, "uicustom", cf->fuicustom);
	mIniWrite_putHex(fp, "uicustom_restore", cf->fuicustom_restore);

	mIniWrite_putHex(fp, "bkgndcol", cf->col_bkgnd);
	mIniWrite_putHex(fp, "blendbkgndcol", cf->col_blendbkgnd);

	//string

	mIniWrite_putStr(fp, "filebrowser_dir", &cf->strFilebrowserDir);
	mIniWrite_putStr(fp, "filelist_extract_dir", &cf->strFilelistExtractDir);
	mIniWrite_putStr(fp, "archive_charcode", &cf->strArchiveCharcode);
	mIniWrite_putStr(fp, "font_panel", &cf->strFont_panel);

	//

	mIniWrite_putNumbers(fp, "button", cf->button_buf, cf->button_num, 2, TRUE);

	//----- 履歴

	mIniWrite_putGroup(fp, "recent_file");
	mIniWrite_putStrArray(fp, 0, cf->strRecent[RECENTFILE_ARRAY_FILE], CONFIG_RECENTFILE_NUM);

	mIniWrite_putGroup(fp, "recent_archive");
	mIniWrite_putStrArray(fp, 0, cf->strRecent[RECENTFILE_ARRAY_ARCHIVE], CONFIG_RECENTFILE_NUM);

	mIniWrite_putGroup(fp, "recent_dir");
	mIniWrite_putStrArray(fp, 0, cf->strRecent[RECENTFILE_ARRAY_DIR], CONFIG_RECENTFILE_NUM);
}

/** 設定ファイル書き込み */

void ConfigSaveFile(void)
{
	FILE *fp;

	fp = mIniWrite_openFile_join(mGuiGetPath_config_text(), APP_CONFIG_FILENAME);
	if(!fp) return;

	//バージョン

	mIniWrite_putGroup(fp, APP_NAME);

	mIniWrite_putInt(fp, "ver", 2);

	//ウィジェット状態

	_save_widgets_state(fp, APPCONF);

	//ConfigData

	_save_configdata(fp, APPCONF);

	//mlk

	mGuiWriteIni_system(fp);

	fclose(fp);
}
