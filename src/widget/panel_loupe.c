/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * パネル:ルーペ
 *****************************************/

#include <mlk_gui.h>
#include <mlk_widget_def.h>
#include <mlk_widget.h>
#include <mlk_window.h>
#include <mlk_panel.h>
#include <mlk_event.h>
#include <mlk_menu.h>

#include "def_appdata.h"
#include "def_config.h"
#include "def_work.h"

#include "panel.h"
#include "image.h"
#include "work_main.h"


//-------------------

typedef struct
{
	mWidget wg;

	int imgno;		//ソース画像番号 (0 or 1)
	mPoint pt_ct,	//中央のソース位置
		pt_canvas;	//現在のカーソル位置 (キャンバス上)
	mDoublePoint dpt_top;	//左上のソース位置
}_mainwg;

//-------------------


//========================
// sub
//========================


/* _mainwg を取得 */

static _mainwg *_get_mainwg(void)
{
	return (_mainwg *)mPanelGetContents(APPDATA->panel[PANEL_LOUPE]);
}

/* 表示情報をリセット */

static void _reset_viewinfo(_mainwg *p)
{
	p->imgno = 0;
	p->pt_ct.x = p->pt_ct.y = 0;
	p->dpt_top.x = p->dpt_top.y = 0;
}

/* 現在のカーソル位置から表示情報セット */

static void _set_viewinfo_curpos(_mainwg *p)
{
	p->imgno = workCalc_getLoupePos(&p->pt_ct, p->pt_canvas.x, p->pt_canvas.y);

	workCalc_getLoupeTopPos(&p->dpt_top, &p->pt_ct, p->imgno, p->wg.w, p->wg.h);
}

/* 右クリックメニュー */

static void _run_menu(mWidget *wg,int x,int y)
{
	mMenu *menu;
	mMenuItem *pi;
	int i,id;
	char m[3] = {'x', '1', 0};

	menu = mMenuNew();

	for(i = 0; i < 4; i++)
	{
		m[1] = '1' + i;
		mMenuAppendText_copy(menu, i + 1, m, -1);
	}

	mMenuSetItemCheck(menu, APPCONF->loupe_scale / 100, 1);

	//

	pi = mMenuPopup(menu, wg, x, y, NULL, MPOPUP_F_LEFT | MPOPUP_F_TOP, NULL);
	id = (pi)? mMenuItemGetID(pi): -1;

	mMenuDestroy(menu);

	//

	if(id != -1)
	{
		APPCONF->loupe_scale = id * 100;
		mWidgetRedraw(wg);
	}
}


//========================
//
//========================


/* 描画 */

static void _draw_handle(mWidget *wg,mPixbuf *pixbuf)
{
	_mainwg *p = (_mainwg *)wg;
	ImageBufDrawLoupeInfo info;

	info.areaw = wg->w;
	info.areah = wg->h;
	info.scale = APPCONF->loupe_scale;
	info.splitno = APPWORK->view.splitno;
	info.dsxleft = p->dpt_top.x;
	info.dsytop  = p->dpt_top.y;
	info.sizesrc = APPWORK->view.size_src[p->imgno];
	info.tblbuf = (APPCONF->color_level_sel == 0)? NULL: APPWORK->level_tbl;

	ImageBuf_drawLoupe(APPWORK->view.imgsrc[p->imgno], pixbuf, &info);
}

/* イベント */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_POINTER
		&& ev->pt.act == MEVENT_POINTER_ACT_PRESS
		&& ev->pt.btt == MLK_BTT_RIGHT)
	{
		//右クリックメニュー

		_run_menu(wg, ev->pt.x, ev->pt.y);
	}

	return 1;
}

/* サイズ変更時 */

static void _resize_handle(mWidget *wg)
{
	_mainwg *p = (_mainwg *)wg;

	//描画左上位置、再計算

	workCalc_getLoupeTopPos(&p->dpt_top, &p->pt_ct, p->imgno, wg->w, wg->h);
}

/* パネル内容作成 */

static mWidget *_create_handle(mPanel *panel,int id,mWidget *parent)
{
	_mainwg *p;

	p = (_mainwg *)mWidgetNew(parent, sizeof(_mainwg));

	p->wg.flayout = MLF_EXPAND_WH;
	p->wg.event = _event_handle;
	p->wg.draw = _draw_handle;
	p->wg.resize = _resize_handle;
	p->wg.notify_to = MWIDGET_NOTIFYTO_SELF;
	p->wg.fevent |= MWIDGET_EVENT_POINTER;

	return (mWidget *)p;
}


//========================
// main
//========================


/** 作成 */

void PanelLoupe_new(mPanelState *state)
{
	mPanel *p;

	p = Panel_create(PANEL_LOUPE, 0, 0, state, _create_handle);

	mPanelCreateWidget(p);
}

/** ファイル閉じた時 */

void PanelLoupe_closed(void)
{
	_mainwg *p = _get_mainwg();

	if(p)
	{
		_reset_viewinfo(p);

		mWidgetRedraw(MLK_WIDGET(p));
	}
}

/** 表示画像が変わった時
 *
 * 現在のカーソル位置から再計算  */

void PanelLoupe_changeImage(void)
{
	_mainwg *p = _get_mainwg();

	if(p)
	{
		_set_viewinfo_curpos(p);

		mWidgetRedraw(MLK_WIDGET(p));
	}
}

/** キャンバスのカーソル位置変更時 */

void PanelLoupe_changeCurPos(int x,int y)
{
	_mainwg *p = _get_mainwg();
	int no;
	mPoint pt;
	mDoublePoint dpt;

	if(!p) return;

	p->pt_canvas.x = x;
	p->pt_canvas.y = y;

	//pt = 画像位置
	
	no = workCalc_getLoupePos(&pt, x, y);

	if(p->imgno != no
		|| p->pt_ct.x != pt.x || p->pt_ct.y != pt.y)
	{
		p->imgno = no;
		p->pt_ct = pt;

		//左上位置
		// :描画時、左上は最小 (0,0)、右下は最大 (srcw,srch) となるように表示される。
		// :端の部分をカーソルが移動した時に、中央位置だけで再描画の判定を行うと、
		// :表示上では端の位置のままなのに無駄に更新が繰り替えされることになるので、
		// :実際の描画時の左上位置で判定する。

		workCalc_getLoupeTopPos(&dpt, &pt, no, p->wg.w, p->wg.h);

		if(dpt.x != p->dpt_top.x || dpt.y != p->dpt_top.y)
		{
			p->dpt_top = dpt;
			
			mWidgetRedraw(MLK_WIDGET(p));
		}
	}
}
