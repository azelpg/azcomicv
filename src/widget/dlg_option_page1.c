/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * 環境設定のページ1
 *****************************************/

#include <mlk_gui.h>
#include <mlk_widget_def.h>
#include <mlk_widget.h>
#include <mlk_pager.h>
#include <mlk_label.h>
#include <mlk_lineedit.h>
#include <mlk_checkbutton.h>
#include <mlk_groupbox.h>
#include <mlk_conflistview.h>
#include <mlk_str.h>

#include "def_config.h"
#include "def_appdata.h"
#include "trid.h"

#include "pv_envdlg.h"


//-----------------

//ビュー

typedef struct
{
	mConfListView *clv;
}_pagedata_view;

//フラグ

#define FLAGS_CHECK_NUM 7

typedef struct
{
	mCheckButton *ck[FLAGS_CHECK_NUM];
}_pagedata_flags;

//表示倍率

typedef struct
{
	mLineEdit *le[4];
}_pagedata_scale;

//文字エンコード

typedef struct
{
	mLineEdit *le;
}_pagedata_charset;

//UIカスタム表示

#define UICUSTOM_MAIN_NUM  5

typedef struct
{
	mCheckButton *ck[UICUSTOM_MAIN_NUM + PANEL_NUM];
}_pagedata_uicustom;

//ほか

typedef struct
{
	mConfListView *clv;
}_pagedata_other;

//-----------------

enum
{
	TRID_VIEW_BKGND_VIEW = 1000,
	TRID_VIEW_BKGND_IMAGE,
	TRID_VIEW_DOUBLE_MARGIN,
	TRID_VIEW_SCALEDOWN_METHOD,
	TRID_VIEW_SCALEUP_METHOD,
	TRID_VIEW_CURSOR_ERASE,
	TRID_VIEW_MARK_SHOW,
	TRID_VIEW_MARK_POS,
	TRID_VIEW_MARKPOS_SELECT,

	TRID_FLAGS_TOP = 1100,

	TRID_SCALE_TITLE_MUL = 1200,
	TRID_SCALE_DOWN,
	TRID_SCALE_UP,
	TRID_SCALE_TITLE_FIX,
	TRID_SCALE_UNDER_100,
	TRID_SCALE_UPPER_100,
	TRID_SCALE_FIX_HELP,

	TRID_CHARSET_HELP = 1300,

	TRID_UICUSTOM_TOP = 1400,

	TRID_OTHER_INFO = 1500,
	TRID_OTHER_PANEL_FONT_USE,
	TRID_OTHER_PANEL_FONT,
	TRID_OTHER_TOOLBAR_SIZE,
};

//-----------------



//**********************************
// ビュー
//**********************************


/* 値取得関数 */

static int _view_getvalue(int type,intptr_t id,mConfListViewValue *val,void *param)
{
	EnvOptData *pd = (EnvOptData *)param;

	switch(id)
	{
		case TRID_VIEW_BKGND_VIEW:
			pd->col_bkgnd = val->color;
			break;
		case TRID_VIEW_BKGND_IMAGE:
			pd->col_blendbkgnd = val->color;
			break;
		case TRID_VIEW_DOUBLE_MARGIN:
			pd->doublepage_margin = val->num;
			break;
		case TRID_VIEW_SCALEDOWN_METHOD:
			pd->scaledown_method = val->select;
			break;
		case TRID_VIEW_SCALEUP_METHOD:
			pd->scaleup_method = val->select;
			break;
		case TRID_VIEW_CURSOR_ERASE:
			pd->cursor_erase_sec = val->num;
			break;
		case TRID_VIEW_MARK_SHOW:
			if(val->checked)
				pd->pagemark_opt |= PAGEMARK_F_SHOW;
			else
				pd->pagemark_opt &= ~PAGEMARK_F_SHOW;
			break;
		case TRID_VIEW_MARK_POS:
			pd->pagemark_opt &= ~3;
			pd->pagemark_opt |= val->select;
			break;
	}

	return 0;
}

/* データ取得 */

static mlkbool _view_getdata(mPager *p,void *pagedat,void *dst)
{
	_pagedata_view *pd = (_pagedata_view *)pagedat;

	mConfListView_getValues(pd->clv, _view_getvalue, dst);

	return TRUE;
}

/** "ビュー" ページ作成 */

mlkbool EnvDlg_createPage_view(mPager *p,mPagerInfo *info)
{
	_pagedata_view *pd;
	mConfListView *clv;
	EnvOptData *cf;

	pd = (_pagedata_view *)mMalloc0(sizeof(_pagedata_view));
	if(!pd) return FALSE;

	info->pagedat = pd;
	info->getdata = _view_getdata;

	//

	cf = mPagerGetDataPtr(p);

	pd->clv = clv = mConfListViewNew(MLK_WIDGET(p), 0, 0);

	mWidgetSetInitSize_fontHeightUnit(MLK_WIDGET(clv), 22, 0);

	mConfListView_addItem_color(clv, MLK_TR(TRID_VIEW_BKGND_VIEW), cf->col_bkgnd, TRID_VIEW_BKGND_VIEW);
	mConfListView_addItem_color(clv, MLK_TR(TRID_VIEW_BKGND_IMAGE), cf->col_blendbkgnd, TRID_VIEW_BKGND_IMAGE);
	mConfListView_addItem_num(clv, MLK_TR(TRID_VIEW_DOUBLE_MARGIN), cf->doublepage_margin, 0, 1000, 0, TRID_VIEW_DOUBLE_MARGIN);

	mConfListView_addItem_select(clv, MLK_TR(TRID_VIEW_SCALEDOWN_METHOD), cf->scaledown_method,
		"mitchell\tLagrange\tLanczos2\tLanczos3\tSpline16\tSpline36",
		FALSE, TRID_VIEW_SCALEDOWN_METHOD);

	mConfListView_addItem_select(clv, MLK_TR(TRID_VIEW_SCALEUP_METHOD), cf->scaleup_method,
		"Nearest neighbor\tbiculic", FALSE, TRID_VIEW_SCALEUP_METHOD);

	mConfListView_addItem_num(clv, MLK_TR(TRID_VIEW_CURSOR_ERASE), cf->cursor_erase_sec, 0, 100, 0, TRID_VIEW_CURSOR_ERASE);
	mConfListView_addItem_check(clv, MLK_TR(TRID_VIEW_MARK_SHOW), cf->pagemark_opt & PAGEMARK_F_SHOW, TRID_VIEW_MARK_SHOW);

	mConfListView_addItem_select(clv, MLK_TR(TRID_VIEW_MARK_POS),
		PAGEMARK_GET_POS(cf->pagemark_opt), MLK_TR(TRID_VIEW_MARKPOS_SELECT), FALSE, TRID_VIEW_MARK_POS);

	mListViewSetColumnWidth_auto(MLK_LISTVIEW(clv), 0);

	return TRUE;
}


//**********************************
// フラグ
//**********************************


/* データ取得 */

static mlkbool _flags_getdata(mPager *p,void *pagedat,void *dst)
{
	_pagedata_flags *pd = (_pagedata_flags *)pagedat;
	EnvOptData *dat = (EnvOptData *)dst;
	uint32_t f;
	int i;

	f = 0;

	for(i = 0; i < FLAGS_CHECK_NUM; i++)
		f |= mCheckButtonIsChecked(pd->ck[i]) << i;

	dat->foption = f;

	return TRUE;
}

/** "フラグ" ページ作成 */

mlkbool EnvDlg_createPage_flags(mPager *p,mPagerInfo *info)
{
	_pagedata_flags *pd;
	EnvOptData *dat;
	int i;
	uint32_t flags;

	pd = (_pagedata_flags *)mMalloc0(sizeof(_pagedata_flags));
	if(!pd) return FALSE;

	info->pagedat = pd;
	info->getdata = _flags_getdata;

	//

	dat = (EnvOptData *)mPagerGetDataPtr(p);
	flags = dat->foption;

	for(i = 0; i < FLAGS_CHECK_NUM; i++)
	{
		pd->ck[i] = mCheckButtonCreate(MLK_WIDGET(p), 0, 0, MLK_MAKE32_4(0,0,0,5), 0,
			MLK_TR(TRID_FLAGS_TOP + i), flags & (1 << i));
	}

	return TRUE;
}


//**********************************
// 表示倍率
//**********************************


/* データ取得 */

static mlkbool _scale_getdata(mPager *p,void *pagedat,void *dst)
{
	_pagedata_scale *pd = (_pagedata_scale *)pagedat;
	EnvOptData *dat = (EnvOptData *)dst;

	dat->scalestep_down = mLineEditGetNum(pd->le[0]);
	dat->scalestep_up = mLineEditGetNum(pd->le[1]);
	dat->fixscale_step_under = mLineEditGetNum(pd->le[2]);
	dat->fixscale_step_upper = mLineEditGetNum(pd->le[3]);

	return TRUE;
}

/** "表示倍率" ページ作成 */

mlkbool EnvDlg_createPage_scale(mPager *p,mPagerInfo *info)
{
	_pagedata_scale *pd;
	EnvOptData *dat;
	mWidget *gb,*ct;
	mLineEdit *le;
	int i;

	pd = (_pagedata_scale *)mMalloc0(sizeof(_pagedata_scale));
	if(!pd) return FALSE;

	info->pagedat = pd;
	info->getdata = _scale_getdata;

	dat = (EnvOptData *)mPagerGetDataPtr(p);

	//一段階

	gb = (mWidget *)mGroupBoxCreate(MLK_WIDGET(p), 0, MLK_MAKE32_4(0,0,0,15), 0, MLK_TR(TRID_SCALE_TITLE_MUL));

	mContainerSetType_grid(MLK_CONTAINER(gb), 2, 6, 7);

	for(i = 0; i < 2; i++)
	{
		mLabelCreate(gb, MLF_MIDDLE, 0, 0, MLK_TR(TRID_SCALE_DOWN + i));

		//
		
		le = pd->le[i] = mLineEditCreate(gb, 0, 0, 0, 0);

		mLineEditSetWidth_textlen(le, 7);

		if(i == 0)
			mLineEditSetNumStatus(le, 1, 999, 3);
		else
			mLineEditSetNumStatus(le, 1001, 10000, 3);

		mLineEditSetNum(le, (i == 0)? dat->scalestep_down: dat->scalestep_up);
	}

	//段階調整

	gb = (mWidget *)mGroupBoxCreate(MLK_WIDGET(p), 0, 0, 0, MLK_TR(TRID_SCALE_TITLE_FIX));

	ct = mContainerCreateGrid(gb, 2, 6, 7, 0, 0);

	for(i = 0; i < 2; i++)
	{
		mLabelCreate(ct, MLF_MIDDLE, 0, 0, MLK_TR(TRID_SCALE_UNDER_100 + i));
		
		le = pd->le[2 + i] = mLineEditCreate(ct, 0, 0, 0, MLINEEDIT_S_SPIN);
		
		mLineEditSetWidth_textlen(le, 6);
		mLineEditSetNumStatus(le, 0, 100, 0);
		mLineEditSetNum(le, (i == 0)? dat->fixscale_step_under: dat->fixscale_step_upper);
	}	

	mLabelCreate(gb, 0, MLK_MAKE32_4(0,10,0,0), MLABEL_S_BORDER, MLK_TR(TRID_SCALE_FIX_HELP));

	return TRUE;
}


//**********************************
// 文字エンコード
//**********************************


/* データ取得 */

static mlkbool _charset_getdata(mPager *p,void *pagedat,void *dst)
{
	_pagedata_charset *pd = (_pagedata_charset *)pagedat;
	EnvOptData *dat = (EnvOptData *)dst;

	mLineEditGetTextStr(pd->le, &dat->strArchiveCharcode);

	return TRUE;
}

/** "文字エンコード" ページ作成 */

mlkbool EnvDlg_createPage_charset(mPager *p,mPagerInfo *info)
{
	_pagedata_charset *pd;
	EnvOptData *dat;

	pd = (_pagedata_charset *)mMalloc0(sizeof(_pagedata_charset));
	if(!pd) return FALSE;

	info->pagedat = pd;
	info->getdata = _charset_getdata;

	dat = (EnvOptData *)mPagerGetDataPtr(p);

	//ラベル

	mLabelCreate(MLK_WIDGET(p), 0, MLK_MAKE32_4(0,0,0,15), 0, MLK_TR(TRID_CHARSET_HELP));

	//エディット

	pd->le = mLineEditCreate(MLK_WIDGET(p), 0, MLF_EXPAND_W, 0, 0);

	mLineEditSetText(pd->le, dat->strArchiveCharcode.buf);

	return TRUE;
}


//**********************************
// UIカスタム表示
//**********************************


/* データ取得 */

static mlkbool _uicustom_getdata(mPager *p,void *pagedat,void *dst)
{
	_pagedata_uicustom *pd = (_pagedata_uicustom *)pagedat;
	EnvOptData *dat = (EnvOptData *)dst;
	uint32_t f;
	int i;

	f = 0;

	for(i = 0; i < UICUSTOM_MAIN_NUM; i++)
	{
		if(mCheckButtonIsChecked(pd->ck[i]))
			f |= 1 << i;
	}

	for(i = 0; i < PANEL_NUM; i++)
	{
		if(mCheckButtonIsChecked(pd->ck[UICUSTOM_MAIN_NUM + i]))
			f |= 1 << (UI_FLAGS_PANEL_BIT_POS + i);
	}
	
	dat->fuicustom = f;

	return TRUE;
}

/** "UIカスタム表示" ページ作成 */

mlkbool EnvDlg_createPage_uicustom(mPager *p,mPagerInfo *info)
{
	_pagedata_uicustom *pd;
	EnvOptData *dat;
	int i;
	uint32_t flags;

	pd = (_pagedata_uicustom *)mMalloc0(sizeof(_pagedata_uicustom));
	if(!pd) return FALSE;

	info->pagedat = pd;
	info->getdata = _uicustom_getdata;

	//

	dat = (EnvOptData *)mPagerGetDataPtr(p);
	flags = dat->fuicustom;

	//各UI

	for(i = 0; i < UICUSTOM_MAIN_NUM; i++)
	{
		pd->ck[i] = mCheckButtonCreate(MLK_WIDGET(p), 0, 0, MLK_MAKE32_4(0,0,0,3), 0,
			MLK_TR(TRID_UICUSTOM_TOP + i), flags & (1 << i));
	}

	//パネル

	MLK_TRGROUP(TRGROUP_PANEL_NAME);

	for(i = 0; i < PANEL_NUM; i++)
	{
		pd->ck[UICUSTOM_MAIN_NUM + i] = mCheckButtonCreate(MLK_WIDGET(p), 0, 0,
			(i == 0)? MLK_MAKE32_4(0,12,0,3): MLK_MAKE32_4(0,0,0,3), 0,
			MLK_TR(i),
			flags & (1 << (UI_FLAGS_PANEL_BIT_POS + i)));
	}

	MLK_TRGROUP(TRGROUP_DLG_ENVOPT);
	
	return TRUE;
}


//**********************************
// ほか
//**********************************


static const uint8_t g_iconsize[] = {16,22,24,32,48,64,0};


/* サイズから選択インデックス取得 */

static int _other_get_size_to_index(int n)
{
	int i;

	for(i = 0; g_iconsize[i]; i++)
	{
		if(n == g_iconsize[i]) return i;
	}

	return 0;
}

/* 値取得関数 */

static int _other_getvalue(int type,intptr_t id,mConfListViewValue *val,void *param)
{
	EnvOptData *dat = (EnvOptData *)param;

	switch(id)
	{
		case TRID_OTHER_PANEL_FONT_USE:
			if(!val->checked)
				mStrEmpty(&dat->strFont_panel);
			else
				mStrSetText(&dat->strFont_panel, "1");
			break;
		case TRID_OTHER_PANEL_FONT:
			//先に上記のチェックが来ているので、
			//空文字列でない時のみセット
			
			if(mStrIsnotEmpty(&dat->strFont_panel))
				mStrSetText(&dat->strFont_panel, val->text);
			break;
		case TRID_OTHER_TOOLBAR_SIZE:
			dat->iconsize = g_iconsize[val->select];
			break;
	}

	return 0;
}

/* データ取得 */

static mlkbool _other_getdata(mPager *p,void *pagedat,void *dst)
{
	_pagedata_other *pd = (_pagedata_other *)pagedat;

	mConfListView_getValues(pd->clv, _other_getvalue, dst);

	return TRUE;
}

/** "ほか" ページ作成 */

mlkbool EnvDlg_createPage_other(mPager *p,mPagerInfo *info)
{
	_pagedata_other *pd;
	mConfListView *clv;
	EnvOptData *dat;

	pd = (_pagedata_other *)mMalloc0(sizeof(_pagedata_other));
	if(!pd) return FALSE;

	info->pagedat = pd;
	info->getdata = _other_getdata;

	dat = mPagerGetDataPtr(p);

	//リスト

	pd->clv = clv = mConfListViewNew(MLK_WIDGET(p), 0, 0);

	mConfListView_addItem_check(clv, MLK_TR(TRID_OTHER_PANEL_FONT_USE),
		mStrIsnotEmpty(&dat->strFont_panel), TRID_OTHER_PANEL_FONT_USE);

	mConfListView_addItem_font(clv, MLK_TR(TRID_OTHER_PANEL_FONT), dat->strFont_panel.buf, (uint32_t)-1, TRID_OTHER_PANEL_FONT);

	mConfListView_addItem_select(clv, MLK_TR(TRID_OTHER_TOOLBAR_SIZE), _other_get_size_to_index(dat->iconsize),
		"16\t22\t24\t32\t48\t64", FALSE, TRID_OTHER_TOOLBAR_SIZE);

	mListViewSetColumnWidth_auto(MLK_LISTVIEW(clv), 0);

	//ヘルプ

	mLabelCreate(MLK_WIDGET(p), 0, MLK_MAKE32_4(0,5,0,0), 0, MLK_TR(TRID_OTHER_INFO));

	return TRUE;
}

