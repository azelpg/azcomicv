/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * ステータスバー
 *****************************************/

#include <mlk_gui.h>
#include <mlk_widget_def.h>
#include <mlk_widget.h>
#include <mlk_label.h>
#include <mlk_str.h>

#include "def_appdata.h"
#include "def_config.h"
#include "def_work.h"


//-------------------

typedef struct
{
	MLK_CONTAINER_DEF

	mLabel *label;
}StatusBar;

//-------------------


/** ステータスバー作成 */

void StatusBar_new(mWidget *parent)
{
	StatusBar *p;

	p = (StatusBar *)mContainerNew(MLK_WIDGET(APPDATA->mainwin), sizeof(StatusBar));

	APPDATA->statusbar = (mWidget *)p;

	//

	p->wg.flayout = MLF_EXPAND_W;
	p->wg.draw = mWidgetDrawHandle_bkgnd;
	p->wg.draw_bkgnd = mWidgetDrawBkgndHandle_fillFace;

	mContainerSetPadding_pack4(MLK_CONTAINER(p), MLK_MAKE32_4(4,2,4,2));

	//ラベル

	p->label = mLabelCreate(MLK_WIDGET(p), MLF_MIDDLE | MLF_EXPAND_W, 0, MLABEL_S_COPYTEXT, NULL);

	//非表示

	if(!(APPCONF->fui & UI_FLAGS_STATUSBAR))
		mWidgetShow(MLK_WIDGET(p), 0);
}

/** 表示状態切り替え */

void StatusBar_toggleVisible(void)
{
	APPCONF->fui ^= UI_FLAGS_STATUSBAR;

	mWidgetShow(APPDATA->statusbar, -1);
	mWidgetReLayout(MLK_WIDGET(APPDATA->mainwin));
}

/** ページ情報セット */

void StatusBar_setInfo(void)
{
	StatusBar *p = (StatusBar *)APPDATA->statusbar;
	WorkData *work = APPWORK;
	FileListItem *pi,*pi2;
	mStr str = MSTR_INIT;
	int no;

	if(!work->item_cur)
	{
		//画像なし
		
		mLabelSetText(p->label, NULL);
	}
	else
	{
		if(!work->view.item_double)
		{
			//単体ページ

			pi = work->item_cur;
			
			mStrSetFormat(&str, "%d / %d  |  \"%s\" %dx%d (%.1F%%)",
				pi->sorted_index + 1, work->imgfile_num,
				pi->name, pi->width, pi->height,
				(int)(work->view.dscale[0] * 1000 + 0.5));
		}
		else
		{
			//見開き

			if(work->view.type == VIEWTYPE_DOUBLE_LR)
			{
				pi  = work->item_cur;
				pi2 = work->view.item_double;
				no  = 0;
			}
			else
			{
				pi2 = work->item_cur;
				pi  = work->view.item_double;
				no  = 1;
			}

			mStrSetFormat(&str,
				"[%d, %d] / %d  |  \"%s\" %dx%d (%.1F%%)  |  \"%s\" %dx%d (%.1F%%)",
				pi->sorted_index + 1, pi2->sorted_index + 1,
				work->imgfile_num,
				pi->name, pi->width, pi->height, (int)(work->view.dscale[no] * 1000 + 0.5),
				pi2->name, pi2->width, pi2->height, (int)(work->view.dscale[!no] * 1000 + 0.5));
		}

		//

		mLabelSetText(p->label, str.buf);

		mStrFree(&str);
	}
}

