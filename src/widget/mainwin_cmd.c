/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * MainWindow : コマンド (色々)
 *****************************************/

#include <stdlib.h>

#include <mlk_gui.h>
#include <mlk_widget_def.h>
#include <mlk_widget.h>
#include <mlk_window.h>
#include <mlk_panel.h>
#include <mlk_sysdlg.h>
#include <mlk_iconbar.h>
#include <mlk_str.h>

#include "def_macro.h"
#include "def_appdata.h"
#include "def_config.h"
#include "def_work.h"

#include "def_mainwin.h"
#include "mainwindow.h"
#include "mainwin_func.h"
#include "panel.h"
#include "image.h"
#include "envoptdlg.h"

#include "work_main.h"

#include "trid.h"
#include "trid_menu.h"


//----------------

void ColorLevelDlg_run(mWindow *parent,uint32_t *histbuf);
void createImageList_icon(void);

//----------------


//============================
// コマンド
//============================


/* 指定位置へ移動: 値取得 */

static int _gotopos_getno(MainWindow *p)
{
	mStr str = MSTR_INIT;
	int no;

	if(!APPWORK->item_cur) return -1;

	MLK_TRGROUP(TRGROUP_DIALOG);

	mStrSetFormat(&str, "%s (1 - %d)",
		MLK_TR(TRID_DLG_IMAGEPOS), APPWORK->imgfile_num);

	no = APPWORK->item_cur->sorted_index + 1;

	if(mSysDlg_inputTextNum(MLK_WINDOW(p),
		MLK_TR(TRID_DLG_GOTO_IMAGEPOS), str.buf,
		MSYSDLG_INPUTTEXT_F_SET_DEFAULT,
		1, APPWORK->imgfile_num, &no))
	{
		no--;
	}
	else
		no = -1;

	mStrFree(&str);

	return no;
}

/** 移動コマンド
 *
 * enable_revdir: ショートカットキーからの実行時など、右綴じ時に進行方向を逆にするか */

void MainWindow_command_move(MainWindow *p,int id,mlkbool enable_revdir)
{
	int no,revdir;

	//右綴じ時、進行方向を逆にする

	revdir = (enable_revdir
		&& (APPCONF->foption & OPTION_F_KEY_REVERSE)
		&& (APPCONF->fimgview & IMGVIEW_F_RIGHT_BINDING));

	//

	switch(id)
	{
		//次のページ
		case TRMENU_CTRL_NEXT_PAGE:
			no = (revdir)? MOVEPAGE_PREV_PAGE: MOVEPAGE_NEXT_PAGE;
			break;
		//前のページ
		case TRMENU_CTRL_PREV_PAGE:
			no = (revdir)? MOVEPAGE_NEXT_PAGE: MOVEPAGE_PREV_PAGE;
			break;
		//次の画像
		case TRMENU_CTRL_NEXT_IMAGE:
			no = (revdir)? MOVEPAGE_PREV_IMAGE: MOVEPAGE_NEXT_IMAGE;
			break;
		//前の画像
		case TRMENU_CTRL_PREV_IMAGE:
			no = (revdir)? MOVEPAGE_NEXT_IMAGE: MOVEPAGE_PREV_IMAGE;
			break;
		//先頭
		case TRMENU_CTRL_TOP_PAGE:
			no = MOVEPAGE_TOP;
			break;
		//終端
		case TRMENU_CTRL_END_PAGE:
			no = MOVEPAGE_END;
			break;

		//指定位置
		case TRMENU_CTRL_GOTO_POS:
			no = _gotopos_getno(p);
			if(no == -1) return;
			break;
	}

	//移動

	MainWindow_movePage(no);
}

/** ショートカットキー:特殊コマンド
 * 
 * メニューに項目はないが、ショートカットキーは設定できるもの */

void MainWindow_sckey_special(MainWindow *p,int id)
{
	switch(id)
	{
		//画像の上下左右スクロール
		case TRMENU_SCKEY_SCROLL_UP:
		case TRMENU_SCKEY_SCROLL_DOWN:
		case TRMENU_SCKEY_SCROLL_LEFT:
		case TRMENU_SCKEY_SCROLL_RIGHT:
			MainWinCanvas_moveScroll(id - TRMENU_SCKEY_SCROLL_UP);
			break;
	}
}


//============================
// UI 表示関連
//============================


/* 現在の UI の表示状態フラグ取得 */

static uint32_t _get_ui_visible_flags(MainWindow *p)
{
	uint32_t f;

	f = APPCONF->fui & UI_FLAGS_MASK_MAIN;

	//パネル (パネル自体が非表示ならすべて0)

	if(f & UI_FLAGS_PANEL)
	{
		if(Panel_isCreated(PANEL_FILEBROWSER))
			f |= UI_FLAGS_PANEL_FILEBROWSER;

		if(Panel_isCreated(PANEL_FILELIST))
			f |= UI_FLAGS_PANEL_FILELIST;

		if(Panel_isCreated(PANEL_LOUPE))
			f |= UI_FLAGS_PANEL_LOUPE;
	}

	return f;
}


/** 各 UI の表示状態を変更することが可能か
 *
 * [全画面] か [カスタム表示状態] の時は不可 */

mlkbool MainWindow_canUIChange(MainWindow *p)
{
	return (!p->is_fullscreen
		&& !(APPCONF->fui & UI_FLAGS_CUSTOM));
}

/** パネルの状態が変わった時、パネルコンテナを表示/非表示
 *
 * 格納するパネルが一つも無い場合は、非表示となる。
 *
 * relayout: パネルコンテナの表示状態が変わった時、再レイアウトを行うか
 * return: コンテナの表示を切り替えたか */

mlkbool MainWindow_onChangePanel(MainWindow *p,mlkbool relayout)
{
	int i,f = 0;

	//コンテナに格納するパネルがあるか

	for(i = 0; i < PANEL_NUM; i++)
	{
		if(mPanelIsStoreVisible(APPDATA->panel[i]))
		{
			f = 1;
			break;
		}
	}

	//状態が異なる場合、表示切り替え

	if(mWidgetIsVisible_self(p->splitter) == f)
		return FALSE;
	else
	{
		mWidgetShow(APPDATA->panelct, -1);
		mWidgetShow(p->splitter, -1);

		if(relayout)
			mWidgetReLayout(MLK_WIDGET(p));

		return TRUE;
	}
}

/** 全画面
 *
 * スクロールバーは現状維持する。
 * カスタム状態からの切り替えも有効。 */

void MainWindow_toggle_fullscreen(MainWindow *p)
{
	uint32_t f;

	p->is_fullscreen = !p->is_fullscreen;

	if(p->is_fullscreen)
		p->fullscreen_restore = _get_ui_visible_flags(p);

	f = p->fullscreen_restore;

	//全画面切り替え

	mToplevelFullscreen(MLK_TOPLEVEL(p), p->is_fullscreen);

	//各ウィジェット表示反転
	//[!] パネルは、ウィンドウの状態の時もある

	if(f & UI_FLAGS_MENUBAR)
		mWidgetShow(p->menubar, -1);

	if(f & UI_FLAGS_TOOLBAR)
		mWidgetShow(p->toolbar, -1);

	if(f & UI_FLAGS_STATUSBAR)
		mWidgetShow(APPDATA->statusbar, -1);

	if(f & UI_FLAGS_PANEL_FILEBROWSER)
		mPanelSetVisible(APPDATA->panel[PANEL_FILEBROWSER], -1);

	if(f & UI_FLAGS_PANEL_FILELIST)
		mPanelSetVisible(APPDATA->panel[PANEL_FILELIST], -1);

	if(f & UI_FLAGS_PANEL_LOUPE)
		mPanelSetVisible(APPDATA->panel[PANEL_LOUPE], -1);

	//

	MainWindow_onChangePanel(p, FALSE);

	//常に再レイアウト
	mWidgetReLayout(MLK_WIDGET(p));

	//ビューサイズが変化するため、更新
	MainWindow_updateViewSize_keepscale();
}

/** UI カスタムの表示状態 */

void MainWindow_toggle_custom(MainWindow *p)
{
	uint32_t f,fcur;

	if(p->is_fullscreen) return;

	APPCONF->fui ^= UI_FLAGS_CUSTOM;

	//f = 状態が変化するものをビットON

	fcur = _get_ui_visible_flags(p);

	if(APPCONF->fui & UI_FLAGS_CUSTOM)
	{
		//カスタム状態へ
		
		APPCONF->fuicustom_restore = fcur;

		f = APPCONF->fuicustom;

		if(!(f & UI_FLAGS_PANEL))
			f &= UI_FLAGS_MASK_MAIN;

		f = fcur ^ f;
	}
	else
		//戻す
		f = fcur ^ APPCONF->fuicustom_restore;

	//---- 表示を反転する

	if(f & UI_FLAGS_MENUBAR)
	{
		mWidgetShow(p->menubar, -1);
		APPCONF->fui ^= UI_FLAGS_MENUBAR;
	}

	if(f & UI_FLAGS_TOOLBAR)
	{
		mWidgetShow(p->toolbar, -1);
		APPCONF->fui ^= UI_FLAGS_TOOLBAR;
	}

	if(f & UI_FLAGS_STATUSBAR)
	{
		mWidgetShow(APPDATA->statusbar, -1);
		APPCONF->fui ^= UI_FLAGS_STATUSBAR;
	}

	if(f & UI_FLAGS_SCROLLBAR)
		MainWinCanvas_toggleScrollBar();

	//パネル

	if(f & UI_FLAGS_PANEL_FILEBROWSER)
		mPanelSetCreate(APPDATA->panel[PANEL_FILEBROWSER], -1);

	if(f & UI_FLAGS_PANEL_FILELIST)
		mPanelSetCreate(APPDATA->panel[PANEL_FILELIST], -1);

	if(f & UI_FLAGS_PANEL_LOUPE)
		mPanelSetCreate(APPDATA->panel[PANEL_LOUPE], -1);

	if(f & UI_FLAGS_PANEL)
		APPCONF->fui ^= UI_FLAGS_PANEL;

	//-----

	MainWindow_onChangePanel(p, FALSE);

	mWidgetReLayout(MLK_WIDGET(p));

	MainWindow_updateViewSize_keepscale();
}

/** パネル全体の表示/非表示
 * 
 * ウィジェット自体を作成/破棄。 */

void MainWindow_togglePanel(MainWindow *p)
{
	int i,f;

	if(!MainWindow_canUIChange(p)) return;

	APPCONF->fui ^= UI_FLAGS_PANEL;

	//

	if(APPCONF->fui & UI_FLAGS_PANEL)
	{
		//------ 復元

		f = APPCONF->fpanel_restore;

		for(i = 0; i < PANEL_NUM; i++)
		{
			if(f & (1 << i))
				mPanelSetCreate(APPDATA->panel[i], 1);
		}
	}
	else
	{
		//------ 非表示

		f = 0;
		
		for(i = 0; i < PANEL_NUM; i++)
		{
			if(Panel_isCreated(i))
				f |= 1 << i;

			mPanelSetCreate(APPDATA->panel[i], 0);
		}

		//復元時のフラグ

		APPCONF->fpanel_restore = f;
	}

	//

	MainWindow_onChangePanel(p, TRUE);
}


//==========================
// ダイアログ
//==========================


/** ビュー固定サイズの指定 */

void MainWindow_dlg_viewfixsize(MainWindow *p)
{
	mStr str = MSTR_INIT;
	int i,n,size[2];
	char *pc;

	MLK_TRGROUP(TRGROUP_DIALOG);

	mStrSetFormat(&str, "%dx%d",
		APPCONF->size_view.w, APPCONF->size_view.h);

	if(mSysDlg_inputText(MLK_WINDOW(p),
		MLK_TR(TRID_DLG_SETSIZE), MLK_TR(TRID_DLG_MES_VIEWFIXSIZE),
		MSYSDLG_INPUTTEXT_F_NOT_EMPTY | MSYSDLG_INPUTTEXT_F_SET_DEFAULT,
		&str))
	{
		//2つの値取得
	
		size[0] = atoi(str.buf);

		for(pc = str.buf; *pc && *pc >= '0' && *pc <= '9'; pc++);
		for(; *pc && (*pc < '0' || *pc > '9'); pc++);

		size[1] = atoi(pc);

		//調整

		for(i = 0; i < 2; i++)
		{
			n = size[i];
			
			if(n < 1)
				n = 1;
			else if(n > MAX_IMAGESIZE)
				n = MAX_IMAGESIZE;

			size[i] = n;
		}

		//セット

		APPCONF->size_view.w = size[0];
		APPCONF->size_view.h = size[1];
		
		workOpt_changeViewFixSize();
	}

	mStrFree(&str);
}

/** 表示倍率の指定 */

void MainWindow_dlg_scale(MainWindow *p)
{
	mStr str = MSTR_INIT;
	int itype = APPCONF->imgsize_type;
	double d;

	//原寸時は倍率変化なし

	if(itype == IMGSIZE_TYPE_ORIGINAL)
		return;

	//固定倍率: いつでも指定可能。
	//他(相対倍率時): 表示画像がある時。

	if(itype != IMGSIZE_TYPE_FIX_SCALE
		&& APPWORK->view.type == VIEWTYPE_NONE)
		return;

	//ダイアログ

	MLK_TRGROUP(TRGROUP_DIALOG);

	if(itype == IMGSIZE_TYPE_FIX_SCALE)
		d = APPWORK->view.dfixscale;
	else
		d = APPWORK->view.drelscale;

	mStrSetDouble(&str, d * 100, 2);

	if(mSysDlg_inputText(MLK_WINDOW(p),
		MLK_TR(TRID_DLG_SETSCALE), MLK_TR(TRID_DLG_MES_SETSCALE),
		MSYSDLG_INPUTTEXT_F_NOT_EMPTY | MSYSDLG_INPUTTEXT_F_SET_DEFAULT,
		&str))
	{
		workSet_scale(mStrToDouble(&str) / 100.0);
	}

	mStrFree(&str);
}

/** 色調整 */

void MainWindow_dlg_color(MainWindow *p)
{
	uint32_t *hist;

	if(!APPWORK->view.imgsrc[0]) return;

	//ヒストグラム取得

	hist = (uint32_t *)mMalloc0(256 * 4);
	if(!hist) return;

	ImageBuf_getHistogram(APPWORK->view.imgsrc[0], hist);

	//ダイアログ
	//[!] hist はダイアログ内で解放される

	ColorLevelDlg_run(MLK_WINDOW(p), hist);
}

/** 環境設定 */

void MainWindow_dlg_envopt(MainWindow *p)
{
	uint32_t f;

	f = EnvOptDlg_run(MLK_WINDOW(p));

	//縮小方法

	if(f & ENVOPT_F_SCALEDOWN_METHOD)
		workData_setScaleDownTable();

	//アイコンサイズ

	if(f & ENVOPT_F_ICONSIZE)
	{
		createImageList_icon();
	
		mIconBarSetImageList(MLK_ICONBAR(p->toolbar), APPDATA->imglist_iconbar);

		mWidgetReLayout(MLK_WIDGET(p));
	}

	//更新

	if(f & ENVOPT_F_UPDATE_FULL)
		MainWindow_updateFull();
	else if(f & ENVOPT_F_UPDATE_VIEW)
		MainWindow_updateViewSize_keepscale();
	else if(f & ENVOPT_F_UPDATE_REDRAW)
		mWidgetRedraw(APPDATA->canvas_page);
}

