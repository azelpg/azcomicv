/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * MainWindow : メインウィンドウ
 *****************************************/

#include <stdio.h>

#include <mlk_gui.h>
#include <mlk_widget_def.h>
#include <mlk_widget.h>
#include <mlk_window.h>
#include <mlk_menubar.h>
#include <mlk_splitter.h>
#include <mlk_iconbar.h>
#include <mlk_panel.h>
#include <mlk_sysdlg.h>
#include <mlk_accelerator.h>
#include <mlk_menu.h>
#include <mlk_event.h>
#include <mlk_key.h>
#include <mlk_str.h>
#include <mlk_iniread.h>

#include "def_macro.h"
#include "def_appdata.h"
#include "def_config.h"
#include "def_work.h"

#include "def_mainwin.h"
#include "mainwindow.h"
#include "mainwin_func.h"
#include "panel.h"
#include "panel_func.h"
#include "work_main.h"
#include "bookmark.h"

#include "trid.h"
#include "trid_menu.h"

#define ICON_DEF_COMMAND_ID
#include "def_icon.h"

#include "menudata.h"


//----------------------

#define _VERSION_TEXT  APP_NAME " ver 2.0.6\n\nCopyright (c) 2017-2023 Azel\n\nThis software is under the MIT License"

//----------------------

//ショートカットキーデフォルト

typedef struct
{
	uint32_t id,key;
}_sckeydat;

static const _sckeydat g_sckey_default[] = {
	{TRMENU_FILE_OPENFILE, MLK_ACCELKEY_CTRL | 'O'},
	{TRMENU_FILE_OPENDIR, MLK_ACCELKEY_CTRL | 'D'},
	{TRMENU_FILE_NEXT, MKEY_PAGE_DOWN},
	{TRMENU_FILE_PREV, MKEY_PAGE_UP},
	{TRMENU_FILE_RELOAD, MLK_ACCELKEY_CTRL | 'R'},
	{TRMENU_FILE_CLOSE, 'C'},
	{TRMENU_CTRL_NEXT_PAGE, MKEY_RIGHT},
	{TRMENU_CTRL_PREV_PAGE, MKEY_LEFT},
	{TRMENU_CTRL_TOP_PAGE, MKEY_HOME},
	{TRMENU_CTRL_END_PAGE, MKEY_END},
	{TRMENU_CTRL_GOTO_POS, 'G'},
	{TRMENU_CTRL_NEXT_IMAGE, 'X'},
	{TRMENU_CTRL_PREV_IMAGE, 'Z'},
	{TRMENU_VIEW_SCALE_UP, MKEY_KP_ADD},
	{TRMENU_VIEW_SCALE_DOWN, MKEY_KP_SUB},
	{TRMENU_UI_FULLSCREEN, MKEY_F11},
	{TRMENU_UI_MINIMIZE, 'N'},
	{0,0}
};

//ツールバーボタン定義

static const uint8_t g_toolbar_def[] = {
	ICON_OPENFILE, ICON_OPENDIR, ICON_OPEN_RECENT_FILE, 254,
	ICON_NEXTFILE, ICON_PREVFILE, 254,
	ICON_TOP_PAGE, ICON_PREV_PAGE, ICON_GOTO_PAGE, ICON_NEXT_PAGE, ICON_END_PAGE, 254,
	ICON_RIGHT_BINDING, ICON_DOUBLE_PAGE, 254,
	ICON_SCALE_UP, ICON_SCALE_DOWN, 254,
	ICON_FULLSCREEN,
	255
};

//----------------------

static int _event_handle(mWidget *wg,mEvent *ev);

BkmItem *BookmarkDlg_run(mWindow *parent);
mlkbool ShortcutKeyDlg_run(mWindow *parent);

//----------------------



//=========================
// sub
//=========================


/** (初期) ショートカットキーセット */

static void _set_shortcutkey(MainWindow *p)
{
	mAccelerator *accel = p->top.accelerator;
	mIniRead *ini;
	const _sckeydat *dat;
	int cmdid,ret;
	uint32_t key;

	ret = mIniRead_loadFile_join(&ini, mGuiGetPath_config_text(), APP_SCKEY_FILENAME);
	if(!ini) return;

	if(ret)
	{
		//ファイルが読み込めなかった場合は、デフォルト

		for(dat = g_sckey_default; dat->id; dat++)
		{
			mAcceleratorAdd(accel, dat->id, dat->key, NULL);

			mMenuSetItemShortcutKey(p->menu_top, dat->id, dat->key);
		}
	}
	else
	{
		if(mIniRead_setGroup(ini, "sckey"))
		{
			//次の項目の番号と数値を取得
		
			while(mIniRead_getNextItem_keyno_int32(ini, &cmdid, &key, TRUE))
			{
				mAcceleratorAdd(accel, cmdid, key, NULL);

				if(!(cmdid >= TRMENU_SCKEY_TOP_ID && cmdid <= TRMENU_SCKEY_LAST_ID))
					mMenuSetItemShortcutKey(p->menu_top, cmdid, key);
			}
		}
	}

	mIniRead_end(ini);
}


//=========================
// 作成 sub
//=========================


/* アクセラレータ作成 */

static void _create_accelerator(MainWindow *p)
{
	mAccelerator *accel;

	accel = mAcceleratorNew();

	mToplevelAttachAccelerator(MLK_TOPLEVEL(p), accel);

	mAcceleratorSetDefaultWidget(accel, MLK_WIDGET(p));
}

/* メニュー作成 */

static void _create_menu(MainWindow *p)
{
	mMenuBar *mbar;
	mMenu *menu;
	int i;

	mbar = mMenuBarNew(MLK_WIDGET(p), 0, MMENUBAR_S_BORDER_BOTTOM);

	p->menubar = (mWidget *)mbar;

	mToplevelAttachMenuBar(MLK_TOPLEVEL(p), mbar);

	//データからメニュー作成

	MLK_TRGROUP(TRGROUP_MAINMENU);

	mMenuBarCreateMenuTrArray16_radio(mbar, g_app_mainmenu_data);

	p->menu_top = mMenuBarGetMenu(mbar);
	p->menu_view = mMenuGetItemSubmenu(p->menu_top, TRMENU_TOP_VIEW);
	p->menu_ui = mMenuGetItemSubmenu(p->menu_top, TRMENU_TOP_UI);

	//履歴 x 3

	for(i = 0; i < 3; i++)
	{
		p->menu_recent[i] = menu = mMenuNew();

		MainWindow_setRecentMenu(p, i);

		mMenuBarSetSubmenu(mbar, TRMENU_FILE_RECENT_FILE + i, menu);
	}

	//非表示

	if(!(APPCONF->fui & UI_FLAGS_MENUBAR))
		mWidgetShow(MLK_WIDGET(mbar), 0);
}

/* ツールバー作成 */

static void _create_toolbar(MainWindow *p)
{
	mIconBar *ib;
	const uint8_t *buf;
	int n,id;
	uint32_t f;

	//作成
	
	ib = mIconBarNew(MLK_WIDGET(p), 0, MICONBAR_S_TOOLTIP);

	p->toolbar = (mWidget *)ib;

	ib->wg.flayout = MLF_EXPAND_W;

	mIconBarSetPadding(ib, 3);
	mIconBarSetImageList(ib, APPDATA->imglist_iconbar);
	mIconBarSetTooltipTrGroup(ib, TRGROUP_TOOLBAR_TOOLTIP);

	//ボタン追加

	buf = g_toolbar_def;

	while(1)
	{
		//n = アイコン番号
		
		n = *(buf++);
		if(n == 255) break;

		if(n == 254)
			mIconBarAddSep(ib);
		else
		{
			if(n >= ICON_NUM) continue;

			id = g_icon_cmdid[n];
			
			f = 0;

			if(id & 0x8000)
				f |= MICONBAR_ITEMF_DROPDOWN;

			if(id & 0x4000)
				f |= MICONBAR_ITEMF_CHECKBUTTON;

			mIconBarAdd(ib, id & 0x3fff, n, n, f);
		}
	}

	//チェック

	mIconBarSetCheck(ib, TRMENU_VIEW_RIGHT_BINDING, APPCONF->fimgview & IMGVIEW_F_RIGHT_BINDING);
	mIconBarSetCheck(ib, TRMENU_VIEW_DOUBLE_PAGE, APPCONF->fimgview & IMGVIEW_F_DOUBLE_PAGE);

	//非表示

	if(!(APPCONF->fui & UI_FLAGS_TOOLBAR))
		mWidgetShow(MLK_WIDGET(ib), 0);
}

/* パネルコンテナ作成 */

static void _create_panelct(mWidget *parent)
{
	mWidget *wg;

	wg = mContainerCreateVert(parent, 0, MLF_FIX_W | MLF_EXPAND_H, 0);

	APPDATA->panelct = wg;

	wg->w = APPCONF->panelct_width;
	wg->draw = mWidgetDrawHandle_bkgnd;
	wg->draw_bkgnd = mWidgetDrawBkgndHandle_fillFace;

	//パネルの推奨サイズの高さが適用されないようにする。
	//これをしておかないと、ステータスバーが隠れてしまう。
	wg->hintRepH = 1;
}

/* メイン部分作成 */

static void _create_main(MainWindow *p)
{
	mWidget *ct;

	//水平コンテナ

	ct = mContainerCreateHorz(MLK_WIDGET(p), 0, MLF_EXPAND_WH, 0);

	//パネルコンテナ

	_create_panelct(ct);

	//スプリッター

	p->splitter = (mWidget *)mSplitterNew(ct, 0, MSPLITTER_S_VERT);

	p->splitter->draw_bkgnd = mWidgetDrawBkgndHandle_fillFace;

	//キャンバス

	MainWinCanvas_new(ct);
}


//=========================
// main
//=========================


/* キー処理 */

static int _keydown_handle(mWidget *wg,uint32_t key,uint32_t state)
{
	if(key == MKEY_ESCAPE)
	{
		//ESC : 全画面戻す

		if(MAINWINDOW(wg)->is_fullscreen)
		{
			mWidgetEventAdd_command(wg,
				MAINWIN_CMDID_FULLSCREEN_RESTORE, 0, MEVENT_COMMAND_FROM_UNKNOWN, NULL);
		}
		return 1;
	}

	return 0;
}

/* 解放処理 */

static void _destroy_handle(mWidget *wg)
{
	mToplevelDestroyAccelerator(MLK_TOPLEVEL(wg));
}

/** メインウィンドウ作成 */

void MainWindow_new(void)
{
	MainWindow *p;

	//ウィンドウ
	
	p = (MainWindow *)mToplevelNew(NULL, sizeof(MainWindow),
			MTOPLEVEL_S_NORMAL | MTOPLEVEL_S_NO_INPUT_METHOD);
	if(!p) return;
	
	APPDATA->mainwin = p;

	p->wg.destroy = _destroy_handle;
	p->wg.event = _event_handle;
	p->wg.foption |= MWIDGET_OPTION_NO_DRAW_BKGND;

	p->top.keydown = _keydown_handle;

	//アイコン

	mToplevelSetIconPNG_file(MLK_TOPLEVEL(p), "!/appicon.png");

	//アクセラレータ

	_create_accelerator(p);

	//メニュー

	_create_menu(p);

	//ツールバー

	_create_toolbar(p);

	//ショートカットキー

	_set_shortcutkey(p);

	//メイン部分

	_create_main(p);

	//ステータスバー

	StatusBar_new(MLK_WIDGET(p));
}

/** 初期表示 */

void MainWindow_show(MainWindow *p,mToplevelSaveState *state)
{
	//パネルの状態から、パネルコンテナを表示/非表示

	MainWindow_onChangePanel(p, FALSE);

	//タイトル

	MainWindow_setTitle(p);

	//状態復元

	if(!state->w || !state->h)
		state->w = 500, state->h = 400;

	state->flags &= ~MTOPLEVEL_SAVESTATE_F_FULLSCREEN;

	mToplevelSetSaveState(MLK_TOPLEVEL(p), state);

	//表示

	mWidgetShow(MLK_WIDGET(p), 1);
}

/** 終了 */

void MainWindow_quit(MainWindow *p)
{
	//開いているファイル位置を履歴にセット
	MainWindow_setRecentOnClose(p);

	mGuiQuit();
}

/** タイトルバーの文字列セット */

void MainWindow_setTitle(MainWindow *p)
{
	mStr *pstr = &APPWORK->strOpenFile;
	int type = APPWORK->opentype;

	if(type == OPENTYPE_EMPTY)
		//空
		mToplevelSetTitle(MLK_TOPLEVEL(p), APP_NAME);
	else
	{
		//"path - appname"
	
		mStr str = MSTR_INIT;

		if(type == OPENTYPE_DIRECTORY)
			mStrCopy(&str, pstr);
		else
			mStrPathGetBasename(&str, pstr->buf);

		mStrAppendText(&str, " - " APP_NAME);

		mToplevelSetTitle(MLK_TOPLEVEL(p), str.buf);

		mStrFree(&str);
	}
}

/** 開いているファイルを閉じる */

void MainWindow_closeFile(MainWindow *p)
{
	if(OPENTYPE_IS_EMPTY()) return;

	//履歴
	
	MainWindow_setRecentOnClose(p);
	MainWindow_setRecentMenu(p, RECENTFILE_ARRAY_FILE);

	//

	workFile_close();

	//更新

	MainWindow_setTitle(p);

	MainWinCanvas_setScrollInfo(TRUE);

	mWidgetRedraw(APPDATA->canvas_page);

	StatusBar_setInfo();

	PanelFileList_closed();
	PanelLoupe_closed();
}

/** メッセージダイアログ表示 */

void MainWindow_showMessage(int trid)
{
	mMessageBoxOK(MLK_WINDOW(APPDATA->mainwin),
		MLK_TR2(TRGROUP_MESSAGE, trid));
}

/** キャンバスのボタン操作でのメニュー表示 */

void MainWindow_runMainMenu_forButton(int x,int y)
{
	MainWindow *p = APPDATA->mainwin;

	mMenuPopup(p->menu_top, MLK_WIDGET(APPDATA->canvas_page), x, y, NULL,
		MPOPUP_F_LEFT | MPOPUP_F_TOP | MPOPUP_F_MENU_SEND_COMMAND,
		MLK_WIDGET(p));
}


//============================
// 更新
//============================


/** ページ移動 */

void MainWindow_movePage(int no)
{
	int ret;
	
	ret = workPage_movePage(no);

	switch(ret)
	{
		//移動した
		case MOVEPAGE_RET_OK:
			MainWindow_updatePage();
			break;
		//終端の状態で次ページの場合、次を開く
		case MOVEPAGE_RET_NEXT_TOP:
			if(APPCONF->foption & OPTION_F_LAST_TO_NEXT)
				MainWindow_openNextPrev(APPDATA->mainwin, TRUE, FALSE);
			break;
		//先頭の状態で前ページの場合、前を開く
		case MOVEPAGE_RET_PREV_LAST:
			if(APPCONF->foption & OPTION_F_TOP_TO_PREV)
				MainWindow_openNextPrev(APPDATA->mainwin, FALSE, TRUE);
			break;
	}
}

/** ページ更新時 */

void MainWindow_updatePage(void)
{
	mWidgetRedraw(APPDATA->canvas_page);

	MainWinCanvas_setScrollInfo(TRUE);
	PanelLoupe_changeImage();
	StatusBar_setInfo();
}

/** 開いた後の更新
 *
 * item_cur に、最初に表示するページが指定されている。
 * lastpage == TRUE の場合、終端位置へ移動。 */

void MainWindow_updateOpened(mlkbool lastpage)
{
	PanelFileList_opened();

	if(lastpage)
		workPage_movePage(MOVEPAGE_END);
	else
		workPage_setFull(VIEWTYPE_AUTO);

	MainWindow_updatePage();
}

/** ページを再セットして更新 */

void MainWindow_updateFull(void)
{
	workPage_setFull(VIEWTYPE_AUTO);
	MainWindow_updatePage();
}

/** 表示画像の変更はなく、表示サイズのみ更新 (表示倍率維持) */

void MainWindow_updateViewSize_keepscale(void)
{
	workPage_setViewSize(FALSE);
	MainWindow_updatePage();
}

/** 表示サイズのみ更新 (相対表示倍率リセット) */

void MainWindow_updateViewSize_resetscale(void)
{
	workPage_setViewSize(TRUE);
	MainWindow_updatePage();
}


//============================
// コマンド
//============================


/* ビュー表示タイプ一括変換 */

static void _view_set_batch(MainWindow *p,int no)
{
	uint32_t f;
	int no2;

	no2 = no & 3;

	f = APPCONF->fimgview & ~(IMGVIEW_F_DOUBLE_PAGE | IMGVIEW_F_RIGHT_BINDING);

	//綴じ方向

	if(no >= 4)
		f |= IMGVIEW_F_RIGHT_BINDING;

	//見開き

	if(no2 >= 2)
		f |= IMGVIEW_F_DOUBLE_PAGE;

	//横長

	switch(no2)
	{
		case 0:
			f &= ~IMGVIEW_F_SINGLE_HORZ_HALF;
			break;
		case 1:
			f |= IMGVIEW_F_SINGLE_HORZ_HALF;
			break;
		case 2:
			f &= ~IMGVIEW_F_DOUBLE_HORZ_SINGLE;
			break;
		case 3:
			f |= IMGVIEW_F_DOUBLE_HORZ_SINGLE;
			break;
	}

	APPCONF->fimgview = f;

	//ツールバーチェック

	mIconBarSetCheck(MLK_ICONBAR(p->toolbar),
		TRMENU_VIEW_RIGHT_BINDING, f & IMGVIEW_F_RIGHT_BINDING);

	mIconBarSetCheck(MLK_ICONBAR(p->toolbar),
		TRMENU_VIEW_DOUBLE_PAGE, f & IMGVIEW_F_DOUBLE_PAGE);

	//

	MainWindow_updateFull();
}

/* 各パネルの表示状態を反転 */

static void _cmd_panel_toggle_visible(MainWindow *p,int no)
{
	if((APPCONF->fui & UI_FLAGS_PANEL)
		&& MainWindow_canUIChange(p))
	{
		mPanelSetCreate(APPDATA->panel[no], -1);

		MainWindow_onChangePanel(p, TRUE);
	}
}

/* しおり追加 */

static void _cmd_bookmark_add(MainWindow *p)
{
	mStr str = MSTR_INIT, str2 = MSTR_INIT;

	if(!APPWORK->item_cur) return;

	workFileList_getItemName_fullpath(&str, APPWORK->item_cur);

	if(BkmList_append(&APPWORK->strOpenFile, &str))
	{
		//メッセージ
		
		mStrSetFormat(&str2, "%s\n\n%t\n(%t)",
			MLK_TR2(TRGROUP_MESSAGE, TRID_MES_BOOKMARK_APPEND),
			&APPWORK->strOpenFile, &str);

		mMessageBoxOK(MLK_WINDOW(p), str2.buf);
	}

	mStrFree(&str);
	mStrFree(&str2);
}

/* しおりリスト */

static void _cmd_bookmark_list(MainWindow *p)
{
	BkmItem *pi;

	pi = BookmarkDlg_run(MLK_WINDOW(p));
	if(pi)
		MainWindow_open(pi->text, MAINWIN_OPEN_PATH_AND_FILE);
}


//============================
// ハンドラ
//============================


/* コマンドイベント */

static void _event_command(MainWindow *p,mEventCommand *ev)
{
	int id = ev->id;

	//移動関連

	if(id >= TRMENU_CTRL_TOP_ID && id <= TRMENU_CTRL_END_ID)
	{
		MainWindow_command_move(p, id, (ev->from == MEVENT_COMMAND_FROM_ACCELERATOR));
		return;
	}

	//画像表示サイズタイプ

	if(id >= TRMENU_VIEW_IMGSIZE_FIT_VIEW && id <= TRMENU_VIEW_IMGSIZE_FIX_SCALE)
	{
		workOpt_changeViewImageType(id - TRMENU_VIEW_IMGSIZE_FIT_VIEW);
		return;
	}

	//ファイル履歴

	if(id >= MAINWIN_CMDID_RECENTFILE_TOP
		&& id < MAINWIN_CMDID_RECENTFILE_TOP + CONFIG_RECENTFILE_NUM * 3)
	{
		id -= MAINWIN_CMDID_RECENTFILE_TOP;
		
		MainWindow_openRecent(p, id / CONFIG_RECENTFILE_NUM,
			id % CONFIG_RECENTFILE_NUM);
		return;
	}

	//ショートカットキー:特殊

	if(id >= TRMENU_SCKEY_TOP_ID && id <= TRMENU_SCKEY_LAST_ID)
	{
		MainWindow_sckey_special(p, id);
		return;
	}

	//------------

	switch(id)
	{
		//---- ファイル

		//ファイル開く
		case TRMENU_FILE_OPENFILE:
			if(ev->from == MEVENT_COMMAND_FROM_ICONBAR_DROP)
				MainWindow_openFromDropdown(p, FALSE);
			else
				MainWindow_openDialog(p, FALSE, 0);
			break;
		//ディレクトリ開く
		case TRMENU_FILE_OPENDIR:
			if(ev->from == MEVENT_COMMAND_FROM_ICONBAR_DROP)
				MainWindow_openFromDropdown(p, TRUE);
			else
				MainWindow_openDialog(p, TRUE, 0);
			break;
		//次へ
		case TRMENU_FILE_NEXT:
			MainWindow_openNextPrev(p, TRUE, FALSE);
			break;
		//前へ
		case TRMENU_FILE_PREV:
			MainWindow_openNextPrev(p, FALSE, FALSE);
			break;
		//再読込
		case TRMENU_FILE_RELOAD:
			MainWindow_open(NULL, MAINWIN_OPEN_RELOAD);
			break;
		//閉じる
		case TRMENU_FILE_CLOSE:
			MainWindow_closeFile(p);
			break;
		//終了
		case TRMENU_FILE_EXIT:
			MainWindow_quit(p);
			break;

		//最近使ったファイル (ツールバーのボタン)
		case TRMENU_FILE_RECENT_FILE:
			MainWindow_runMenu_recentFile(p);
			break;

		//履歴消去
		case MAINWIN_CMDID_RECENT_FILE_CLEAR:
		case MAINWIN_CMDID_RECENT_ARCHIVE_CLEAR:
		case MAINWIN_CMDID_RECENT_DIR_CLEAR:
			id -= MAINWIN_CMDID_RECENT_FILE_CLEAR;
			
			mStrArrayFree(APPCONF->strRecent[id], CONFIG_RECENTFILE_NUM);
			MainWindow_setRecentMenu(p, id);
			break;

		//---- 表示

		//右綴じ
		case TRMENU_VIEW_RIGHT_BINDING:
			APPCONF->fimgview ^= IMGVIEW_F_RIGHT_BINDING;

			mIconBarSetCheck(MLK_ICONBAR(p->toolbar),
				TRMENU_VIEW_RIGHT_BINDING, APPCONF->fimgview & IMGVIEW_F_RIGHT_BINDING);

			MainWindow_updateFull();
			break;
		//見開き
		case TRMENU_VIEW_DOUBLE_PAGE:
			APPCONF->fimgview ^= IMGVIEW_F_DOUBLE_PAGE;

			mIconBarSetCheck(MLK_ICONBAR(p->toolbar),
				TRMENU_VIEW_DOUBLE_PAGE, APPCONF->fimgview & IMGVIEW_F_DOUBLE_PAGE);

			MainWindow_updateFull();
			break;
		//(単ページ)横長分割
		case TRMENU_VIEW_SINGLE_HORZ_HALF:
			APPCONF->fimgview ^= IMGVIEW_F_SINGLE_HORZ_HALF;

			if(!(APPCONF->fimgview & IMGVIEW_F_DOUBLE_PAGE))
				MainWindow_updateFull();
			break;
		//(見開き)横長単体
		case TRMENU_VIEW_DOUBLE_HORZ_SINGLE:
			APPCONF->fimgview ^= IMGVIEW_F_DOUBLE_HORZ_SINGLE;

			if(APPCONF->fimgview & IMGVIEW_F_DOUBLE_PAGE)
				MainWindow_updateFull();
			break;

		//一括変換
		case TRMENU_VIEW_BATCH_L_SINGLE:
		case TRMENU_VIEW_BATCH_L_SINGLE_HORZ:
		case TRMENU_VIEW_BATCH_L_DOUBLE:
		case TRMENU_VIEW_BATCH_L_DOUBLE_HORZ:
		case TRMENU_VIEW_BATCH_R_SINGLE:
		case TRMENU_VIEW_BATCH_R_SINGLE_HORZ:
		case TRMENU_VIEW_BATCH_R_DOUBLE:
		case TRMENU_VIEW_BATCH_R_DOUBLE_HORZ:
			_view_set_batch(p, id - TRMENU_VIEW_BATCH_L_SINGLE);
			break;

		//ビューサイズ > ウィンドウサイズ
		case TRMENU_VIEW_VIEWSIZE_FIT_WINDOW:
			APPCONF->fimgview &= ~IMGVIEW_F_VIEWSIZE_FIT_FIX;
			MainWindow_updateViewSize_resetscale();
			break;
		//ビューサイズ > 指定サイズ
		case TRMENU_VIEW_VIEWSIZE_FIT_FIXSIZE:
			APPCONF->fimgview |= IMGVIEW_F_VIEWSIZE_FIT_FIX;
			MainWindow_updateViewSize_resetscale();
			break;
		//ビューサイズ > サイズの指定
		case TRMENU_VIEW_VIEWSIZE_SET_SIZE:
			MainWindow_dlg_viewfixsize(p);
			break;

		//表示倍率 > 一段階拡大/縮小
		case TRMENU_VIEW_SCALE_UP:
		case TRMENU_VIEW_SCALE_DOWN:
			workSet_scaleUpDown((id == TRMENU_VIEW_SCALE_DOWN));
			break;
		//表示倍率 > 表示倍率指定
		case TRMENU_VIEW_SCALE_SET:
			MainWindow_dlg_scale(p);
			break;
		
		//ソート順 > タイプ
		case TRMENU_VIEW_SORT_TYPE_STORING:
		case TRMENU_VIEW_SORT_TYPE_NAME:
		case TRMENU_VIEW_SORT_TYPE_NAME_NATURAL:
		case TRMENU_VIEW_SORT_TYPE_MODIFY:
			APPCONF->filesort_type = id - TRMENU_VIEW_SORT_TYPE_STORING;
			workFileList_sort();
			PanelFileList_refresh();
			break;
		//ソート順 > 逆順
		case TRMENU_VIEW_SORT_REVERSE:
			APPCONF->fimgview ^= IMGVIEW_F_SORT_REVERSE;
			workFileList_sort();
			PanelFileList_refresh();
			break;

		//設定 > 拡大しない
		case TRMENU_VIEW_SETTING_NO_SCALEUP:
			APPCONF->fimgview ^= IMGVIEW_F_NO_SCALE_UP;
			MainWindow_updateViewSize_resetscale();
			break;
		//設定 > 先頭は常に単ページ
		case TRMENU_VIEW_SETTING_TOP_IS_SINGLE:
			workOpt_toggle_topIsSingle();
			break;
		//レベル補正
		case TRMENU_VIEW_COLOR_LEVEL:
			MainWindow_dlg_color(p);
			break;

		//---- しおり

		//追加
		case TRMENU_BOOKMARK_ADD:
			_cmd_bookmark_add(p);
			break;
		//リスト
		case TRMENU_BOOKMARK_LIST:
			_cmd_bookmark_list(p);
			break;
	
		//---- UI

		//全画面
		case TRMENU_UI_FULLSCREEN:
		case MAINWIN_CMDID_FULLSCREEN_RESTORE: //ESC キー
			MainWindow_toggle_fullscreen(p);
			break;
		//最小化
		case TRMENU_UI_MINIMIZE:
			if(!p->is_fullscreen)
				mToplevelMinimize(MLK_TOPLEVEL(p));
			break;
		//カスタムの表示状態
		case TRMENU_UI_CUSTOMVIEW:
			MainWindow_toggle_custom(p);
			break;

		//ファイルブラウザ
		case TRMENU_UI_PANEL_FILEBROWSER:
			_cmd_panel_toggle_visible(p, PANEL_FILEBROWSER);
			break;
		//ファイルリスト
		case TRMENU_UI_PANEL_FILELIST:
			_cmd_panel_toggle_visible(p, PANEL_FILELIST);
			break;
		//ルーペ
		case TRMENU_UI_PANEL_LOUPE:
			_cmd_panel_toggle_visible(p, PANEL_LOUPE);
			break;

		//メニューバー
		case TRMENU_UI_MENUBAR:
			if(MainWindow_canUIChange(p))
			{
				APPCONF->fui ^= UI_FLAGS_MENUBAR;

				mWidgetShow(p->menubar, -1);
				mWidgetReLayout(MLK_WIDGET(p));
			}
			break;
		//ツールバー
		case TRMENU_UI_TOOLBAR:
			if(MainWindow_canUIChange(p))
			{
				APPCONF->fui ^= UI_FLAGS_TOOLBAR;

				mWidgetShow(p->toolbar, -1);
				mWidgetReLayout(MLK_WIDGET(p));
			}
			break;
		//ステータスバー
		case TRMENU_UI_STATUSBAR:
			if(MainWindow_canUIChange(p))
				StatusBar_toggleVisible();
			break;
		//スクロールバー
		case TRMENU_UI_SCROLLBAR:
			if(MainWindow_canUIChange(p))
				MainWinCanvas_toggleScrollBar();
			break;
		//パネル
		case TRMENU_UI_PANEL:
			MainWindow_togglePanel(p);
			break;

		//---- 設定

		//環境設定
		case TRMENU_OPT_ENV:
			MainWindow_dlg_envopt(p);
			break;
		//ショートカットキー設定
		case TRMENU_OPT_SCKEY:
			ShortcutKeyDlg_run(MLK_WINDOW(p));
			break;
		//バージョン情報
		case TRMENU_OPT_VERSION:
			mSysDlg_about(MLK_WINDOW(p), _VERSION_TEXT);
			break;
	}
}

/** メニューポップアップ時 (表示) */

static void _event_menupopup_view(MainWindow *p,mMenu *menu)
{
	ConfigData *cf = APPCONF;

	//タイプ

	mMenuSetItemCheck(menu, TRMENU_VIEW_RIGHT_BINDING, cf->fimgview & IMGVIEW_F_RIGHT_BINDING);
	mMenuSetItemCheck(menu, TRMENU_VIEW_DOUBLE_PAGE, cf->fimgview & IMGVIEW_F_DOUBLE_PAGE);
	mMenuSetItemCheck(menu, TRMENU_VIEW_SINGLE_HORZ_HALF, cf->fimgview & IMGVIEW_F_SINGLE_HORZ_HALF);
	mMenuSetItemCheck(menu, TRMENU_VIEW_DOUBLE_HORZ_SINGLE, cf->fimgview & IMGVIEW_F_DOUBLE_HORZ_SINGLE);

	//画像表示サイズ

	mMenuSetItemCheck(menu, TRMENU_VIEW_IMGSIZE_FIT_VIEW + cf->imgsize_type, 1);

	//ビューのサイズ

	mMenuSetItemCheck(menu,
		TRMENU_VIEW_VIEWSIZE_FIT_WINDOW + ((cf->fimgview & IMGVIEW_F_VIEWSIZE_FIT_FIX) != 0), 1);

	//ソート順

	mMenuSetItemCheck(menu, TRMENU_VIEW_SORT_TYPE_STORING + cf->filesort_type, 1);
	mMenuSetItemCheck(menu, TRMENU_VIEW_SORT_REVERSE, cf->fimgview & IMGVIEW_F_SORT_REVERSE);

	//設定

	mMenuSetItemCheck(menu, TRMENU_VIEW_SETTING_NO_SCALEUP, cf->fimgview & IMGVIEW_F_NO_SCALE_UP);
	mMenuSetItemCheck(menu, TRMENU_VIEW_SETTING_TOP_IS_SINGLE, cf->fimgview & IMGVIEW_F_DOUBLE_TOP_IS_SINGLE);
}

/** メニューポップアップ時 (UI) */

static void _event_menupopup_ui(MainWindow *p,mMenu *menu)
{
	uint32_t fui;
	int i,f,fbase,nofull;

	fui = APPCONF->fui;
	nofull = !p->is_fullscreen;

	//全画面

	mMenuSetItemCheck(menu, TRMENU_UI_FULLSCREEN, p->is_fullscreen);

	//最小化

	mMenuSetItemEnable(menu, TRMENU_UI_MINIMIZE, nofull);

	//カスタム表示

	mMenuSetItemCheck(menu, TRMENU_UI_CUSTOMVIEW, fui & UI_FLAGS_CUSTOM);
	mMenuSetItemEnable(menu, TRMENU_UI_CUSTOMVIEW, nofull);

	//---- [以下、全画面/カスタム時は無効]

	fbase = (nofull && !(fui & UI_FLAGS_CUSTOM));

	//パネル

	f = (fbase && (fui & UI_FLAGS_PANEL));
	
	for(i = 0; i < PANEL_NUM; i++)
	{
		mMenuSetItemCheck(menu, TRMENU_UI_PANEL_FILEBROWSER + i,
			Panel_isCreated(i));
		
		mMenuSetItemEnable(menu, TRMENU_UI_PANEL_FILEBROWSER + i, f);
	}

	//ほか UI

	for(i = 0; i < 5; i++)
	{
		mMenuSetItemCheck(menu, TRMENU_UI_MENUBAR + i, fui & (1<<i));
		mMenuSetItemEnable(menu, TRMENU_UI_MENUBAR + i, fbase);
	}
}

/** イベントハンドラ */

int _event_handle(mWidget *wg,mEvent *ev)
{
	MainWindow *p = (MainWindow *)wg;

	switch(ev->type)
	{
		//コマンド
		case MEVENT_COMMAND:
			_event_command(p, (mEventCommand *)ev);
			break;

		//メニューポップアップ
		case MEVENT_MENU_POPUP:
			if(ev->popup.menu == p->menu_view)
				_event_menupopup_view(p, ev->popup.menu);
			else if(ev->popup.menu == p->menu_ui)
				_event_menupopup_ui(p, ev->popup.menu);
			break;

		//パネルの状態変化
		case MEVENT_PANEL:
			if(ev->panel.act == MPANEL_ACT_CLOSE
				|| ev->panel.act == MPANEL_ACT_TOGGLE_STORE)
				MainWindow_onChangePanel(p, TRUE);
			break;
	
		//閉じるボタン
		case MEVENT_CLOSE:
			MainWindow_quit(p);
			break;
		
		default:
			return FALSE;
	}

	return TRUE;
}
