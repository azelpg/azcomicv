/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * 環境設定ダイアログ
 *****************************************/

#include <mlk_gui.h>
#include <mlk_widget_def.h>
#include <mlk_widget.h>
#include <mlk_window.h>
#include <mlk_event.h>
#include <mlk_columnitem.h>
#include <mlk_listview.h>
#include <mlk_pager.h>
#include <mlk_str.h>
#include <mlk_buf.h>

#include "def_config.h"
#include "trid.h"

#include "envoptdlg.h"
#include "pv_envdlg.h"


//--------------------

typedef struct
{
	MLK_DIALOG_DEF

	mListView *lv;
	mPager *pager;

	EnvOptData dat;
}_dialog;

enum
{
	WID_LIST = 100
};

enum
{
	TRID_TITLE,
	TRID_PAGE_VIEW,
	TRID_PAGE_FLAGS,
	TRID_PAGE_SCALE,
	TRID_PAGE_CHARENCODE,
	TRID_PAGE_BUTTON,
	TRID_PAGE_UICUSTOM,
	TRID_PAGE_OTHER,
};

#define PAGE_NUM  7

//--------------------

mlkbool EnvDlg_createPage_view(mPager *p,mPagerInfo *info);
mlkbool EnvDlg_createPage_flags(mPager *p,mPagerInfo *info);
mlkbool EnvDlg_createPage_scale(mPager *p,mPagerInfo *info);
mlkbool EnvDlg_createPage_charset(mPager *p,mPagerInfo *info);
mlkbool EnvDlg_createPage_uicustom(mPager *p,mPagerInfo *info);
mlkbool EnvDlg_createPage_other(mPager *p,mPagerInfo *info);
mlkbool EnvDlg_createPage_button(mPager *p,mPagerInfo *info);

static const mFuncPagerCreate g_func_page[] = {
	EnvDlg_createPage_view, EnvDlg_createPage_flags, EnvDlg_createPage_scale,
	EnvDlg_createPage_charset, EnvDlg_createPage_button, EnvDlg_createPage_uicustom,
	EnvDlg_createPage_other
};

//--------------------



//===================


/* ページ作成 */

static void _create_page(_dialog *p,int no)
{
	mPagerSetPage(p->pager, g_func_page[no]);

	if(!mWidgetResize_calchint_larger(MLK_WIDGET(p->wg.toplevel)))
		mWidgetLayout_redraw(MLK_WIDGET(p->pager));
}

/* イベントハンドラ */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_NOTIFY
		&& ev->notify.id == WID_LIST
		&& ev->notify.notify_type == MLISTVIEW_N_CHANGE_FOCUS)
	{
		//ページ変更

		_create_page((_dialog *)wg, MLK_COLUMNITEM(ev->notify.param1)->param);
		
		return TRUE;
	}

	return mDialogEventDefault_okcancel(wg, ev);
}


//=======================
// 作成
//=======================


/* データ解放 */

static void _free_data(EnvOptData *p)
{
	mStrFree(&p->strArchiveCharcode);
	mStrFree(&p->strFont_panel);

	mBufFree(&p->buf_button);
}

/* 編集用データにコピー */

static void _copy_data(EnvOptData *pd,ConfigData *ps)
{
	pd->col_bkgnd = ps->col_bkgnd;
	pd->col_blendbkgnd = ps->col_blendbkgnd;
	pd->doublepage_margin = ps->doublepage_margin;
	pd->scaleup_method = ps->scaleup_method;
	pd->scaledown_method = ps->scaledown_method;
	pd->cursor_erase_sec = ps->cursor_erase_sec;
	pd->pagemark_opt = ps->pagemark_opt;

	pd->foption = ps->foption;

	pd->scalestep_up = ps->scalestep_up;
	pd->scalestep_down = ps->scalestep_down;
	pd->fixscale_step_under = ps->fixscale_step_under;
	pd->fixscale_step_upper = ps->fixscale_step_upper;

	mStrCopy(&pd->strArchiveCharcode, &ps->strArchiveCharcode);

	pd->fuicustom = ps->fuicustom;

	pd->iconsize = ps->iconsize;
	mStrCopy(&pd->strFont_panel, &ps->strFont_panel);

	//ボタン

	mBufAlloc(&pd->buf_button, 64, 64);
	mBufAppend(&pd->buf_button, ps->button_buf, ps->button_num * 2);
}

/* 終了時、値を適用 */

static uint32_t _apply_value(_dialog *p)
{
	EnvOptData *ps = &p->dat;
	ConfigData *pd = APPCONF;
	uint32_t f = 0;

	//変更確認
	// : 画像背景色の場合、キャッシュを再読込しなければならないので、更新はしない

	if(ps->col_bkgnd != pd->col_bkgnd
		|| ps->doublepage_margin != pd->doublepage_margin
		|| ps->scaledown_method != pd->scaledown_method
		|| ps->scaleup_method != pd->scaleup_method)
		f |= ENVOPT_F_UPDATE_VIEW;

	if(ps->pagemark_opt != pd->pagemark_opt)
		f |= ENVOPT_F_UPDATE_REDRAW;

	if(ps->scaledown_method != pd->scaledown_method)
		f |= ENVOPT_F_SCALEDOWN_METHOD;

	if(ps->iconsize != pd->iconsize)
		f |= ENVOPT_F_ICONSIZE;

	//---- 値のセット

	pd->col_bkgnd = ps->col_bkgnd;
	pd->col_blendbkgnd = ps->col_blendbkgnd;
	pd->doublepage_margin = ps->doublepage_margin;
	pd->scaleup_method = ps->scaleup_method;
	pd->scaledown_method = ps->scaledown_method;
	pd->cursor_erase_sec = ps->cursor_erase_sec;
	pd->pagemark_opt = ps->pagemark_opt;

	pd->foption = ps->foption;

	pd->scalestep_up = ps->scalestep_up;
	pd->scalestep_down = ps->scalestep_down;
	pd->fixscale_step_under = ps->fixscale_step_under;
	pd->fixscale_step_upper = ps->fixscale_step_upper;

	mStrCopy(&pd->strArchiveCharcode, &ps->strArchiveCharcode);

	pd->fuicustom = ps->fuicustom;

	pd->iconsize = ps->iconsize;
	mStrCopy(&pd->strFont_panel, &ps->strFont_panel);

	//ボタン

	mFree(pd->button_buf);

	pd->button_buf = mBufCopyToBuf(&ps->buf_button);
	pd->button_num = ps->buf_button.cursize / 2;

	return f;
}

/* ウィジェット作成 */

static void _create_widget(_dialog *p)
{
	mWidget *ct;
	mListView *lv;
	int i,fonth;

	fonth = mWidgetGetFontHeight(MLK_WIDGET(p));

	ct = mContainerCreateHorz(MLK_WIDGET(p), 10, MLF_EXPAND_WH, 0);

	//リスト

	p->lv = lv = mListViewCreate(ct, WID_LIST, MLF_EXPAND_H, 0,
		0, MSCROLLVIEW_S_VERT | MSCROLLVIEW_S_FRAME);

	lv->wg.initH = fonth * 18;

	for(i = 0; i < PAGE_NUM; i++)
		mListViewAddItem_text_static_param(lv, MLK_TR(TRID_PAGE_VIEW + i), i);

	mListViewSetItemHeight_min(lv, fonth + 2);
	mListViewSetAutoWidth(lv, TRUE);

	mListViewSetFocusItem_index(lv, 0);

	//mPager

	p->pager = mPagerCreate(ct, MLF_EXPAND_WH, 0);

	mPagerSetDataPtr(p->pager, &p->dat);
}

/* ダイアログ作成 */

static _dialog *_create_dialog(mWindow *parent)
{
	_dialog *p;

	p = (_dialog *)mDialogNew(parent, sizeof(_dialog), MTOPLEVEL_S_DIALOG_NORMAL);
	if(!p) return NULL;

	p->wg.event = _event_handle;

	//データコピー

	_copy_data(&p->dat, APPCONF);

	//

	MLK_TRGROUP(TRGROUP_DLG_ENVOPT);

	mToplevelSetTitle(MLK_TOPLEVEL(p), MLK_TR(TRID_TITLE));

	mContainerSetPadding_same(MLK_CONTAINER(p), 8);

	//ウィジェット

	_create_widget(p);

	mContainerCreateButtons_okcancel(MLK_WIDGET(p), MLK_MAKE32_4(0,10,0,0));

	//初期ページ

	mPagerSetPage(p->pager, EnvDlg_createPage_view);

	return p;
}

/** 環境設定ダイアログ実行
 *
 * return: フラグ */

uint32_t EnvOptDlg_run(mWindow *parent)
{
	_dialog *p;
	uint32_t ret;

	p = _create_dialog(parent);
	if(!p) return 0;

	mWindowResizeShow_initSize(MLK_WINDOW(p));

	ret = mDialogRun(MLK_DIALOG(p), FALSE);
	
	//適用

	if(ret)
	{
		//現在の値取得

		mPagerGetPageData(p->pager);

		//

		ret = _apply_value(p);
	}

	//削除

	_free_data(&p->dat);

	mWidgetDestroy(MLK_WIDGET(p));

	return ret;
}

