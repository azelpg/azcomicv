/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * 色調整ダイアログ
 *****************************************/

#include <stdlib.h>

#include <mlk_gui.h>
#include <mlk_widget_def.h>
#include <mlk_widget.h>
#include <mlk_window.h>
#include <mlk_label.h>
#include <mlk_button.h>
#include <mlk_checkbutton.h>
#include <mlk_guicol.h>
#include <mlk_event.h>
#include <mlk_pixbuf.h>
#include <mlk_str.h>

#include "def_appdata.h"
#include "def_config.h"
#include "work_main.h"
#include "trid.h"


//---------------------

typedef struct
{
	mWidget wg;
	mPixbuf *img;
	
	int val[3],
		dragno;		//-1:なし, 0-2
	double dmid;	//中間位置の比率
}_histogram;

typedef struct
{
	MLK_DIALOG_DEF

	_histogram *hist;
	mLabel *label;

	const char *valname[3];
}_dialog;

//---------------------

enum
{
	TRID_TITLE,
	TRID_NONE,
	TRID_MIN,
	TRID_MIDDLE,
	TRID_MAX,
	TRID_RESET
};

enum
{
	WID_HIST = 100,
	WID_RESERT,
	WID_CHECK_NONE,
	WID_CHECK_TOP
};

//---------------------

#define _HIST_PADDING_X		4	//枠を含む
#define _HIST_CURSOR_H		9
#define _HIST_HEIGHT		152	//枠を含む

#define _HIST_N_CHANGEPOS   0
#define _HIST_N_RELEASE     1

static const uint8_t g_pat_norm[] = { 0x08,0x08,0x14,0x14,0x22,0x22,0x41,0x7f },
	g_pat_current[]={ 0x08,0x08,0x1c,0x1c,0x3e,0x3e,0x7f,0x7f };

//---------------------


//=========================
// ヒストグラム
//=========================


/* カーソル描画 */

static void _hist_draw_cursor(_histogram *p)
{
	mPixbuf *img = p->img;
	int i;

	//消去

	mPixbufFillBox(img, 0, _HIST_HEIGHT, img->width, _HIST_CURSOR_H, MGUICOL_PIX(FACE));

	//各カーソル
	// ドラッグ中のカーソルは中を塗りつぶす

	for(i = 0; i < 3; i++)
	{
		mPixbufDraw1bitPattern(img,
			p->val[i] + _HIST_PADDING_X - 3, _HIST_HEIGHT + 1,
			(p->dragno == i)? g_pat_current: g_pat_norm,
			7, 8,
			MPIXBUF_TPCOL, MGUICOL_PIX(TEXT));
	}
}

/* イメージに描画 (作成時に一度のみ) */

static void _hist_draw_image(_histogram *p,uint32_t *histbuf)
{
	mPixbuf *img = p->img;
	int i,n;
	uint32_t peek1,peek2,val;
	mPixCol pix;

	//背景

	mPixbufFillBox(img, 0, 0, img->width, img->height, MGUICOL_PIX(FACE));

	//枠

	mPixbufBox(img, _HIST_PADDING_X - 1, 0, 256 + 2, _HIST_HEIGHT, 0);

	//--------- ヒストグラム

	//ピーク値と2番目の値

	peek1 = peek2 = histbuf[0];

	for(i = 1; i < 256; i++)
	{
		val = histbuf[i];

		if(val > peek1)
		{
			peek2 = peek1;
			peek1 = val;
		}

		if(peek1 > val && val > peek2)
			peek2 = val;
	}

	//ピーク値決定
	//1番目と2番目の差が、1番目の2割以上なら、2番目 * 1.1 をピークにする

	if(peek2 && peek1 - peek2 > peek1 / 5)
		peek1 = (uint32_t)(peek2 * 1.1 + 0.5);

	//ヒストグラム背景

	mPixbufFillBox(img, _HIST_PADDING_X, 1, 256, _HIST_HEIGHT - 2, MGUICOL_PIX(WHITE));

	//中央線

	mPixbufLineV(img, _HIST_PADDING_X + 128, 1, _HIST_HEIGHT - 2, mRGBtoPix(0xdddddd));

	//グラフ

	pix = mRGBtoPix(0x2A3D86);

	for(i = 0; i < 256; i++)
	{
		if(histbuf[i])
		{
			n = (int)((double)histbuf[i] / peek1 * _HIST_HEIGHT);
			if(n > _HIST_HEIGHT) n = _HIST_HEIGHT;

			if(n)
				mPixbufLineV(img, _HIST_PADDING_X + i, _HIST_HEIGHT - 1 - n, n, pix);
		}
	}

	//------- カーソル

	_hist_draw_cursor(p);
}

/* (ドラッグ中) カーソル位置変更 */

static mlkbool _hist_changepos(_histogram *p,int pos)
{
	int no,min,max;

	no = p->dragno;

	//調整
	
	min = (no == 0)? 0: p->val[no - 1] + 1;
	max = (no == 2)? 255: p->val[no + 1] - 1;

	if(pos < min)
		pos = min;
	else if(pos > max)
		pos = max;

	//セット

	if(p->val[no] == pos)
		return FALSE;
	else
	{
		p->val[no] = pos;

		//最小/最大の場合、中間を押し時と同じ比率の位置にする

		if(no != 1)
			p->val[1] = (int)((p->val[2] - p->val[0]) * p->dmid + p->val[0] + 0.5);

		//更新

		_hist_draw_cursor(p);

		mWidgetRedraw(MLK_WIDGET(p));

		//通知

		mWidgetEventAdd_notify(MLK_WIDGET(p), NULL, _HIST_N_CHANGEPOS, 0, 0);

		return TRUE;
	}
}

/* ボタン押し時 */

static void _hist_on_press(_histogram *p,int x)
{
	int i,pos,min,no,d;

	//X 位置

	pos = x - _HIST_PADDING_X;
	
	if(pos < 0) pos = 0;
	else if(pos > 255) pos = 255;

	//一番距離の近いカーソル

	for(i = 0, min = 256; i < 3; i++)
	{
		d = abs(p->val[i] - pos);
		
		if(d < min)
		{
			min = d;
			no = i;
		}
	}

	//

	p->dragno = no;

	//中間位置の比率

	p->dmid = (double)(p->val[1] - p->val[0]) / (p->val[2] - p->val[0]);

	//位置変更
	//(位置が変わらなかった場合はカーソルの更新のみ)

	if(!_hist_changepos(p, pos))
	{
		_hist_draw_cursor(p);
		mWidgetRedraw(MLK_WIDGET(p));
	}
	
	mWidgetGrabPointer(MLK_WIDGET(p));
}

/* ドラッグ移動中 */

static void _hist_on_motion(_histogram *p,int x)
{
	x -= _HIST_PADDING_X;

	if(x < 0) x = 0;
	else if(x > 255) x = 255;

	_hist_changepos(p, x);
}

/* グラブ解除 */

static void _hist_ungrab(_histogram *p)
{
	if(p->dragno != -1)
	{
		p->dragno = -1;
		mWidgetUngrabPointer();

		//カーソル戻す
		_hist_draw_cursor(p);
		mWidgetRedraw(MLK_WIDGET(p));

		mWidgetEventAdd_notify(MLK_WIDGET(p), NULL, _HIST_N_RELEASE, 0, 0);
	}
}

/* イベント */

static int _hist_event_handle(mWidget *wg,mEvent *ev)
{
	_histogram *p = (_histogram *)wg;

	switch(ev->type)
	{
		case MEVENT_POINTER:
			if(ev->pt.act == MEVENT_POINTER_ACT_MOTION)
			{
				//移動

				if(p->dragno != -1)
					_hist_on_motion(p, ev->pt.x);
			}
			else if(ev->pt.act == MEVENT_POINTER_ACT_PRESS)
			{
				//押し

				if(ev->pt.btt == MLK_BTT_LEFT && p->dragno == -1)
					_hist_on_press(p, ev->pt.x);
			}
			else if(ev->pt.act == MEVENT_POINTER_ACT_RELEASE)
			{
				//離し
				
				if(ev->pt.btt == MLK_BTT_LEFT)
					_hist_ungrab(p);
			}
			break;
		
		case MEVENT_FOCUS:
			if(ev->focus.is_out)
				_hist_ungrab(p);
			break;
		default:
			return FALSE;
	}

	return TRUE;
}

/* 描画 */

static void _hist_draw_handle(mWidget *wg,mPixbuf *pixbuf)
{
	mPixbufBlt(pixbuf, 0, 0, ((_histogram *)wg)->img, 0, 0, -1, -1);
}

/* 破棄ハンドラ */

static void _hist_destroy_handle(mWidget *wg)
{
	mPixbufFree(((_histogram *)wg)->img);
}

/* 作成 */

static _histogram *_histogram_new(mWidget *parent,int id,uint32_t *histbuf,int *defval)
{
	_histogram *p;
	int i;

	p = (_histogram *)mWidgetNew(parent, sizeof(_histogram));
	if(!p) return NULL;

	p->wg.id = id;
	p->wg.fevent |= MWIDGET_EVENT_POINTER;
	p->wg.destroy = _hist_destroy_handle;
	p->wg.event = _hist_event_handle;
	p->wg.draw = _hist_draw_handle;
	p->wg.hintW = 256 + _HIST_PADDING_X * 2;
	p->wg.hintH = _HIST_HEIGHT + _HIST_CURSOR_H;

	for(i = 0; i < 3; i++)
		p->val[i] = defval[i];

	p->dragno = -1;

	//イメージ

	p->img = mPixbufCreate(p->wg.hintW, p->wg.hintH, 0);

	_hist_draw_image(p, histbuf);

	return p;
}

/* 値セット */

static void _histogram_set_value(_histogram *p,uint32_t val)
{
	int i;

	for(i = 0; i < 3; i++)
		p->val[i] = (val >> ((2 - i) << 3)) & 255;

	_hist_draw_cursor(p);

	mWidgetRedraw(MLK_WIDGET(p));
}



//*********************************
// ダイアログ
//*********************************


//==========================
// sub
//==========================


/* 更新 */

static void _update(void)
{
	workData_setLevelTable();

	mWidgetRedraw(APPDATA->canvas_page);
}

/* ウィジェット有効/無効 */

static void _enable_widget(_dialog *p,int sel)
{
	mWidgetEnable(MLK_WIDGET(p->hist), (sel != 0));
}

/* ラベル文字列セット */

static void _set_value_label(_dialog *p)
{
	mStr str = MSTR_INIT;
	int *pval = p->hist->val;

	mStrSetFormat(&str, "%s [%d] %s [%d] %s [%d]",
		p->valname[0], pval[0],
		p->valname[1], pval[1],
		p->valname[2], pval[2]);

	mLabelSetText(p->label, str.buf);

	mStrFree(&str);
}

/* スロット変更 */

static void _change_slot(_dialog *p,int no)
{
	APPCONF->color_level_sel = no;

	//有効/無効

	_enable_widget(p, no);

	//ON の場合、値セット

	if(no != 0)
	{
		_histogram_set_value(p->hist, APPCONF->color_level[no - 1]);

		_set_value_label(p);
	}

	//更新

	_update();
}

/* ヒストグラムのドラッグ終了時 */

static void _release_hist(_dialog *p)
{
	int i;
	uint32_t val = 0;

	for(i = 0; i < 3; i++)
		val |= p->hist->val[i] << ((2 - i) << 3);

	//値変更

	i = APPCONF->color_level_sel - 1;

	if(APPCONF->color_level[i] != val)
	{
		APPCONF->color_level[i] = val;
		_update();
	}
}

/* リセット */

static void _reset_value(_dialog *p)
{
	int sel = APPCONF->color_level_sel;

	if(sel != 0)
	{
		sel--;
		
		APPCONF->color_level[sel] = (128 << 8) | 255;

		_histogram_set_value(p->hist, APPCONF->color_level[sel]);
		_set_value_label(p);

		_update();
	}
}


//==========================
// main
//==========================


/* イベント */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_NOTIFY)
	{
		_dialog *p = (_dialog *)wg;
		int id = ev->notify.id;

		if(id >= WID_CHECK_NONE && id < WID_CHECK_TOP + CONFIG_COLOR_LEVEL_SLOT_NUM)
		{
			//スロット変更

			_change_slot(p, id - WID_CHECK_NONE);
		}
		else
		{
			switch(id)
			{
				//ヒストグラム
				case WID_HIST:
					if(ev->notify.notify_type == _HIST_N_CHANGEPOS)
						//ドラッグ中
						_set_value_label(p);
					else
						_release_hist(p);
					break;
				//リセット
				case WID_RESERT:
					_reset_value(p);
					break;
				//OK
				case MLK_WID_OK:
					mDialogEnd(MLK_DIALOG(p), FALSE);
					break;
			}
		}
	}

	return mDialogEventDefault(wg, ev);
}

/* ダイアログ作成 */

static _dialog *_create_dlg(mWindow *parent,uint32_t *histbuf)
{
	_dialog *p;
	mWidget *ct;
	int i,sel,n,val[3];
	char m[2] = {0,0};

	p = (_dialog *)mDialogNew(parent, sizeof(_dialog), MTOPLEVEL_S_DIALOG_NORMAL);
	if(!p) return NULL;

	p->wg.event = _event_handle;
	p->ct.sep = 15;

	//

	mContainerSetPadding_same(MLK_CONTAINER(p), 8);

	MLK_TRGROUP(TRGROUP_DLG_COLOR_LEVEL);

	mToplevelSetTitle(MLK_TOPLEVEL(p), MLK_TR(TRID_TITLE));

	//値名

	for(i = 0; i < 3; i++)
		p->valname[i] = MLK_TR(TRID_MIN + i);

	//補正値、初期値

	sel = APPCONF->color_level_sel;

	if(sel == 0)
		val[0] = 0, val[1] = 128, val[2] = 255;
	else
	{
		n = APPCONF->color_level[sel - 1];
		
		for(i = 0; i < 3; i++)
			val[i] = (n >> ((2 - i) << 3)) & 255;
	}

	//------- ウィジェット

	//ラジオボタン

	ct = mContainerCreateHorz(MLK_WIDGET(p), 4, 0, 0);

	mCheckButtonCreate(ct, WID_CHECK_NONE, 0, 0,
		MCHECKBUTTON_S_RADIO, MLK_TR(TRID_NONE), (sel == 0));

	for(i = 0; i < CONFIG_COLOR_LEVEL_SLOT_NUM; i++)
	{
		m[0] = '1' + i;
		
		mCheckButtonCreate(ct, WID_CHECK_TOP + i, 0, 0,
			MCHECKBUTTON_S_RADIO | MCHECKBUTTON_S_COPYTEXT,
			m, (sel == i + 1));
	}

	//ヒストグラム

	p->hist = _histogram_new(MLK_WIDGET(p), WID_HIST, histbuf, val);

	//ラベル

	p->label = mLabelCreate(MLK_WIDGET(p), MLF_EXPAND_W, 0, MLABEL_S_BORDER | MLABEL_S_COPYTEXT, NULL);

	//ボタン

	ct = mContainerCreateHorz(MLK_WIDGET(p), 5, MLF_EXPAND_W, 0);

	mButtonCreate(ct, WID_RESERT, 0, 0, 0, MLK_TR(TRID_RESET));

	mButtonCreate(ct, MLK_WID_OK, MLF_EXPAND_X | MLF_RIGHT, 0, 0, MLK_TR_SYS(MLK_TRSYS_OK));

	//-------

	_enable_widget(p, sel);
	_set_value_label(p);

	return p;
}

/** 実行 */

void ColorLevelDlg_run(mWindow *parent,uint32_t *histbuf)
{
	_dialog *p;

	p = _create_dlg(parent, histbuf);

	//イメージ描画後は不要なため、解放
	mFree(histbuf);

	if(!p) return;

	mWindowResizeShow_initSize(MLK_WINDOW(p));

	mDialogRun(MLK_DIALOG(p), TRUE);
}
