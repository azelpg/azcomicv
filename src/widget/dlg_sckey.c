/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * ショートカットキー設定
 *****************************************/

#include <stdio.h>

#include <mlk_gui.h>
#include <mlk_widget_def.h>
#include <mlk_widget.h>
#include <mlk_window.h>
#include <mlk_button.h>
#include <mlk_conflistview.h>
#include <mlk_event.h>
#include <mlk_menu.h>
#include <mlk_accelerator.h>
#include <mlk_iniwrite.h>
#include <mlk_key.h>

#include "def_macro.h"
#include "def_appdata.h"
#include "def_mainwin.h"

#include "trid.h"
#include "trid_menu.h"


//----------------------

typedef struct
{
	MLK_DIALOG_DEF

	mConfListView *list;
}_dialog;

//----------------------

enum
{
	WID_LIST = 100,
	WID_BTT_CLEAR,
	WID_BTT_CLEARALL
};

enum
{
	TRID_TITLE,
	TRID_CLEAR,
	TRID_CLEARALL,
	TRID_SPECIAL,

	TRID_SPECIAL_CMD_TOP = 100
};

#define _SPECIAL_CMD_NUM  4

//----------------------



/* アイテム追加
 *
 * topitem: 各トップのアイテムか
 * return: サブメニューがある場合、サブメニューのポインタ */

static mMenu *_append_item(mConfListView *list,mMenuItem *menuitem,mlkbool topitem)
{
	mMenuItemInfo *info;
	int id;

	info = mMenuItemGetInfo(menuitem);

	if(topitem)
	{
		//トップアイテム

		mConfListView_addItem_header(list, info->text);
	}
	else
	{
		id = info->id;

		//リストに追加しないもの (セパレータ、履歴)

		if((info->flags & MMENUITEM_F_SEP)
			|| (id == TRMENU_FILE_RECENT_FILE || id == TRMENU_FILE_RECENT_DIR
				|| id == TRMENU_FILE_RECENT_ARCHIVE))
			return NULL;

		//サブメニューの親の場合は、項目として追加しない

		if(info->submenu)
			return info->submenu;

		//追加

		mConfListView_addItem_accel(list, info->text, info->shortcut_key, id);
	}

	return info->submenu;
}

/* リスト作成 */

static void _set_list(_dialog *p)
{
	mMenuItem *mi_top,*mi,*mi2;
	mMenu *submenu,*submenu2;
	mConfListView *list;
	mAccelerator *accel;
	int i;

	list = p->list;

	//------ メニューから

	mi_top = mMenuGetTopItem(APPDATA->mainwin->menu_top);

	for(; mi_top; mi_top = mMenuItemGetNext(mi_top))
	{
		//トップ

		submenu = _append_item(list, mi_top, TRUE);
		
		//メイン項目

		for(mi = mMenuGetTopItem(submenu); mi; mi = mMenuItemGetNext(mi))
		{
			submenu2 = _append_item(list, mi, FALSE);

			//サブメニュー

			if(submenu2)
			{
				for(mi2 = mMenuGetTopItem(submenu2); mi2; mi2 = mMenuItemGetNext(mi2))
					_append_item(list, mi2, FALSE);
			}
		}
	}

	//------ 特殊

	mConfListView_addItem_header(list, MLK_TR(TRID_SPECIAL));

	accel = APPDATA->mainwin->top.accelerator;

	for(i = 0; i < _SPECIAL_CMD_NUM; i++)
	{
		mConfListView_addItem_accel(list, MLK_TR(TRID_SPECIAL_CMD_TOP + i),
			mAcceleratorGetKey(accel, TRMENU_SCKEY_TOP_ID + i),
			TRMENU_SCKEY_TOP_ID + i);
	}

	//

	mListViewSetColumnWidth_auto(MLK_LISTVIEW(list), 0);
}


/* イベント */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_NOTIFY)
	{
		_dialog *p = (_dialog *)wg;
		
		switch(ev->notify.id)
		{
			//クリア
			case WID_BTT_CLEAR:
				mConfListView_setFocusValue_zero(p->list);
				break;
			//すべてクリア
			case WID_BTT_CLEARALL:
				mConfListView_clearAll_accel(p->list);
				break;
		}
	}

	return mDialogEventDefault_okcancel(wg, ev);
}


//=====================


/* ダイアログ作成 */

static _dialog *_create_dlg(mWindow *parent)
{
	_dialog *p;
	mWidget *ct;

	p = (_dialog *)mDialogNew(parent, sizeof(_dialog), MTOPLEVEL_S_DIALOG_NORMAL);
	if(!p) return NULL;

	p->wg.event = _event_handle;
	p->ct.sep = 7;

	//

	mContainerSetPadding_same(MLK_CONTAINER(p), 8);

	MLK_TRGROUP(TRGROUP_DLG_SHORTCUTKEY);

	mToplevelSetTitle(MLK_TOPLEVEL(p), MLK_TR(TRID_TITLE));

	//------- ウィジェット

	//リスト

	p->list = mConfListViewNew(MLK_WIDGET(p), 0, 0);

	p->list->wg.id = WID_LIST;

	mWidgetSetInitSize_fontHeightUnit(MLK_WIDGET(p->list), 24, 18);

	//キークリア

	mButtonCreate(MLK_WIDGET(p), WID_BTT_CLEAR, MLF_RIGHT, 0, 0, MLK_TR(TRID_CLEAR));

	//すべてクリア/OK/cancel

	ct = mContainerCreateHorz(MLK_WIDGET(p), 3, MLF_EXPAND_W, MLK_MAKE32_4(0,12,0,0));

	mButtonCreate(ct, WID_BTT_CLEARALL, MLF_EXPAND_X, MLK_MAKE32_4(0,0,10,0), 0, MLK_TR(TRID_CLEARALL));
	mButtonCreate(ct, MLK_WID_OK, 0, 0, 0, MLK_TR_SYS(MLK_TRSYS_OK));
	mButtonCreate(ct, MLK_WID_CANCEL, 0, 0, 0, MLK_TR_SYS(MLK_TRSYS_CANCEL));

	//------- 初期化

	_set_list(p);

	return p;
}


//====================


typedef struct
{
	mAccelerator *accel;
	mMenu *menu;
	FILE *fp;
}_getvalinfo;


static int _list_getvalue(int type,intptr_t item_param,mConfListViewValue *val,void *param)
{
	_getvalinfo *p = (_getvalinfo *)param;
	int id = item_param;
	uint32_t key = val->key;

	//ESC は全画面戻すで固定のため、使用不可

	if(key == MKEY_ESCAPE)
		key = 0;

	//

	if(key)
		mAcceleratorAdd(p->accel, id, key, NULL);

	if(!(id >= TRMENU_SCKEY_TOP_ID && id <= TRMENU_SCKEY_LAST_ID))
		mMenuSetItemShortcutKey(p->menu, id, key);

	if(key && p->fp)
		mIniWrite_putHex_keyno(p->fp, id, key);

	return 0;
}

/* キーをセット & 設定ファイルに書き込み */

static void _set_and_write(mConfListView *list)
{
	_getvalinfo info;

	info.accel = APPDATA->mainwin->top.accelerator;
	info.menu = APPDATA->mainwin->menu_top;

	//

	info.fp = mIniWrite_openFile_join(mGuiGetPath_config_text(), APP_SCKEY_FILENAME);

	if(info.fp)
		mIniWrite_putGroup(info.fp, "sckey");

	//

	mAcceleratorClear(info.accel);

	mConfListView_getValues(list, _list_getvalue, &info);

	if(info.fp)
		fclose(info.fp);
}

/** ダイアログ実行 */

mlkbool ShortcutKeyDlg_run(mWindow *parent)
{
	_dialog *p;
	int ret;

	p = _create_dlg(parent);
	if(!p) return FALSE;

	mWindowResizeShow_initSize(MLK_WINDOW(p));

	ret = mDialogRun(MLK_DIALOG(p), FALSE);

	if(ret)
		_set_and_write(p->list);

	mWidgetDestroy(MLK_WIDGET(p));

	return ret;
}
