/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/********************************************
 * Canvas/CanvasPage
 *
 * メインウィンドウのキャンバス部分ウィジェット
 ********************************************/

#include <mlk_gui.h>
#include <mlk_widget_def.h>
#include <mlk_widget.h>
#include <mlk_scrollview.h>
#include <mlk_scrollbar.h>
#include <mlk_event.h>
#include <mlk_pixbuf.h>
#include <mlk_cursor.h>

#include "def_appdata.h"
#include "def_config.h"
#include "def_work.h"
#include "def_optbutton.h"

#include "mainwindow.h"
#include "panel_func.h"
#include "work_main.h"

#include "trid_menu.h"


//----------------

typedef struct _mScrollView Canvas;

typedef struct
{
	MLK_SCROLLVIEWPAGE_DEF

	mCursor cursor_empty;	//空のカーソル

	int fdrag,		//ドラッグフラグ
		grab_btt;	//ドラッグ中:押されたボタン
	mPoint ptlast;
}CanvasPage;

//----------------

enum
{
	_DRAG_F_NONE,
	_DRAG_F_SCROLL
};

#define _TIMERID_UPDATE_SCROLL 1
#define _TIMERID_CURSOR_ERASE  2

//----------------



//*********************************************
// CanvasPage (mScrollViewPage)
//*********************************************


/* メインウィンドウにコマンド通知 */

static void _send_command_event(int id)
{
	mWidgetEventAdd_command(MLK_WIDGET(APPDATA->mainwin),
		id, 0, MEVENT_COMMAND_FROM_UNKNOWN, NULL);
}

/* ボタン設定から、実行するコマンド取得
 *
 * return: -1 でなし */

static int _button_get_command(mEventPointer *ev)
{
	uint16_t *ps;
	int i,btt,rawbtt,val;

	ps = APPCONF->button_buf;
	if(!ps) return -1;

	//ボタン値
	// btt = 通常のボタン
	// rawbtt = 生ボタン指定時 (-1 でなし)

	btt = 0;
	if(ev->state & MLK_STATE_CTRL) btt |= OPTBUTTON_F_CTRL;
	if(ev->state & MLK_STATE_SHIFT) btt |= OPTBUTTON_F_SHIFT;

	rawbtt = btt;

	btt |= OPTBUTTON_MAKE(ev->btt, 0);

	if(ev->raw_btt < 0)
		rawbtt = -1;
	else
		rawbtt |= OPTBUTTON_MAKE(ev->raw_btt, 0) | OPTBUTTON_F_RAW;

	//検索

	for(i = APPCONF->button_num; i > 0; i--, ps++)
	{
		val = OPTBUTTON_MASK_BUTTON(*ps);
		
		if((!(val & OPTBUTTON_F_RAW) && val == btt)
			|| ((val & OPTBUTTON_F_RAW) && rawbtt != -1 && val == rawbtt))
		{
			return OPTBUTTON_GET_COMMAND(*ps);
		}
	}

	return -1;
}

/* ボタン押し時 */

static void _page_press(CanvasPage *p,mEventPointer *ev)
{
	int cmd,n;
	mSize size;

	cmd = _button_get_command(ev);
	if(cmd == -1) return;

	//コマンド

	switch(cmd)
	{
		//メニュー
		case OPTBUTTON_CMD_MENU:
			MainWindow_runMainMenu_forButton(ev->x, ev->y);
			break;
		//ドラッグでスクロール
		case OPTBUTTON_CMD_SCROLL:
			workPage_getScrollMax(&size);

			if(size.w > p->wg.w || size.h > p->wg.h)
			{
				p->fdrag = _DRAG_F_SCROLL;
				p->grab_btt = ev->btt;
				p->ptlast.x = ev->x;
				p->ptlast.y = ev->y;
				
				mWidgetGrabPointer(MLK_WIDGET(p));
			}
			break;
		//ページ移動 (クリック位置の方向)
		case OPTBUTTON_CMD_MOVEPAGE_CLICK:
			n = (ev->x >= p->wg.w / 2);
			if(APPCONF->fimgview & IMGVIEW_F_RIGHT_BINDING) n = !n;
			
			_send_command_event((n)? TRMENU_CTRL_NEXT_PAGE: TRMENU_CTRL_PREV_PAGE);
			break;
		//前/次のページ
		case OPTBUTTON_CMD_NEXT_PAGE:
		case OPTBUTTON_CMD_PREV_PAGE:
			_send_command_event(
				(cmd == OPTBUTTON_CMD_NEXT_PAGE)? TRMENU_CTRL_NEXT_PAGE: TRMENU_CTRL_PREV_PAGE);
			break;
		//前/次の画像
		case OPTBUTTON_CMD_NEXT_IMAGE:
		case OPTBUTTON_CMD_PREV_IMAGE:
			_send_command_event(
				(cmd == OPTBUTTON_CMD_NEXT_IMAGE)? TRMENU_CTRL_NEXT_IMAGE: TRMENU_CTRL_PREV_IMAGE);
			break;
		//一段階拡大、縮小
		case OPTBUTTON_CMD_SCALE_UP:
		case OPTBUTTON_CMD_SCALE_DOWN:
			workSet_scaleUpDown((cmd == OPTBUTTON_CMD_SCALE_DOWN));
			break;
		//スクロール
		case OPTBUTTON_CMD_SCROLL_UP:
		case OPTBUTTON_CMD_SCROLL_DOWN:
		case OPTBUTTON_CMD_SCROLL_LEFT:
		case OPTBUTTON_CMD_SCROLL_RIGHT:
			MainWinCanvas_moveScroll(cmd - OPTBUTTON_CMD_SCROLL_UP);
			break;
	}
}

/* ドラッグ移動 : スクロール */

static void _page_motion_scroll(CanvasPage *p,int x,int y)
{
	Canvas *canv = (Canvas *)p->wg.parent;
	int dx,dy;
	mPoint pt,ptnew;

	dx = p->ptlast.x - x;
	dy = p->ptlast.y - y;

	if(dx == 0 && dy == 0) return;

	pt = APPWORK->view.ptscr;

	mScrollBarSetPos(canv->sv.scrh, pt.x + dx);
	mScrollBarSetPos(canv->sv.scrv, pt.y + dy);

	ptnew.x = mScrollBarGetPos(canv->sv.scrh);
	ptnew.y = mScrollBarGetPos(canv->sv.scrv);

	if(pt.x != ptnew.x || pt.y != ptnew.y)
	{
		APPWORK->view.ptscr = ptnew;
	
		mWidgetTimerAdd_ifnothave(MLK_WIDGET(p), _TIMERID_UPDATE_SCROLL, 5, 0);
	}
	
	p->ptlast.x = x;
	p->ptlast.y = y;
}

/* カーソルが移動した */

static void _moved_cursor(CanvasPage *p)
{
	mWidgetSetCursor(MLK_WIDGET(p), 0);

	if(APPCONF->cursor_erase_sec)
	{
		mWidgetTimerAdd(MLK_WIDGET(p), _TIMERID_CURSOR_ERASE,
			APPCONF->cursor_erase_sec * 1000, 0);
	}
}

/* グラブ解放 */

static void _page_ungrab(CanvasPage *p)
{
	if(p->fdrag)
	{
		if(p->fdrag == _DRAG_F_SCROLL)
		{
			if(mWidgetTimerDelete(MLK_WIDGET(p), _TIMERID_UPDATE_SCROLL))
				mWidgetRedraw(MLK_WIDGET(p));
		}
	
		p->fdrag = _DRAG_F_NONE;
		mWidgetUngrabPointer();
	}
}

/* イベント */

static int _page_event_handle(mWidget *wg,mEvent *ev)
{
	CanvasPage *p = (CanvasPage *)wg;

	switch(ev->type)
	{
		//ポインタ
		case MEVENT_POINTER:
			switch(ev->pt.act)
			{
				//移動
				case MEVENT_POINTER_ACT_MOTION:
					_moved_cursor(p);
					
					if(p->fdrag == _DRAG_F_NONE)
					{
						//ルーペ
						PanelLoupe_changeCurPos(ev->pt.x, ev->pt.y);
					}
					else
						//スクロール
						_page_motion_scroll(p, ev->pt.x, ev->pt.y);
					break;
				//押し
				case MEVENT_POINTER_ACT_PRESS:
				case MEVENT_POINTER_ACT_DBLCLK:
					if(p->fdrag == _DRAG_F_NONE)
						_page_press(p, (mEventPointer *)ev);
					break;
				//離し
				case MEVENT_POINTER_ACT_RELEASE:
					if(p->fdrag && p->grab_btt == ev->pt.btt)
						_page_ungrab(p);
					break;
			}
			break;

		case MEVENT_ENTER:
			_moved_cursor(p);
			break;

		//タイマー
		case MEVENT_TIMER:
			if(ev->timer.id == _TIMERID_UPDATE_SCROLL)
				mWidgetRedraw(wg);
			else if(ev->timer.id == _TIMERID_CURSOR_ERASE)
				mWidgetSetCursor(wg, p->cursor_empty);

			mWidgetTimerDelete(wg, ev->timer.id);
			break;

		case MEVENT_FOCUS:
			if(ev->focus.is_out)
				_page_ungrab(p);
			break;

		//ファイルドロップ
		case MEVENT_DROP_FILES:
			MainWindow_open(*(ev->dropfiles.files), MAINWIN_OPEN_NORMAL);
			break;
	}

	return 1;
}

/* 描画 */

static void _page_draw_handle(mWidget *wg,mPixbuf *pixbuf)
{
	if(APPWORK->view.type == VIEWTYPE_NONE)
		mPixbufFillBox(pixbuf, 0, 0, wg->w, wg->h, mRGBtoPix(APPCONF->col_bkgnd));
	else
		workPage_draw(pixbuf, wg->w, wg->h);
}

/* サイズ変更時 */

static void _page_resize_handle(mWidget *wg)
{
	APPWORK->size_canvas.w = wg->w;
	APPWORK->size_canvas.h = wg->h;

	MainWinCanvas_setScrollInfo(FALSE);
}

/* 破棄 */

static void _page_destroy(mWidget *wg)
{
	mCursorFree(((CanvasPage *)wg)->cursor_empty);
}

/* CanvasPage 作成 */

static CanvasPage *_create_page(mWidget *parent)
{
	CanvasPage *p;

	p = (CanvasPage *)mScrollViewPageNew(parent, sizeof(CanvasPage));
	if(!p) return NULL;

	APPDATA->canvas_page = (mWidget *)p;

	p->wg.destroy = _page_destroy;
	p->wg.draw = _page_draw_handle;
	p->wg.event = _page_event_handle;
	p->wg.resize = _page_resize_handle;
	
	p->wg.fevent |= MWIDGET_EVENT_POINTER;
	p->wg.fstate |= MWIDGET_STATE_ENABLE_DROP;
	p->wg.foption |= MWIDGET_OPTION_SCROLL_TO_POINTER;

	p->cursor_empty = mCursorCreateEmpty();

	return p;
}



//*********************************************
// Canvas (mScrollView)
//*********************************************



/* スクロール処理 */

static void _canvas_scroll(Canvas *p,int pos,int flags,mlkbool vert)
{
	if(flags & MSCROLLBAR_ACTION_F_CHANGE_POS)
	{
		if(vert)
			APPWORK->view.ptscr.y = pos;
		else
			APPWORK->view.ptscr.x = pos;
		
		mWidgetRedraw(APPDATA->canvas_page);
	}
}

/* イベントハンドラ */

static int _canvas_event_handle(mWidget *wg,mEvent *ev)
{
	Canvas *p = (Canvas *)wg;

	switch(ev->type)
	{
		case MEVENT_NOTIFY:
			if(ev->notify.widget_from == MLK_WIDGET(p->sv.scrh))
			{
				//水平スクロール

				if(ev->notify.notify_type == MSCROLLBAR_N_ACTION)
					_canvas_scroll(p, ev->notify.param1, ev->notify.param2, FALSE);
			}
			else if(ev->notify.widget_from == MLK_WIDGET(p->sv.scrv))
			{
				//垂直スクロール

				if(ev->notify.notify_type == MSCROLLBAR_N_ACTION)
					_canvas_scroll(p, ev->notify.param1, ev->notify.param2, TRUE);
			}
			break;
	}

	return 1;
}


/** メインウィンドウのキャンバス作成 */

void MainWinCanvas_new(mWidget *parent)
{
	Canvas *p;

	p = (Canvas *)mScrollViewNew(parent, sizeof(Canvas),
			MSCROLLVIEW_S_HORZVERT | MSCROLLVIEW_S_SCROLL_NOTIFY_SELF);
	if(!p) return;

	APPDATA->canvas = (mWidget *)p;

	p->wg.event = _canvas_event_handle;
	p->wg.flayout = MLF_EXPAND_WH;
	p->wg.notify_to = MWIDGET_NOTIFYTO_SELF;

	if(APPCONF->fui & UI_FLAGS_SCROLLBAR)
		mScrollViewSetFixBar(p, 1);

	//page

	p->sv.page = (mScrollViewPage *)_create_page(MLK_WIDGET(p));
}

/** スクロールバーの表示切り替え */

void MainWinCanvas_toggleScrollBar(void)
{
	mScrollView *p = MLK_SCROLLVIEW(APPDATA->canvas);

	APPCONF->fui ^= UI_FLAGS_SCROLLBAR;

	mScrollViewSetFixBar(p, -1);
	mScrollViewLayout(p);
}

/** スクロール情報セット
 *
 * reset: 位置をクリア */

void MainWinCanvas_setScrollInfo(mlkbool reset)
{
	Canvas *p = (Canvas *)APPDATA->canvas;
	mSize size;
	int x;

	workPage_getScrollMax(&size);

	mScrollBarSetStatus(p->sv.scrh, 0, size.w, p->sv.page->wg.w);
	mScrollBarSetStatus(p->sv.scrv, 0, size.h, p->sv.page->wg.h);

	if(reset)
	{
		if(APPCONF->fimgview & IMGVIEW_F_RIGHT_BINDING)
			//右綴じ時は、右端から
			x = size.w;
		else
			x = 0;
		
		mScrollBarSetPos(p->sv.scrh, x);
		mScrollBarSetPos(p->sv.scrv, 0);
	}

	APPWORK->view.ptscr.x = mScrollBarGetPos(p->sv.scrh);
	APPWORK->view.ptscr.y = mScrollBarGetPos(p->sv.scrv);
}

/* ページ値分水平スクロールして、上端/下端へ */

static void _scroll_horz_page(Canvas *p,int dir,int bottom)
{
	mScrollBarAddPos(p->sv.scrh, dir * mScrollBarGetPage(p->sv.scrh));
	mScrollBarSetPos(p->sv.scrv, (bottom)? mScrollBarGetMaxPos(p->sv.scrv): 0);
}

/* 端の状態で垂直スクロール時のページ移動が有効な時
 *
 * 垂直スクロールが、移動方向の端の位置にある状態で、
 * さらにスクロールした時、水平スクロールがページ方向の端でない場合は、
 * スクロールのページ値分移動して上端/下端から。
 * 水平方向も端の場合は、ページを移動する。
 * [!] ただし、上方向で前のページ移動時は、先頭のスクロール位置からとなる。 */

static mlkbool _scroll_vert_edge(Canvas *p,int w)
{
	int dir,prev_x,next_x;

	dir = (APPCONF->fimgview & IMGVIEW_F_RIGHT_BINDING)? -1: 1;

	prev_x = 0;
	next_x = mScrollBarGetMaxPos(p->sv.scrh);

	if(dir < 0)
		prev_x = next_x, next_x = 0;

	//

	if(w < 0 && mScrollBarIsPos_top(p->sv.scrv))
	{
		//上へ
		
		if(APPWORK->view.ptscr.x == prev_x)
			_send_command_event(TRMENU_CTRL_PREV_PAGE);
		else
			_scroll_horz_page(p, -dir, TRUE);

		return TRUE;
	}
	else if(w > 0 && mScrollBarIsPos_bottom(p->sv.scrv))
	{
		//下へ

		if(APPWORK->view.ptscr.x == next_x)
			_send_command_event(TRMENU_CTRL_NEXT_PAGE);
		else
			_scroll_horz_page(p, dir, FALSE);
		
		return TRUE;
	}

	return FALSE;
}

/** 画像をスクロール (キー or ボタン操作)
 *
 * dir: [0]上 [1]下 [2]左 [3]右 */

void MainWinCanvas_moveScroll(int dir)
{
	Canvas *p = (Canvas *)APPDATA->canvas;
	mScrollBar *scr;
	int n,ret;

	//対象スクロールバーと幅

	if(dir < 2)
	{
		scr = p->sv.scrv;
		n = p->sv.page->wg.h >> 1;
	}
	else
	{
		scr = p->sv.scrh;
		n = p->sv.page->wg.w >> 1;
	}

	//方向

	if(!(dir & 1)) n = -n;

	//スクロール
	
	ret = FALSE;

	if(dir < 2 && (APPCONF->foption & OPTION_F_VSCROLL_MOVE_PAGE))
		ret = _scroll_vert_edge(p, n);

	if(!ret)
		ret = mScrollBarAddPos(scr, n);

	//スクロール位置変更

	if(ret)
	{
		APPWORK->view.ptscr.x = mScrollBarGetPos(p->sv.scrh);
		APPWORK->view.ptscr.y = mScrollBarGetPos(p->sv.scrv);

		mWidgetRedraw(MLK_WIDGET(p->sv.page));
	}
}
