/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * 環境設定のページ2 (ボタン操作)
 *****************************************/

#include <mlk_gui.h>
#include <mlk_widget_def.h>
#include <mlk_widget.h>
#include <mlk_window.h>
#include <mlk_event.h>
#include <mlk_pager.h>
#include <mlk_label.h>
#include <mlk_button.h>
#include <mlk_checkbutton.h>
#include <mlk_combobox.h>
#include <mlk_listview.h>
#include <mlk_listheader.h>
#include <mlk_columnitem.h>
#include <mlk_str.h>
#include <mlk_buf.h>

#include "def_optbutton.h"

#include "pv_envdlg.h"


//-------------

typedef struct
{
	mListView *list;
	mComboBox *cb_com;
}_pagedata_button;

enum
{
	WID_LIST = 100,
	WID_CB_COMMAND,
	WID_BTT_ADD,
	WID_BTT_DEL
};

enum
{
	TRID_BUTTON = 1600,
	TRID_COMMAND,
	TRID_ADDBUTTON,
	TRID_DELETE,
	TRID_BUTTON_NO,
	TRID_BUTTON_SPECIFY,
	TRID_BUTTON_SET,
	TRID_MODKEY,

	TRID_COMMAND_TOP = 1700
};

static const char *g_name_button[] = {
	"Left","Right","Middle","ScrUp","ScrDown","ScrLeft","ScrRight"
};

//-------------


//**********************************
// ボタン追加ダイアログ
//**********************************


typedef struct
{
	MLK_DIALOG_DEF

	mComboBox *cb_btt;
	mLabel *label;
	mCheckButton *ck_ctrl,*ck_shift;
}_dialog;


/* ボタン番号イベント */

static int _buttonno_event(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_POINTER
		&& ev->pt.act == MEVENT_POINTER_ACT_PRESS
		&& ev->pt.raw_btt >= 0 && ev->pt.raw_btt <= OPTBUTTON_MAX_BTTNO)
	{
		wg->param1 = ev->pt.raw_btt;

		mLabelSetText_int(MLK_LABEL(wg), ev->pt.raw_btt);
	}

	return 1;
}

/* ダイアログ作成 */

static _dialog *_create_dialog(mWindow *parent)
{
	_dialog *p;
	mWidget *ctg,*ct;
	mComboBox *cb;
	int i;

	p = (_dialog *)mDialogNew(parent, sizeof(_dialog), MTOPLEVEL_S_DIALOG_NORMAL);
	if(!p) return NULL;

	p->wg.event = mDialogEventDefault_okcancel;
	p->ct.sep = 15;

	mContainerSetPadding_same(MLK_CONTAINER(p), 8);

	mToplevelSetTitle(MLK_TOPLEVEL(p), MLK_TR(TRID_ADDBUTTON));

	ctg = mContainerCreateGrid(MLK_WIDGET(p), 2, 7, 10, MLF_EXPAND_W, 0);

	//ボタン

	mLabelCreate(ctg, MLF_MIDDLE, 0, 0, MLK_TR(TRID_BUTTON));

	p->cb_btt = cb = mComboBoxCreate(ctg, 0, 0, 0, 0);

	for(i = 0; i < 7; i++)
		mComboBoxAddItem_static(cb, g_name_button[i], i + 1);

	mComboBoxAddItem_static(cb, MLK_TR(TRID_BUTTON_SPECIFY), -1);

	mComboBoxSetAutoWidth(cb);
	mComboBoxSetSelItem_atIndex(cb, 0);

	//ボタン番号

	mLabelCreate(ctg, MLF_MIDDLE, 0, 0, MLK_TR(TRID_BUTTON_NO));

	p->label = mLabelCreate(ctg, MLF_EXPAND_W, 0, MLABEL_S_BORDER, MLK_TR(TRID_BUTTON_SET));

	p->label->wg.fevent |= MWIDGET_EVENT_POINTER;
	p->label->wg.event = _buttonno_event;

	//装飾キー

	mLabelCreate(ctg, MLF_MIDDLE, 0, 0, MLK_TR(TRID_MODKEY));

	ct = mContainerCreateHorz(ctg, 6, 0, 0);

	p->ck_ctrl = mCheckButtonCreate(ct, 0, 0, 0, 0, "Ctrl", FALSE);
	p->ck_shift = mCheckButtonCreate(ct, 0, 0, 0, 0, "Shift", FALSE);

	//OK/キャンセル

	mContainerCreateButtons_okcancel(MLK_WIDGET(p), 0);

	return p;
}

/** ダイアログ実行
 *
 * return: ボタンの値。-1 でキャンセル */

static int _addbutton_dlg(mWindow *parent)
{
	_dialog *p;
	int ret = -1,btt;

	p = _create_dialog(parent);
	if(!p) return -1;

	mWindowResizeShow_initSize(MLK_WINDOW(p));

	if(mDialogRun(MLK_DIALOG(p), FALSE))
	{
		ret = 0;

		//ボタン
		
		btt = mComboBoxGetItemParam(p->cb_btt, -1);

		if(btt == -1)
		{
			ret |= OPTBUTTON_F_RAW;
			btt = p->label->wg.param1;
			if(btt == 0) btt = 1;
		}

		ret |= OPTBUTTON_MAKE(btt, 0);

		//

		if(mCheckButtonIsChecked(p->ck_ctrl))
			ret |= OPTBUTTON_F_CTRL;

		if(mCheckButtonIsChecked(p->ck_shift))
			ret |= OPTBUTTON_F_SHIFT;
	}

	mWidgetDestroy(MLK_WIDGET(p));

	return ret;
}


//**********************************
// ページ
//**********************************


/* リスト文字列取得 */

static void _get_item_text(mStr *str,uint16_t val)
{
	int btt,cmd;

	mStrEmpty(str);

	//ボタン名

	btt = OPTBUTTON_GET_BUTTON(val);

	if(val & OPTBUTTON_F_CTRL)
		mStrAppendText(str, "Ctrl+");

	if(val & OPTBUTTON_F_SHIFT)
		mStrAppendText(str, "Shift+");

	if(val & OPTBUTTON_F_RAW)
		mStrAppendInt(str, btt);
	else
		mStrAppendText(str, g_name_button[btt - 1]);

	//コマンド

	cmd = OPTBUTTON_GET_COMMAND(val);

	mStrAppendChar(str, '\t');
	mStrAppendText(str, MLK_TR(TRID_COMMAND_TOP + cmd));
}

/* リストセット */

static void _set_list(mListView *list,mBuf *buf)
{
	int i;
	uint16_t *ps;
	mStr str = MSTR_INIT;

	ps = (uint16_t *)buf->buf;

	for(i = buf->cursize / 2; i > 0; i--, ps++)
	{
		_get_item_text(&str, *ps);

		mListViewAddItem_text_copy_param(list, str.buf, *ps);
	}

	mStrFree(&str);
}

/* リスト内に同じボタンがあるか */

static mlkbool _check_same_button(mListView *list,int val)
{
	mColumnItem *pi;

	val = OPTBUTTON_GET_BUTTON_FULL(val);

	for(pi = mListViewGetTopItem(list); pi; pi = (mColumnItem *)pi->i.next)
	{
		if(OPTBUTTON_GET_BUTTON_FULL(pi->param) == val)
			return TRUE;
	}

	return FALSE;
}

/* ボタン追加 */

static void _add_button(mPager *p,_pagedata_button *pd)
{
	mStr str = MSTR_INIT;
	int val;

	val = _addbutton_dlg(p->wg.toplevel);
	if(val < 0) return;

	//同じボタンがあれば追加しない

	if(_check_same_button(pd->list, val))
		return;

	//アイテム追加

	_get_item_text(&str, val);

	mListViewAddItem_text_copy_param(pd->list, str.buf, val);

	mStrFree(&str);
}

/* コマンド変更 */

static void _change_command(_pagedata_button *pd,int cmd)
{
	mColumnItem *pi;

	pi = mListViewGetFocusItem(pd->list);
	if(!pi) return;

	OPTBUTTON_SET_COMMAND(pi->param, cmd);

	mListViewSetItemColumnText(pd->list, pi, 1, MLK_TR(TRID_COMMAND_TOP + cmd));
}

/* イベント */

static int _button_event(mPager *p,mEvent *ev,void *pagedat)
{
	_pagedata_button *pd = (_pagedata_button *)pagedat;

	if(ev->type == MEVENT_NOTIFY)
	{
		switch(ev->notify.id)
		{
			//リスト
			case WID_LIST:
				if(ev->notify.notify_type == MLISTVIEW_N_CHANGE_FOCUS)
				{
					//フォーカス変更時、コマンドをセット

					mComboBoxSetSelItem_atIndex(pd->cb_com,
						OPTBUTTON_GET_COMMAND( ((mColumnItem *)ev->notify.param1)->param ) );
				}
				break;
			//ボタン追加
			case WID_BTT_ADD:
				_add_button(p, pd);
				break;
			//削除
			case WID_BTT_DEL:
				mListViewDeleteItem_focus(pd->list);
				break;

			//コマンド選択
			case WID_CB_COMMAND:
				if(ev->notify.notify_type == MCOMBOBOX_N_CHANGE_SEL)
					_change_command(pd, ev->notify.param2);
				break;
		}
	}

	return 1;
}

/* データ取得 */

static mlkbool _button_getdata(mPager *p,void *pagedat,void *dst)
{
	_pagedata_button *pd = (_pagedata_button *)pagedat;
	EnvOptData *dat = (EnvOptData *)dst;
	mColumnItem *pi;
	uint16_t val;

	mBufReset(&dat->buf_button);

	for(pi = mListViewGetTopItem(pd->list); pi; pi = (mColumnItem *)pi->i.next)
	{
		val = pi->param;
		mBufAppend(&dat->buf_button, &val, 2);
	}

	return TRUE;
}

/** "ボタン操作" ページ作成 */

mlkbool EnvDlg_createPage_button(mPager *p,mPagerInfo *info)
{
	_pagedata_button *pd;
	EnvOptData *dat;
	mWidget *ct;
	mListHeader *lh;

	pd = (_pagedata_button *)mMalloc0(sizeof(_pagedata_button));
	if(!pd) return FALSE;

	info->pagedat = pd;
	info->getdata = _button_getdata;
	info->event = _button_event;

	dat = (EnvOptData *)mPagerGetDataPtr(p);

	//----------

	//リスト

	pd->list = mListViewCreate(MLK_WIDGET(p), WID_LIST, MLF_EXPAND_WH, 0,
		MLISTVIEW_S_MULTI_COLUMN | MLISTVIEW_S_HAVE_HEADER | MLISTVIEW_S_GRID_COL,
		MSCROLLVIEW_S_HORZVERT_FRAME);

	lh = mListViewGetHeaderWidget(pd->list);

	mListHeaderAddItem(lh, MLK_TR(TRID_BUTTON), 100, 0, 0);
	mListHeaderAddItem(lh, MLK_TR(TRID_COMMAND), 100, MLISTHEADER_ITEM_F_EXPAND, 0);

	_set_list(pd->list, &dat->buf_button);

	mListViewSetColumnWidth_auto(pd->list, 0);

	//ボタン

	ct = mContainerCreateHorz(MLK_WIDGET(p), 3, 0, MLK_MAKE32_4(0,6,0,10));

	mButtonCreate(ct, WID_BTT_ADD, 0, 0, 0, MLK_TR(TRID_ADDBUTTON));
	mButtonCreate(ct, WID_BTT_DEL, 0, 0, 0, MLK_TR(TRID_DELETE));

	//コマンド

	ct = mContainerCreateHorz(MLK_WIDGET(p), 6, 0, 0);

	mLabelCreate(ct, MLF_MIDDLE, 0, 0, MLK_TR(TRID_COMMAND));

	pd->cb_com = mComboBoxCreate(ct, WID_CB_COMMAND, MLF_EXPAND_W, 0, 0);

	mComboBoxAddItems_tr(pd->cb_com, TRID_COMMAND_TOP, OPTBUTTON_CMD_NUM, 0);
	mComboBoxSetAutoWidth(pd->cb_com);
	mComboBoxSetSelItem_atIndex(pd->cb_com, 0);

	return TRUE;
}

