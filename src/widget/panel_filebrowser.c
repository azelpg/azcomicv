/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * パネル:ファイルブラウザ
 *****************************************/

#include <mlk_gui.h>
#include <mlk_widget_def.h>
#include <mlk_widget.h>
#include <mlk_window.h>
#include <mlk_panel.h>
#include <mlk_lineedit.h>
#include <mlk_listview.h>
#include <mlk_sysdlg.h>
#include <mlk_event.h>
#include <mlk_menu.h>
#include <mlk_str.h>
#include <mlk_dir.h>
#include <mlk_file.h>
#include <mlk_filestat.h>
#include <mlk_columnitem.h>

#include "def_appdata.h"
#include "def_config.h"
#include "def_work.h"

#include "panel.h"
#include "mainwindow.h"
#include "trid.h"

//-------------------

typedef struct
{
	MLK_CONTAINER_DEF

	mLineEdit *edit_dir;
	mListView *list;
}_topwg;

typedef struct
{
	mColumnItem i;
	uint64_t modify;	//更新日時
}_listitem;

//-------------------

enum
{
	ITEMPARAM_PARENT,	//親へ
	ITEMPARAM_DIR,		//ディレクトリ
	ITEMPARAM_FILE
};

enum
{
	WID_LIST = 100
};

enum
{
	TRID_MENU_OPEN,
	TRID_MENU_SETDIR,
	TRID_MENU_RECENT,
	TRID_MENU_REFRESH,
	TRID_MENU_SHOW_HIDDEN_FILES,
	TRID_MENU_SORT_SUB,
	TRID_MENU_SORT_FILENAME,
	TRID_MENU_SORT_MODIFY,
	TRID_MENU_SORT_REVERSE,

	MENUID_RECENT_TOP = 1000
};

//右クリックメニュー

static const uint16_t g_menudat[] = {
	TRID_MENU_OPEN, MMENU_ARRAY16_SEP,
	TRID_MENU_SETDIR, TRID_MENU_RECENT, TRID_MENU_REFRESH, MMENU_ARRAY16_SEP,
	TRID_MENU_SHOW_HIDDEN_FILES,
	TRID_MENU_SORT_SUB,
	MMENU_ARRAY16_SUB_START,
		TRID_MENU_SORT_FILENAME|MMENU_ARRAY16_RADIO,
		TRID_MENU_SORT_MODIFY|MMENU_ARRAY16_RADIO,
		MMENU_ARRAY16_SEP,
		TRID_MENU_SORT_REVERSE|MMENU_ARRAY16_CHECK,
	MMENU_ARRAY16_SUB_END,
	MMENU_ARRAY16_END
};

/* main.c */
int sortfunc_in_directory(const char *name1,uint64_t modify1,const char *name2,uint64_t modify2);

//-------------------


//========================
// sub
//========================


/* _topwg を取得 */

static _topwg *_get_topwg(void)
{
	return (_topwg *)mPanelGetContents(APPDATA->panel[PANEL_FILEBROWSER]);
}

/* パスを正規化 */

static void _path_normalize(void)
{
	mStr *str = &APPCONF->strFilebrowserDir;

	//空、または存在しないパスの場合は、ホームディレクトリ

	if(mStrIsEmpty(str) || !mIsExistDir(str->buf))
		mStrPathSetHome(str);
}

/* アイテムソート関数 */

static int _sort_func(mListItem *item1,mListItem *item2,void *param)
{
	_listitem *p1,*p2;

	p1 = (_listitem *)item1;
	p2 = (_listitem *)item2;

	//タイプ別

	if(p1->i.param < p2->i.param)
		return -1;
	else if(p1->i.param > p2->i.param)
		return 1;
	else
		return sortfunc_in_directory(p1->i.text, p1->modify, p2->i.text, p2->modify);
}

/* アイテム追加
 *
 * st: NULL 以外で、更新日時セット
 * copy: 文字列を複製するか */

static void _append_item(mListView *list,const char *name,int type,mFileStat *st,mlkbool copy)
{
	_listitem *pi;
	int icon;

	if(type == ITEMPARAM_FILE)
		icon = FILEICON_FILE;
	else if(type == ITEMPARAM_DIR)
		icon = FILEICON_DIR;
	else
		icon = FILEICON_PARENT;

	pi = (_listitem *)mListViewAddItem_size(list, sizeof(_listitem), name, icon,
		(copy)? MCOLUMNITEM_F_COPYTEXT: 0, type);

	if(pi && st)
		pi->modify = st->time_modify;
}

/* リストセット */

static void _set_list(_topwg *p)
{
	mListView *list = p->list;
	mDir *dir;
	const char *fname;
	mFileStat st;
	int show_hidden,is_top;

	_path_normalize();

	is_top = mStrPathIsTop(&APPCONF->strFilebrowserDir);

	mListViewDeleteAllItem(list);

	//場所

	mLineEditSetText(p->edit_dir, APPCONF->strFilebrowserDir.buf);

	//------- ディレクトリ内ファイル

	dir = mDirOpen(APPCONF->strFilebrowserDir.buf);

	if(dir)
	{
		show_hidden = ((APPCONF->filebrowser_opt & FILEBROWSER_OPT_F_SHOW_HIDDEN_FILES) != 0);

		while(mDirNext(dir))
		{
			fname = mDirGetFilename(dir);
			mDirGetStat(dir, &st);

			if(st.flags & MFILESTAT_F_DIRECTORY)
			{
				//ディレクトリ

				if(mDirIsSpecName(dir))
				{
					//親へ

					if(!is_top && mDirIsSpecName_parent(dir))
						_append_item(list, "..", ITEMPARAM_PARENT, NULL, FALSE);
				}
				else if(show_hidden || !mDirIsHiddenFile(dir))
					//隠しファイルを隠す場合は、除く
					_append_item(list, fname, ITEMPARAM_DIR, &st, TRUE);
			}
			else
			{
				//ファイル

				if(show_hidden || !mDirIsHiddenFile(dir))
					_append_item(list, fname, ITEMPARAM_FILE, &st, TRUE);
			}
		}

		mDirClose(dir);
	}
	
	//ソート

	mListViewSortItem(list, _sort_func, NULL);

	mListViewSetAutoWidth(list, FALSE);
}

/* 同期時、ファイルアイテム選択 */

static void _sync_item(_topwg *p)
{
	mStr str = MSTR_INIT;

	mStrPathGetBasename(&str, APPWORK->strOpenFile.buf);

	mListViewSetFocusItem_text(p->list, str.buf);
	mListViewScrollToFocus_margin(p->list, 2);

	mStrFree(&str);
}

/* ファイルを開く */

static void _open_file(_topwg *p,mColumnItem *item)
{
	mStr str = MSTR_INIT;

	mStrCopy(&str, &APPCONF->strFilebrowserDir);
	mStrPathJoin(&str, item->text);

	MainWindow_open(str.buf, MAINWIN_OPEN_FILEBROWSER);

	mStrFree(&str);
}

/* ダブルクリック時 */

static void _event_dblclk(_topwg *p,mColumnItem *item)
{
	switch(item->param)
	{
		//親へ
		case ITEMPARAM_PARENT:
			mStrPathRemoveBasename(&APPCONF->strFilebrowserDir);
			_set_list(p);
			break;
		//ディレクトリ
		case ITEMPARAM_DIR:
			mStrPathJoin(&APPCONF->strFilebrowserDir, item->text);
			_set_list(p);
			break;
		//ファイル
		case ITEMPARAM_FILE:
			_open_file(p, item);
			break;
	}
}

/* ディレクトリ移動 */

static void _cmd_set_dir(_topwg *p)
{
	mStr str = MSTR_INIT;

	if(mSysDlg_selectDir(
		Panel_getDialogParent(PANEL_FILEBROWSER),
		APPCONF->strFilebrowserDir.buf, 0, &str))
	{
		mStrCopy(&APPCONF->strFilebrowserDir, &str);
		mStrFree(&str);

		_set_list(p);
	}
}

/* 右クリックメニュー実行 */

static void _run_menu(_topwg *p,mColumnItem *item,int x,int y)
{
	mMenu *menu,*sub;
	mMenuItem *pi;
	uint32_t opt;
	int id;

	opt = APPCONF->filebrowser_opt;

	//----- メニュー

	MLK_TRGROUP(TRGROUP_FILEBROWSER);

	menu = mMenuNew();

	mMenuAppendTrText_array16_radio(menu, g_menudat);

	//開く

	if(!item)
		mMenuSetItemEnable(menu, TRID_MENU_OPEN, 0);

	//履歴 sub

	sub = mMenuNew();

	mMenuAppendStrArray(sub, APPCONF->strRecent[RECENTFILE_ARRAY_DIR],
		MENUID_RECENT_TOP, CONFIG_RECENTFILE_NUM);

	mMenuSetItemSubmenu(menu, TRID_MENU_RECENT, sub);

	//隠しファイル表示

	mMenuSetItemCheck(menu, TRID_MENU_SHOW_HIDDEN_FILES,
		opt & FILEBROWSER_OPT_F_SHOW_HIDDEN_FILES);

	//ソート順

	mMenuSetItemCheck(menu,
		TRID_MENU_SORT_FILENAME + FILEBROWSER_OPT_GET_SORTTYPE(opt), 1);

	mMenuSetItemCheck(menu, TRID_MENU_SORT_REVERSE,
		opt & FILEBROWSER_OPT_F_SORT_REVERSE);

	//実行

	pi = mMenuPopup(menu, MLK_WIDGET(p->list), x, y, NULL,
		MPOPUP_F_LEFT | MPOPUP_F_TOP, NULL);
	
	id = (pi)? mMenuItemGetID(pi): -1;

	mMenuDestroy(menu);

	//----- コマンド

	if(id == -1) return;

	switch(id)
	{
		//開く
		case TRID_MENU_OPEN:
			_open_file(p, item);
			break;
		//ディレクトリ変更
		case TRID_MENU_SETDIR:
			_cmd_set_dir(p);
			break;
		//更新
		case TRID_MENU_REFRESH:
			_set_list(p);
			break;
		//隠しファイルを表示
		case TRID_MENU_SHOW_HIDDEN_FILES:
			APPCONF->filebrowser_opt ^= FILEBROWSER_OPT_F_SHOW_HIDDEN_FILES;

			_set_list(p);
			break;
		//ソート順
		case TRID_MENU_SORT_FILENAME:
		case TRID_MENU_SORT_MODIFY:
			FILEBROWSER_OPT_SET_SORTTYPE(APPCONF->filebrowser_opt, id - TRID_MENU_SORT_FILENAME);

			mListViewSortItem(p->list, _sort_func, NULL);
			break;
		//ソート順:逆順
		case TRID_MENU_SORT_REVERSE:
			APPCONF->filebrowser_opt ^= FILEBROWSER_OPT_F_SORT_REVERSE;
			mListViewSortItem(p->list, _sort_func, NULL);
			break;
		//履歴
		default:
			mStrCopy(&APPCONF->strFilebrowserDir,
				APPCONF->strRecent[RECENTFILE_ARRAY_DIR] + (id - MENUID_RECENT_TOP));

			_set_list(p);
			break;
	}
}


//========================
//
//========================


/* イベント */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_NOTIFY && ev->notify.id == WID_LIST)
	{
		_topwg *p = (_topwg *)wg;
		
		if(ev->notify.notify_type == MLISTVIEW_N_ITEM_L_DBLCLK)
			//ダブルクリック
			_event_dblclk(p, (mColumnItem *)ev->notify.param1);
		else if(ev->notify.notify_type == MLISTVIEW_N_ITEM_R_CLICK)
		{
			//右クリック

			_run_menu(p, (mColumnItem *)ev->notify.param1,
				ev->notify.param2 >> 16, ev->notify.param2 & 0xffff);
		}
	}

	return 1;
}

/* パネルの内容作成 */

static mWidget *_create_handle(mPanel *panel,int id,mWidget *parent)
{
	_topwg *p;
	mListView *list;

	p = (_topwg *)mContainerNew(parent, sizeof(_topwg));

	p->wg.flayout = MLF_EXPAND_WH;
	p->wg.event = _event_handle;
	p->wg.notify_to = MWIDGET_NOTIFYTO_SELF;

	//パス

	p->edit_dir = mLineEditCreate(MLK_WIDGET(p), 0, MLF_EXPAND_W, MLK_MAKE32_4(3,2,3,2),
		MLINEEDIT_S_READ_ONLY | MLINEEDIT_S_NO_FRAME);

	//リストビュー

	p->list = list = mListViewCreate(MLK_WIDGET(p), WID_LIST, MLF_EXPAND_WH, 0, 0,
		MSCROLLVIEW_S_HORZVERT_FRAME);

	mListViewSetImageList(list, APPDATA->imglist_fileicon);

	_set_list(p);
	
	return (mWidget *)p;
}

//===================


/** パネル作成 */

void PanelFileBrowser_new(mPanelState *state)
{
	mPanel *p;

	p = Panel_create(PANEL_FILEBROWSER, 0, 0, state, _create_handle);

	mPanelCreateWidget(p);
}

/** 開いた時の同期 */

void PanelFileBrowser_sync_opened(void)
{
	_topwg *p = _get_topwg();

	if(p)
	{
		_set_list(p);

		//アーカイブ時、ファイルを選択

		if(APPWORK->opentype == OPENTYPE_ARCHIVE)
			_sync_item(p);
	}
}
