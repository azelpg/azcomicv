/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * MainWindow : ファイル関連
 *****************************************/

#include <string.h>

#include <mlk_gui.h>
#include <mlk_widget_def.h>
#include <mlk_window.h>
#include <mlk_iconbar.h>
#include <mlk_menu.h>
#include <mlk_sysdlg.h>
#include <mlk_str.h>
#include <mlk_file.h>
#include <mlk_filestat.h>
#include <mlk_list.h>
#include <mlk_filelist.h>

#include "def_appdata.h"
#include "def_config.h"
#include "def_work.h"

#include "def_mainwin.h"
#include "mainwindow.h"
#include "mainwin_func.h"
#include "panel_func.h"
#include "work_main.h"

#include "trid.h"
#include "trid_menu.h"


//----------------

//main.c
int sortfunc_in_directory(const char *name1,uint64_t modify1,const char *name2,uint64_t modify2);

//----------------


//=========================
// 履歴
//=========================


/** 現在のファイルを閉じる時、履歴にセット */

void MainWindow_setRecentOnClose(MainWindow *p)
{
	mStr *pstr,str = MSTR_INIT, str1 = MSTR_INIT;
	int i;

	if(!APPWORK->item_cur) return;

	pstr = APPCONF->strRecent[RECENTFILE_ARRAY_FILE];

	//履歴内に同じパスがあるか

	for(i = 0; i < CONFIG_RECENTFILE_NUM; i++)
	{
		mStrCopy(&str, pstr + i);
		mStrFindChar_toend(&str, '\t');

		if(mStrPathCompareEq(&str, APPWORK->strOpenFile.buf))
			break;
	}

	//なければ、新規追加

	if(i == CONFIG_RECENTFILE_NUM) i = -1;

	//セット文字列

	workFileList_getItemName_fullpath(&str1, APPWORK->item_cur);

	mStrCopy(&str, &APPWORK->strOpenFile);
	mStrAppendChar(&str, '\t');
	mStrAppendStr(&str, &str1);

	//セット

	mStrArraySetRecent(pstr, CONFIG_RECENTFILE_NUM, i, str.buf);

	mStrFree(&str);
	mStrFree(&str1);
}

/** 各履歴のメニューをセット */

void MainWindow_setRecentMenu(MainWindow *p,int type)
{
	mMenu *menu;
	mStr *pstr;
	int i,idtop;

	menu = p->menu_recent[type];
	pstr = APPCONF->strRecent[type];
	idtop = MAINWIN_CMDID_RECENTFILE_TOP + type * CONFIG_RECENTFILE_NUM;

	//削除

	mMenuDeleteAll(menu);

	//セット

	if(type != RECENTFILE_ARRAY_FILE)
	{
		//--- アーカイブ,ディレクトリ
		
		mMenuAppendStrArray(menu, pstr, idtop, CONFIG_RECENTFILE_NUM);
	}
	else
	{
		//--- ファイル

		mStr str = MSTR_INIT, str1 = MSTR_INIT;

		for(i = 0; i < CONFIG_RECENTFILE_NUM; i++)
		{
			if(mStrIsEmpty(pstr + i)) break;

			//ファイル名
			mStrGetSplitText(&str, pstr[i].buf, '\t', 1);
			mStrPathGetBasename(&str1, str.buf);

			//パス名
			mStrGetSplitText(&str, pstr[i].buf, '\t', 0);
			
			mStrAppendFormat(&str, " | %t", &str1);

			mMenuAppendText_copy(menu, idtop + i, str.buf, str.len);
		}

		mStrFree(&str);
		mStrFree(&str1);
	}

	//履歴の消去

	if(mStrIsnotEmpty(pstr))
	{
		mMenuAppendSep(menu);

		mMenuAppendText(menu, MAINWIN_CMDID_RECENT_FILE_CLEAR + type,
			MLK_TR2(TRGROUP_MAINMENU, TRMENU_FILE_RECENT_CLEAR));
	}
}

/** 最近閉じたファイル: ツールバーのボタン時 */

void MainWindow_runMenu_recentFile(MainWindow *p)
{
	mBox box;

	//履歴が一つもない (先頭が空)

	if(mStrIsEmpty(APPCONF->strRecent[RECENTFILE_ARRAY_FILE]))
		return;

	//メインメニューの mMenu を使う

	mIconBarGetItemBox(MLK_ICONBAR(p->toolbar), TRMENU_FILE_RECENT_FILE, &box);

	mMenuPopup(p->menu_recent[RECENTFILE_ARRAY_FILE], MLK_WIDGET(p->toolbar),
		0, 0, &box,
		MPOPUP_F_LEFT | MPOPUP_F_BOTTOM | MPOPUP_F_FLIP_X | MPOPUP_F_MENU_SEND_COMMAND,
		MLK_WIDGET(p));
}


//=========================
// 開く
//=========================


/* 開く時のエラーメッセージ
 *
 * err: -1 で画像ファイルが一つもない */

static void _open_errmes(MainWindow *p,mlkerr err)
{
	mStr str = MSTR_INIT;
	int id;

	mStrCopy(&str, &APPWORK->strOpenFile);
	mStrAppendText(&str, "\n\n");

	switch(err)
	{
		case -1: id = TRID_MES_ERR_NO_IMAGE; break;
		case MLKERR_OPEN: id = TRID_MES_ERR_OPEN; break;
		case MLKERR_DAMAGED: id = TRID_MES_ERR_OPEN; break;
		case MLKERR_UNSUPPORTED: id = TRID_MES_ERR_UNSUPPORTED; break;
		default: id = TRID_MES_ERROR; break;
	}
	
	mStrAppendText(&str, MLK_TR2(TRGROUP_MESSAGE, id));

	mMessageBoxErr(MLK_WINDOW(p), str.buf);

	mStrFree(&str);
}

/** 各履歴から開く
 *
 * no: 履歴のインデックス位置 */

void MainWindow_openRecent(MainWindow *p,int type,int no)
{
	mStr *pstr,str = MSTR_INIT;
	int ret;

	pstr = APPCONF->strRecent[type];

	//ファイル/ディレクトリが存在するか

	mStrCopy(&str, pstr + no);

	if(type == RECENTFILE_ARRAY_FILE)
		mStrFindChar_toend(&str, '\t');

	ret = mIsExistPath(str.buf);

	mStrFree(&str);

	//

	if(ret)
	{
		//開く
		
		MainWindow_open(pstr[no].buf,
			(type == RECENTFILE_ARRAY_FILE)? MAINWIN_OPEN_PATH_AND_FILE: MAINWIN_OPEN_NORMAL);
	}
	else
	{
		//----- 存在しない場合

		//メッセージ
		
		MainWindow_showMessage(TRID_MES_RECENT_UNEXIST);

		//履歴から削除
		
		mStrArrayShiftUp(pstr, no, CONFIG_RECENTFILE_NUM - 1);

		MainWindow_setRecentMenu(p, type);
	}
}

/** 開く処理 */

void MainWindow_open(const char *path,int type)
{
	MainWindow *p;
	mStr strfname = MSTR_INIT, strdir = MSTR_INIT,
		strpath = MSTR_INIT, strpagefile = MSTR_INIT;
	mFileStat st;
	mlkerr ret;

	p = APPDATA->mainwin;

	//特殊時、パス取得
	// :strpath = 読み込むパス
	// :strpagefile = ページのファイル名

	if(type == MAINWIN_OPEN_RELOAD)
	{
		//----- 再読込

		if(OPENTYPE_IS_EMPTY()) return;

		mStrCopy(&strpath, &APPWORK->strOpenFile);
		path = strpath.buf;

		workFileList_getItemName_fullpath(&strpagefile, APPWORK->item_cur);
	}
	else if(type == MAINWIN_OPEN_PATH_AND_FILE)
	{
		//----- "パス\tファイル" の文字列から

		mStrSetText(&strpath, path);
		mStrFindChar_toend(&strpath, '\t');

		mStrGetSplitText(&strpagefile, path, '\t', 1);

		path = strpath.buf;
	}

	//----------

	//閉じる

	MainWindow_closeFile(p);

	//-------- ファイル情報取得

	//ファイル情報取得

	if(!mGetFileStat(path, &st))
		st.flags = 0;

	//ファイル名取得

	mStrPathGetBasename(&strfname, path);

	//ディレクトリパス取得

	if(st.flags & MFILESTAT_F_DIRECTORY)
		mStrSetText(&strdir, path);
	else
		mStrPathGetDir(&strdir, path);

	//------ 開く

	ret = workFile_open(path, &strfname, &strdir,
		((st.flags & MFILESTAT_F_DIRECTORY) != 0),
		strpagefile.buf);

	//失敗した場合でも、以下に続く

	//------ 履歴追加

	//アーカイブ

	if(APPWORK->opentype == OPENTYPE_ARCHIVE)
	{
		mStrArrayAddRecent(APPCONF->strRecent[RECENTFILE_ARRAY_ARCHIVE],
			CONFIG_RECENTFILE_NUM, path);
		
		MainWindow_setRecentMenu(p, RECENTFILE_ARRAY_ARCHIVE);
	}

	//ディレクトリ

	mStrArrayAddRecent(APPCONF->strRecent[RECENTFILE_ARRAY_DIR],
		CONFIG_RECENTFILE_NUM, strdir.buf);

	MainWindow_setRecentMenu(p, RECENTFILE_ARRAY_DIR);

	//------ 更新

	//タイトル

	MainWindow_setTitle(p);

	//ファイルブラウザ同期
	//(再読込時 or ファイルブラウザから開く時はそのまま)

	if((APPCONF->foption & OPTION_F_SYNC_FILEBROWSER)
		&& type != MAINWIN_OPEN_RELOAD
		&& type != MAINWIN_OPEN_FILEBROWSER)
	{
		mStrCopy(&APPCONF->strFilebrowserDir, &strdir);
		PanelFileBrowser_sync_opened();
	}

	//解放

	mStrFree(&strfname);
	mStrFree(&strdir);
	mStrFree(&strpath);
	mStrFree(&strpagefile);

	//成功時

	if(ret == MLKERR_OK)
	{
		//CMD_LASTPAGE = 前のファイルの終端位置を開く時
	
		MainWindow_updateOpened((type == MAINWIN_OPEN_CMD_LASTPAGE));
	}

	//エラーメッセージ

	if(ret)
		_open_errmes(p, ret);
	else if(APPWORK->imgfile_num == 0)
		_open_errmes(p, -1);
}

/** 開くダイアログ
 *
 * recentno: ディレクトリ履歴の番号 (初期ディレクトリとして使う) */

void MainWindow_openDialog(MainWindow *p,mlkbool directory,int recentno)
{
	mStr str = MSTR_INIT;
	char *initdir;

	//初期ディレクトリ

	initdir = APPCONF->strRecent[RECENTFILE_ARRAY_DIR][recentno].buf;

	//ダイアログ

	if(directory)
	{
		if(!mSysDlg_selectDir(MLK_WINDOW(p), initdir, 0, &str))
			return;
	}
	else
	{
		if(!mSysDlg_openFile(MLK_WINDOW(p), "All Files\t*", 0,
			initdir, 0, &str))
			return;
	}

	//開く

	MainWindow_open(str.buf, MAINWIN_OPEN_NORMAL);

	mStrFree(&str);
}

/** ツールバー・ドロップダウンからのファイル/ディレクトリ開く
 *
 * 初期ディレクトリを、ディレクトリ履歴から選択して、ダイアログを開く */

void MainWindow_openFromDropdown(MainWindow *p,mlkbool directory)
{
	mMenu *menu;
	mMenuItem *pi;
	mBox box;
	int no;

	//メニュー (ディレクトリ履歴)

	menu = mMenuNew();

	mMenuAppendStrArray(menu,
		APPCONF->strRecent[RECENTFILE_ARRAY_DIR], 0, CONFIG_RECENTFILE_NUM);

	mIconBarGetItemBox(MLK_ICONBAR(p->toolbar),
		(directory)? TRMENU_FILE_OPENDIR: TRMENU_FILE_OPENFILE,
		&box);

	pi = mMenuPopup(menu, p->toolbar, 0, 0, &box, MPOPUP_F_LEFT | MPOPUP_F_BOTTOM | MPOPUP_F_FLIP_X, NULL);
	no = (pi)? mMenuItemGetID(pi): -1;

	mMenuDestroy(menu);

	//開くダイアログ

	if(no != -1)
		MainWindow_openDialog(p, directory, no);
}



//==================================
// 次、前のアーカイブ/ディレクトリ
//==================================


typedef struct
{
	mStr *pstr,
		*pstrdir;
	mlkbool directory;
}_filelistinfo;


/* ソート関数 */

static int _filelist_sort(mListItem *item1,mListItem *item2,void *param)
{
	mFileListItem *p1,*p2;

	p1 = (mFileListItem *)item1;
	p2 = (mFileListItem *)item2;

	return sortfunc_in_directory(p1->name, p1->st.time_modify, p2->name, p2->st.time_modify);
}

/* アイテム追加判定関数 */

static int _func_filelist_add(const char *fname,const mFileStat *st,void *param)
{
	_filelistinfo *p = (_filelistinfo *)param;

	if(p->directory)
		return ((st->flags & MFILESTAT_F_DIRECTORY) != 0);
	else
	{
		mStrCopy(p->pstr, p->pstrdir);
		mStrPathJoin(p->pstr, fname);
		
		return workFile_checkArchive(p->pstr->buf);
	}
}

/* 次、前のファイル取得
 *
 * strdst: 検索パスが入っている。結果のフルパスがセットされる。 */

static mlkbool _get_nextprev_path(mStr *strdst,mlkbool directory,mlkbool next)
{
	mList list = MLIST_INIT;
	mStr str = MSTR_INIT;
	_filelistinfo info;
	mFileListItem *pi;

	//リスト作成

	info.pstr = &str;
	info.pstrdir = strdst;
	info.directory = directory;

	if(!mFileList_create(&list, strdst->buf, _func_filelist_add, &info))
		return FALSE;

	//ソート

	mListSort(&list, _filelist_sort, 0);

	//現在のファイルを検索

	mStrPathGetBasename(&str, APPWORK->strOpenFile.buf);

	pi = mFileList_find_name(&list, str.buf);

	//指定方向のファイルパスを取得

	if(pi)
	{
		if(next)
			pi = (mFileListItem *)pi->i.next;
		else
			pi = (mFileListItem *)pi->i.prev;

		if(pi)
			mStrPathJoin(strdst, pi->name);
	}

	//
	
	mStrFree(&str);
	mListDeleteAll(&list);

	return (pi != NULL);
}


/** 次 or 前のアーカイブ/ディレクトリを開く
 *
 * lastpage: 初期状態で終端位置へ移動する */

void MainWindow_openNextPrev(MainWindow *p,mlkbool next,mlkbool lastpage)
{
	mStr str = MSTR_INIT;

	if(OPENTYPE_IS_EMPTY()) return;

	//検索ディレクトリ

	mStrPathGetDir(&str, APPWORK->strOpenFile.buf);

	//パス取得 & 開く

	if(_get_nextprev_path(&str,
		(APPWORK->opentype == OPENTYPE_DIRECTORY), next))
	{
		MainWindow_open(str.buf,
			(lastpage)? MAINWIN_OPEN_CMD_LASTPAGE: MAINWIN_OPEN_NORMAL);
	}

	mStrFree(&str);
}

