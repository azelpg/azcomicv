/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * パネル処理
 *****************************************/

#include <mlk_gui.h>
#include <mlk_widget_def.h>
#include <mlk_panel.h>

#include "def_appdata.h"
#include "panel.h"
#include "trid.h"


//------------------
/* ID (配置順も兼ねる。値の小さいほうが上) */

static const uint8_t g_panel_id[] = {1,2,0};

//------------------


//=========================
// sub
//=========================


/* ソート用比較関数 */

static int _sort_handle(mPanel *p,int id1,int id2)
{
	return (id1 > id2)? 1: -1;
}

/* 内容作成時のハンドラ */

static mWidget *_create_handle(mPanel *p,int id,mWidget *parent)
{
	mToplevel *top;
	mFuncPanelCreate func;

	//ウィンドウモード時は、アクセラレータをセット
	// :パネルのウィンドウにフォーカスがある時でもショートカットキーが動作するように

	top = mPanelGetWindow(p);
	if(top)
		top->top.accelerator = MLK_TOPLEVEL(APPDATA->mainwin)->top.accelerator;

	//作成関数

	func = (mFuncPanelCreate)mPanelGetParam1(p);
	
	return (func)(p, id, parent);
}


//=========================
// main
//=========================


/** 作成
 *
 * panel_style: 追加のスタイル
 * window_style: 追加のウィンドウスタイル */

mPanel *Panel_create(int no,uint32_t panel_style,
	uint32_t window_style,mPanelState *state,mFuncPanelCreate create)
{
	mPanel *p;

	p = mPanelNew(g_panel_id[no], 0,
		MPANEL_S_HAVE_CLOSE | MPANEL_S_HAVE_STORE | MPANEL_S_HAVE_SHUT
		 | panel_style);

	APPDATA->panel[no] = p;

	mPanelSetParam1(p, (intptr_t)create);
	mPanelSetTitle(p, MLK_TR2(TRGROUP_PANEL_NAME, no));
	mPanelSetState(p, state);
	mPanelSetFont(p, APPDATA->font_panel);
	mPanelSetStoreParent(p, APPDATA->panelct);
	mPanelSetNotifyWidget(p, MLK_WIDGET(APPDATA->mainwin));

	mPanelSetFunc_create(p, _create_handle);
	mPanelSetFunc_sort(p, _sort_handle);

	mPanelSetWindowInfo(p, MLK_WINDOW(APPDATA->mainwin),
		MTOPLEVEL_S_NORMAL | MTOPLEVEL_S_PARENT | MTOPLEVEL_S_NO_INPUT_METHOD | window_style);

	return p;
}

/** 内容ウィジェットに対して処理ができる状態か
 *
 * 閉じている (作成されていない) 場合は FALSE。
 * 画面上に表示されていなくても、作成されていれば TRUE。 */

mlkbool Panel_isCreated(int no)
{
	return mPanelIsCreated(APPDATA->panel[no]);
}

/** 内容ウィジェットが画面に表示されている状態か
 *
 * 閉じている or 畳まれている or 非表示時は FALSE */

mlkbool Panel_isOnScreen(int no)
{
	return mPanelIsVisibleContents(APPDATA->panel[no]);
}

/** ダイアログ用の親ウィンドウ取得 */

mWindow *Panel_getDialogParent(int no)
{
	mToplevel *top;

	top = mPanelGetWindow(APPDATA->panel[no]);
	if(!top) top = MLK_TOPLEVEL(APPDATA->mainwin);

	return (mWindow *)top;
}


//============================
// 全パネル処理
//============================


/** すべて削除 (終了時) */

void Panel_all_destroy(void)
{
	mPanel **pp = APPDATA->panel;
	int i;

	for(i = 0; i < PANEL_NUM; i++, pp++)
		mPanelDestroy(*pp);
}

/** 初期表示処理 */

void Panel_all_show(void)
{
	mPanel **pp = APPDATA->panel;
	int i;

	for(i = 0; i < PANEL_NUM; i++, pp++)
		mPanelShowWindow(*pp);
}
