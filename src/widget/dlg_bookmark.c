/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * しおりリストダイアログ
 *****************************************/

#include <string.h>

#include <mlk_gui.h>
#include <mlk_widget_def.h>
#include <mlk_widget.h>
#include <mlk_window.h>
#include <mlk_event.h>
#include <mlk_button.h>
#include <mlk_listview.h>
#include <mlk_listheader.h>
#include <mlk_iconbar.h>
#include <mlk_imagelist.h>
#include <mlk_str.h>
#include <mlk_columnitem.h>

#include "def_appdata.h"
#include "def_config.h"
#include "bookmark.h"
#include "trid.h"


//---------------------

typedef struct
{
	MLK_DIALOG_DEF

	mListView *list;
	BkmItem *openitem;
}_dialog;

//---------------------

enum
{
	TRID_TITLE,
	TRID_CLOSE,
	TRID_FILENAME,
	TRID_FILEPOS,
	TRID_PATH,
	
	TRID_OPEN = 100,
	TRID_DELETE,
	TRID_UP,
	TRID_DOWN
};

enum
{
	WID_LIST = 100
};

//---------------------

static const unsigned char g_img_icon[]={
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x10,0x80,0x01,0xc0,0x03,
0x7c,0x00,0x1c,0x38,0xc0,0x03,0xc0,0x03,0x82,0x00,0x3e,0x7c,0xe0,0x07,0xc0,0x03,
0x02,0x0f,0x7c,0x3e,0xf0,0x0f,0xc0,0x03,0x02,0x10,0xf8,0x1f,0xf8,0x1f,0xc0,0x03,
0xc2,0xff,0xf0,0x0f,0xfc,0x3f,0xc0,0x03,0x22,0x80,0xe0,0x07,0xfe,0x7f,0xc0,0x03,
0x22,0x80,0xe0,0x07,0xc0,0x03,0xfe,0x7f,0x12,0x40,0xf0,0x0f,0xc0,0x03,0xfc,0x3f,
0x12,0x40,0xf8,0x1f,0xc0,0x03,0xf8,0x1f,0x0a,0x20,0x7c,0x3e,0xc0,0x03,0xf0,0x0f,
0x0a,0x20,0x3e,0x7c,0xc0,0x03,0xe0,0x07,0x06,0x10,0x1c,0x38,0xc0,0x03,0xc0,0x03,
0xfe,0x1f,0x08,0x10,0xc0,0x03,0x80,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00 };

//---------------------


//==========================
// sub
//==========================


/* リストセット */

static void _set_list(mListView *list)
{
	BkmItem *pi;
	char *pc;
	mStr str = MSTR_INIT,dst = MSTR_INIT;

	pi = (BkmItem *)APPDATA->list_bkm.top;

	for(; pi; pi = (BkmItem *)pi->i.next)
	{
		pc = strchr(pi->text, '\t');
		if(!pc) continue;

		mStrSetText_len(&str, pi->text, pc - pi->text);

		//ファイル名
		mStrPathGetBasename(&dst, str.buf);
		mStrAppendChar(&dst, '\t');

		//ファイル位置
		mStrAppendText(&dst, pc + 1);
		mStrAppendChar(&dst, '\t');

		//パス
		mStrPathRemoveBasename(&str);
		mStrAppendStr(&dst, &str);
		
		mListViewAddItem_text_static_param(list, dst.buf, (intptr_t)pi);
	}

	mStrFree(&str);
	mStrFree(&dst);
}

/* 選択されているアイテム取得 */

static BkmItem *_get_selitem(_dialog *p)
{
	mColumnItem *lvi;

	lvi = mListViewGetFocusItem(p->list);
	if(lvi)
		return (BkmItem *)lvi->param;
	else
		return NULL;
}

/* 開く */

static void _cmd_open(_dialog *p)
{
	BkmItem *pi;

	pi = _get_selitem(p);
	if(pi)
	{
		p->openitem = pi;
		mDialogEnd(MLK_DIALOG(p), TRUE);
	}
}

/* 削除 */

static void _cmd_delete(_dialog *p)
{
	BkmItem *pi;

	pi = _get_selitem(p);
	if(pi)
	{
		BkmList_delete(pi);

		mListViewDeleteItem_focus(p->list);
	}
}

/* 上下移動 */

static void _cmd_move(_dialog *p,mlkbool down)
{
	BkmItem *pi;

	pi = _get_selitem(p);
	if(pi)
	{
		BkmList_moveUpDown(pi, down);
	
		mListViewMoveItem_updown(p->list, NULL, down);
	}
}


//==========================
// main
//==========================


/* イベント */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	_dialog *p = (_dialog *)wg;

	switch(ev->type)
	{
		//通知
		case MEVENT_NOTIFY:
			if(ev->notify.id == WID_LIST)
			{
				//リストをダブルクリックで開く

				if(ev->notify.notify_type == MLISTVIEW_N_ITEM_L_DBLCLK)
					_cmd_open(p);
			}
			else if(ev->notify.id == MLK_WID_OK)
				//閉じる
				mDialogEnd(MLK_DIALOG(p), FALSE);
			break;

		//コマンド
		case MEVENT_COMMAND:
			switch(ev->cmd.id)
			{
				//開く
				case TRID_OPEN:
					_cmd_open(p);
					break;
				//削除
				case TRID_DELETE:
					_cmd_delete(p);
					break;
				//上へ
				case TRID_UP:
					_cmd_move(p, FALSE);
					break;
				//下へ
				case TRID_DOWN:
					_cmd_move(p, TRUE);
					break;
			}
			break;
	}

	return mDialogEventDefault(wg, ev);
}

/* ダイアログ作成 */

static _dialog *_create_dlg(mWindow *parent)
{
	_dialog *p;
	mWidget *ct;
	mListHeader *lh;
	mIconBar *ib;
	mToplevelSaveState *winst;
	int i;

	p = (_dialog *)mDialogNew(parent, sizeof(_dialog), MTOPLEVEL_S_DIALOG_NORMAL);
	if(!p) return NULL;

	p->wg.event = _event_handle;
	p->ct.sep = 6;

	//

	mContainerSetPadding_same(MLK_CONTAINER(p), 8);

	MLK_TRGROUP(TRGROUP_DLG_BOOKMARK);

	mToplevelSetTitle(MLK_TOPLEVEL(p), MLK_TR(TRID_TITLE));

	//------- ウィジェット

	//リスト

	p->list = mListViewCreate(MLK_WIDGET(p), WID_LIST, MLF_EXPAND_WH, 0,
		MLISTVIEW_S_MULTI_COLUMN | MLISTVIEW_S_HAVE_HEADER,
		MSCROLLVIEW_S_HORZVERT_FRAME);

	lh = mListViewGetHeaderWidget(p->list);

	for(i = 0; i < 3; i++)
	{
		mListHeaderAddItem(lh, MLK_TR(TRID_FILENAME + i),
			(i == 2)? 100: APPCONF->bkm_headerw[i],
			(i == 2)? MLISTHEADER_ITEM_F_EXPAND: 0, 0);
	}

	//ボタン

	ct = mContainerCreateHorz(MLK_WIDGET(p), 5, MLF_EXPAND_W, 0);

	ib = mIconBarNew(ct, 0, MICONBAR_S_TOOLTIP | MICONBAR_S_DESTROY_IMAGELIST);

	mIconBarSetImageList(ib, mImageListCreate_1bit_textcol(g_img_icon, 64, 16, 16));
	mIconBarSetTooltipTrGroup(ib, TRGROUP_DLG_BOOKMARK);

	for(i = 0; i < 4; i++)
		mIconBarAdd(ib, TRID_OPEN + i, i, TRID_OPEN + i, 0);

	//

	mButtonCreate(ct, MLK_WID_OK, MLF_EXPAND_X | MLF_RIGHT, 0, 0, MLK_TR(TRID_CLOSE));

	//-----

	_set_list(p->list);

	//状態

	winst = &APPDATA->bkmwin_state;

	if(winst->w && winst->h)
	{
		//復元
		mToplevelSetSaveState(MLK_TOPLEVEL(p), winst);
		mWidgetShow(MLK_WIDGET(p), 1);
	}
	else
	{
		//初期表示

		mWidgetSetInitSize_fontHeightUnit(MLK_WIDGET(p->list), 24, 12);
		mWindowResizeShow_initSize(MLK_WINDOW(p));
	}

	return p;
}

/* 終了時、状態保存 */

static void _save_state(_dialog *p)
{
	mListHeader *lh;

	mToplevelGetSaveState(MLK_TOPLEVEL(p), &APPDATA->bkmwin_state);

	lh = mListViewGetHeaderWidget(p->list);

	APPCONF->bkm_headerw[0] = mListHeaderGetItemWidth(lh, 0);
	APPCONF->bkm_headerw[1] = mListHeaderGetItemWidth(lh, 1);
}

/** 実行
 *
 * return: 開くしおりアイテム (NULL でなし) */

BkmItem *BookmarkDlg_run(mWindow *parent)
{
	_dialog *p;
	BkmItem *item;

	p = _create_dlg(parent);
	if(!p) return FALSE;

	mDialogRun(MLK_DIALOG(p), FALSE);

	item = p->openitem;

	_save_state(p);

	mWidgetDestroy(MLK_WIDGET(p));

	return item;
}
