/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * パネル:ファイルリスト
 *****************************************/

#include <mlk_gui.h>
#include <mlk_widget_def.h>
#include <mlk_widget.h>
#include <mlk_window.h>
#include <mlk_panel.h>
#include <mlk_listview.h>
#include <mlk_columnitem.h>
#include <mlk_event.h>
#include <mlk_menu.h>
#include <mlk_sysdlg.h>
#include <mlk_str.h>

#include "def_appdata.h"
#include "def_config.h"
#include "def_work.h"

#include "mainwin_func.h"
#include "panel.h"
#include "work_main.h"
#include "trid.h"


//-------------------

/*

- パネルが閉じている状態でも、表示位置は維持できるように、
  WorkData::filelist_parent に、表示する親位置がセットされている。

- アイテムの param = FileListItem *

*/

#define _PARAM_TO_PARENT  1	//親へ戻る時のアイテムパラメータ

enum
{
	TRID_EXTRACT_FILE
};

//-------------------


//========================
// sub
//========================


/* mListView を取得 */

static mListView *_get_list(void)
{
	return (mListView *)mPanelGetContents(APPDATA->panel[PANEL_FILELIST]);
}

/* リストセット */

static void _set_list(mListView *p)
{
	FileListItem *pif;

	mListViewDeleteAllItem(p);

	//先頭アイテム

	pif = APPWORK->filelist_parent;

	if(pif)
		//親がある場合、最初の子
		pif = (FileListItem *)pif->i.first;
	else
		//NULL の場合、先頭アイテム
		pif = workFileList_getTopItem();

	//親へ戻る

	if(APPWORK->filelist_parent)
		mListViewAddItem(p, "..", FILEICON_PARENT, 0, _PARAM_TO_PARENT);

	//アイテム追加

	for(; pif; pif = (FileListItem *)pif->i.next)
	{
		if(pif->item_type == FILELISTITEM_TYPE_FILE)
			mListViewAddItem(p, pif->name, FILEICON_FILE, 0, (intptr_t)pif);
		else
			mListViewAddItem(p, pif->name, FILEICON_DIR, 0, (intptr_t)pif);
	}

	//

	mListViewSetAutoWidth(p, FALSE);
}

/* 左クリック時 */

static void _item_click(mListView *p,mColumnItem *pi)
{
	FileListItem *pif;

	if(pi->param == _PARAM_TO_PARENT)
	{
		//親へ戻る

		APPWORK->filelist_parent = (FileListItem *)(APPWORK->filelist_parent)->i.parent;

		_set_list(p);
	}
	else
	{
		pif = (FileListItem *)pi->param;

		if(pif->item_type == FILELISTITEM_TYPE_FILE)
		{
			//画像: その位置へ移動

			MainWindow_movePage(pif->sorted_index);
		}
		else
		{
			//ディレクトリ: 親移動

			APPWORK->filelist_parent = pif;

			_set_list(p);
		}
	}
}

/* ファイルを別名で保存 */

static void _extract_file(mListView *p,FileListItem *pi)
{
	mStr str = MSTR_INIT;

	mStrSetText(&str, pi->name);

	if(mSysDlg_saveFile(p->wg.toplevel, "All Files\t*", 0,
		APPCONF->strFilelistExtractDir.buf, MSYSDLG_FILE_F_DEFAULT_FILENAME,
		&str, NULL))
	{
		mStrPathGetDir(&APPCONF->strFilelistExtractDir, str.buf);

		if(!workFileList_extractFile(str.buf, pi))
			mMessageBoxErrTr(p->wg.toplevel, TRGROUP_MESSAGE, TRID_MES_FAILED);
	}

	mStrFree(&str);
}

/* メニュー表示 */

static void _run_menu(mListView *p,int x,int y,mColumnItem *item)
{
	mMenu *menu;
	mMenuItem *pi;
	int id;

	if(!item
		|| item->param == _PARAM_TO_PARENT
		|| ((FileListItem *)item->param)->item_type == FILELISTITEM_TYPE_DIRECTORY)
		return;

	//メニュー表示

	MLK_TRGROUP(TRGROUP_FILELIST);

	menu = mMenuNew();

	mMenuAppendText(menu, TRID_EXTRACT_FILE, MLK_TR(TRID_EXTRACT_FILE));

	pi = mMenuPopup(menu, MLK_WIDGET(p), x, y, NULL, MPOPUP_F_LEFT | MPOPUP_F_TOP, NULL);
	id = (pi)? mMenuItemGetID(pi): -1;

	mMenuDestroy(menu);

	if(id == -1) return;

	//

	switch(id)
	{
		//ファイルを別名で保存
		case TRID_EXTRACT_FILE:
			_extract_file(p, (FileListItem *)item->param);
			break;
	}
}


//========================
//
//========================


/* イベント */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_NOTIFY && ev->notify.id == 100)
	{
		int f = APPCONF->foption & OPTION_F_FILELIST_SINGLE_CLICK;

		if((f && ev->notify.notify_type == MLISTVIEW_N_CHANGE_FOCUS)
			|| (!f && ev->notify.notify_type == MLISTVIEW_N_ITEM_L_DBLCLK))
		{
			//左ダブルクリック or シングルクリック

			_item_click(MLK_LISTVIEW(wg), (mColumnItem *)ev->notify.param1);
		}
		else if(ev->notify.notify_type == MLISTVIEW_N_ITEM_R_CLICK)
		{
			//右クリック

			_run_menu(MLK_LISTVIEW(wg),
				ev->notify.param2 >> 16, ev->notify.param2 & 0xffff,
				(mColumnItem *)ev->notify.param1);
		}

		return 1;
	}

	return mListViewHandle_event(wg, ev);
}

/* パネル内容作成 */

static mWidget *_create_handle(mPanel *panel,int id,mWidget *parent)
{
	mListView *list;

	//リストビュー

	list = mListViewCreate(parent, 100, MLF_EXPAND_WH, 0, 0, MSCROLLVIEW_S_HORZVERT_FRAME);

	list->wg.event = _event_handle;
	list->wg.notify_to = MWIDGET_NOTIFYTO_SELF;
	
	mListViewSetImageList(list, APPDATA->imglist_fileicon);

	_set_list(list);

	return (mWidget *)list;
}


//========================
// main
//========================


/** 作成 */

void PanelFileList_new(mPanelState *state)
{
	mPanel *p;

	p = Panel_create(PANEL_FILELIST, 0, 0, state, _create_handle);

	mPanelCreateWidget(p);
}

/** ファイル開いた時 */

void PanelFileList_opened(void)
{
	mListView *p = _get_list();
	FileListItem *picur;

	if(!p) return;

	picur = APPWORK->item_cur;
	if(!picur) return;

	//同期する場合、現在のファイルの親
	
	if(APPCONF->foption & OPTION_F_SYNC_FILELIST)
		APPWORK->filelist_parent = (FileListItem *)picur->i.parent;

	//リストセット

	_set_list(p);

	//同期時、ファイル選択

	if(APPCONF->foption & OPTION_F_SYNC_FILELIST)
	{
		mListViewSetFocusItem_param(p, (intptr_t)picur);
		mListViewScrollToFocus_margin(p, 0);
	}
}

/** ファイル閉じた時 */

void PanelFileList_closed(void)
{
	mListView *p = _get_list();

	if(p)
		mListViewDeleteAllItem(p);
}

/** リスト更新 (ソート順変更時) */

void PanelFileList_refresh(void)
{
	mListView *p = _get_list();

	if(p)
		_set_list(p);
}

/** ページ移動後、現在のページ位置と同期 */

void PanelFileList_sync_moved(void)
{
	mListView *p;
	FileListItem *picur,*parent_now;

	//親位置 (パネルが閉じていても、位置はセット)

	picur = APPWORK->item_cur;
	if(!picur) return;

	parent_now = APPWORK->filelist_parent; //現在の位置を保存

	APPWORK->filelist_parent = (FileListItem *)picur->i.parent;

	//---- リスト更新

	p = _get_list();
	if(!p) return;

	//親が異なる場合、リスト再セット

	if(APPWORK->filelist_parent != parent_now)
		_set_list(p);

	//ファイル選択

	mListViewSetFocusItem_param(p, (intptr_t)picur);
	mListViewScrollToFocus_margin(p, 2);
}

