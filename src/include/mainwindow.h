/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/************************************
 * MainWindow 関数
 ************************************/

typedef struct _MainWindow MainWindow;

enum
{
	MAINWIN_OPEN_NORMAL,		//通常 (ファイル/アーカイブ/ディレクトリ)
	MAINWIN_OPEN_CMD_LASTPAGE,	//コマンド:終端位置へ移動 (前のファイルの終端位置を開く時)
	MAINWIN_OPEN_FILEBROWSER,	//ファイルブラウザから開く
	MAINWIN_OPEN_RELOAD,		//再読込
	MAINWIN_OPEN_PATH_AND_FILE	//"path\tfile" (履歴、しおり)
};

/* mainwindow.c */

void MainWindow_new(void);
void MainWindow_show(MainWindow *p,mToplevelSaveState *state);

void MainWindow_setTitle(MainWindow *p);
void MainWindow_closeFile(MainWindow *p);
void MainWindow_showMessage(int trid);
void MainWindow_runMainMenu_forButton(int x,int y);

/* mainwin_file.c */

void MainWindow_setRecentOnClose(MainWindow *p);
void MainWindow_setRecentMenu(MainWindow *p,int type);
void MainWindow_runMenu_recentFile(MainWindow *p);

void MainWindow_open(const char *path,int type);
void MainWindow_openRecent(MainWindow *p,int type,int no);
void MainWindow_openDialog(MainWindow *p,mlkbool directory,int recentno);
void MainWindow_openFromDropdown(MainWindow *p,mlkbool directory);
void MainWindow_openNextPrev(MainWindow *p,mlkbool next,mlkbool lastpage);

/* mainwin_cmd.c */

void MainWindow_command_move(MainWindow *p,int id,mlkbool enable_revdir);
void MainWindow_sckey_special(MainWindow *p,int id);

mlkbool MainWindow_canUIChange(MainWindow *p);
mlkbool MainWindow_onChangePanel(MainWindow *p,mlkbool relayout);
void MainWindow_toggle_fullscreen(MainWindow *p);
void MainWindow_toggle_custom(MainWindow *p);
void MainWindow_togglePanel(MainWindow *p);

void MainWindow_dlg_viewfixsize(MainWindow *p);
void MainWindow_dlg_scale(MainWindow *p);
void MainWindow_dlg_color(MainWindow *p);
void MainWindow_dlg_envopt(MainWindow *p);

/* canvas.c */

void MainWinCanvas_new(mWidget *parent);
void MainWinCanvas_toggleScrollBar(void);
void MainWinCanvas_setScrollInfo(mlkbool reset);
void MainWinCanvas_moveScroll(int dir);

/* statusbar.c */

void StatusBar_new(mWidget *parent);
void StatusBar_toggleVisible(void);
void StatusBar_setInfo(void);
