/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/******************************
 * 翻訳文字列 ID
 ******************************/

/* グループ ID */

enum
{
	TRGROUP_TOOLBAR_TOOLTIP,
	TRGROUP_PANEL_NAME,
	TRGROUP_FILEBROWSER,
	TRGROUP_FILELIST,
	TRGROUP_DIALOG,
	TRGROUP_DLG_COLOR_LEVEL,
	TRGROUP_DLG_BOOKMARK,
	TRGROUP_DLG_SHORTCUTKEY,
	TRGROUP_DLG_ENVOPT,

	TRGROUP_MESSAGE = 1000,
	TRGROUP_MAINMENU,
};

/* メッセージ */
enum
{
	TRID_MES_ERROR,
	TRID_MES_ERR_OPEN,
	TRID_MES_ERR_DAMAGED,
	TRID_MES_ERR_UNSUPPORTED,
	TRID_MES_ERR_NO_IMAGE,
	TRID_MES_RECENT_UNEXIST,	//履歴のファイルが存在しない
	TRID_MES_FAILED,
	TRID_MES_BOOKMARK_APPEND	//しおり追加した
};

/* ダイアログ */
enum
{
	TRID_DLG_GOTO_IMAGEPOS,
	TRID_DLG_IMAGEPOS,
	TRID_DLG_SETSIZE,
	TRID_DLG_MES_VIEWFIXSIZE,
	TRID_DLG_SETSCALE,
	TRID_DLG_MES_SETSCALE
};
