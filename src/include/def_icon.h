/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/********************************
 * ツールバーアイコン定義
 ********************************/

//アイコンの画像番号

enum
{
	ICON_OPENFILE,
	ICON_OPENDIR,
	ICON_OPEN_RECENT_FILE,
	ICON_NEXTFILE,
	ICON_PREVFILE,
	ICON_NEXT_PAGE,
	ICON_PREV_PAGE,
	ICON_TOP_PAGE,
	ICON_END_PAGE,
	ICON_GOTO_PAGE,
	ICON_RIGHT_BINDING,
	ICON_DOUBLE_PAGE,
	ICON_SCALE_UP,
	ICON_SCALE_DOWN,
	ICON_FULLSCREEN,

	ICON_NUM
};

//アイコン画像名

#if defined(ICON_DEF_NAME)
static const char *g_icon_names =
	"document-open\0folder-open\0document-open-recent\0go-down\0go-up\0"
	"go-next\0go-previous\0go-first\0go-last\0go-jump\0"
	"format-justify-right\0edit-copy\0zoom-in\0zoom-out\0view-fullscreen\0";
#endif

//アイコンのコマンドID
//0x8000 = dropdown, 0x4000 = check

#if defined(ICON_DEF_COMMAND_ID)
static const uint16_t g_icon_cmdid[] = {
	TRMENU_FILE_OPENFILE|0x8000, TRMENU_FILE_OPENDIR|0x8000, TRMENU_FILE_RECENT_FILE,
	TRMENU_FILE_NEXT, TRMENU_FILE_PREV,
	TRMENU_CTRL_NEXT_PAGE, TRMENU_CTRL_PREV_PAGE, TRMENU_CTRL_TOP_PAGE, TRMENU_CTRL_END_PAGE,
	TRMENU_CTRL_GOTO_POS,
	TRMENU_VIEW_RIGHT_BINDING|0x4000, TRMENU_VIEW_DOUBLE_PAGE|0x4000,
	TRMENU_VIEW_SCALE_UP, TRMENU_VIEW_SCALE_DOWN, TRMENU_UI_FULLSCREEN
};
#endif

