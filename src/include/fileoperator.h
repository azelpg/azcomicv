/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * ディレクトリやアーカイブの操作
 **********************************/

typedef struct _FileOperator FileOperator;
typedef struct _ImageBuf ImageBuf;

//列挙時のファイル情報

typedef struct
{
	const char *name;		//アーカイブの場合、パス付き (path/file)
	uint64_t time_modify;	//更新日時
	uint32_t filepos;		//アーカイブの場合、ファイル位置
}FileOpInfo;

//ファイルを指定するハンドル

typedef struct
{
	const char *name;
	uint32_t filepos;
}FileOpHandle;

//FileOperator: ベース

typedef void (*FuncFileOpEnum)(FileOpInfo *info,void *param);

struct _FileOperator
{
	//破棄時
	void (*destroy)(FileOperator *p);

	//ファイル列挙
	// : ディレクトリは除く。ファイルは、拡張子を元にした画像ファイルのみ。
	// : 各ファイルごとに func が呼ばれる。
	mlkerr (*enum_entry)(FileOperator *p,FuncFileOpEnum func,void *param);

	//ファイルの抽出
	mlkbool (*extract)(FileOperator *p,FileOpHandle *fh,const char *filename);

	//画像の読み込み
	ImageBuf *(*loadimage)(FileOperator *p,FileOpHandle *fh);
};

mlkerr FileOperator_openDir(FileOperator **pp,const char *path);
mlkerr FileOperator_openZip(FileOperator **pp,const char *filename);

