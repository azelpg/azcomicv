/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/************************************
 * WorkData 関数
 ************************************/

typedef struct _FileListItem FileListItem;

enum
{
	MOVEPAGE_TOP = -1,
	MOVEPAGE_END = -2,
	MOVEPAGE_NEXT_PAGE = -3,
	MOVEPAGE_PREV_PAGE = -4,
	MOVEPAGE_NEXT_IMAGE = -5,
	MOVEPAGE_PREV_IMAGE = -6,

	MOVEPAGE_RET_OK = 0,
	MOVEPAGE_RET_UNMOVED,	//移動なし
	MOVEPAGE_RET_NEXT_TOP,	//次の先頭へ
	MOVEPAGE_RET_PREV_LAST	//前の終端へ
};

/* main */

int WorkData_new(void);
void WorkData_free(void);
void workData_setScaleDownTable(void);
void workData_setLevelTable(void);

void workFile_close(void);
mlkerr workFile_open(const char *path,mStr *strfname,mStr *strdir,mlkbool directory,
	const char *pagefilename);
mlkbool workFile_checkArchive(const char *filename);

/* filelist */

FileListItem *workFileList_getTopItem(void);
void workFileList_getItemName_fullpath(mStr *str,FileListItem *pi);

mlkerr workFileList_create(void);
void workFileList_sort(void);
mlkbool workFileList_extractFile(const char *filename,FileListItem *pi);

/* page */

int workPage_movePage(int no);

mlkbool workPage_setFull(int viewtype);
void workPage_setViewSize(mlkbool reset_relscale);

void workOpt_changeViewImageType(int type);
void workOpt_changeViewFixSize(void);
void workOpt_toggle_topIsSingle(void);

void workSet_scaleUpDown(mlkbool down);
void workSet_scale(double d);

void workPage_getScrollMax(mSize *size);

int workCalc_getLoupePos(mPoint *ptdst,int x,int y);
void workCalc_getLoupeTopPos(mDoublePoint *ptdst,mPoint *ptsrc,int imgno,int areaw,int areah);

void workPage_draw(mPixbuf *pixbuf,int wgw,int wgh);

