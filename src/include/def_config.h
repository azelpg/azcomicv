/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************
 * 設定データ
 *****************************/

#define CONFIG_RECENTFILE_NUM   10		//ファイル履歴数
#define CONFIG_COLOR_LEVEL_SLOT_NUM  3	//色補正のスロット数


/** 設定データ */

typedef struct _ConfigData
{
	mSize size_view;		//ビューの指定サイズ

	int32_t panelct_width,		//パネルコンテナの幅
		bkm_headerw[2],			//しおりウィンドウ、リストのヘッダの各幅
		scalestep_up,			//表示倍率、一段階変更時に掛ける値 (1.000=1000)
		scalestep_down,
		fixscale_step_under,	//固定倍率時の調整段階 (0 でなし、1=1%) [100%以下、100%以上時]
		fixscale_step_upper,
		loupe_scale,			//ルーペの表示倍率 (100 = 100%)
		doublepage_margin,		//見開き、左右間の余白
		cursor_erase_sec;		//指定秒数経過でカーソル消去 (0 でなし)

	uint32_t col_bkgnd,		//背景色
		col_blendbkgnd,		//アルファ合成の背景色
		foption,			//オプションフラグ
		fimgview,			//画像ビューの表示フラグ
		fui,				//UI の表示フラグ
		fpanel_restore,		//パネル全体の非表示時、元に戻す時のフラグ (0bit から順に)
		fuicustom,				//UIカスタム表示フラグ
		fuicustom_restore,		//カスタム状態を戻す時のフラグ
		filebrowser_opt,		//ファイルブラウザの設定
		color_level[CONFIG_COLOR_LEVEL_SLOT_NUM];	//色補正 (8bit, 最小|中間|最大)

	uint8_t color_level_sel,	//色補正選択スロット (0:なし)
		imgsize_type,		//画像サイズタイプ
		filesort_type,		//ファイルソートサイプ
		scaleup_method,		//拡大方法
		scaledown_method,	//縮小方法
		iconsize,			//ツールバーのアイコンサイズ
		pagemark_opt;		//ページ情報マークの設定 (0-1bit:位置[0=上/1=中央/2=下], 2bit:表示)

	mStr strFont_panel,			//パネルフォント (空文字列でGUIフォント)
		strArchiveCharcode,		//アーカイブ、ファイル名の文字コード (',' で区切る。最大3つ。デフォルトでロケール文字列)
		strFilebrowserDir,		//ファイルブラウザの現在パス
		strFilelistExtractDir,	//ファイルリストのファイル抽出ディレクトリ
		strRecent[3][CONFIG_RECENTFILE_NUM];	//履歴

	uint16_t *button_buf;		//ボタン操作 (NULL で空)
	int button_num;
}ConfigData;


#define APPCONF g_app_config
extern ConfigData *g_app_config;

//-----------------

//ファイル履歴番号
enum
{
	RECENTFILE_ARRAY_FILE,
	RECENTFILE_ARRAY_ARCHIVE,
	RECENTFILE_ARRAY_DIR
};

//画像表示サイズ
enum
{
	IMGSIZE_TYPE_FIT_VIEW,
	IMGSIZE_TYPE_FIT_VIEW_WIDTH,
	IMGSIZE_TYPE_FIT_VIEW_HEIGHT,
	IMGSIZE_TYPE_ORIGINAL,
	IMGSIZE_TYPE_FIX_SCALE
};

//ファイルソートタイプ
enum
{
	FILESORT_TYPE_STORING,
	FILESORT_TYPE_NAME,
	FILESORT_TYPE_NAME_NATURAL,
	FILESORT_TYPE_MODIFY
};

//拡大補間タイプ
enum
{
	SCALEUP_METHOD_NEAREST,
	SCALEUP_METHOD_BICUBIC
};

//縮小補間タイプ
enum
{
	SCALEDOWN_METHOD_MITCHELL,
	SCALEDOWN_METHOD_LAGRANGE,
	SCALEDOWN_METHOD_LANCZOS2,
	SCALEDOWN_METHOD_LANCZOS3,
	SCALEDOWN_METHOD_SPLINE16,
	SCALEDOWN_METHOD_SPLINE36
};

//画像ビューフラグ
enum
{
	IMGVIEW_F_RIGHT_BINDING = 1<<0,			//右綴じ
	IMGVIEW_F_DOUBLE_PAGE = 1<<1,			//見開き
	IMGVIEW_F_SINGLE_HORZ_HALF = 1<<2,		//(単ページ)横長画像は分割
	IMGVIEW_F_DOUBLE_HORZ_SINGLE = 1<<3,	//(見開き)横長画像は単体表示
	IMGVIEW_F_NO_SCALE_UP = 1<<4,			//拡大しない
	IMGVIEW_F_DOUBLE_TOP_IS_SINGLE = 1<<5,	//(見開き)先頭ページは常に単ページに
	IMGVIEW_F_SORT_REVERSE = 1<<6,			//ファイルソート:逆順
	IMGVIEW_F_VIEWSIZE_FIT_FIX = 1<<7		//ビューを固定サイズに合わせる
};

//オプションフラグ
enum
{
	OPTION_F_LAST_TO_NEXT = 1<<0,			//ページ終端で次のファイルへ
	OPTION_F_TOP_TO_PREV = 1<<1,			//ページ先頭で前のファイルへ
	OPTION_F_SYNC_FILEBROWSER  = 1<<2,		//開いた時、ディレクトリリスト同期
	OPTION_F_SYNC_FILELIST = 1<<3,			//ページ移動時、ファイルリストと同期
	OPTION_F_KEY_REVERSE = 1<<4,			//右綴じ時、キーでの移動方向を逆に
	OPTION_F_VSCROLL_MOVE_PAGE = 1<<5,		//垂直スクロールの端でスクロール時、ページ移動
	OPTION_F_FILELIST_SINGLE_CLICK = 1<<6	//ファイルリストはシングルクリック操作
};

//UI フラグ
enum
{
	UI_FLAGS_MENUBAR = 1<<0,
	UI_FLAGS_TOOLBAR = 1<<1,
	UI_FLAGS_STATUSBAR = 1<<2,
	UI_FLAGS_SCROLLBAR = 1<<3,
	UI_FLAGS_PANEL = 1<<4,
	UI_FLAGS_CUSTOM = 1<<5,

	//全画面/カスタム時のマスク
	UI_FLAGS_MASK_MAIN = UI_FLAGS_MENUBAR
		| UI_FLAGS_TOOLBAR | UI_FLAGS_STATUSBAR
		| UI_FLAGS_SCROLLBAR | UI_FLAGS_PANEL,

	//[!] パネルの表示状態は、パネルのステータスフラグで判断する。
	// 以下は、現在の状態取得、またはカスタム用。
	
	UI_FLAGS_PANEL_FILEBROWSER = 1<<8,
	UI_FLAGS_PANEL_FILELIST = 1<<9,
	UI_FLAGS_PANEL_LOUPE = 1<<10,

	UI_FLAGS_PANEL_BIT_POS = 8
};

//ファイルブラウザ設定

enum
{
	FILEBROWSER_OPT_F_SHOW_HIDDEN_FILES = 1<<0,	//隠しファイルを表示
	FILEBROWSER_OPT_F_SORT_REVERSE = 1<<1,		//ソート逆順

	FILEBROWSER_OPT_SORTTYPE_SHIFT = 2,
	FILEBROWSER_OPT_SORTTYPE_MASK = 3,

	FILEBROWSER_OPT_SORTTYPE_NAME = 0,
	FILEBROWSER_OPT_SORTTYPE_MODIFY = 1,
};

#define FILEBROWSER_OPT_GET_SORTTYPE(n)  (((n) >> FILEBROWSER_OPT_SORTTYPE_SHIFT) & FILEBROWSER_OPT_SORTTYPE_MASK)
#define FILEBROWSER_OPT_SET_SORTTYPE(n,v)  n &= ~(FILEBROWSER_OPT_SORTTYPE_MASK << FILEBROWSER_OPT_SORTTYPE_SHIFT); n |= (v) << FILEBROWSER_OPT_SORTTYPE_SHIFT

//ページマーク設定

#define PAGEMARK_F_SHOW  4
#define PAGEMARK_GET_POS(n)  ((n) & 3)

