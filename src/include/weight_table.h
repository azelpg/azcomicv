/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/******************************
 * 拡大縮小の重みテーブル
 ******************************/

#define WEIGHTTBL_LENBIT   6 	//距離のビット数
#define WEIGHTTBL_LENVAL   (1 << WEIGHTTBL_LENBIT)
#define WEIGHTTBL_LENVAL2  (WEIGHTTBL_LENVAL * 2)
#define WEIGHTTBL_LENVAL3  (WEIGHTTBL_LENVAL * 3)

void WeightTable_setUp_bicubic(double *buf);
void WeightTable_setDown_mitchell(double *buf);
void WeightTable_setDown_lagrange(double *buf);
void WeightTable_setDown_lanczos2(double *buf);
void WeightTable_setDown_lanczos3(double *buf);
void WeightTable_setDown_spline16(double *buf);
void WeightTable_setDown_spline36(double *buf);
