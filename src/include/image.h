/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/************************************
 * イメージデータ
 ************************************/

typedef struct _ImageBuf
{
	uint8_t *buf;
	int w,h,pitch;
}ImageBuf;

//画像描画情報

typedef struct
{
	int areaw,areah,
		movx,movy,		//画像の位置移動 (pixbuf に対する)
		splitno,		//横長分割 (-1:なし, 0:1枚目, 1:2枚目)
		is_nearest;		//拡大時ニアレストネイバーか
	mSize sizesrc,
		sizedst;
	uint8_t *tblbuf;	//色調整テーブル (NULL でなし)
}ImageBufDrawPageInfo;

//ルーペ描画情報

typedef struct
{
	int areaw,areah,
		scale,
		splitno;		//横長分割 (-1:なし 0:1枚目 1:2枚目)
	double dsxleft,		//左上のソース位置
		dsytop;
	mSize sizesrc;
	uint8_t *tblbuf;	//色調整テーブル (NULL でなし)
}ImageBufDrawLoupeInfo;


//----------

void ImageBuf_free(ImageBuf *p);
void ImageBuf_freeptr(ImageBuf **pp);
ImageBuf *ImageBuf_new(int w,int h);

ImageBuf *ImageBuf_createErrImage(void);
ImageBuf *ImageBuf_loadFile(const char *filename,const void *imgbuf,int bufsize,uint32_t colbkgnd);

void ImageBuf_getHistogram(ImageBuf *p,uint32_t *buf);
ImageBuf *ImageBuf_resize(ImageBuf *p,int neww,int newh,int range,int srcw,int splitno,
	double *tblup,double *tbldown);

void ImageBuf_drawPage(ImageBuf *p,mPixbuf *pixbuf,ImageBufDrawPageInfo *info);
void ImageBuf_drawLoupe(ImageBuf *p,mPixbuf *pixbuf,ImageBufDrawLoupeInfo *info);

