/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/************************************
 * WorkData 内部処理関数
 ************************************/

/* sub */

FileListItem *workSub_getItem_nextImage(WorkData *p,FileListItem *pi,mlkbool nextdir);
FileListItem *workSub_getItem_prevImage(WorkData *p,FileListItem *pi,mlkbool prevdir);
FileListItem *workSub_getItem_prevAdjust(WorkData *p,FileListItem *pi,int *pviewtype);

FileListItem *workSub_getItem_fromSortedIndex(WorkData *p,int no);
FileListItem *workSub_getItem_fromName(WorkData *p,const char *name);

mlkbool workSub_isFirstImageItem(FileListItem *pi);
mlkbool workSub_isEnableDoublePage(WorkData *p,FileListItem *pi,FileListItem *pinext);

ImageBuf *workSub_getImageCache(WorkData *p,FileListItem *pi);
mlkbool workSub_setImageInfo(WorkData *p,FileListItem *pi);
ImageBuf *workSub_loadImageCache(WorkData *p,FileListItem *pi);
void workSub_createViewImage(WorkData *p,int no);

/* page_viewsize */

void workSub_setViewSize(WorkData *p);

