/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*************************
 * 作業用データ定義
 *************************/

typedef struct _FileOperator FileOperator;
typedef struct _ImageBuf ImageBuf;


//------------------------
// ファイルアイテム

typedef struct _FileListItem
{
	mTreeItem i;

	char *name;				//ファイル名
	uint64_t time_modify;	//更新日時
	uint32_t filepos;		//[アーカイブ時] ファイル位置
	int store_index,		//格納順におけるインデックス番号
		sorted_index;		//ソート後のインデックス番号 (ディレクトリ時は -1)
	uint16_t width,			//画像サイズ (0 でなし)
		height;
	uint8_t item_type;		//アイテムタイプ
}FileListItem;

enum
{
	FILELISTITEM_TYPE_FILE,
	FILELISTITEM_TYPE_DIRECTORY
};

//画像が横長か
#define FILELISTITEM_IS_HORIZONTAL(p)  ((p)->width > (p)->height)


//---------------------------
// 表示用データ

typedef struct
{
	int type,			//表示タイプ
		splitno;		//単ページ時、横長分割 (-1:なし, 0:左側, 1:右側)
	double dscale[2],	//現在の各画像の表示倍率 (すべての倍率を適用した値)
		dscale_rel1[2],	//相対倍率が 1.0 の時の表示倍率
		dfixscale,		//固定倍率
			//起動時に 1.0 に初期化され、起動中は維持される。
			//画像表示サイズが固定倍率の時に使われる。
		drelscale;		//相対倍率
			//画像の表示中のみ維持され、現在の表示倍率に上乗せされる。
			//ページ移動時、1.0 にリセット。

	mSize size_src[2],		//ソース画像のサイズ (分割時は分割後のサイズ)
		size_dst[2],		//実際に表示する時のサイズ
		size_dstfull;		//全体のサイズ (見開き時は、2枚分＋余白のサイズ)
	mPoint ptscr,			//スクロール位置 (size_dstfull が全体の範囲)
		ptdrawmov[2];		//各画像描画時の、左上からの移動位置
			//size_dstfull の範囲を全体として、(0,0) が全体の左上となる

	ImageBuf *imgsrc[2],	//ソースイメージ (キャッシュから)
		*imgdst[2];			//表示用イメージ (NULL でソースを使う)
	FileListItem *item_double;	//見開きで2枚表示する場合、2枚目のアイテム (NULL で単体)
}WorkViewData;

enum
{
	VIEWTYPE_AUTO = -1,
	VIEWTYPE_AUTO_NO_FIRST_PAGE = -2, //"先頭は常に先頭"の判定を無効にする

	VIEWTYPE_NONE = 0,
	VIEWTYPE_SINGLE,		//単体
	VIEWTYPE_SINGLE_SPLIT1,	//単体分割 (1枚目)
	VIEWTYPE_SINGLE_SPLIT2,	//単体分割 (2枚目)
	VIEWTYPE_DOUBLE_LR,		//見開き (->)
	VIEWTYPE_DOUBLE_RL		//見開き (<-)
};

#define VIEWTYPE_IS_SINGLE(n)  ((n) == VIEWTYPE_SINGLE || (n) == VIEWTYPE_SINGLE_SPLIT1 || (n) == VIEWTYPE_SINGLE_SPLIT2)
#define VIEWTYPE_IS_DOUBLE(n)  ((n) == VIEWTYPE_DOUBLE_LR || (n) == VIEWTYPE_DOUBLE_RL)

//---------------------------
// 画像キャッシュ

typedef struct
{
	mListItem i;
	ImageBuf *img;
	FileListItem *item;
}ImageCache;

#define IMAGECACHE_MAXNUM  3


//---------------------------
// WorkData

typedef struct _WorkData
{
	WorkViewData view;		//表示関連データ

	mTree filetree;			//ファイルのツリーリスト
	mList list_imgcache;	//キャッシュイメージ (終端のほうが新しい)

	int opentype,			//現在開いているタイプ
		imgfile_num;		//画像ファイルの数

	mSize size_canvas;		//キャンバスのサイズ

	FileOperator *fileop;	//ディレクトリ/アーカイブの操作

	FileListItem *item_cur,	//現在位置のアイテム
		//[!] 画像が一つもない場合は NULL になっているので、注意
		*item_top,			//先頭のファイルアイテム
		*item_last,			//終端のファイルアイテム
		*filelist_parent;	//ファイルリスト表示の親位置

	double *weighttbl_up,	//重みテーブル
		*weighttbl_down;
	uint8_t *level_tbl;		//色レベル補正用テーブル (256byte)

	mStr strOpenFile,		//開いているファイルのパス
		//ディレクトリの場合は、ディレクトリパス。
		//アーカイブの場合は、ファイル名。
		strImgExts;			//対応画像の拡張子 (':' で区切る)
}WorkData;

enum
{
	OPENTYPE_EMPTY,
	OPENTYPE_DIRECTORY,
	OPENTYPE_ARCHIVE
};

#define OPENTYPE_IS_EMPTY()    (g_app_work->opentype == OPENTYPE_EMPTY)
#define OPENTYPE_IS_OPENED()   (g_app_work->opentype != OPENTYPE_EMPTY)

//-------------

extern WorkData *g_app_work;
#define APPWORK g_app_work
