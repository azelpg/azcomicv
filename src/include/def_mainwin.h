/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * MainWindow ウィジェット定義
 **********************************/

#define MAINWINDOW(p)  ((MainWindow *)(p))

typedef struct _MainWindow
{
	MLK_TOPLEVEL_DEF

	mWidget *menubar,
			*splitter,
			*toolbar;

	mMenu *menu_top,
		*menu_view,			//メニュー "表示"
		*menu_ui,			//メニュー "UI"
		*menu_recent[3];	//各履歴

	int is_fullscreen;		//全画面フラグ
	uint32_t fullscreen_restore;	//全画面戻す時のフラグ
}MainWindow;

/* コマンドID */
enum
{
	MAINWIN_CMDID_RECENT_FILE_CLEAR = 0x10000,	//履歴消去
	MAINWIN_CMDID_RECENT_ARCHIVE_CLEAR,
	MAINWIN_CMDID_RECENT_DIR_CLEAR,
	MAINWIN_CMDID_FULLSCREEN_RESTORE,	//全画面戻す (ESC 時)

	MAINWIN_CMDID_RECENTFILE_TOP = 0x10000 + 100
};


