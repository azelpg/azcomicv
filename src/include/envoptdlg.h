/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * 環境設定ダイアログ
 *****************************************/

uint32_t EnvOptDlg_run(mWindow *parent);

enum
{
	ENVOPT_F_UPDATE_FULL = 1<<0,	//ページ画像の変更有りで更新
	ENVOPT_F_UPDATE_VIEW = 1<<1,	//ページ画像の変更なしで更新
	ENVOPT_F_UPDATE_REDRAW = 1<<2,	//キャンバスの再描画のみ
	ENVOPT_F_SCALEDOWN_METHOD = 1<<3,	//縮小方法
	ENVOPT_F_ICONSIZE = 1<<4		//ツールバーアイコンサイズ
};
