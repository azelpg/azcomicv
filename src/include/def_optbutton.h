/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************
 * ボタン操作関連定義
 *****************************/

//[7bit]コマンド [6bit]ボタン番号 [3bit]フラグ

#define OPTBUTTON_F_CTRL   0x8000
#define OPTBUTTON_F_SHIFT  0x4000
#define OPTBUTTON_F_RAW    0x2000

#define OPTBUTTON_MAKE(btt,cmd)   (((btt) << 7) | (cmd))
#define OPTBUTTON_MASK_BUTTON(n)  ((n) & ~0x7f)
#define OPTBUTTON_GET_COMMAND(n)  ((n) & 0x7f)
#define OPTBUTTON_GET_BUTTON(n)   (((n) >> 7) & 0x3f)
#define OPTBUTTON_GET_BUTTON_FULL(n)  ((n) >> 7)
#define OPTBUTTON_SET_COMMAND(n,cmd)  n &= ~0x7f; n |= cmd

#define OPTBUTTON_MAX_BTTNO   63

//コマンド

enum
{
	OPTBUTTON_CMD_MENU,
	OPTBUTTON_CMD_SCROLL,
	OPTBUTTON_CMD_MOVEPAGE_CLICK,
	OPTBUTTON_CMD_NEXT_PAGE,
	OPTBUTTON_CMD_PREV_PAGE,
	OPTBUTTON_CMD_NEXT_IMAGE,
	OPTBUTTON_CMD_PREV_IMAGE,
	OPTBUTTON_CMD_SCALE_UP,
	OPTBUTTON_CMD_SCALE_DOWN,
	OPTBUTTON_CMD_SCROLL_UP,
	OPTBUTTON_CMD_SCROLL_DOWN,
	OPTBUTTON_CMD_SCROLL_LEFT,
	OPTBUTTON_CMD_SCROLL_RIGHT,

	OPTBUTTON_CMD_NUM
};

