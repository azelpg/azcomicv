/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * ImageBuf
 *
 * RGBA イメージ (R-G-B-A 順)
 *****************************************/

#include <string.h>

#include <mlk.h>
#include <mlk_loadimage.h>
#include <mlk_pixbuf.h>

#include "def_macro.h"
#include "image.h"
#include "weight_table.h"


//-----------------

/* エラー画像 (PNG) 160x100 */
static const unsigned char g_pngdat_errorimg[259] = {
0x89,0x50,0x4e,0x47,0x0d,0x0a,0x1a,0x0a,0x00,0x00,0x00,0x0d,0x49,0x48,0x44,0x52,
0x00,0x00,0x00,0xa0,0x00,0x00,0x00,0x64,0x01,0x03,0x00,0x00,0x00,0x3b,0x78,0xa9,
0xcc,0x00,0x00,0x00,0x06,0x50,0x4c,0x54,0x45,0xff,0x00,0x00,0xff,0xff,0xff,0x41,
0x1d,0x34,0x11,0x00,0x00,0x00,0xb8,0x49,0x44,0x41,0x54,0x78,0x5e,0xed,0xd1,0x41,
0x0e,0x83,0x20,0x10,0x85,0x61,0x48,0x17,0x1c,0xc3,0xa3,0x70,0x34,0xec,0x09,0xbc,
0x12,0x49,0x17,0x5e,0xa3,0x8d,0x17,0x90,0x9d,0x4d,0xc8,0x4c,0xe7,0x59,0xcb,0x18,
0x25,0xe9,0xa6,0xab,0x86,0xc9,0xbf,0xfa,0x12,0x04,0xc1,0x98,0xda,0x04,0x3e,0x0d,
0x35,0x6c,0xf8,0x3f,0xd8,0x1b,0x6f,0x1c,0x4b,0xe4,0x38,0xda,0x1d,0x5a,0x96,0x66,
0xc3,0x51,0x60,0xc5,0x89,0xfd,0x38,0xb3,0x34,0xe5,0x90,0xb2,0x57,0x5c,0x02,0x22,
0x9f,0xa8,0x53,0xcc,0x41,0x1a,0xb8,0x4b,0x5c,0x90,0x2b,0xf8,0xdc,0xb0,0xe3,0xe1,
0x3b,0x2e,0x08,0x48,0xdb,0xee,0x77,0xcb,0xef,0x23,0x01,0xb3,0x62,0xbc,0x20,0x41,
0x39,0x7f,0x59,0x1e,0x1d,0x02,0x5a,0xc5,0xf1,0x86,0xb0,0x7c,0x66,0x3d,0xe7,0x03,
0x01,0x97,0xa0,0x98,0xd0,0xba,0xd1,0x0e,0x4f,0x47,0x2a,0x88,0xdf,0x54,0xac,0x5e,
0xc8,0x86,0xb8,0xba,0x3d,0x12,0xc2,0x25,0xcb,0x37,0xca,0x73,0x90,0x93,0xf0,0x1c,
0xfe,0x7a,0x40,0x3c,0xdc,0x07,0x8f,0xd3,0xb0,0x61,0xc3,0x5f,0x60,0x6d,0x5e,0xda,
0x51,0x0a,0x39,0x9e,0x5d,0xe9,0xa0,0x00,0x00,0x00,0x00,0x49,0x45,0x4e,0x44,0xae,
0x42,0x60,0x82 };

//対応フォーマット

static const mFuncLoadImageCheck g_load_checks[] = {
	mLoadImage_checkPNG, mLoadImage_checkJPEG,

#if defined(HAVE_FORMAT_PSD)
	mLoadImage_checkPSD,
#endif

#if defined(HAVE_FORMAT_TIFF)
	mLoadImage_checkTIFF,
#endif

#if defined(HAVE_FORMAT_WEBP)
	mLoadImage_checkWEBP,
#endif

#if defined(HAVE_FORMAT_AVIF)
	mLoadImage_checkAVIF,
#endif

	mLoadImage_checkGIF, mLoadImage_checkBMP,
	0
};

//-----------------



/** 解放 */

void ImageBuf_free(ImageBuf *p)
{
	//[!] 拡大縮小画像の作成失敗時は (ImageBuf *)1 になっている

	if(p && p != (ImageBuf *)1)
	{
		mFree(p->buf);
		mFree(p);
	}
}

/** 解放 (解放後、ポインタを NULL にする) */

void ImageBuf_freeptr(ImageBuf **pp)
{
	ImageBuf_free(*pp);
	*pp = NULL;
}

/** 作成 */

ImageBuf *ImageBuf_new(int w,int h)
{
	ImageBuf *p;

	p = (ImageBuf *)mMalloc0(sizeof(ImageBuf));
	if(!p) return NULL;

	p->w = w;
	p->h = h;
	p->pitch = w * 4;

	//バッファ確保

	p->buf = (uint8_t *)mMalloc(w * h * 4);
	if(!p->buf)
	{
		mFree(p);
		return NULL;
	}

	return p;
}

/** 画像読み込み失敗時のエラー画像を作成 */

ImageBuf *ImageBuf_createErrImage(void)
{
	return ImageBuf_loadFile(NULL,
		g_pngdat_errorimg, sizeof(g_pngdat_errorimg), 0);
}

/** ヒストグラム取得 */

void ImageBuf_getHistogram(ImageBuf *p,uint32_t *buf)
{
	uint32_t i;
	uint8_t *ps = p->buf;

	for(i = p->w * p->h; i; i--, ps += 4)
	{
		buf[(ps[0] * 77 + ps[1] * 150 + ps[2] * 29) >> 8]++;
	}
}


//==============================
// 画像読み込み
//==============================


/* RGBA -> RGB */

static void _rgba_to_rgb(ImageBuf *p,uint32_t colbkgnd)
{
	uint8_t *pd;
	int r,g,b,a,bkR,bkG,bkB;
	uint32_t i;

	pd = p->buf;

	bkR = MLK_RGB_R(colbkgnd);
	bkG = MLK_RGB_G(colbkgnd);
	bkB = MLK_RGB_B(colbkgnd);

	for(i = p->w * p->h; i; i--, pd += 4)
	{
		r = pd[0];
		g = pd[1];
		b = pd[2];
		a = pd[3];
		
		if(a == 0)
		{
			r = bkR;
			g = bkG;
			b = bkB;
		}
		else if(a != 255)
		{
			r = (r - bkR) * a / 255 + bkR;
			g = (g - bkG) * a / 255 + bkG;
			b = (b - bkB) * a / 255 + bkB;
		}

		pd[0] = r;
		pd[1] = g;
		pd[2] = b;
	}
}

/** 画像ファイルを読み込み */

ImageBuf *ImageBuf_loadFile(const char *filename,
	const void *srcbuf,int bufsize,uint32_t colbkgnd)
{
	ImageBuf *img;
	mLoadImage li;
	mLoadImageType ltype;
	int success = 0;

	//mLoadImage

	mLoadImage_init(&li);

	if(filename)
	{
		li.open.type = MLOADIMAGE_OPEN_FILENAME;
		li.open.filename = filename;
	}
	else
	{
		li.open.type = MLOADIMAGE_OPEN_BUF;
		li.open.buf = srcbuf;
		li.open.size = bufsize;
	}

	li.convert_type = MLOADIMAGE_CONVERT_TYPE_RGBA;
	li.flags = MLOADIMAGE_FLAGS_TRANSPARENT_TO_ALPHA;

	//タイプ判定

	if(mLoadImage_checkFormat(&ltype, &li.open, (mFuncLoadImageCheck *)g_load_checks, 0))
		return NULL;

	//開く

	img = NULL;

	if((ltype.open)(&li)) goto ERR;

	//サイズ確認

	if(li.width > MAX_IMAGESIZE || li.height > MAX_IMAGESIZE)
		goto ERR;

	//画像作成

	img = ImageBuf_new(li.width, li.height);
	if(!img) goto ERR;

	if(!mLoadImage_allocImageFromBuf(&li, img->buf, img->pitch))
		goto ERR;

	//読み込み

	if((ltype.getimage)(&li)) goto ERR;

	success = 1;

ERR:
	(ltype.close)(&li);
	mFree(li.imgbuf);

	if(success)
	{
		_rgba_to_rgb(img, colbkgnd);
		return img;
	}
	else
	{
		ImageBuf_free(img);
		return NULL;
	}
}


//==============================
// ページ描画
//==============================
/* - 背景は描画済みなので、範囲外は描画しなくて良い。
 *   ただし、sizedst 分の範囲は必ず描画する。
 * - ソース画像から直接描画する場合は、単体分割の場合があるので、
 *   分割後の範囲のみ描画する。 */


typedef struct
{
	int x,y,w,h,
		sx_top,
		pitchdst,
		dstbpp;
}_drawpageinfo;

#define _FIX_BIT  28
#define _FIX_VAL  ((int64_t)1 << _FIX_BIT)
#define _FIX_HALF ((int64_t)1 << (_FIX_BIT - 1))


/* 共通初期化
 *
 * 描画先の movx,y - sizedst.w,h の範囲を描画。
 * (movx,movy) がソースの (0,0) となる。 */

static uint8_t *_drawpage_init(_drawpageinfo *drawinfo,
	ImageBuf *srcimg,mPixbuf *pixbuf,ImageBufDrawPageInfo *info)
{
	mBox box;
	int dx,dy,dw,dh,areaw,areah;
	uint8_t *dstbuf;

	//イメージの表示範囲をクリッピング

	dx = info->movx, dy = info->movy;
	dw = info->sizedst.w, dh = info->sizedst.h;
	areaw = info->areaw, areah = info->areah;

	if(dx + dw <= 0 || dy + dh <= 0
		|| dx >= areaw || dy >= areah)
		return NULL;

	if(dx < 0) dw += dx, dx = 0;
	if(dy < 0) dh += dy, dy = 0;

	if(dx + dw > areaw) dw = areaw - dx;
	if(dy + dh > areah) dh = areah - dy;

	//mPixbuf クリッピング

	if(!mPixbufClip_getBox_d(pixbuf, &box, dx, dy, dw, dh))
		return NULL;

	//描画情報

	drawinfo->x = box.x;
	drawinfo->y = box.y;
	drawinfo->w = box.w;
	drawinfo->h = box.h;

	dstbuf = mPixbufGetBufPtFast(pixbuf, box.x, box.y);
	drawinfo->dstbpp = pixbuf->pixel_bytes;
	drawinfo->pitchdst = pixbuf->line_bytes - box.w * pixbuf->pixel_bytes;

	drawinfo->sx_top = (info->splitno == 1)? srcimg->w - info->sizesrc.w: 0;

	return dstbuf;
}

/* 原寸サイズ
 *
 * - 描画範囲内にソース範囲外の部分は含まれない。 */

static void _drawpage_original(ImageBuf *p,mPixbuf *pixbuf,ImageBufDrawPageInfo *info)
{
	_drawpageinfo dinfo;
	mFuncPixbufSetBuf setpix;
	uint8_t *pd,*psY,*ps,*tblbuf,rr,gg,bb;
	int pitchs,ix,iy;

	pd = _drawpage_init(&dinfo, p, pixbuf, info);
	if(!pd) return;

	pitchs = p->pitch;
	tblbuf = info->tblbuf;

	mPixbufGetFunc_setbuf(pixbuf, &setpix);

	psY = p->buf + (dinfo.y - info->movy) * pitchs;

	//

	for(iy = dinfo.h; iy; iy--)
	{
		ps = psY + (dinfo.sx_top + dinfo.x - info->movx) * 4;

		for(ix = dinfo.w; ix; ix--)
		{
			rr = ps[0];
			gg = ps[1];
			bb = ps[2];

			if(tblbuf)
			{
				rr = tblbuf[rr];
				gg = tblbuf[gg];
				bb = tblbuf[bb];
			}

			(setpix)(pd, mRGBtoPix_sep(rr, gg, bb));

			pd += dinfo.dstbpp;
			ps += 4;
		}

		pd += dinfo.pitchdst;
		psY += pitchs;
	}
}

/* 拡大 (ニアレストネイバー)
 *
 * - ソース位置が負の値になることはない。
 * - 誤差上、ソース位置が最大値以上になることはある。 */

static void _drawpage_nearest(ImageBuf *p,mPixbuf *pixbuf,ImageBufDrawPageInfo *info)
{
	_drawpageinfo dinfo;
	uint8_t *pd,*psY,*ps,*tblbuf,rr,gg,bb;
	int pitchs,sw,sh,ix,iy,n;
	int64_t fsx,fsy,fsx_left,fincx,fincy;
	mFuncPixbufSetBuf setpix;

	pd = _drawpage_init(&dinfo, p, pixbuf, info);
	if(!pd) return;

	sw = info->sizesrc.w;
	sh = info->sizesrc.h;
	pitchs = p->pitch;

	tblbuf = info->tblbuf;

	mPixbufGetFunc_setbuf(pixbuf, &setpix);

	//

	fincx = (int64_t)((double)info->sizesrc.w / info->sizedst.w * _FIX_VAL);
	fincy = (int64_t)((double)info->sizesrc.h / info->sizedst.h * _FIX_VAL);

	fsx_left = (dinfo.x - info->movx) * fincx;
	fsy      = (dinfo.y - info->movy) * fincy;

	//-------

	for(iy = dinfo.h; iy; iy--, fsy += fincy)
	{
		n = fsy >> _FIX_BIT;
		if(n >= sh) n = sh - 1;

		//X

		psY = p->buf + n * pitchs;

		for(ix = dinfo.w, fsx = fsx_left; ix; ix--, fsx += fincx, pd += dinfo.dstbpp)
		{
			n = fsx >> _FIX_BIT;
			if(n >= sw) n = sw - 1;

			ps = psY + ((dinfo.sx_top + n) * 4);
			rr = ps[0];
			gg = ps[1];
			bb = ps[2];

			if(tblbuf)
			{
				rr = tblbuf[rr];
				gg = tblbuf[gg];
				bb = tblbuf[bb];
			}

			(setpix)(pd, mRGBtoPix_sep(rr, gg, bb));
		}

		pd += dinfo.pitchdst;
	}
}

/** ページ描画 */

void ImageBuf_drawPage(ImageBuf *p,mPixbuf *pixbuf,ImageBufDrawPageInfo *info)
{
	if(!p || p == (ImageBuf *)1)
	{
		//画像がない場合、黒で塗りつぶし
		//(1 の場合は拡大縮小画像の作成エラー)

		mPixbufFillBox(pixbuf,
			info->movx, info->movy, info->sizedst.w, info->sizedst.h, 0);
	}
	else if(info->is_nearest
		&& (info->sizesrc.w < info->sizedst.w || info->sizesrc.h < info->sizedst.h))
		//拡大:ニアレストネイバー
		_drawpage_nearest(p, pixbuf, info);
	else
		//原寸
		_drawpage_original(p, pixbuf, info);
}


//========================
// ルーペ描画
//========================


/** ルーペ描画 */

void ImageBuf_drawLoupe(ImageBuf *p,mPixbuf *pixbuf,ImageBufDrawLoupeInfo *info)
{
	uint8_t *pd,*psY,*ps,*tblbuf,rr,gg,bb;
	int pitchs,pitchd,sw,sh,ix,iy,n,dstbpp,sxtop;
	int64_t fsx,fsy,fsx_left,finc;
	double dscale;
	mBox box;
	mPixCol bkgnd;
	mFuncPixbufSetBuf setpix;

	bkgnd = mRGBtoPix(0xcccccc);

	if(!p)
	{
		mPixbufFillBox(pixbuf, 0, 0, info->areaw, info->areah, bkgnd);
		return;
	}

	//

	if(!mPixbufClip_getBox_d(pixbuf, &box, 0, 0, info->areaw, info->areah))
		return;

	tblbuf = info->tblbuf;

	pd = mPixbufGetBufPtFast(pixbuf, box.x, box.y);
	dstbpp = pixbuf->pixel_bytes;
	pitchd = pixbuf->line_bytes - box.w * dstbpp;

	mPixbufGetFunc_setbuf(pixbuf, &setpix);

	sw = info->sizesrc.w;
	sh = info->sizesrc.h;
	pitchs = p->pitch;

	sxtop = (info->splitno == 1)? p->w - info->sizesrc.w: 0;

	//

	dscale = 100.0 / info->scale;
	finc = (int64_t)(dscale * _FIX_VAL);

	fsx_left = info->dsxleft * _FIX_VAL;
	fsy = info->dsytop * _FIX_VAL;

	//右下がソースサイズに合うように

	if(fsx_left + info->areaw * finc >= ((int64_t)sw << _FIX_BIT))
		fsx_left = ((int64_t)sw << _FIX_BIT) - info->areaw * finc;

	if(fsy + info->areah * finc >= ((int64_t)sh << _FIX_BIT))
		fsy = ((int64_t)sh << _FIX_BIT) - info->areah * finc;

	if(fsx_left < 0) fsx_left = 0;
	if(fsy < 0) fsy = 0;

	//

	fsx_left += box.x * finc;
	fsy += box.y * finc;

	//-------

	for(iy = box.h; iy; iy--, fsy += finc)
	{
		n = fsy >> _FIX_BIT;
		if(n >= sh)
		{
			mPixbufBufLineH(pixbuf, pd, box.w, bkgnd);
			pd += pixbuf->line_bytes;
			continue;
		}

		//X

		psY = p->buf + n * pitchs;

		for(ix = box.w, fsx = fsx_left; ix; ix--, fsx += finc, pd += dstbpp)
		{
			n = fsx >> _FIX_BIT;

			if(n >= sw)
				(setpix)(pd, bkgnd);
			else
			{
				ps = psY + ((sxtop + n) * 4);
				rr = ps[0];
				gg = ps[1];
				bb = ps[2];

				if(tblbuf)
				{
					rr = tblbuf[rr];
					gg = tblbuf[gg];
					bb = tblbuf[bb];
				}

				(setpix)(pd, mRGBtoPix_sep(rr, gg, bb));
			}
		}

		pd += pitchd;
	}
}


//========================
// リサイズ
//========================

#define _RESIZE_FBIT  16
#define _RESIZE_FVAL  (1 << _RESIZE_FBIT)
#define _RESIZE_FHALF (1 << (_RESIZE_FBIT - 1))

//距離の値。大体 -3.x〜3.x くらいが最大値になるので大きめで良い
#define _RESIZE_LEN_BIT 25
#define _RESIZE_LEN_VAL (1 << _RESIZE_LEN_BIT)


/* 拡大:垂直リサイズ */

static mlkbool _resize_up_vert(uint8_t *dstbuf,int dsth,
	uint8_t *srcbuf,int srcw,int srch,int pitchsrc,int xpos_top,double *weightbuf)
{
	int ix,iy,i,pos,n,flen,c[3],tbl_weight[4];
	double dsy,dscale,dsum,dweight[4];
	uint8_t *ps,*tbl_psY[4];

	dscale = (double)srch / dsth;
	dsy = 0.5 * dscale;

	for(iy = dsth; iy > 0; iy--, dsy += dscale)
	{
		//テーブル

		pos = (int)(dsy - 2.5);
		flen = (int)((pos + 0.5 - dsy) * WEIGHTTBL_LENVAL);
		dsum = 0;

		for(i = 0; i < 4; i++, flen += WEIGHTTBL_LENVAL)
		{
			//位置

			n = pos + i;
			if(n < 0) n = 0;
			else if(n >= srch) n = srch - 1;

			tbl_psY[i] = srcbuf + n * pitchsrc;

			//重み

			n = flen;
			if(n < 0) n = -n;

			dweight[i] = (n < WEIGHTTBL_LENVAL2)? weightbuf[n]: 0;
			dsum += dweight[i];
		}

		for(i = 0; i < 4; i++)
			tbl_weight[i] = (int)(dweight[i] / dsum * _RESIZE_FVAL);

		//----

		for(ix = srcw, pos = xpos_top; ix > 0; ix--, pos += 4, dstbuf += 4)
		{
			c[0] = c[1] = c[2] = 0;
		
			for(i = 0; i < 4; i++)
			{
				ps = tbl_psY[i] + pos;
				n = tbl_weight[i];

				c[0] += ps[0] * n;
				c[1] += ps[1] * n;
				c[2] += ps[2] * n;
			}

			for(i = 0; i < 3; i++)
			{
				n = c[i];

				if(n <= 0)
					n = 0;
				else
				{
					n = (n + _RESIZE_FHALF) >> _RESIZE_FBIT;
					if(n > 255) n = 255;
				}

				dstbuf[i] = n;
			}
		}
	}

	return TRUE;
}

/* 拡大:水平リサイズ */

static mlkbool _resize_up_horz(uint8_t *dstbuf,int dstw,
	uint8_t *srcbuf,int srcw,int srch,double *weightbuf)
{
	int ix,iy,i,pos,n,pitchs,pitchd,flen,xpos,c[3],tbl_xpos[4],tbl_weight[4];
	double dsx,dscale,dsum,dweight[4];
	uint8_t *ps,*pd,*psY;

	dscale = (double)srcw / dstw;
	pitchs = srcw * 4;
	pitchd = dstw * 4;

	dsx = 0.5 * dscale;

	for(ix = dstw, xpos = 0; ix > 0; ix--, dsx += dscale, xpos += 4)
	{
		//テーブル

		pos = (int)(dsx - 2.5);
		flen = (int)((pos + 0.5 - dsx) * WEIGHTTBL_LENVAL);
		dsum = 0;

		for(i = 0; i < 4; i++, flen += WEIGHTTBL_LENVAL)
		{
			//位置

			n = pos + i;
			if(n < 0) n = 0;
			else if(n >= srcw) n = srcw - 1;

			tbl_xpos[i] = n * 4;

			//重み

			n = flen;
			if(n < 0) n = -n;

			dweight[i] = (n < WEIGHTTBL_LENVAL2)? weightbuf[n]: 0;
			dsum += dweight[i];
		}

		for(i = 0; i < 4; i++)
			tbl_weight[i] = (int)(dweight[i] / dsum * _RESIZE_FVAL);

		//----

		psY = srcbuf;
		pd = dstbuf + xpos;
		
		for(iy = srch; iy > 0; iy--, psY += pitchs, pd += pitchd)
		{
			c[0] = c[1] = c[2] = 0;
		
			for(i = 0; i < 4; i++)
			{
				ps = psY + tbl_xpos[i];
				n = tbl_weight[i];

				c[0] += ps[0] * n;
				c[1] += ps[1] * n;
				c[2] += ps[2] * n;
			}

			for(i = 0; i < 3; i++)
			{
				n = c[i];

				if(n <= 0)
					n = 0;
				else
				{
					n = (n + _RESIZE_FHALF) >> _RESIZE_FBIT;
					if(n > 255) n = 255;
				}

				pd[i] = n;
			}
		}
	}

	return TRUE;
}

/* 縮小:垂直リサイズ */

static mlkbool _resize_down_vert(uint8_t *dstbuf,int dsth,
	uint8_t *srcbuf,int srcw,int srch,int pitchsrc,int xpos_top,int range,double *weightbuf)
{
	int ix,iy,i,pos,n,tap,flen,fleninc,c[3],*tbl_weight;
	double dsy,dscale,dscalerev,dsum,*dweight;
	uint8_t *ps,**tbl_psY;

	dscale = (double)dsth / srch;
	dscalerev = (double)srch / dsth;
	tap = (int)(dscalerev * range * 2 + 0.5);
	fleninc = (int)(dscale * _RESIZE_LEN_VAL + 0.5);

	//バッファ確保

	tbl_weight = (int *)mMalloc(sizeof(int) * tap);
	tbl_psY = (uint8_t **)mMalloc(sizeof(void*) * tap);
	dweight = (double *)mMalloc(sizeof(double) * tap);

	if(!tbl_weight || !tbl_psY || !dweight)
	{
		mFree(tbl_weight);
		mFree(tbl_psY);
		mFree(dweight);
		return FALSE;
	}

	//

	dsy = (0.5 - range) * dscalerev + 0.5;

	for(iy = 0; iy < dsth; iy++, dsy += dscalerev)
	{
		pos = (int)dsy;

		//テーブル

		flen = (int)(((pos + 0.5) * dscale - (iy + 0.5)) * _RESIZE_LEN_VAL);
		dsum = 0;

		for(i = 0; i < tap; i++, flen += fleninc)
		{
			//位置

			n = pos + i;
			if(n < 0) n = 0;
			else if(n >= srch) n = srch - 1;

			tbl_psY[i] = srcbuf + n * pitchsrc;

			//重み

			n = flen >> (_RESIZE_LEN_BIT - WEIGHTTBL_LENBIT);
			if(n < 0) n = -n;

			dweight[i] = (n < WEIGHTTBL_LENVAL3)? weightbuf[n]: 0;
			dsum += dweight[i];
		}

		for(i = 0; i < tap; i++)
			tbl_weight[i] = (int)(dweight[i] / dsum * _RESIZE_FVAL);

		//----

		for(ix = srcw, pos = xpos_top; ix > 0; ix--, pos += 4, dstbuf += 4)
		{
			c[0] = c[1] = c[2] = 0;
		
			for(i = 0; i < tap; i++)
			{
				ps = tbl_psY[i] + pos;
				n = tbl_weight[i];

				c[0] += ps[0] * n;
				c[1] += ps[1] * n;
				c[2] += ps[2] * n;
			}

			for(i = 0; i < 3; i++)
			{
				n = c[i];

				if(n <= 0)
					n = 0;
				else
				{
					n = (n + _RESIZE_FHALF) >> _RESIZE_FBIT;
					if(n > 255) n = 255;
				}

				dstbuf[i] = n;
			}
		}
	}

	mFree(tbl_weight);
	mFree(tbl_psY);
	mFree(dweight);

	return TRUE;
}

/* 縮小:水平リサイズ */

static mlkbool _resize_down_horz(uint8_t *dstbuf,int dstw,
	uint8_t *srcbuf,int srcw,int srch,int range,double *weightbuf)
{
	int ix,iy,i,pos,n,tap,pitchs,pitchd,xpos,flen,fleninc,c[3],*tbl_xpos,*tbl_weight;
	double dsx,dscale,dscalerev,dsum,*dweight;
	uint8_t *ps,*pd,*psY;

	pitchs = srcw * 4;
	pitchd = dstw * 4;

	dscale = (double)dstw / srcw;
	dscalerev = (double)srcw / dstw;
	tap = (int)(dscalerev * range * 2 + 0.5);
	fleninc = (int)(dscale * _RESIZE_LEN_VAL + 0.5);

	//バッファ確保

	tbl_weight = (int *)mMalloc(sizeof(int) * tap);
	tbl_xpos = (int *)mMalloc(sizeof(int) * tap);
	dweight = (double *)mMalloc(sizeof(double) * tap);

	if(!tbl_weight || !tbl_xpos || !dweight)
	{
		mFree(tbl_weight);
		mFree(tbl_xpos);
		mFree(dweight);
		return FALSE;
	}

	//

	dsx = (0.5 - range) * dscalerev + 0.5;

	for(ix = 0, xpos = 0; ix < dstw; ix++, dsx += dscalerev, xpos += 4)
	{
		pos = (int)dsx;

		//テーブル

		flen = (int)(((pos + 0.5) * dscale - (ix + 0.5)) * _RESIZE_LEN_VAL);
		dsum = 0;

		for(i = 0; i < tap; i++, flen += fleninc)
		{
			//位置

			n = pos + i;
			if(n < 0) n = 0;
			else if(n >= srcw) n = srcw - 1;

			tbl_xpos[i] = n * 4;

			//重み

			n = flen >> (_RESIZE_LEN_BIT - WEIGHTTBL_LENBIT);
			if(n < 0) n = -n;

			dweight[i] = (n < WEIGHTTBL_LENVAL3)? weightbuf[n]: 0;
			dsum += dweight[i];
		}

		for(i = 0; i < tap; i++)
			tbl_weight[i] = (int)(dweight[i] / dsum * _RESIZE_FVAL);

		//----

		psY = srcbuf;
		pd = dstbuf + xpos;
		
		for(iy = srch; iy > 0; iy--, psY += pitchs, pd += pitchd)
		{
			c[0] = c[1] = c[2] = 0;
		
			for(i = 0; i < tap; i++)
			{
				ps = psY + tbl_xpos[i];
				n = tbl_weight[i];

				c[0] += ps[0] * n;
				c[1] += ps[1] * n;
				c[2] += ps[2] * n;
			}

			for(i = 0; i < 3; i++)
			{
				n = c[i];

				if(n <= 0)
					n = 0;
				else
				{
					n = (n + _RESIZE_FHALF) >> _RESIZE_FBIT;
					if(n > 255) n = 255;
				}

				pd[i] = n;
			}
		}
	}

	mFree(tbl_weight);
	mFree(tbl_xpos);
	mFree(dweight);

	return TRUE;
}

/** リサイズした画像を作成
 *
 * 重みテーブルはセット済みであること。
 *
 * srcw: ソースの幅 (分割時は分割後の幅)
 * splitno: 分割タイプ (-1:なし, 0:1枚目, 1:2枚目) */

ImageBuf *ImageBuf_resize(ImageBuf *p,int neww,int newh,int range,int srcw,int splitno,
	double *tblup,double *tbldown)
{
	ImageBuf *imgnew;
	uint8_t *tmpbuf;
	mlkbool scaleup,ret;
	int xpos_top;

	if(!p) return NULL;

	scaleup = (p->w < neww);
	xpos_top = (splitno == 1)? (p->w - srcw) * 4: 0;

	//出力画像作成

	imgnew = ImageBuf_new(neww, newh);
	if(!imgnew) return NULL;

	//垂直リサイズのバッファ

	tmpbuf = (uint8_t *)mMalloc(srcw * newh * 4);
	if(!tmpbuf)
	{
		ImageBuf_free(imgnew);
		return NULL;
	}

	//垂直リサイズ

	if(scaleup)
		ret = _resize_up_vert(tmpbuf, newh, p->buf, srcw, p->h, p->w * 4, xpos_top, tblup);
	else
		ret = _resize_down_vert(tmpbuf, newh, p->buf, srcw, p->h, p->w * 4, xpos_top, range, tbldown);

	if(!ret) goto ERR;

	//水平リサイズ

	if(scaleup)
		ret = _resize_up_horz(imgnew->buf, neww, tmpbuf, srcw, newh, tblup);
	else
		ret = _resize_down_horz(imgnew->buf, neww, tmpbuf, srcw, newh, range, tbldown);

	if(!ret) goto ERR;

	mFree(tmpbuf);

	return imgnew;

ERR:
	mFree(tmpbuf);
	ImageBuf_free(imgnew);
	return NULL;
}

