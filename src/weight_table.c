/*$
azcomicv
Copyright (c) 2017-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/******************************
 * 拡大縮小の重みテーブル
 ******************************/
/*
 * - 高速化のため、重みの計算に必要な "距離" を固定少数点数で扱い、
 *   段階的な重みのテーブル値を作成する。
 * - 距離は 0〜 [(Lanczos3/spline36) 3.0 (それ以外) 2.0]
 */

#include <math.h>

#include <mlk.h>

#include "weight_table.h"


//-----------------

#define _TBLNUM_UP    WEIGHTTBL_LENVAL2
#define _TBLNUM_DOWN  WEIGHTTBL_LENVAL3

//-----------------


/** Bicubic テーブルセット */

void WeightTable_setUp_bicubic(double *buf)
{
	int i;
	double d = 0;

	for(i = 0; i < _TBLNUM_UP; i++)
	{
		d = (double)i / WEIGHTTBL_LENVAL;

		if(d < 1.0)
			d = 1.0 - 2 * d * d + d * d * d;
		else if(d < 2.0)
			d = 4.0 - 8 * d + 5 * d * d - d * d * d;
		else
			d = 0;

		*(buf++) = d;
	}
}

/** mitchell テーブルセット */

void WeightTable_setDown_mitchell(double *buf)
{
	int i;
	double d;

	for(i = 0; i < _TBLNUM_DOWN; i++)
	{
		d = (double)i / WEIGHTTBL_LENVAL;

		if (d < 1.0)
			d = 7 * d * d * d / 6 - 2 * d * d + 8.0 / 9.0;
		else if(d < 2.0)
			d = 2 * d * d - 10 * d / 3 - 7 * d * d * d / 18 + 16.0 / 9.0;
		else
			d = 0;

		*(buf++) = d;
	}
}

/** Lagrange テーブルセット */

void WeightTable_setDown_lagrange(double *buf)
{
	int i;
	double d;

	for(i = 0; i < _TBLNUM_DOWN; i++)
	{
		d = (double)i / WEIGHTTBL_LENVAL;

		if (d < 1.0)
			d = 0.5 * (d - 2) * (d + 1) * (d - 1);
		else if(d < 2.0)
			d = -(d - 3) * (d - 2) * (d - 1) / 6;
		else
			d = 0;

		*(buf++) = d;
	}
}

/** Lanczos2 テーブルセット */

void WeightTable_setDown_lanczos2(double *buf)
{
	int i;
	double d;

	for(i = 0; i < _TBLNUM_DOWN; i++)
	{
		d = (double)i / WEIGHTTBL_LENVAL;

		if(d < MLK_MATH_DBL_EPSILON)
			d = 1;
		else if(d < 2.0)
		{
			d *= MLK_MATH_PI;
			d = sin(d) * sin(d / 2.0) / (d * d / 2.0);
		}
		else
			d = 0;

		*(buf++) = d;
	}
}

/** Lanczos3 テーブルセット */

void WeightTable_setDown_lanczos3(double *buf)
{
	int i;
	double d;

	for(i = 0; i < _TBLNUM_DOWN; i++)
	{
		d = (double)i / WEIGHTTBL_LENVAL;

		if(d < MLK_MATH_DBL_EPSILON)
			d = 1;
		else if(d < 3.0)
		{
			d *= MLK_MATH_PI;
			d = sin(d) * sin(d / 3.0) / (d * d / 3.0);
		}
		else
			d = 0;

		*(buf++) = d;
	}
}

/** spline16 テーブルセット */

void WeightTable_setDown_spline16(double *buf)
{
	int i;
	double d;

	for(i = 0; i < _TBLNUM_DOWN; i++)
	{
		d = (double)i / WEIGHTTBL_LENVAL;

		if(d < 1.0)
			d = (d - 1) * (5 * d * d - 4 * d - 5) / 5;
		else if(d < 2.0)
			d = (5 * d - 12) * (d - 1) * (d - 2) / -15;
		else
			d = 0;

		*(buf++) = d;
	}
}

/** spline36 テーブルセット */

void WeightTable_setDown_spline36(double *buf)
{
	int i;
	double d;

	for(i = 0; i < _TBLNUM_DOWN; i++)
	{
		d = (double)i / WEIGHTTBL_LENVAL;

		if(d < 1.0)
			d = (d - 1) * (247 * d * d - 206 * d - 209) / 209;
		else if(d < 2.0)
			d = (19 * d - 45) * (d - 1) * (d - 2) * 6 / -209;
		else if(d < 3.0)
			d = (19 * d - 64) * (d - 2) * (d - 3) / 209;
		else
			d = 0;

		*(buf++) = d;
	}
}

