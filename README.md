# azcomicv

http://azsky2.html.xdomain.jp/

シンプルな漫画ビューア。

対応画像: BMP/PNG/JPEG/GIF/TIFF/WebP/PSD/AVIF<br>
対応アーカイブ: ZIP

## 動作環境

- Linux、macOS(要XQuartz) ほか
- X11

## コンパイル/インストール

ninja、pkg-config コマンドが必要です。<br>
各開発用ファイルのパッケージも必要になります。ReadMe を参照してください。

~~~
$ ./configure
$ cd build
$ ninja
# ninja install
~~~
