/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_PV_WINDOW_H
#define MLK_PV_WINDOW_H

//(focus_ready_type)

#define MWINDOW_FOCUS_READY_TYPE_FOCUSOUT_WINDOW 0	//ウィンドウのフォーカスアウトによる
#define MWINDOW_FOCUS_READY_TYPE_NO_FOCUS 1			//ウィンドウにフォーカスがない状態でセットされた

//状態フラグのマクロ

#define MLK_WINDOW_IS_MAP(p)    ((p)->win.fstate & MWINDOW_STATE_MAP)
#define MLK_WINDOW_IS_UNMAP(p)  (!((p)->win.fstate & MWINDOW_STATE_MAP))
#define MLK_WINDOW_ISNOT_NORMAL_SIZE(p) ((p)->win.fstate & (MWINDOW_STATE_MAXIMIZED|MWINDOW_STATE_FULLSCREEN))
#define MLK_WINDOW_IS_NORMAL_SIZE(p)    (!((p)->win.fstate & (MWINDOW_STATE_MAXIMIZED|MWINDOW_STATE_FULLSCREEN)))


/*--- mTooltip ---*/

typedef struct
{
	mWidgetLabelText txt;
}mTooltipData;

struct _mTooltip
{
	mWidget wg;
	mContainerData ct;
	mWindowData win;
	mPopupData pop;
	mTooltipData ttip;
};


/*--- func ---*/

/* mlk_pv_window.c */

mWindow *__mWindowNew(mWidget *parent,int size);

void __mWindowUpdateRootRect(mWindow *p,mRect *rc);
void __mWindowUpdateRoot(mWindow *p,int x,int y,int w,int h);
void __mWindowUpdateRect(mWindow *p,mRect *rc);
void __mWindowUpdate(mWindow *p,int x,int y,int w,int h);

void __mWindowGetSize_excludeDecoration(mWindow *p,int w,int h,mSize *size);
void __mWindowOnResize_setNormal(mWindow *p,int w,int h);
void __mWindowOnResize_configure(mWindow *p,int w,int h);
void __mWindowOnConfigure_state(mWindow *p,uint32_t mask);

mlkbool __mWindowSetFocus(mWindow *win,mWidget *focus,int from);
mWidget *__mWindowGetStateEnable_first(mWindow *win,uint32_t flags,mlkbool is_focus);
mWidget *__mWindowGetTakeFocus_first(mWindow *win);
mWidget *__mWindowGetTakeFocus_next(mWindow *win);
mWidget *__mWindowGetDefaultFocusWidget(mWindow *win,int *dst_from);
mlkbool __mWindowMoveFocusNext(mWindow *win);
mWidget *__mWindowGetDNDDropTarget(mWindow *p,int x,int y);

void __mPopupGetRect(mRect *rcdst,mRect *rc,int w,int h,uint32_t f,int flip);

void __mWindowDecoSetInfo(mWindow *p,mWindowDecoInfo *dst);
void __mWindowDecoRunSetWidth(mWindow *p,int type);
void __mWindowDecoRunUpdate(mWindow *p,int type);

/* mlk_menuwindow.c */

mMenuItem *__mMenuWindowRunPopup(mWidget *parent,int x,int y,mBox *box,uint32_t flags,mMenu *menu,mWidget *send);
mMenuItem *__mMenuWindowRunMenuBar(mMenuBar *menubar,mBox *box,mMenu *menu,mMenuItem *item);
void __mMenuWindowMoveMenuBarItem(mMenuItem *item);

#endif
