/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_PV_MENU_H
#define MLK_PV_MENU_H

#define MLK_MENUITEM(p)  ((mMenuItem *)(p))

/** mMenu 実体 */

struct _mMenu
{
	mList list;
	mMenuItem *parent;	//サブメニューの場合、親のアイテム
};

/** mMenuItem 実体 */

struct _mMenuItem
{
	mListItem i;

	mMenuItemInfo info;	//基本情報

	mMenu *parent;		//親の mMenu
	char *sckeytxt;		//ショートカットキーの文字列 ("Ctrl+A" など)
	int16_t text_w,		//ラベルテキストの幅 (-1 でサイズ計算)
		sckey_w;		//ショートカットキーテキストの幅
	char hotkey;		//&+* のホットキー文字 (0 でなし。0-9,A-Z のみ)
};


/* mMenuItem */

void __mMenuItemInitLabel(mMenuItem *p);
void __mMenuItemInitSckey(mMenuItem *p);
mMenuItem *__mMenuItemGetNext(mMenuItem *p,mMenu *root);
void __mMenuItemSetSubmenu(mMenuItem *p,mMenu *submenu);
mlkbool __mMenuItemIsSelectable(mMenuItem *p);
mlkbool __mMenuItemIsShowableSubmenu(mMenuItem *p);
mMenuItem *__mMenuItemGetRadioInfo(mMenuItem *p,mMenuItem **pptop,mMenuItem **ppend);
void __mMenuItemSetCheckRadio(mMenuItem *p);

/* mMenu */

mMenuItem *__mMenuFindItem_hotkey(mMenu *menu,char key);
void __mMenuInitTextSize(mMenu *p,mFont *font);
mMenuItem *__mMenuGetSelectableTopItem(mMenu *menu);
mMenuItem *__mMenuGetSelectableBottomItem(mMenu *menu);
mMenuItem *__mMenuGetPrevSelectableItem(mMenu *p,mMenuItem *item);
mMenuItem *__mMenuGetNextSelectableItem(mMenu *p,mMenuItem *item);

#endif
