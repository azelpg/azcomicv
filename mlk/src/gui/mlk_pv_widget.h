/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_PV_WIDGET_H
#define MLK_PV_WIDGET_H

#define MLK_WIDGET_IS_TOPLEVEL(p)  (( ((mWidget *)(p))->ftype & MWIDGET_TYPE_WINDOW_TOPLEVEL) != 0)

/* mWidgetRect */

void __mWidgetRectSetSame(mWidgetRect *p,int val);
void __mWidgetRectSetPack4(mWidgetRect *p,uint32_t val);

/* mWidget */

void __mWidgetCreateInit(mWidget *p,int id,uint32_t flayout,uint32_t margin_pack);

mWidget *__mWidgetGetTreeNext(mWidget *p);
mWidget *__mWidgetGetTreeNextPass(mWidget *p);
mWidget *__mWidgetGetTreeNextPass_root(mWidget *p,mWidget *root);
mWidget *__mWidgetGetTreeNext_root(mWidget *p,mWidget *root);

mWidget *__mWidgetGetListSkipNum(mWidget *p,int dir);

void __mWidgetSetUIFlags_upper(mWidget *p,uint32_t flags);
void __mWidgetSetUIFlags_draw(mWidget *p);
mWidget *__mWidgetGetTreeNext_uiflag(mWidget *p,uint32_t follow_mask,uint32_t run_mask);
mWidget *__mWidgetGetTreeBottom_uiflag(mWidget *p,uint32_t follow_mask);
mWidget *__mWidgetGetTreePrev_uiflag(mWidget *p,uint32_t follow_mask,uint32_t run_mask);
mWidget *__mWidgetGetTreeNext_draw(mWidget *p);
mlkbool __mWidgetGetWindowRect(mWidget *p,mRect *dst,mPoint *ptoffset,mlkbool root);

void __mWidgetOnResize(mWidget *p);
mlkbool __mWidgetLayoutMoveResize(mWidget *p,int x,int y,int w,int h);
void __mWidgetCalcHint(mWidget *p);
int __mWidgetGetLayoutW(mWidget *p);
int __mWidgetGetLayoutH(mWidget *p);
int __mWidgetGetLayoutW_space(mWidget *p);
int __mWidgetGetLayoutH_space(mWidget *p);
void __mWidgetGetLayoutCalcSize(mWidget *p,mSize *hint,mSize *init);

void __mWidgetDrawBkgnd(mWidget *wg,mBox *box,mlkbool force);

void __mWidgetRemoveSelfFocus(mWidget *p);
void __mWidgetRemoveFocus_forDisabled(mWidget *p);
void __mWidgetLeave_forDisable(mWidget *p);
mlkbool __mWidgetMoveFocus_arrowkey(mWidget *p,uint32_t key);

#endif
