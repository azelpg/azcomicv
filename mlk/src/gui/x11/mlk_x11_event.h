/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_X11_EVENT_H
#define MLK_X11_EVENT_H

typedef struct
{
	XEvent *xev;
	mWindow *win;		//イベントの対象ウィンドウ

	int waitev_type;		//待ちイベントタイプ (MLKX11_WAITEVENT_*)
	mlkbool waitev_check,	//待ちイベントをチェックするか (各イベントごとで対象のウィンドウか)
		waitev_flag;		//待ちイベントが来たかどうかの結果フラグ
}_X11_EVENT;


/* mlk_x11_event_sub.c */

mWindow *mX11Event_getWindow(mAppX11 *p,Window id);
void mX11Event_setTime(mAppX11 *p,XEvent *ev);
int mX11Event_convertButton(int btt);
uint32_t mX11Event_convertState(unsigned int state);

char *mX11Event_key_getIMString(_X11_EVENT *p,XKeyEvent *xev,KeySym *keysym,char *dst_char,int *dst_len);
mlkbool mX11Event_checkDblClk(mWindow *win,int x,int y,int btt,Time time);
int mX11Event_proc_button(_X11_EVENT *p,int btt,uint32_t state,int press);

void mX11Event_clientmessage_wmprotocols(_X11_EVENT *p);
void mX11Event_property_netwmstate(_X11_EVENT *p);
void mX11Event_map_setRequest(mAppX11 *p,mWindow *win);

#endif
