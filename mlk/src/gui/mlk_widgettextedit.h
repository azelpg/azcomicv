/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_WIDGET_TEXT_EDIT_H
#define MLK_WIDGET_TEXT_EDIT_H

typedef struct _mWidgetTextEdit mWidgetTextEdit;

typedef struct
{
	uint32_t *text;
	int len,width,is_sel;
}mWidgetTextEditState;

enum
{
	MWIDGETTEXTEDIT_KEYPROC_CHANGE_CURSOR = 1,
	MWIDGETTEXTEDIT_KEYPROC_CHANGE_TEXT,
	MWIDGETTEXTEDIT_KEYPROC_UPDATE
};


void mWidgetTextEdit_free(mWidgetTextEdit *p);
void mWidgetTextEdit_init(mWidgetTextEdit *p,mWidget *widget,mlkbool multi_line);

mlkbool mWidgetTextEdit_resize(mWidgetTextEdit *p,int len);
void mWidgetTextEdit_empty(mWidgetTextEdit *p);
mlkbool mWidgetTextEdit_isEmpty(mWidgetTextEdit *p);

mlkbool mWidgetTextEdit_isSelected_atPos(mWidgetTextEdit *p,int pos);
void mWidgetTextEdit_moveCursor_toBottom(mWidgetTextEdit *p);
int mWidgetTextEdit_multi_getPosAtPixel(mWidgetTextEdit *p,int x,int y,int lineh);
int mWidgetTextEdit_getLineState(mWidgetTextEdit *p,int top,int end,mWidgetTextEditState *dst);

void mWidgetTextEdit_setText(mWidgetTextEdit *p,const char *text);
mlkbool mWidgetTextEdit_insertText(mWidgetTextEdit *p,const char *text,char ch);

mlkbool mWidgetTextEdit_selectAll(mWidgetTextEdit *p);
mlkbool mWidgetTextEdit_deleteSelText(mWidgetTextEdit *p);
void mWidgetTextEdit_deleteText(mWidgetTextEdit *p,int pos,int len);
void mWidgetTextEdit_expandSelect(mWidgetTextEdit *p,int pos);
void mWidgetTextEdit_moveCursorPos(mWidgetTextEdit *p,int pos,int select);
void mWidgetTextEdit_dragExpandSelect(mWidgetTextEdit *p,int pos);

mlkbool mWidgetTextEdit_paste(mWidgetTextEdit *p);
void mWidgetTextEdit_copy(mWidgetTextEdit *p);
mlkbool mWidgetTextEdit_cut(mWidgetTextEdit *p);

int mWidgetTextEdit_keyProc(mWidgetTextEdit *p,uint32_t key,uint32_t state,int enable_edit);

#endif
