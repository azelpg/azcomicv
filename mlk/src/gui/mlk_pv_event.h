/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_PV_EVENT_H
#define MLK_PV_EVENT_H

//__mEventProcButton 時のフラグ
enum
{
	MEVENTPROC_BUTTON_F_PRESS = 1<<0,
	MEVENTPROC_BUTTON_F_DBLCLK = 1<<1,
	MEVENTPROC_BUTTON_F_ADD_PENTAB = 1<<2	//PENTAB イベントを生成
};

mlkbool __mEventIsModalSkip(mWindow *win);
mlkbool __mEventIsModalWindow(mWindow *win);
void __mEventOnUngrab(void);
char **__mEventGetURIList_ptr(uint8_t *datbuf);

void __mEventProcClose(mWindow *win);
void __mEventProcConfigure(mWindow *win,int w,int h,uint32_t state,uint32_t state_mask,uint32_t flags);
void __mEventProcFocusIn(mWindow *win);
void __mEventProcFocusOut(mWindow *win);
void __mEventProcEnter(mWindow *win,int fx,int fy);
void __mEventProcLeave(mWindow *win);
void __mEventReEnter(void);

mWidget *__mEventProcKey(mWindow *win,uint32_t key,uint32_t rawcode,uint32_t state,int press,mlkbool key_repeat);

void __mEventProcScroll(mWindow *win,int hval,int vval,int hstep,int vstep,uint32_t state);
mWidget *__mEventProcButton(mWindow *win,int fx,int fy,int btt,int rawbtt,uint32_t state,uint8_t flags);
mWidget *__mEventProcMotion(mWindow *win,int fx,int fy,uint32_t state,mlkbool add_pentab);

void __mEventProcButton_pentab(mWindow *win,
	double x,double y,double pressure,int btt,int rawbtt,uint32_t state,uint32_t evflags,uint8_t flags);
void __mEventProcMotion_pentab(mWindow *win,double x,double y,
	double pressure,uint32_t state,uint32_t evflags);

#endif
