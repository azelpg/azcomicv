/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * AVIF 読み込み
 *****************************************/

#include <string.h>
#include <libheif/heif.h>

#include <mlk.h>
#include <mlk_loadimage.h>
#include <mlk_io.h>
#include <mlk_imageconv.h>
#include <mlk_util.h>


//---------------

typedef struct
{
	mIO *io;
	mLoadImage *loadimg;

	struct heif_image *img;
	struct heif_reader reader;
	struct heif_error herr;
}avifdata;

#define _DATA(p) ((avifdata *)(p))

//---------------


//=================================
// heif_reader
//=================================


/* 位置を取得 */

static int64_t _reader_get_position(void *p)
{
	return mIO_getPos(_DATA(p)->io);
}

/* 読み込み */

static int _reader_read(void *data,size_t size,void *p)
{
	return mIO_readOK(_DATA(p)->io, data, size);
}

/* シーク */

static int _reader_seek(int64_t position,void *p)
{
	return mIO_seekTop(_DATA(p)->io, position);
}

/* 指定サイズまで読み込めるか */

static enum heif_reader_grow_status _reader_wait_for_file_size(int64_t target_size,void *p)
{
	if(mIO_isExistSize(_DATA(p)->io, target_size))
		return heif_reader_grow_status_size_reached;
	else
		return heif_reader_grow_status_size_beyond_eof;
}


//=================================
// sub
//=================================


/* heif_error 処理 */

static int _herror(avifdata *p)
{
	if(p->herr.code && p->herr.message[0])
		p->loadimg->errmessage = mStrdup(p->herr.message);

	return p->herr.code;
}

/* Exif メタデータ */

static void _get_metadata(avifdata *p,mLoadImage *pli,struct heif_image_handle *imgh)
{
	heif_item_id id;
	uint8_t *buf;
	size_t size;
	uint32_t offset;

	//ID 取得

	if(!heif_image_handle_get_list_of_metadata_block_IDs(imgh, "Exif", &id, 1))
		return;

	//取得

	size = heif_image_handle_get_metadata_size(imgh, id);
	if(size < 4) return;

	buf = (uint8_t *)mMalloc(size);
	if(!buf) return;

	p->herr = heif_image_handle_get_metadata(imgh, id, buf);
	if(_herror(p))
	{
		mFree(buf);
		return;
	}

	//先頭 4byte は TIFF ヘッダ位置へのオフセット (BE)

	offset = mGetBufBE32(buf);

	//EXIF から解像度取得 ("MM"or"II" の位置から)

	if(4 + offset < size)
		mLoadImage_getEXIF_resolution(pli, buf + 4 + offset, size - (4 + offset));

	mFree(buf);
}

/* 開いた時の処理 */

static mlkerr _proc_open(avifdata *p,mLoadImage *pli)
{
	struct heif_context *ctx;
	struct heif_image_handle *imgh = 0;
	struct heif_image *img = 0;
	enum heif_chroma chroma;

	p->loadimg = pli;

	//heif_context 作成

	ctx = heif_context_alloc();
	if(!ctx) return MLKERR_ALLOC;

	//読み込み

	p->reader.reader_api_version = 1;
	p->reader.get_position = _reader_get_position;
	p->reader.read = _reader_read;
	p->reader.seek = _reader_seek;
	p->reader.wait_for_file_size = _reader_wait_for_file_size;

	p->herr = heif_context_read_from_reader(ctx, &p->reader, p, 0);
	if(_herror(p)) goto ERR;

	//プライマリ画像取得

	p->herr = heif_context_get_primary_image_handle(ctx, &imgh);
	if(_herror(p)) goto ERR;

	//元のカラータイプ

	pli->src_coltype = (heif_image_handle_has_alpha_channel(imgh))
		? MLOADIMAGE_COLTYPE_RGBA: MLOADIMAGE_COLTYPE_RGB;

	//カラーの判定

	if(pli->convert_type == MLOADIMAGE_CONVERT_TYPE_RGBA)
		chroma = heif_chroma_interleaved_RGBA;
	else if(pli->convert_type == MLOADIMAGE_CONVERT_TYPE_RGB)
		chroma = heif_chroma_interleaved_RGB;
	else
	{
		//生データ
		
		chroma = (heif_image_handle_has_alpha_channel(imgh))
			? heif_chroma_interleaved_RGBA: heif_chroma_interleaved_RGB;
	}

	//デコード (RGB or RGBA)

	p->herr = heif_decode_image(imgh, &img, heif_colorspace_RGB, chroma, 0);
	if(_herror(p)) goto ERR;

	p->img = img;

	//情報取得

	pli->width = heif_image_get_width(img, heif_channel_interleaved);
	pli->height = heif_image_get_height(img, heif_channel_interleaved);
	pli->bits_per_sample = 8;

	//カラータイプ

	if(chroma == heif_chroma_interleaved_RGBA)
		pli->coltype = MLOADIMAGE_COLTYPE_RGBA;
	else
		pli->coltype = MLOADIMAGE_COLTYPE_RGB;

	//メタデータ

	_get_metadata(p, pli, imgh);

	//

	heif_image_handle_release(imgh);
	heif_context_free(ctx);

	return MLKERR_OK;

ERR:
	if(imgh) heif_image_handle_release(imgh);
	heif_context_free(ctx);

	return MLKERR_UNKNOWN;
}


//=================================
// main
//=================================


/* 終了 */

static void _avif_close(mLoadImage *pli)
{
	avifdata *p = (avifdata *)pli->handle;

	if(p)
	{
		if(p->img)
			heif_image_release(p->img);

		mIO_close(p->io);
	}

	mLoadImage_closeHandle(pli);
}

/* 開く */

static mlkerr _avif_open(mLoadImage *pli)
{
	int ret;

	ret = mLoadImage_createHandle(pli, sizeof(avifdata), MIO_ENDIAN_LITTLE);
	if(ret) return ret;

	//処理

	return _proc_open((avifdata *)pli->handle, pli);
}

/* イメージ読み込み */

static mlkerr _avif_getimage(mLoadImage *pli)
{
	avifdata *p = (avifdata *)pli->handle;
	uint8_t **ppbuf;
	const uint8_t *ps;
	mFuncImageConv funcconv;
	mImageConv conv;
	int pitchs,i,height,prog,prog_cur;

	//変換パラメータ

	mLoadImage_setImageConv(pli, &conv);

	funcconv = (pli->coltype == MLOADIMAGE_COLTYPE_RGBA)? mImageConv_rgba8: mImageConv_rgb8;

	//変換

	ps = heif_image_get_plane_readonly(p->img, heif_channel_interleaved, &pitchs);

	ppbuf = pli->imgbuf;
	height = pli->height;
	prog_cur = 0;

	for(i = 0; i < height; i++)
	{
		conv.srcbuf = ps;
		conv.dstbuf = *ppbuf;

		(funcconv)(&conv);

		//
	
		ppbuf++;
		ps += pitchs;

		//進捗

		if(pli->progress)
		{
			prog = (i + 1) * 100 / height;

			if(prog != prog_cur)
			{
				prog_cur = prog;
				(pli->progress)(pli, prog);
			}
		}
	}

	return MLKERR_OK;
}

/**@ AVIF 判定と関数セット */

mlkbool mLoadImage_checkAVIF(mLoadImageType *p,uint8_t *buf,int size)
{
	if(buf)
	{
		if(size < 12)
			return FALSE;
		else if(memcmp(buf + 4, "ftypavif", 8) && memcmp(buf + 4, "ftypavis", 8))
			return FALSE;
	}

	p->format_tag = MLK_MAKE32_4('A','V','I','F');
	p->open = _avif_open;
	p->getimage = _avif_getimage;
	p->close = _avif_close;
		
	return TRUE;
}

