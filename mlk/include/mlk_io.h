/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_IO_H
#define MLK_IO_H

typedef struct _mIO mIO;

enum MIO_ENDIAN
{
	MIO_ENDIAN_HOST = 0,
	MIO_ENDIAN_LITTLE,
	MIO_ENDIAN_BIG
};


#ifdef __cplusplus
extern "C" {
#endif

void mIO_close(mIO *p);

mIO *mIO_openReadFile(const char *filename);
mIO *mIO_openReadBuf(const void *buf,mlksize bufsize);
mIO *mIO_openReadFILEptr(void *fp);

void mIO_setEndian(mIO *p,int type);
void *mIO_getFILE(mIO *p);

mlksize mIO_getPos(mIO *p);
int mIO_seekTop(mIO *p,mlksize pos);
int mIO_seekCur(mIO *p,mlkfoff seek);
int mIO_isExistSize(mIO *p,mlksize size);

int mIO_read(mIO *p,void *buf,int32_t size);
int mIO_readOK(mIO *p,void *buf,int32_t size);
int mIO_readSkip(mIO *p,int32_t size);

int mIO_readByte(mIO *p,void *buf);
int mIO_read16(mIO *p,void *buf);
int mIO_read24(mIO *p,void *buf);
int mIO_read32(mIO *p,void *buf);
int mIO_readFormat(mIO *p,const char *format,...);

#ifdef __cplusplus
}
#endif

#endif
