/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_TEXTPARAM_H
#define MLK_TEXTPARAM_H

typedef struct _mTextParam mTextParam;

#ifdef __cplusplus
extern "C" {
#endif

mTextParam *mTextParam_new(const char *text,int splitpair,int splitparam);
void mTextParam_free(mTextParam *p);

mlkbool mTextParam_getInt(mTextParam *p,const char *key,int *dst);
mlkbool mTextParam_getIntRange(mTextParam *p,const char *key,int *dst,int min,int max);
mlkbool mTextParam_getDouble(mTextParam *p,const char *key,double *dst);
mlkbool mTextParam_getDoubleInt(mTextParam *p,const char *key,int *dst,int dig);
mlkbool mTextParam_getDoubleIntRange(mTextParam *p,const char *key,int *dst,int dig,int min,int max);
mlkbool mTextParam_getTextRaw(mTextParam *p,const char *key,char **dst);
mlkbool mTextParam_getTextDup(mTextParam *p,const char *key,char **buf);
mlkbool mTextParam_getTextStr(mTextParam *p,const char *key,mStr *str);
int mTextParam_findWord(mTextParam *p,const char *key,const char *wordlist,mlkbool iscase);

#ifdef __cplusplus
}
#endif

#endif
