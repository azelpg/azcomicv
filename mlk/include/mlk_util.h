/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_UTIL_H
#define MLK_UTIL_H

#ifdef __cplusplus
extern "C" {
#endif

void **mAllocArrayBuf(int size,int num);
void **mAllocArrayBuf_align(int size,int alignment,int num);
void mFreeArrayBuf(void **ppbuf,int num);

mlkbool mIsByteOrderLE(void);
uint32_t mRGBAtoHostOrder(uint32_t c);

int mDPMtoDPI(int dpm);
int mDPItoDPM(int dpi);

int mGetOnBitPosL(uint32_t val);
int mGetOffBitPosL(uint32_t val);
int mGetOnBitCount(uint32_t val);

mlkbool mIsChangeFlagState(int type,int current);
mlkbool mGetChangeFlagState(int type,int current,int *ret);

uint32_t mCalcStringHash(const char *str);
int mCalcMaxLog2(uint32_t n);

uint16_t mGetBufBE16(const void *buf);
uint32_t mGetBufBE32(const void *buf);
uint16_t mGetBufLE16(const void *buf);
uint32_t mGetBufLE32(const void *buf);

void mSetBufBE16(uint8_t *buf,uint16_t val);
void mSetBufBE32(uint8_t *buf,uint32_t val);
void mSetBufLE16(uint8_t *buf,uint16_t val);
void mSetBufLE32(uint8_t *buf,uint32_t val);

int mSetBuf_format(void *buf,const char *format,...);
int mGetBuf_format(const void *buf,const char *format,...);

void mCopyBuf_BE_16bit(void *dst,const void *src,uint32_t cnt);
void mCopyBuf_BE_32bit(void *dst,const void *src,uint32_t cnt);
void mConvertBuf_BE_16bit(void *buf,uint32_t cnt);
void mConvertBuf_BE_32bit(void *buf,uint32_t cnt);
void mReverseBit(uint8_t *buf,uint32_t bytes);
void mReverseVal_8bit(uint8_t *buf,uint32_t cnt);
void mReverseVal_16bit(void *buf,uint32_t cnt);
void mSwapByte_16bit(void *buf,uint32_t cnt);
void mSwapByte_32bit(void *buf,uint32_t cnt);

void mAddRecentVal_16bit(void *buf,int num,int addval,int endval);
void mAddRecentVal_32bit(void *buf,int num,uint32_t addval,uint32_t endval);

int mGetBase64EncodeSize(int size);
int mEncodeBase64(void *dst,int dstsize,const void *src,int size);
int mDecodeBase64(void *dst,int dstsize,const char *src,int len);

/* sys */

char *mGetProcessName(void);
mlkbool mExec(const char *cmd);
char *mGetSelfExePath(void);

#ifdef __cplusplus
}
#endif

#endif
