/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_FONTCONFIG_H
#define MLK_FONTCONFIG_H

typedef struct _FcPattern * mFcPattern;

#ifdef __cplusplus
extern "C" {
#endif

mlkbool mFontConfig_init(void);
void mFontConfig_finish(void);

mFcPattern mFontConfig_matchFontInfo(const mFontInfo *info);
mFcPattern mFontConfig_matchFamily(const char *family,const char *style);

void mFCPattern_free(mFcPattern pat);
mlkbool mFCPattern_getFile(mFcPattern pat,char **file,int *index);

char *mFCPattern_getText(mFcPattern pat,const char *object);
mlkbool mFCPattern_getBool_def(mFcPattern pat,const char *object,mlkbool def);
int mFCPattern_getInt_def(mFcPattern pat,const char *object,int def);
double mFCPattern_getDouble_def(mFcPattern pat,const char *object,double def);
mlkbool mFCPattern_getDouble(mFcPattern pat,const char *object,double *dst);
mlkbool mFCPattern_getMatrix(mFcPattern pat,const char *object,double *matrix);

#ifdef __cplusplus
}
#endif

#endif
