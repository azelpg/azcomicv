/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_CHECKBUTTON_H
#define MLK_CHECKBUTTON_H

#define MLK_CHECKBUTTON(p)  ((mCheckButton *)(p))
#define MLK_CHECKBUTTON_DEF mWidget wg; mCheckButtonData ckbtt;

typedef struct
{
	mWidgetLabelText txt;
	uint32_t fstyle,
		flags;
	int grabbed_key;
}mCheckButtonData;

struct _mCheckButton
{
	mWidget wg;
	mCheckButtonData ckbtt;
};

enum MCHECKBUTTON_STYLE
{
	MCHECKBUTTON_S_COPYTEXT = 1<<0,
	MCHECKBUTTON_S_RADIO = 1<<1,
	MCHECKBUTTON_S_RADIO_GROUP = 1<<2,
	MCHECKBUTTON_S_BUTTON = 1<<3
};

enum MCHECKBUTTON_FLAGS
{
	MCHECKBUTTON_F_CHECKED = 1<<0,
	MCHECKBUTTON_F_GRABBED = 1<<1,
	MCHECKBUTTON_F_CHANGED = 1<<2
};

enum MCHECKBUTTON_NOTIFY
{
	MCHECKBUTTON_N_CHANGE
};


#ifdef __cplusplus
extern "C" {
#endif

mCheckButton *mCheckButtonNew(mWidget *parent,int size,uint32_t fstyle);
mCheckButton *mCheckButtonCreate(mWidget *parent,int id,
	uint32_t flayout,uint32_t margin_pack,uint32_t fstyle,const char *text,int checked);

void mCheckButtonDestroy(mWidget *p);
void mCheckButtonHandle_calcHint(mWidget *p);
void mCheckButtonHandle_draw(mWidget *p,mPixbuf *pixbuf);
int mCheckButtonHandle_event(mWidget *wg,mEvent *ev);

mlkbool mCheckButtonIsRadioType(mCheckButton *p);
int mCheckButtonGetCheckBoxSize(void);
void mCheckButtonSetText(mCheckButton *p,const char *text);
void mCheckButtonSetText_copy(mCheckButton *p,const char *text);
mlkbool mCheckButtonSetState(mCheckButton *p,int type);
mlkbool mCheckButtonSetGroupSel(mCheckButton *p,int no);
int mCheckButtonGetGroupSel(mCheckButton *p);
mlkbool mCheckButtonIsChecked(mCheckButton *p);
mCheckButton *mCheckButtonGetRadioInfo(mCheckButton *start,mCheckButton **pptop,mCheckButton **ppend);

#ifdef __cplusplus
}
#endif

#endif
