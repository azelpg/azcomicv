/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_GROUPBOX_H
#define MLK_GROUPBOX_H

#define MLK_GROUPBOX(p)  ((mGroupBox *)(p))
#define MLK_GROUPBOX_DEF mWidget wg; mContainerData ct; mGroupBoxData gb;

typedef struct
{
	mWidgetLabelText txt;
	mWidgetRect padding;
	uint32_t fstyle;
}mGroupBoxData;

struct _mGroupBox
{
	mWidget wg;
	mContainerData ct;
	mGroupBoxData gb;
};

enum MGROUPBOX_STYLE
{
	MGROUPBOX_S_COPYTEXT = 1<<0
};


#ifdef __cplusplus
extern "C" {
#endif

mGroupBox *mGroupBoxNew(mWidget *parent,int size,uint32_t fstyle);
mGroupBox *mGroupBoxCreate(mWidget *parent,uint32_t flayout,uint32_t margin_pack,uint32_t fstyle,const char *text);
void mGroupBoxDestroy(mWidget *wg);

void mGroupBoxHandle_calcHint(mWidget *wg);
void mGroupBoxHandle_draw(mWidget *wg,mPixbuf *pixbuf);

void mGroupBoxSetPadding_pack4(mGroupBox *p,uint32_t pack);
void mGroupBoxSetText(mGroupBox *p,const char *text);
void mGroupBoxSetText_copy(mGroupBox *p,const char *text);

#ifdef __cplusplus
}
#endif

#endif
