/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_CONFLISTVIEW_H
#define MLK_CONFLISTVIEW_H

#include <mlk_listview.h>

#define MLK_CONFLISTVIEW(p)  ((mConfListView *)(p))
#define MLK_CONFLISTVIEW_DEF MLK_LISTVIEW_DEF mConfListViewData clv;

typedef union
{
	char *text;
	int num;
	int checked;
	int select;
	uint32_t color;
	uint32_t key;
}mConfListViewValue;

typedef int (*mFuncConfListView_getValue)(int type,intptr_t item_param,mConfListViewValue *val,void *param);

typedef struct
{
	mWidget *editwg;
	mColumnItem *item;
}mConfListViewData;

struct _mConfListView
{
	MLK_LISTVIEW_DEF
	mConfListViewData clv;
};

enum MCONFLISTVIEW_NOTIFY
{
	MCONFLISTVIEW_N_CHANGE_VAL
};

enum MCONFLISTVIEW_VALTYPE
{
	MCONFLISTVIEW_VALTYPE_TEXT,
	MCONFLISTVIEW_VALTYPE_NUM,
	MCONFLISTVIEW_VALTYPE_CHECK,
	MCONFLISTVIEW_VALTYPE_SELECT,
	MCONFLISTVIEW_VALTYPE_COLOR,
	MCONFLISTVIEW_VALTYPE_ACCEL,
	MCONFLISTVIEW_VALTYPE_FONT,
	MCONFLISTVIEW_VALTYPE_FILE
};


#ifdef __cplusplus
extern "C" {
#endif

mConfListView *mConfListViewNew(mWidget *parent,int size,uint32_t fstyle);

void mConfListViewDestroy(mWidget *p);
int mConfListViewHandle_event(mWidget *wg,mEvent *ev);

void mConfListView_setColumnWidth_name(mConfListView *p);

void mConfListView_addItem_header(mConfListView *p,const char *text);
void mConfListView_addItem_text(mConfListView *p,const char *name,const char *val,intptr_t param);
void mConfListView_addItem_num(mConfListView *p,const char *name,int val,int min,int max,int dig,intptr_t param);
void mConfListView_addItem_check(mConfListView *p,const char *name,int checked,intptr_t param);
void mConfListView_addItem_select(mConfListView *p,const char *name,int def,const char *list,mlkbool copy,intptr_t param);
void mConfListView_addItem_color(mConfListView *p,const char *name,mRgbCol col,intptr_t param);
void mConfListView_addItem_accel(mConfListView *p,const char *name,uint32_t key,intptr_t param);
void mConfListView_addItem_font(mConfListView *p,const char *name,const char *infotext,uint32_t flags,intptr_t param);
void mConfListView_addItem_file(mConfListView *p,const char *name,const char *path,const char *filter,intptr_t param);

void mConfListView_setItemValue(mConfListView *p,mColumnItem *item,mConfListViewValue *val);
void mConfListView_setItemValue_atIndex(mConfListView *p,int index,mConfListViewValue *val);

mlkbool mConfListView_getValues(mConfListView *p,mFuncConfListView_getValue func,void *param);

void mConfListView_setFocusValue_zero(mConfListView *p);
void mConfListView_clearAll_accel(mConfListView *p);

intptr_t mConfListViewItem_getParam(mColumnItem *pi);
uint32_t mConfListViewItem_getAccelKey(mColumnItem *pi);

#ifdef __cplusplus
}
#endif

#endif
