/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_SCROLLVIEW_H
#define MLK_SCROLLVIEW_H

#define MLK_SCROLLVIEW(p)      ((mScrollView *)(p))
#define MLK_SCROLLVIEWPAGE(p)  ((mScrollViewPage *)(p))
#define MLK_SCROLLVIEW_DEF      mWidget wg; mScrollViewData sv;
#define MLK_SCROLLVIEWPAGE_DEF  mWidget wg; mScrollViewPageData svp;

typedef void (*mFuncScrollViewPage_getScrollPage)(mWidget *p,int horz,int vert,mSize *dst);

typedef struct
{
	mScrollBar *scrh,*scrv;
	mScrollViewPage *page;
	uint32_t fstyle;
}mScrollViewData;

struct _mScrollView
{
	mWidget wg;
	mScrollViewData sv;
};

typedef struct
{
	mFuncScrollViewPage_getScrollPage getscrollpage;
}mScrollViewPageData;

struct _mScrollViewPage
{
	mWidget wg;
	mScrollViewPageData svp;
};

enum MSCROLLVIEW_STYLE
{
	MSCROLLVIEW_S_HORZ     = 1<<0,
	MSCROLLVIEW_S_VERT     = 1<<1,
	MSCROLLVIEW_S_FIX_HORZ = 1<<2,
	MSCROLLVIEW_S_FIX_VERT = 1<<3,
	MSCROLLVIEW_S_FRAME    = 1<<4,
	MSCROLLVIEW_S_SCROLL_NOTIFY_SELF = 1<<5,
	MSCROLLVIEW_S_NOTIFY_CHANGE_SCROLL_VISIBLE = 1<<6,
	MSCROLLVIEW_S_ALL_NOTIFY_SELF = 1<<7,
	
	MSCROLLVIEW_S_HORZVERT = MSCROLLVIEW_S_HORZ | MSCROLLVIEW_S_VERT,
	MSCROLLVIEW_S_FIX_HORZVERT = MSCROLLVIEW_S_HORZVERT | MSCROLLVIEW_S_FIX_HORZ | MSCROLLVIEW_S_FIX_VERT,
	MSCROLLVIEW_S_HORZVERT_FRAME = MSCROLLVIEW_S_HORZVERT | MSCROLLVIEW_S_FRAME
};

enum MSCROLLVIEW_NOTIFY
{
	MSCROLLVIEW_N_CHANGE_SCROLL_VISIBLE,

	MSCROLLVIEW_NOTIFY_NEXT_VALUE = 100
};

enum MSCROLLVIEWPAGE_NOTIFY
{
	MSCROLLVIEWPAGE_N_SCROLL_ACTION_HORZ,
	MSCROLLVIEWPAGE_N_SCROLL_ACTION_VERT
};


#ifdef __cplusplus
extern "C" {
#endif

/* mScrollView */

mScrollView *mScrollViewNew(mWidget *parent,int size,uint32_t fstyle);

void mScrollViewDestroy(mWidget *wg);
void mScrollViewHandle_resize(mWidget *wg);
void mScrollViewHandle_layout(mWidget *wg);
void mScrollViewHandle_draw(mWidget *wg,mPixbuf *pixbuf);

void mScrollViewSetPage(mScrollView *p,mScrollViewPage *page);
void mScrollViewSetFixBar(mScrollView *p,int type);
void mScrollViewLayout(mScrollView *p);
void mScrollViewSetScrollStatus_horz(mScrollView *p,int min,int max,int page);
void mScrollViewSetScrollStatus_vert(mScrollView *p,int min,int max,int page);
int mScrollViewGetScrollShowStatus(mScrollView *p);
void mScrollViewEnableScrollBar(mScrollView *p,int type);
void mScrollViewGetMaxPageSize(mScrollView *p,mSize *size);

/* mScrollViewPage */

mScrollViewPage *mScrollViewPageNew(mWidget *parent,int size);

void mScrollViewPageHandle_resize(mWidget *wg);

void mScrollViewPage_setHandle_getScrollPage(mScrollViewPage *p,mFuncScrollViewPage_getScrollPage handle);
mlkbool mScrollViewPage_setScrollPage_horz(mScrollViewPage *p,int page);
mlkbool mScrollViewPage_setScrollPage_vert(mScrollViewPage *p,int page);

void mScrollViewPage_getScrollPos(mScrollViewPage *p,mPoint *pt);
int mScrollViewPage_getScrollPos_horz(mScrollViewPage *p);
int mScrollViewPage_getScrollPos_vert(mScrollViewPage *p);
void mScrollViewPage_getScrollBar(mScrollViewPage *p,mScrollBar **scrh,mScrollBar **scrv);
mScrollBar *mScrollViewPage_getScrollBar_horz(mScrollViewPage *p);
mScrollBar *mScrollViewPage_getScrollBar_vert(mScrollViewPage *p);

#ifdef __cplusplus
}
#endif

#endif
