/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_BUTTON_H
#define MLK_BUTTON_H

#define MLK_BUTTON(p)  ((mButton *)(p))
#define MLK_BUTTON_DEF mWidget wg; mButtonData btt;

typedef void (*mFuncButtonHandle_pressed)(mWidget *p);

typedef struct
{
	mWidgetLabelText txt;
	uint32_t fstyle,
		flags;
	int grabbed_key;
	mFuncButtonHandle_pressed pressed;
}mButtonData;

struct _mButton
{
	mWidget wg;
	mButtonData btt;
};

enum MBUTTON_STYLE
{
	MBUTTON_S_COPYTEXT = 1<<0,
	MBUTTON_S_DIRECT_PRESS = 1<<1,
	MBUTTON_S_REAL_W = 1<<2,
	MBUTTON_S_REAL_H = 1<<3,
	
	MBUTTON_S_REAL_WH = MBUTTON_S_REAL_W | MBUTTON_S_REAL_H
};

enum MBUTTON_FLAGS
{
	MBUTTON_F_PRESSED = 1<<0,
	MBUTTON_F_GRABBED = 1<<1
};

enum MBUTTON_NOTIFY
{
	MBUTTON_N_PRESSED
};


#ifdef __cplusplus
extern "C" {
#endif

mButton *mButtonNew(mWidget *parent,int size,uint32_t fstyle);
mButton *mButtonCreate(mWidget *parent,int id,uint32_t flayout,uint32_t margin_pack,uint32_t fstyle,const char *text);
void mButtonDestroy(mWidget *p);

void mButtonHandle_calcHint(mWidget *p);
void mButtonHandle_draw(mWidget *p,mPixbuf *pixbuf);
int mButtonHandle_event(mWidget *wg,mEvent *ev);

void mButtonSetText(mButton *p,const char *text);
void mButtonSetText_copy(mButton *p,const char *text);
void mButtonSetState(mButton *p,mlkbool pressed);
mlkbool mButtonIsPressed(mButton *p);

void mButtonDrawBase(mButton *p,mPixbuf *pixbuf);

#ifdef __cplusplus
}
#endif

#endif
