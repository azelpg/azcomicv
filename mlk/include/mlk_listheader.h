/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_LISTHEADER_H
#define MLK_LISTHEADER_H

#define MLK_LISTHEADER(p)      ((mListHeader *)(p))
#define MLK_LISTHEADER_ITEM(p) ((mListHeaderItem *)(p))
#define MLK_LISTHEADER_DEF     mWidget wg; mListHeaderData lh;

typedef struct _mListHeaderItem mListHeaderItem;

typedef struct
{
	mList list;
	mListHeaderItem *item_drag,
		*item_sort;
	uint32_t fstyle;
	int scrx,
		is_sort_rev,
		fpress,
		drag_left;
}mListHeaderData;

struct _mListHeader
{
	mWidget wg;
	mListHeaderData lh;
};

struct _mListHeaderItem
{
	mListItem i;
	char *text;
	int width;
	uint32_t flags;
	intptr_t param;
};

enum MLISTHEADER_STYLE
{
	MLISTHEADER_S_SORT = 1<<0
};

enum MLISTHEADER_NOTIFY
{
	MLISTHEADER_N_RESIZE,
	MLISTHEADER_N_CHANGE_SORT
};

enum MLISTHEADER_ITEM_FLAGS
{
	MLISTHEADER_ITEM_F_COPYTEXT = 1<<0,
	MLISTHEADER_ITEM_F_RIGHT  = 1<<1,
	MLISTHEADER_ITEM_F_FIX    = 1<<2,
	MLISTHEADER_ITEM_F_EXPAND = 1<<3
};

#ifdef __cplusplus
extern "C" {
#endif

mListHeader *mListHeaderNew(mWidget *parent,int size,uint32_t fstyle);
mListHeader *mListHeaderCreate(mWidget *parent,int id,uint32_t flayout,uint32_t margin_pack,uint32_t fstyle);

void mListHeaderDestroy(mWidget *p);
void mListHeaderHandle_calcHint(mWidget *p);
int mListHeaderHandle_event(mWidget *wg,mEvent *ev);

void mListHeaderAddItem(mListHeader *p,const char *text,int width,uint32_t flags,intptr_t param);
mListHeaderItem *mListHeaderGetTopItem(mListHeader *p);
mListHeaderItem *mListHeaderGetItem_atIndex(mListHeader *p,int index);
int mListHeaderGetItemWidth(mListHeader *p,int index);
int mListHeaderGetFullWidth(mListHeader *p);
void mListHeaderSetScrollPos(mListHeader *p,int scrx);
void mListHeaderSetSort(mListHeader *p,int index,mlkbool rev);
void mListHeaderSetExpandItemWidth(mListHeader *p);
void mListHeaderSetItemWidth(mListHeader *p,int index,int width,mlkbool add_pad);
int mListHeaderGetPos_atX(mListHeader *p,int x);
int mListHeaderGetItemX(mListHeader *p,int index);

#ifdef __cplusplus
}
#endif

#endif
