/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_UNICODE_H
#define MLK_UNICODE_H

#ifdef __cplusplus
extern "C" {
#endif

int mUTF8GetCharBytes(const char *p);
int mUTF8GetChar(mlkuchar *dst,const char *src,int maxlen,const char **ppnext);

int mUTF8Validate(char *str,int len);
int mUTF8CopyValidate(char *dst,const char *src,int len);
int mUTF8toUTF16(const char *src,int srclen,mlkuchar16 *dst,int dstlen);
int mUTF8toUTF32(const char *src,int srclen,mlkuchar *dst,int dstlen);
mlkuchar *mUTF8toUTF32_alloc(const char *src,int srclen,int *dstlen);

int mUnicharToUTF8(mlkuchar c,char *dst,int maxlen);
int mUnicharToUTF16(mlkuchar c,mlkuchar16 *dst,int maxlen);

int mUTF32GetLen(const mlkuchar *p);
mlkuchar *mUTF32Dup(const mlkuchar *src);
int mUTF32toUTF8(const mlkuchar *src,int srclen,char *dst,int dstlen);
char *mUTF32toUTF8_alloc(const mlkuchar *src,int srclen,int *dstlen);
int mUTF32toInt_float(const mlkuchar *str,int dig);
int mUTF32Compare(const mlkuchar *str1,const uint32_t *str2);

int mUTF16GetLen(const mlkuchar16 *p);
int mUTF16GetChar(mlkuchar *dst,const mlkuchar16 *src,const mlkuchar16 **ppnext);
int mUTF16toUTF8(const mlkuchar16 *src,int srclen,char *dst,int dstlen);
char *mUTF16toUTF8_alloc(const mlkuchar16 *src,int srclen,int *dstlen);

#ifdef __cplusplus
}
#endif

#endif
