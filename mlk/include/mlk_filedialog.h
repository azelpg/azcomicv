/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_FILEDIALOG_H
#define MLK_FILEDIALOG_H

#define MLK_FILEDIALOG(p)  ((mFileDialog *)(p))
#define MLK_FILEDIALOG_DEF MLK_DIALOG_DEF mFileDialogData fdlg;

typedef struct
{
	uint32_t fstyle;
	int type;
	mStr *strdst;
	int *dst_filtertype;
	
	char *filter;

	void (*on_selectfile)(mFileDialog *,const char *path);
	void (*on_changetype)(mFileDialog *,int type);
	mlkbool (*on_okcancel)(mFileDialog *,mlkbool is_ok,const char *path);

	mFileListView *flist;
	mLineEdit *edit_dir,
		*edit_name;
	mComboBox *cbtype;
	mWidget *btt_moveup,
		*btt_menu;
}mFileDialogData;

struct _mFileDialog
{
	MLK_DIALOG_DEF
	mFileDialogData fdlg;
};


enum MFILEDIALOG_STYLE
{
	MFILEDIALOG_S_MULTI_SEL = 1<<0,
	MFILEDIALOG_S_NO_OVERWRITE_MES = 1<<1,
	MFILEDIALOG_S_DEFAULT_FILENAME = 1<<2
};

enum MFILEDIALOG_TYPE
{
	MFILEDIALOG_TYPE_OPENFILE,
	MFILEDIALOG_TYPE_SAVEFILE,
	MFILEDIALOG_TYPE_DIR
};


#ifdef __cplusplus
extern "C" {
#endif

mFileDialog *mFileDialogNew(mWindow *parent,int size,uint32_t fstyle,int dlgtype);

void mFileDialogDestroy(mWidget *p);
int mFileDialogHandle_event(mWidget *wg,mEvent *ev);

void mFileDialogInit(mFileDialog *p,const char *filter,int def_filter,const char *initdir,mStr *strdst);
void mFileDialogShow(mFileDialog *p);

#ifdef __cplusplus
}
#endif

#endif
