/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_ACCELERATOR_H
#define MLK_ACCELERATOR_H

#ifdef __cplusplus
extern "C" {
#endif

mAccelerator *mAcceleratorNew(void);
void mAcceleratorDestroy(mAccelerator *p);

void mAcceleratorSetDefaultWidget(mAccelerator *p,mWidget *wg);

void mAcceleratorAdd(mAccelerator *p,int cmdid,uint32_t key,mWidget *wg);
void mAcceleratorClear(mAccelerator *p);

uint32_t mAcceleratorGetKey(mAccelerator *p,int cmdid);
void mAcceleratorGetKeyText(mStr *str,uint32_t key);

uint32_t mAcceleratorGetAccelKey_keyevent(mEvent *ev);

#ifdef __cplusplus
}
#endif

#endif
