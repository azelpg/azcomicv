/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_INIWRITE_H
#define MLK_INIWRITE_H

#ifdef __cplusplus
extern "C" {
#endif

FILE *mIniWrite_openFile(const char *filename);
FILE *mIniWrite_openFile_join(const char *path,const char *filename);

void mIniWrite_putGroup(FILE *fp,const char *name);
void mIniWrite_putGroupInt(FILE *fp,int no);

void mIniWrite_putInt(FILE *fp,const char *key,int n);
void mIniWrite_putInt_keyno(FILE *fp,int keyno,int n);
void mIniWrite_putHex(FILE *fp,const char *key,uint32_t n);
void mIniWrite_putHex_keyno(FILE *fp,int keyno,uint32_t n);
void mIniWrite_putText(FILE *fp,const char *key,const char *text);
void mIniWrite_putText_keyno(FILE *fp,int keyno,const char *text);
void mIniWrite_putStr(FILE *fp,const char *key,mStr *str);
void mIniWrite_putStrArray(FILE *fp,int keytop,mStr *array,int num);
void mIniWrite_putDouble(FILE *fp,const char *key,double d);

void mIniWrite_putPoint(FILE *fp,const char *key,mPoint *pt);
void mIniWrite_putSize(FILE *fp,const char *key,mSize *size);
void mIniWrite_putBox(FILE *fp,const char *key,mBox *box);

void mIniWrite_putNumbers(FILE *fp,const char *key,const void *buf,int num,int bytes,mlkbool hex);
void mIniWrite_putBase64(FILE *fp,const char *key,const void *buf,uint32_t size);

#ifdef __cplusplus
}
#endif

#endif
