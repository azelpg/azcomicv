/*$
mlk
Copyright (c) 2020-2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_PANELHEADER_H
#define MLK_PANELHEADER_H

#define MLK_PANELHEADER(p)  ((mPanelHeader *)(p))
#define MLK_PANELHEADER_DEF mWidget wg; mPanelHeaderData ph;

typedef struct
{
	const char *text;
	uint32_t fstyle;
	int fpress;
}mPanelHeaderData;

struct _mPanelHeader
{
	mWidget wg;
	mPanelHeaderData ph;
};

enum MPANELHEADER_STYLE
{
	MPANELHEADER_S_HAVE_CLOSE = 1<<0,
	MPANELHEADER_S_HAVE_STORE = 1<<1,
	MPANELHEADER_S_HAVE_SHUT = 1<<2,
	MPANELHEADER_S_HAVE_RESIZE = 1<<3,
	MPANELHEADER_S_STORE_LEAVED = 1<<4,
	MPANELHEADER_S_SHUT_CLOSED = 1<<5,
	MPANELHEADER_S_RESIZE_DISABLED = 1<<6,

	MPANELHEADER_S_ALL_BUTTONS = MPANELHEADER_S_HAVE_CLOSE
		| MPANELHEADER_S_HAVE_STORE | MPANELHEADER_S_HAVE_SHUT | MPANELHEADER_S_HAVE_RESIZE
};

enum MPANELHEADER_NOTIFY
{
	MPANELHEADER_N_PRESS_BUTTON
};

enum MPANELHEADER_BTT
{
	MPANELHEADER_BTT_CLOSE = 1,
	MPANELHEADER_BTT_STORE,
	MPANELHEADER_BTT_SHUT,
	MPANELHEADER_BTT_RESIZE
};


#ifdef __cplusplus
extern "C" {
#endif

mPanelHeader *mPanelHeaderNew(mWidget *parent,int size,uint32_t fstyle);

void mPanelHeaderDestroy(mWidget *p);
int mPanelHeaderHandle_event(mWidget *wg,mEvent *ev);
void mPanelHeaderHandle_draw(mWidget *p,mPixbuf *pixbuf);
void mPanelHeaderHandle_calcHint(mWidget *wg);

void mPanelHeaderSetTitle(mPanelHeader *p,const char *title);
void mPanelHeaderSetShut(mPanelHeader *p,int type);
void mPanelHeaderSetResize(mPanelHeader *p,int type);

#ifdef __cplusplus
}
#endif

#endif
